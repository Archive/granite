/*
 * Granite Automation Interfaces
 */
module Granite {

	/*
	 * Forward Declarations
	 */
	interface App;
	interface Sequencer;
	interface Score;
	interface ScorePatternInstance;
	interface ScorePattern;
	interface ScoreTrack;
	interface ScoreMIDIClip;

	/*
	 * Types
	 */
	typedef short				ScoreTimebase;
	typedef long				ScoreTime;
	typedef short				ScoreTempo;

	typedef sequence<ScorePatternInstance>	ScorePatternInstances;
	typedef sequence<ScoreTrack>		ScoreTracks;
	typedef sequence<ScoreMIDIClip>		ScoreMIDIClips;

	struct ScoreMeter {
		unsigned short N;
		unsigned short D;
	};

	/*
	 * ScoreEvents
	 */
	enum ScoreEventType {
		SCORE_EVENT_NOTE,
		SCORE_EVENT_AFTERTOUCH,
		SCORE_EVENT_KEY_AFTERTOUCH,
		SCORE_EVENT_PROGRAM_CHANGE,
		SCORE_EVENT_CONTROLLER,
		SCORE_EVENT_PITCH_BEND,
		SCORE_EVENT_TEMPO,
		SCORE_EVENT_METER
	};

	struct ScoreEventNote {
		ScoreTime time;
		short value;
		short velocity;
		ScoreTime duration;
	};

	struct ScoreEventAftertouch {
		ScoreTime time;
		short value;
	};

	struct ScoreEventKeyAftertouch {
		ScoreTime time;
		short note;
		short value;
	};

	struct ScoreEventProgramChange {
		ScoreTime time;
		short value;
	};

	struct ScoreEventController {
		ScoreTime time;
		short number;
		short value;
	};

	struct ScoreEventPitchBend {
		ScoreTime time;
		short value;
	};

	struct ScoreEventTempo {
		ScoreTime time;
		ScoreTempo value;
	};

	struct ScoreEventMeter {
		ScoreTime time;
		ScoreMeter value;
	};

	union ScoreEvent switch (ScoreEventType) {
	case SCORE_EVENT_NOTE: ScoreEventNote Note;
	case SCORE_EVENT_AFTERTOUCH: ScoreEventAftertouch Aftertouch;
	case SCORE_EVENT_KEY_AFTERTOUCH: ScoreEventKeyAftertouch KeyAftertouch;
	case SCORE_EVENT_PROGRAM_CHANGE: ScoreEventProgramChange ProgramChange;
	case SCORE_EVENT_CONTROLLER: ScoreEventController Controller;
	case SCORE_EVENT_PITCH_BEND: ScoreEventPitchBend PitchBend;
	case SCORE_EVENT_TEMPO: ScoreEventTempo Tempo;
	case SCORE_EVENT_METER: ScoreEventMeter Meter;
	};

	/*
	 * Context used for automation.
	 */
	interface Context {
		readonly attribute ScorePatternInstance pattern_instance;
		readonly attribute ScorePattern pattern;
		readonly attribute ScoreTrack track;
		readonly attribute ScoreMIDIClips midi_clips;
	};

	/*
	 * App
	 */
	interface App {
		readonly attribute Score score;
		readonly attribute Sequencer sequencer;
		readonly attribute Context cxt;

		void new ();
		void save (in string filename);
		void load (in string filename);

		ScorePattern pattern_new ();
		ScoreTrack track_new ();
		ScoreMIDIClip midi_clip_new ();
	};

	/*
	 * Sequencer
	 */
	interface Sequencer {
		attribute ScoreTimebase timebase;
		readonly attribute ScoreTime cur_time;

		void get_current_meter (in ScoreTime time, out ScoreMeter meter);
	};

	/*
	 * Score
	 */
	interface Score {
		void set_pattern_instance_start ();
		ScorePatternInstance get_next_pattern_instance ();
		void kill_pattern_instance ();
		void insert_pattern (in ScorePattern pattern, in ScoreTime time);
	};

	/*
	 * ScorePatternInstance
	 */
	interface ScorePatternInstance {
		attribute ScoreTime time;
		attribute ScoreTime length;
		attribute short repeat;
	};

	/*
	 * ScorePattern
	 */
	interface ScorePattern {
		attribute string name;
		readonly attribute short num_tracks;
		readonly attribute ScoreTracks selected_tracks;

		short get_current_track ();
		void set_current_track (in short track_num);
		ScoreTrack get_track (in short track_num);
		void remove_track (in short track_num);
		void insert_track (in ScoreTrack track, in short index);
		short index_from_track (in ScoreTrack track);
	};

	/*
	 * ScoreTrack
	 */
	interface ScoreTrack {
		attribute string name;
		attribute short port;
		attribute short channel;

		void set_clip_start ();
		ScoreMIDIClip get_next_clip ();
		void kill_clip ();
		void insert_clip (in ScoreMIDIClip clip, in ScoreTime time);
	};

	/*
	 * ScoreMIDIClip
	 */
	interface ScoreMIDIClip  {
		readonly attribute string name;

		void set_event_start ();
		boolean get_next_event (out ScoreEvent event);
		void set_event (in ScoreEvent event);
		void kill_event ();
		void insert_event (in ScoreEvent event);
	};

#if 0
	/*
	 * Plugin
	 */
	enum PluginType {
		PLUGIN_RESIDENT,
		PLUGIN_TRANSIENT
	};

	interface Plugin {
		readonly attribute PluginType type;

		boolean prepare (in Context pc);
		void process (in Clip clip);
	};
#endif

	/*
	 * Below...: To be replaced by above, but in use right now...
	 */

	/*
        * ScoreMIDIClip Filter Plugin
        */
	interface ScoreMIDIClipFilter  {
		boolean prepare (in Sequencer sequencer);
		void process (in ScoreMIDIClip clip);
	};

};
