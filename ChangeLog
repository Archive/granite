2001-11-10  Pablo Saratxaga  <pablo@mandrakesoft.com>

	* configure.in: Added "az" to ALL_LINGUAS.

2001-11-27  Christian Rose  <menthos@menthos.com>

	* configure.in: Added "sv" to ALL_LINGUAS.

1999-04-23  Andrew T. Veliath  <andrewtv@usa.net>

	* src/granite.c (granite_prefs_changed_real): If we change any
	settings which require a restart, let the user know.

	* src/prefs-dialog.[ch], *-prefs.[ch]: Add preference stuff.  Now
	we are editing, saving and loading preferences, for both global
	and per-project settings in XML and using gnome-config.  Now to
	add more preferences :).

	* src/sequencer-xml-io.[ch]: New files.

1999-04-22  Andrew T. Veliath  <andrewtv@usa.net>

	* prefs-dialog.[ch], granite-prefs.h: New files.

	* src/*.[ch]: Disable gsevents so they can be rewritten sometime.

	* src/common.h (debugf): More descriptive.

	* src/*.c: Change some destroy methods to finalize.

	* src/: File renaming to help keep things sane.

	* Add mime types, keys, and various updates.

1999-02-20  Christopher Gabriel  <cgabriel@tin.it>

	* Added 'docs' subdir as a repository for documentation and 
	related stuff
	* Added the wish/to-do written by Alessandro Fabbri
	in the granite/docs/

1999-02-15  Andrew T. Veliath  <andrewtv@usa.net>

	* Integrate structure for SWIG-based CORBA plugins, with Perl as
	the first module to be supported.

1999-02-12  Andrew T. Veliath  <andrewtv@usa.net>

	* Start work on CORBA automation. Mostly stubs at the moment.

	* Added meter and tempo events.  These aren't functional now, and
	will eventually be integrated into a system which handles causal
	event streams.

1999-02-07  Andrew T. Veliath  <andrewtv@usa.net>

	* Rewrote Granite object as a Gtk+ object.

1999-02-06  Andrew T. Veliath  <andrewtv@usa.net>

	* Arm record just a bit before count-in stops.

	* Clip start time and track moving works.

	* Added status bar.

1999-02-02  Andrew T. Veliath  <andrewtv@usa.net>

	* src/score.c (score_midi_clip_insert_event): Make sure to hash
	if event if state is at the current time as well.

	* src/gtktpclist.c: Change behavior to first click focus, second
	click edit.  Also, have escape and focus out kill the edit.

	* Add prepare method to ScoreMIDIClipFilter.

	* Score time string conversion now handles absolute and deltas
	correctly.

	* Added a few simple plugins to midiclip base CORBA library:
	transpose, time shift, and quantize.

	* Added preliminary Granite plugin architecture for normal clip
	manipulation (non-realtime), using GNORBA and the interfaces in
	idl/Granite.idl.

	* More sequencer improvements.

1999-01-31  Andrew T. Veliath  <andrewtv@usa.net>

	* Preliminary recording count-in added.

	* Partial XML saving and loading works.

	* Sequencer improvements, still needs work.

	* Lots of stuff with event roll insertion, deletion, etc.

1999-01-25  Andrew T. Veliath  <andrewtv@usa.net>

	* src/gtktpclist.[ch]: Lots of work to fix keyboard traversal, and
	add row [multi] selection, which is now separate from cell focus.

	* Lots of experimental work on the sequencing engine.

1998-12-29  Andrew T. Veliath  <andrewtv@usa.net>

	* Added peer sequencer support, with masters and slave MDI
	windows.

1998-12-27  Andrew T. Veliath  <andrewtv@usa.net>

	* Added preliminary Gnome MDI support.
