typedef void				Object;

typedef short				ScoreTimebase;
typedef int				ScoreTime;
typedef short				ScoreTempo;

typedef struct {
#ifdef SWIG
	ScoreMeter ();
	~ScoreMeter ();
#endif
	unsigned short N;
	unsigned short D;
} ScoreMeter;

typedef enum {
	SCORE_EVENT_NOTE,
	SCORE_EVENT_AFTERTOUCH,
	SCORE_EVENT_KEY_AFTERTOUCH,
	SCORE_EVENT_PROGRAM_CHANGE,
	SCORE_EVENT_CONTROLLER,
	SCORE_EVENT_PITCH_BEND,
	SCORE_EVENT_TEMPO,
	SCORE_EVENT_METER
} ScoreEventType;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	short value;
	short velocity;
	ScoreTime duration;
} ScoreEventNote;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	short value;
} ScoreEventAftertouch;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	short note;
	short value;
} ScoreEventKeyAftertouch;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	short value;
} ScoreEventProgramChange;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	short number;
	short value;
} ScoreEventController;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	short value;
} ScoreEventPitchBend;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	ScoreTempo value;
} ScoreEventTempo;

typedef struct {
	ScoreEventType type;
	ScoreTime time;
	ScoreMeter value;
} ScoreEventMeter;

typedef union {
#ifdef SWIG
	ScoreEvent ();
	~ScoreEvent ();
#endif
	ScoreEventType type;
	ScoreEventNote Note;
	ScoreEventAftertouch Aftertouch;
	ScoreEventKeyAftertouch KeyAftertouch;
	ScoreEventProgramChange ProgramChange;
	ScoreEventController Controller;
	ScoreEventPitchBend PitchBend;
	ScoreEventTempo Tempo;
	ScoreEventMeter Meter;
} ScoreEvent;

void add_se (Object *app, ScoreEvent *ev);
