%module Granite

%{
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include "Granite-SWIG.h"
%}

%include "Granite-SWIG.h"

extern Object *Init (void);
extern Object *GetContext (Object *a);
