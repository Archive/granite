#include <gnome.h>
#include <libgnorba/gnorba.h>
#include "plugin.h"
#include "Granite-SWIG.h"

static initialized;

Object *
Init (void)
{
	CORBA_ORB orb;
	CORBA_Environment ev;
	CORBA_Object app;
	static char *argv[] = { "granite-swig" };
	int argc = 1;

	CORBA_exception_init (&ev);
	if (!initialized) {
		orb = gnome_CORBA_init ("granite-swig", "0.0", &argc, argv, 0, &ev);
		initialized = TRUE;
	}
	app = goad_server_activate_with_id (
		NULL, "granite", GOAD_ACTIVATE_EXISTING_ONLY, NULL);
	CORBA_exception_free (&ev);

	return app;
}

Object *
GetContext (Object *app)
{
	CORBA_Environment ev;
	CORBA_Object cxt;

	CORBA_exception_init (&ev);
	cxt = Granite_App__get_cxt (app, &ev);
	CORBA_exception_free (&ev);

	return cxt;
}

void
add_se (Object *app, ScoreEvent *ev)
{
	g_message ("ScoreEvent type: %d", ev->type);
	g_message ("ScoreEvent time: %d", ev->Note.time);
	g_message ("ScoreEvent value: %d", ev->Note.value);
	g_message ("ScoreEvent duration: %d", ev->Note.duration);
}
