/**************************************************************************

    midiclip.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <gnome.h>
#include <libgnorba/gnorba.h>
#include <plugin.h>

static CORBA_Object	transpose_activator		(PortableServer_POA poa,
							 const char *goad_id,
							 const char **params,
							 gpointer *impl_ptr,
							 CORBA_Environment *ev);
static void		transpose_deactivator		(PortableServer_POA poa,
							 const char *goad_id,
							 gpointer impl_ptr,
							 CORBA_Environment *ev);

static CORBA_Object	time_shift_activator		(PortableServer_POA poa,
							 const char *goad_id,
							 const char **params,
							 gpointer *impl_ptr,
							 CORBA_Environment *ev);
static void		time_shift_deactivator		(PortableServer_POA poa,
							 const char *goad_id,
							 gpointer impl_ptr,
							 CORBA_Environment *ev);

static CORBA_Object	quantize_activator		(PortableServer_POA poa,
							 const char *goad_id,
							 const char **params,
							 gpointer *impl_ptr,
							 CORBA_Environment *ev);
static void		quantize_deactivator		(PortableServer_POA poa,
							 const char *goad_id,
							 gpointer impl_ptr,
							 CORBA_Environment *ev);

static const char *repo_id[] = {
	"IDL:Granite/ScoreMIDIClipFilter:1.0",
	NULL
};

static const GnomePluginObject objects[] = {
	{ repo_id, "transpose", NULL, "Transpose",
	  transpose_activator, transpose_deactivator },
	{ repo_id, "timeshift", NULL, "Time Shift",
	  time_shift_activator, time_shift_deactivator },
	{ repo_id, "quantize", NULL, "Quantize",
	  quantize_activator, quantize_deactivator },
	{ NULL }
};

const GnomePlugin GNOME_Plugin_info = {
	objects,
	"Granite Base MIDI Clip Filters"
};

/*
 * Use a common base entry point vector
 */
static PortableServer_ServantBase__epv base_epv = {
	NULL, NULL, NULL,
};

/*
 * Transpose Plugin
 */

typedef struct {
	POA_Granite_ScoreMIDIClipFilter servant;
	gint amt;
} TransposeFilter;

static CORBA_boolean
transpose_prepare (PortableServer_Servant servant,
		   const Granite_Sequencer sequencer,
		   CORBA_Environment *ev)
{
	TransposeFilter *tf;
	gchar *ret;
	gdouble amt = 0.0;

	tf = (TransposeFilter *) servant;

	ret = gtk_dialog_cauldron ("Transpose",
				   GTK_CAULDRON_DIALOG | GTK_CAULDRON_SPACE4,
				   "%[ %SBj ]ppp / (%Bqrg || %Bqrg)p",
				   "Transpose",
				   1.0, 0, &amt, -127.0, 127.0, 1.0, 1.0, 1.0,
				   GNOME_STOCK_BUTTON_OK,
				   GNOME_STOCK_BUTTON_CANCEL);

	if (!strcmp (ret, GNOME_STOCK_BUTTON_CANCEL))
		return CORBA_FALSE;

	tf->amt = (gint) amt;

	return CORBA_TRUE;
}

static void
transpose_process (PortableServer_Servant servant,
		   const Granite_ScoreMIDIClip clip,
		   CORBA_Environment *ev)
{
	TransposeFilter *tf;
	Granite_ScoreEvent e;

	tf = (TransposeFilter *) servant;
	
	Granite_ScoreMIDIClip_set_event_start (clip, ev);
	while (Granite_ScoreMIDIClip_get_next_event (clip, &e, ev)) {
		if (e._d != Granite_SCORE_EVENT_NOTE)
			continue;
		e._u.Note.value += tf->amt;
		Granite_ScoreMIDIClip_set_event (clip, &e, ev);
	}
}

static POA_Granite_ScoreMIDIClipFilter__epv transpose_epv = {
	NULL,
	transpose_prepare,
	transpose_process,
};
static POA_Granite_ScoreMIDIClipFilter__vepv transpose_vepv = {
	&base_epv, &transpose_epv
};
static CORBA_Object
transpose_activator (PortableServer_POA poa,
		     const char *goad_id, const char **params,
		     gpointer *impl_ptr, CORBA_Environment *ev)
{
	TransposeFilter *servant;

	servant = g_new0 (TransposeFilter, 1);
	servant->servant.vepv = &transpose_vepv;
	POA_Granite_ScoreMIDIClipFilter__init (&servant->servant, ev);
	g_return_val_if_fail (ev->_major == CORBA_NO_EXCEPTION, NULL);
	CORBA_free (PortableServer_POA_activate_object (poa, &servant->servant, ev));
	g_return_val_if_fail (ev->_major == CORBA_NO_EXCEPTION, NULL);
	*impl_ptr = servant;

	return PortableServer_POA_servant_to_reference (poa, servant, ev);
}

static void
transpose_deactivator (PortableServer_POA poa, const char *goad_id,
		       gpointer impl_ptr, CORBA_Environment *ev)
{
	POA_Granite_ScoreMIDIClipFilter__fini (
		(POA_Granite_ScoreMIDIClipFilter *)impl_ptr, ev);
	g_free (impl_ptr);
}

/*
 * Time Shift Plugin
 */

typedef struct {
	POA_Granite_ScoreMIDIClipFilter servant;
	Granite_ScoreTime amt;
} TimeShiftFilter;

static CORBA_boolean
time_shift_prepare (PortableServer_Servant servant,
		    const Granite_Sequencer sequencer,
		    CORBA_Environment *ev)
{
	TimeShiftFilter *tf;
	gchar *ret;
	gdouble amt = 0.0;

	tf = (TimeShiftFilter *) servant;

	ret = gtk_dialog_cauldron ("Time_Shift",
				   GTK_CAULDRON_DIALOG | GTK_CAULDRON_SPACE4,
				   "%[ %SBj ]ppp / (%Bqrg || %Bqrg)p",
				   "Time_Shift",
				   1.0, 0, &amt, -1000000.0, 1000000.0, 1.0, 1.0, 1.0,
				   GNOME_STOCK_BUTTON_OK,
				   GNOME_STOCK_BUTTON_CANCEL);

	if (!strcmp (ret, GNOME_STOCK_BUTTON_CANCEL))
		return CORBA_FALSE;

	tf->amt = (Granite_ScoreTime) amt;

	return CORBA_TRUE;
}

static void
time_shift_process (PortableServer_Servant servant,
		    const Granite_ScoreMIDIClip clip,
		    CORBA_Environment *ev)
{
	TimeShiftFilter *tf;
	Granite_ScoreEvent e;

	tf = (TimeShiftFilter *) servant;

	Granite_ScoreMIDIClip_set_event_start (clip, ev);
	while (Granite_ScoreMIDIClip_get_next_event (clip, &e, ev)) {
		if (e._d != Granite_SCORE_EVENT_NOTE)
			continue;
		e._u.Note.time += tf->amt;
		Granite_ScoreMIDIClip_set_event (clip, &e, ev);
	}
}

static POA_Granite_ScoreMIDIClipFilter__epv time_shift_epv = {
	NULL,
	time_shift_prepare,
	time_shift_process,
};
static POA_Granite_ScoreMIDIClipFilter__vepv time_shift_vepv = {
	&base_epv, &time_shift_epv
};
static CORBA_Object
time_shift_activator (PortableServer_POA poa,
		      const char *goad_id, const char **params,
		      gpointer *impl_ptr, CORBA_Environment *ev)
{
	TimeShiftFilter *servant;

	servant = g_new0 (TimeShiftFilter, 1);
	servant->servant.vepv = &time_shift_vepv;
	POA_Granite_ScoreMIDIClipFilter__init (&servant->servant, ev);
	g_return_val_if_fail (ev->_major == CORBA_NO_EXCEPTION, NULL);
	CORBA_free (PortableServer_POA_activate_object (poa, &servant->servant, ev));
	g_return_val_if_fail (ev->_major == CORBA_NO_EXCEPTION, NULL);
	*impl_ptr = servant;

	return PortableServer_POA_servant_to_reference (poa, servant, ev);
}

static void
time_shift_deactivator (PortableServer_POA poa, const char *goad_id,
			gpointer impl_ptr, CORBA_Environment *ev)
{
	POA_Granite_ScoreMIDIClipFilter__fini (
		(POA_Granite_ScoreMIDIClipFilter *)impl_ptr, ev);
	g_free (impl_ptr);
}

/*
 * Quantize Plugin
 */

typedef struct {
	POA_Granite_ScoreMIDIClipFilter servant;
	Granite_ScoreTime amt;
	gint start_time;
	gint duration;
} QuantizeFilter;

static CORBA_boolean
quantize_prepare (PortableServer_Servant servant,
		  const Granite_Sequencer sequencer,
		  CORBA_Environment *ev)
{
	QuantizeFilter *qf;
	Granite_ScoreTimebase timebase;
	Granite_ScoreMeter meter;
	gdouble amt = 16.0;
	gchar *ret;

	qf = (QuantizeFilter *) servant;

	qf->start_time = TRUE;
	qf->duration = TRUE;

	ret = gtk_dialog_cauldron ("Quantize",
				   GTK_CAULDRON_DIALOG | GTK_CAULDRON_SPACE4,
				   "%[ (( %C // %C )p || %SBj)ppp ]ppp / (%Bqrg || %Bqrg)p",
				   "Quantize",
				   "Start Time", &qf->start_time,
				   "Duration", &qf->duration,
				   1.0, 0, &amt, 1.0, 1024.0, 1.0, 1.0, 1.0,
				   GNOME_STOCK_BUTTON_OK,
				   GNOME_STOCK_BUTTON_CANCEL);

	if (!strcmp (ret, GNOME_STOCK_BUTTON_CANCEL))
		return CORBA_FALSE;

	timebase = Granite_Sequencer__get_timebase (sequencer, ev);
	Granite_Sequencer_get_current_meter (sequencer, 0, &meter, ev);

	qf->amt = (Granite_ScoreTime) amt;
	if (qf->amt < 1) qf->amt = 1;
	qf->amt = meter.N * timebase / qf->amt;

	return CORBA_TRUE;
}

static void
quantize_process (PortableServer_Servant servant,
		  const Granite_ScoreMIDIClip clip,
		  CORBA_Environment *ev)
{
	QuantizeFilter *qf;
	Granite_ScoreEvent e;
	Granite_ScoreTime new_time;

	qf = (QuantizeFilter *) servant;

	Granite_ScoreMIDIClip_set_event_start (clip, ev);
	while (Granite_ScoreMIDIClip_get_next_event (clip, &e, ev)) {
		if (e._d != Granite_SCORE_EVENT_NOTE)
			continue;
		if (qf->start_time) {
			new_time = (e._u.Note.time + qf->amt / 2) / qf->amt * qf->amt;
			e._u.Note.time = new_time;
		}
		if (qf->duration) {
			new_time = (e._u.Note.duration + qf->amt / 2) / qf->amt * qf->amt;
			if (new_time < qf->amt) /* Durations of zero aren't good */
				new_time = qf->amt;
			e._u.Note.duration = new_time;
		}
		Granite_ScoreMIDIClip_set_event (clip, &e, ev);
	}
}

static POA_Granite_ScoreMIDIClipFilter__epv quantize_epv = {
	NULL,
	quantize_prepare,
	quantize_process,
};
static POA_Granite_ScoreMIDIClipFilter__vepv quantize_vepv = {
	&base_epv, &quantize_epv
};
static CORBA_Object
quantize_activator (PortableServer_POA poa,
		    const char *goad_id, const char **params,
		    gpointer *impl_ptr, CORBA_Environment *ev)
{
	QuantizeFilter *servant;
	CORBA_Object obj;

	servant = g_new0 (QuantizeFilter, 1);
	servant->servant.vepv = &quantize_vepv;
	POA_Granite_ScoreMIDIClipFilter__init (&servant->servant, ev);
	g_return_val_if_fail (ev->_major == CORBA_NO_EXCEPTION, NULL);
	CORBA_free (PortableServer_POA_activate_object (poa, &servant->servant, ev));
	g_return_val_if_fail (ev->_major == CORBA_NO_EXCEPTION, NULL);
	*impl_ptr = servant;
	obj = PortableServer_POA_servant_to_reference (poa, servant, ev);
	goad_server_register (CORBA_OBJECT_NIL, obj, "quantize", "object", ev);	/* Want this? */

	return obj;
}

static void
quantize_deactivator (PortableServer_POA poa, const char *goad_id,
		      gpointer impl_ptr, CORBA_Environment *ev)
{
	goad_server_unregister (CORBA_OBJECT_NIL, "quantize", "object", ev);
	POA_Granite_ScoreMIDIClipFilter__fini (
		(POA_Granite_ScoreMIDIClipFilter *)impl_ptr, ev);
	g_free (impl_ptr);
}
