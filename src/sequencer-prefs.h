/**************************************************************************

    sequencer-prefs.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SEQUENCER_PREFS_H__
#define __SEQUENCER_PREFS_H__

typedef struct _SequencerPrefs	SequencerPrefs;

struct _SequencerPrefs {
	guint send_mtc : 1;
};

void		sequencer_prefs_init	(SequencerPrefs *prefs);
void		sequencer_prefs_load	(SequencerPrefs *prefs);
void		sequencer_prefs_save	(const SequencerPrefs *prefs);
void		sequencer_prefs_free	(SequencerPrefs *prefs);

#endif /* __SEQUENCER_PREFS_H__ */
