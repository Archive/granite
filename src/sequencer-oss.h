/**************************************************************************

    sequencer-oss.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SEQUENCER_OSS_H__
#define __SEQUENCER_OSS_H__

struct _Sequencer {
	/* Common sequencer data */
	GString *device;

	/* Prefs */
	SequencerPrefs prefs;

	/* MIDI ports */
	gint num_midi_ports;
	SequencerMIDI *midi_ports;

	/* Sequencer global state */
	Score *score;
	ScoreTimebase timebase;	 /* Parts per quarter note */
	ScoreTime start_time;
	ScoreTime new_start_time;
	ScoreTime real_time;
	guint start_time_changed : 1;
	guint playing : 1;
	guint recording : 1;

	/* Default state (used when there is no master) */
	SequencerState default_state;

	/* Sequencer per-master state */
	SequencerState active_state;
	SequencerMaster *master; /* Current owned master */
	pthread_mutex_t master_mutex;

	ScoreEvents *events_out; /* Events in the output queue */
	ScoreEvents *note_off_out; /* Queued note off events */
	ScoreEvents *events_in;	/* Events being recorded  */
	ScoreTime last_record_start_time;

	ScoreMeter current_meter; /* Current meter */
	ScoreTempo current_tempo; /* Current tempo */

	gint fd;
	ScoreTime last_midi_in_time;

	pthread_cond_t midi_out_refill_cond;
	pthread_mutex_t midi_out_refill_lock;

	pthread_cond_t midi_out_cond;
	pthread_mutex_t midi_out_lock;

	pthread_cond_t midi_stop_cond;
	pthread_mutex_t midi_stop_lock;

	pthread_mutex_t midi_write_lock;
	SequencerThread midi_out;
	SequencerThread midi_in;
};

#endif /* __SEQUENCER_OSS_H__ */
