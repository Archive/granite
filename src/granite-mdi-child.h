/**************************************************************************

    granite-mdi-child.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __GRANITE_MDI_CHILD_H__
#define __GRANITE_MDI_CHILD_H__

#include <libgnomeui/gnome-mdi.h>
#include <libgnomeui/gnome-mdi-child.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "sequencer-gtk-peer.h"

#define GRANITE_TYPE_MDI_CHILD              (granite_mdi_child_get_type ())
#define GRANITE_MDI_CHILD(obj)              (GTK_CHECK_CAST ((obj), GRANITE_TYPE_MDI_CHILD, GraniteMDIChild))
#define GRANITE_MDI_CHILD_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), GRANITE_TYPE_MDI_CHILD, GraniteMDIChildClass))
#define GRANITE_IS_MDI_CHILD(obj)           (GTK_CHECK_TYPE ((obj), GRANITE_TYPE_MDI_CHILD))
#define GRANITE_IS_MDI_CHILD_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), GRANITE_TYPE_MDI_CHILD))

typedef struct _GraniteMDIChild             GraniteMDIChild;
typedef struct _GraniteMDIChildClass        GraniteMDIChildClass;

struct _GraniteMDIChild
{
	GnomeMDIChild parent;

	Granite *granite;

	SequencerGtkPeer gtkpeer;
};

struct _GraniteMDIChildClass
{
	GnomeMDIChildClass parent_class;
};

GtkType			granite_mdi_child_get_type	(void);
void			granite_mdi_child_construct	(GraniteMDIChild *child,
							 Granite *granite);

#endif /* __GRANITE_MDI_CHILD_H__ */
