/**************************************************************************

    granite-ui.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "ui.h"
#include "prefs-dialog.h"
#include "granite-xml-io.h"

/* File menu */

static void
ok_clicked (GtkWidget *widget, gint *result)
{
	*result = 1;
	gtk_main_quit ();
}

static gchar *
get_filename (GnomeMDI *mdi, const gchar *prompt, const gchar *default_filename)
{
	GtkFileSelection *fs;
	GnomeApp *app;
	GtkWidget *widget;
	gint result;
	gchar *rv;

	app = gnome_mdi_get_active_window (mdi);
	fs = GTK_FILE_SELECTION (gtk_file_selection_new (prompt));
	widget = GTK_WIDGET (fs);

	gtk_window_set_modal (GTK_WINDOW (fs), TRUE);
	if (app)
		gtk_window_set_transient_for (GTK_WINDOW (fs), GTK_WINDOW (app));
	if (default_filename)
		gtk_file_selection_set_filename (fs, default_filename);
	gtk_signal_connect (GTK_OBJECT (fs->ok_button), "clicked",
			    GTK_SIGNAL_FUNC (ok_clicked), &result);
        gtk_signal_connect (GTK_OBJECT (fs->cancel_button), "clicked",
                            GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

	result = 0;
	gtk_widget_show (widget);
	gtk_grab_add (widget);
	gtk_main ();

	if (result == 1)
		rv = g_strdup (gtk_file_selection_get_filename (fs));
	else
		rv = NULL;

	gtk_widget_destroy (widget);

	return rv;
}

static void
new_cmd (GtkWidget *widget, GnomeMDI *mdi)
{
	Granite *granite;

	g_return_if_fail (GNOME_IS_MDI (mdi));

	granite = gtk_object_get_user_data (GTK_OBJECT (mdi));
	granite_new_score (granite);
}

static void
open_cmd (GtkWidget *widget, GnomeMDI *mdi)
{
	Granite *granite;
	gchar *filename;

	g_return_if_fail (GNOME_IS_MDI (mdi));

	granite = gtk_object_get_user_data (GTK_OBJECT (mdi));
	filename = get_filename (mdi, _("Open Score"), NULL);
	if (filename) {
		granite_xml_load_state (granite, filename);
		granite_set_score_name (granite, filename);
		granite->score->named = TRUE;
		granite->score->modified = FALSE;
		g_free (filename);
	}
}

static void
save_as_cmd (GtkWidget *widget, GnomeMDI *mdi)
{
	Granite *granite;
	gchar *filename;

	g_return_if_fail (GNOME_IS_MDI (mdi));

	granite = gtk_object_get_user_data (GTK_OBJECT (mdi));
	filename = get_filename (mdi, _("Save Score As"), granite->score->name->str);
	if (filename) {
		granite_xml_save_state (granite, filename);
		granite_set_score_name (granite, filename);
		granite->score->named = TRUE;
		granite->score->modified = FALSE;
		g_free (filename);
	}
}

static void
save_cmd (GtkWidget *widget, GnomeMDI *mdi)
{
	Granite *granite;
	gchar *filename;
	GnomeApp *app;
	GString *s;

	g_return_if_fail (GNOME_IS_MDI (mdi));

	granite = gtk_object_get_user_data (GTK_OBJECT (mdi));
	if (!granite->score->named) {
		save_as_cmd (widget, mdi);
		return;
	}
	filename = granite->score->name->str;
	granite_xml_save_state (granite, filename);
	granite->score->modified = FALSE;
	app = gnome_mdi_get_active_window (mdi);
	s = g_string_new (_("Saved "));
	g_string_append (s, g_basename (filename));
	gnome_app_flash (app, s->str);
	g_string_free (s, TRUE);
}

static void
setup_cmd (GtkWidget *widget, GnomeMDI *mdi)
{
	Granite *granite;

	g_return_if_fail (GNOME_IS_MDI (mdi));

	granite = gtk_object_get_user_data (GTK_OBJECT (mdi));
	if (!granite->prefs_dialog)
		granite->prefs_dialog = prefs_dialog_new (granite);
	gtk_widget_show (granite->prefs_dialog);
}

/* Help menu */

static void
about_cmd ()
{
        GtkWidget *about;
        const gchar *authors[] = {
                "Andrew T. Veliath",
                NULL
        };

        about = gnome_about_new (PACKAGE, VERSION,
                                 "(C) 1998, 1999 Andrew T. Veliath",
                                 authors,
                                 _("GNOME Music Sequencer\n"),
                                 NULL);
        gtk_window_set_modal (GTK_WINDOW (about), TRUE);
        gnome_dialog_set_close (GNOME_DIALOG (about), TRUE);
        gtk_widget_show (about);
}

static void
quit_cmd (GtkWidget *widget, GnomeMDI *mdi)
{
	Granite *granite;

	g_return_if_fail (GNOME_IS_MDI (mdi));

	granite = gtk_object_get_user_data (GTK_OBJECT (mdi));
	sequencer_shutdown (&granite->sequencer);
	granite_prefs_save (&granite->prefs);
	gnome_config_drop_all ();
	gtk_exit (0);
}

static void
mdi_destroy (GnomeMDI *mdi, GnomeMDIChild *child)
{
	quit_cmd (NULL, mdi);
}

static void
child_changed (GnomeMDI *mdi, GnomeMDIChild *child)
{
	GraniteMDIChild *gc;
	SequencerPeer *peer;
	const char *types[] = { "unknown", "master", "slave" };

	gc = GRANITE_MDI_CHILD (gnome_mdi_get_active_child (mdi));
	if (!gc)
		return;

	g_return_if_fail (GRANITE_IS_MDI_CHILD (gc));

	child = GNOME_MDI_CHILD (gc);
	peer = SEQUENCER_PEER (&gc->gtkpeer);

	debugf ("Child changed, new child is %s", types[peer->type]);

	/* If the new child is a master, try to get a sequencer lock for it */
	if (peer->type == SEQUENCER_PEER_MASTER) {
		gboolean rv;

		rv = sequencer_trylock (&gc->granite->sequencer, peer);
		if (rv)
			debugf ("Exclusive sequencer lock obtained for %s",
				child->name);
		else
			debugf ("Exclusive sequencer lock denied for %s",
				child->name);
	}
}

static void
view_changed (GnomeMDI *mdi, GtkWidget *old_view)
{
	GtkWidget *view;
	GnomeApp *app;
	GnomeUIInfo *menuinfo;

	view = gnome_mdi_get_active_view (mdi);
	if (!view)
		return;

	app = gnome_mdi_get_app_from_view (view);
	menuinfo = gnome_mdi_get_child_menu_info (app);
	gnome_app_install_menu_hints (app, menuinfo);
}

static gint
remove_child (GnomeMDI *mdi, GnomeMDIChild *child)
{
	Granite *granite;

	g_return_val_if_fail (child != NULL, FALSE);
	g_return_val_if_fail (GRANITE_IS_MDI_CHILD (child), FALSE);

	granite = GRANITE_MDI_CHILD (child)->granite;

	if (GNOME_MDI_CHILD (granite->score_edit) == child) {
		debugf ("Try to close ScoreEdit");
		return FALSE;
	}

	return TRUE;
}

static void
app_created (GnomeMDI *mdi, GnomeApp *app)
{
	GtkWidget *status;

	g_return_if_fail (app != NULL);
	g_return_if_fail (GNOME_IS_APP (app));

	gtk_window_set_default_size (GTK_WINDOW (app), 500, 200);
	status = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_NEVER);
	gnome_app_set_statusbar_custom (app, status, status);
	gnome_app_install_menu_hints (app, gnome_mdi_get_menubar_info (app));
	gtk_widget_show (status);
}

static GnomeUIInfo empty_menu[] = {
        GNOMEUIINFO_END
};

static GnomeUIInfo granite_menu_file[] = {
	GNOMEUIINFO_MENU_NEW_ITEM (N_("_New"), N_("Create a new score"), new_cmd, NULL),
	GNOMEUIINFO_MENU_OPEN_ITEM (open_cmd, NULL),
	GNOMEUIINFO_MENU_SAVE_ITEM (save_cmd, NULL),
	GNOMEUIINFO_MENU_SAVE_AS_ITEM (save_as_cmd, NULL),
        GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_PREFERENCES_ITEM (setup_cmd, NULL),
        GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_EXIT_ITEM (quit_cmd, NULL),
        GNOMEUIINFO_END
};

static GnomeUIInfo granite_menu_help[] = {
        GNOMEUIINFO_HELP ("granite"),
        GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_ABOUT_ITEM (about_cmd, NULL),
        GNOMEUIINFO_END
};

static GnomeUIInfo granite_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE (granite_menu_file),
	GNOMEUIINFO_MENU_WINDOWS_TREE (empty_menu),
	GNOMEUIINFO_MENU_HELP_TREE (granite_menu_help),
        GNOMEUIINFO_END
};

void
granite_ui_construct (Granite *granite)
{
	granite->mdi = GNOME_MDI (gnome_mdi_new ("granite", "Granite " VERSION));
	gtk_object_set_user_data (GTK_OBJECT (granite->mdi), granite);
	gnome_mdi_set_mode (granite->mdi, GNOME_MDI_TOPLEVEL);	
        gtk_signal_connect (GTK_OBJECT (granite->mdi), "destroy",
                            GTK_SIGNAL_FUNC (mdi_destroy), granite->mdi);
	gtk_signal_connect (GTK_OBJECT (granite->mdi), "child_changed",
			    GTK_SIGNAL_FUNC (child_changed), granite->mdi);
	gtk_signal_connect (GTK_OBJECT (granite->mdi), "view_changed",
			    GTK_SIGNAL_FUNC (view_changed), granite->mdi);
	gtk_signal_connect (GTK_OBJECT (granite->mdi), "remove_child",
			    GTK_SIGNAL_FUNC (remove_child), granite->mdi);
	gtk_signal_connect (GTK_OBJECT (granite->mdi), "app_created",
			    GTK_SIGNAL_FUNC (app_created), granite->mdi);

        gnome_mdi_set_menubar_template (granite->mdi, granite_menu);
        gnome_mdi_set_child_menu_path (granite->mdi, _("File"));
        gnome_mdi_set_child_list_path (granite->mdi, _("Windows/"));

	granite->score_edit = GTK_OBJECT (score_edit_new (granite, granite->score));
	gnome_mdi_add_child (granite->mdi, GNOME_MDI_CHILD (granite->score_edit));
	gnome_mdi_add_view (granite->mdi, GNOME_MDI_CHILD (granite->score_edit));
}
