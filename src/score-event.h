/**************************************************************************

    score-event.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SCORE_EVENT_H
#define __SCORE_EVENT_H

typedef enum {
	SCORE_EVENT_NOTE,
	SCORE_EVENT_AFTERTOUCH,
	SCORE_EVENT_KEY_AFTERTOUCH,
	SCORE_EVENT_PROGRAM_CHANGE,
	SCORE_EVENT_CONTROLLER,
	SCORE_EVENT_PITCH_BEND,
	SCORE_EVENT_TEMPO,
	SCORE_EVENT_METER,
	SCORE_EVENT_LOCAL,
	SCORE_EVENT_NUM_TYPES
} ScoreEventType;

typedef struct {
	gint value;
	gint velocity;
	ScoreTime duration;
} ScoreEventNote;

typedef struct {
	gint value;
} ScoreEventAftertouch;

typedef struct {
	gint note;
	gint value;
} ScoreEventKeyAftertouch;

typedef struct {
	gint value;
	/* Do other methods, controller 0, etc. */
} ScoreEventProgramChange;

typedef enum {
	SCORE_CONTROLLER_BANK_SEL		= 0,
	SCORE_CONTROLLER_MODWHEEL		= 1,
	SCORE_CONTROLLER_BREATH			= 2,
	SCORE_CONTROLLER_FOOT			= 4,
	SCORE_CONTROLLER_PORTAMENTO_TIME	= 5,
	SCORE_CONTROLLER_DATA_ENTRY		= 6,
	SCORE_CONTROLLER_VOLUME			= 7,
	SCORE_CONTROLLER_BALANCE		= 8,
	SCORE_CONTROLLER_PAN			= 10,
	SCORE_CONTROLLER_EXPRESSION		= 11
} ScoreControllerType;

typedef struct {
	gint number;
	gint value;
} ScoreEventController;

typedef struct {
	gint value;
} ScoreEventPitchBend;

typedef struct {
	ScoreTempo value;
} ScoreEventTempo;

typedef struct {
	ScoreMeter value;
} ScoreEventMeter;

typedef struct {
	gint value;
} ScoreEventLocal;

struct _ScoreTrack;

typedef struct ScoreEvent {
	ScoreEventType type;

	union {
		ScoreEventNote Note;
		ScoreEventAftertouch Aftertouch;
		ScoreEventKeyAftertouch KeyAftertouch;
		ScoreEventProgramChange ProgramChange;
		ScoreEventController Controller;
		ScoreEventPitchBend PitchBend;
		ScoreEventTempo Tempo;
		ScoreEventMeter Meter;
		ScoreEventLocal Local;
	} u;

	/* For playback (not saved) */
	guint8 port;
	guint8 channel;
	gpointer data;
} ScoreEvent;

typedef struct {
	GMutex *lock;
	GMemChunk *mem_chunk;
	GTree *heap;		/* Heap of time-sorted ScoreEvent lists */
	guint size;
} ScoreEvents;

ScoreEvents *	score_events_new		(void);
void		score_events_destroy		(ScoreEvents *score_events);
void		score_events_make_empty		(ScoreEvents *se);
void		score_events_free_event_slot	(ScoreEvents *se,
						 GSList *slot);
ScoreTime	score_events_get_max_time	(ScoreEvents *se);
ScoreTime	score_events_get_min_time	(ScoreEvents *se);
void		score_events_insert_event	(ScoreEvents *se,
						 ScoreEvent *event,
						 ScoreTime time);
void		score_events_remove_event	(ScoreEvents *se,
						 ScoreEvent *event,
						 ScoreTime time);
void		score_events_change_event_time	(ScoreEvents *se,
						 ScoreEvent *event,
						 ScoreTime old_time,
						 ScoreTime new_time);
ScoreEvent *	score_event_find_most_recent_unique_event (ScoreEvents *se,
							   ScoreEventType type,
							   ScoreTime *evtime);

/* Thread safety */
#define score_events_lock(se)			g_mutex_lock ((se)->lock)
#define score_events_unlock(se)			g_mutex_unlock ((se)->lock)
#define score_events_new_event(se)		((ScoreEvent *)(g_chunk_new (ScoreEvent, se->mem_chunk)))
#define score_events_free_event(se,ev)		(g_chunk_free (ev, se->mem_chunk))

#endif /* __SCORE_EVENT_H */
