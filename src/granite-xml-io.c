/**************************************************************************

    score-xml-io.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "ui.h"
#include "granite-xml-io.h"
#include "score-xml-io.h"
#include "sequencer-xml-io.h"

#define FILE_IDENT		"http://localhost"

gint
xmlGetInt (xmlNodePtr node, const gchar *name)
{
	name = xmlGetProp (node, name);
	if (name)
		return atoi (name);
	else
		return 0;
}

xmlNodePtr
xmlSearchChild (xmlNodePtr node, const gchar *name)
{
	xmlNodePtr down;
	xmlNodePtr child;

	for (child = node->childs; child; child = child->next)
		if (!strcmp (child->name, name))
			return child;

	for (child = node->childs; child; child = child->next)
		if ((down = xmlSearchChild (child, name)))
			return down;

	return NULL;
}

static xmlNodePtr
granite_write_xml_state (XMLContext *xc, Granite *granite)
{
	xmlNodePtr workspace;
	xmlNodePtr node;
/*	xmlNodePtr layout; */

	workspace = xmlNewDocNode (xc->doc, xc->ns, "Workspace", NULL);

	node = sequencer_write_xml (xc, &granite->sequencer.prefs);
	xmlAddChild (workspace, node);

	node = score_write_xml (xc, granite->score);
	xmlAddChild (workspace, node);

	return workspace;
}

gboolean
granite_xml_save_state (Granite *granite, const gchar *filename)
{
	XMLContext xc;
	gint rv;

	g_return_val_if_fail (granite != NULL, FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	xc.doc = xmlNewDoc ("0.0");
	xc.ns = xmlNewGlobalNs (xc.doc, FILE_IDENT, "grnt");
	xc.doc->root = granite_write_xml_state (&xc, granite);
//	xmlSetDocCompressMode (xc.doc, 9);
	rv = xmlSaveFile (filename, xc.doc);
	xmlFreeDoc (xc.doc);

	return rv < 0 ? FALSE : TRUE;
}

gboolean
granite_xml_load_state (Granite *granite, const gchar *filename)
{
	XMLContext xc;
	Score *score;
	SequencerPrefs sequencer_prefs;

	g_return_val_if_fail (granite != NULL, FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	xc.doc = xmlParseFile (filename);
	if (xc.doc == NULL)
		return FALSE;
	xc.ns = xmlSearchNsByHref (xc.doc, xc.doc->root, FILE_IDENT);
	if (!xc.ns || strcmp (xc.doc->root->name, "Workspace")) {
		g_warning ("Not a granite workspace");
		return FALSE;
	}

	sequencer_prefs_init (&sequencer_prefs);
	sequencer_read_xml (&xc, &sequencer_prefs);

	score = score_read_xml (&xc);
	xmlFreeDoc (xc.doc);
	if (score) {
		debugf ("Score loaded, loading sequencer settings");
		sequencer_prefs_changed (&granite->sequencer, &sequencer_prefs);
		granite_set_score (granite, score);
	} else
		debugf ("Score NOT loaded");

	sequencer_prefs_free (&sequencer_prefs);

	return score ? TRUE : FALSE;
}
