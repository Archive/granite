/**************************************************************************

    score-prefs.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "score-prefs.h"

void
score_prefs_init (ScorePrefs *prefs)
{
	memset (prefs, 0, sizeof (ScorePrefs));
}

void
score_prefs_load (ScorePrefs *prefs)
{
	debugf ("get score prefs");
	gnome_config_push_prefix (GRANITE_CFG "Score Defaults/");
	gnome_config_pop_prefix ();
}

void
score_prefs_save (const ScorePrefs *prefs)
{
	debugf ("save score prefs");
	gnome_config_push_prefix (GRANITE_CFG "Score Defaults/");
	gnome_config_pop_prefix ();
	gnome_config_sync ();
}

void
score_prefs_free (ScorePrefs *prefs)
{
}
