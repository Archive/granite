/**************************************************************************

    selection-hruler.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SELECTION_HRULER_H__
#define __SELECTION_HRULER_H__

#include <gtk/gtkhruler.h>

#define TYPE_SELECTION_HRULER            (selection_hruler_get_type ())
#define SELECTION_HRULER(obj)            (GTK_CHECK_CAST ((obj), TYPE_SELECTION_HRULER, SelectionHRuler))
#define SELECTION_HRULER_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), TYPE_SELECTION_HRULER, SelectionHRulerClass))
#define IS_SELECTION_HRULER(obj)         (GTK_CHECK_TYPE ((obj), TYPE_SELECTION_HRULER))
#define IS_SELECTION_HRULER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), TYPE_SELECTION_HRULER))

typedef struct _SelectionHRuler        SelectionHRuler;
typedef struct _SelectionHRulerClass   SelectionHRulerClass;

struct _SelectionHRuler
{
	GtkHRuler parent;

	GdkGC *sel_gc;

	gfloat snap_amount;
	gfloat selection_from;
	gfloat selection_to;
};

struct _SelectionHRulerClass
{
	GtkHRulerClass parent_class;

	void (* selection_changed)		(SelectionHRuler *ruler);
	void (* selection_value_changed)	(SelectionHRuler *ruler);
};

GtkType			selection_hruler_get_type	(void);
GtkWidget *		selection_hruler_new 		(void);

#endif /* __SELECTION_HRULER_H__ */
