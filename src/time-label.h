/**************************************************************************

    time-label.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __TIME_LABEL_H__
#define __TIME_LABEL_H__

#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "sequencer-gtk-peer.h"

#define TYPE_TIME_LABEL             (time_label_get_type ())
#define TIME_LABEL(obj)             (GTK_CHECK_CAST ((obj), TYPE_TIME_LABEL, TimeLabel))
#define TIME_LABEL_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), TYPE_TIME_LABEL, TimeLabelClass))
#define IS_TIME_LABEL(obj)          (GTK_CHECK_TYPE ((obj), TYPE_TIME_LABEL))
#define IS_TIME_LABEL_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), TYPE_TIME_LABEL))

typedef struct _TimeLabel            TimeLabel;
typedef struct _TimeLabelClass       TimeLabelClass;

struct _TimeLabel
{
	GtkMisc misc;

	SequencerGtkPeer gtkpeer;

	ScoreTime time;
	ScoreTimeFormat format;

	gchar str[64];

	guint time_as_delta : 1;
	guint update_enabled : 1;
};

struct _TimeLabelClass
{
	GtkMiscClass parent_class;
};

GtkType			time_label_get_type	(void);
GtkWidget *		time_label_new 		(SequencerGtkPeer *gtkmaster);
void			time_label_set_update	(TimeLabel *tl,
						 gboolean enable);
void			time_label_set_format	(TimeLabel *tl,
						 ScoreTimeFormat format);
void			time_label_set_delta	(TimeLabel *tl,
						 gboolean delta);
void			time_label_set_value	(TimeLabel *tl,
						 ScoreTime time);

#endif /* __TIME_LABEL_H__ */
