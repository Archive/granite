/**************************************************************************

    prefs-dialog.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __PREFS_DIALOG_H__
#define __PREFS_DIALOG_H__

#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"

#define TYPE_PREFS_DIALOG             (prefs_dialog_get_type ())
#define PREFS_DIALOG(obj)             (GTK_CHECK_CAST ((obj), TYPE_PREFS_DIALOG, PrefsDialog))
#define PREFS_DIALOG_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), TYPE_PREFS_DIALOG, PrefsDialogClass))
#define IS_PREFS_DIALOG(obj)          (GTK_CHECK_TYPE ((obj), TYPE_PREFS_DIALOG))
#define IS_PREFS_DIALOG_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), TYPE_PREFS_DIALOG))

typedef struct _PrefsDialog            PrefsDialog;
typedef struct _PrefsDialogClass       PrefsDialogClass;

struct _PrefsDialog
{
	GnomePropertyBox parent;

	/* Granite */
	GtkWidget *device_entry;

	/* Score */

	/* Sequencer */
	GtkWidget *send_mtc;
};

struct _PrefsDialogClass
{
	GnomePropertyBoxClass parent_class;
};

GtkType			prefs_dialog_get_type	(void);
GtkWidget *		prefs_dialog_new	(Granite *granite);

#endif /* __PREFS_DIALOG_H__ */
