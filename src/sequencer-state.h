/**************************************************************************

    sequencer-state.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SEQUENCER_STATE_H__
#define __SEQUENCER_STATE_H__

typedef guint	(*SequencerLoadEventsFunc)	(ScoreEvents *se,
						 ScoreTime offset_time,
						 ScoreTime from_time,
						 ScoreTime to_time,
						 gpointer user_data);
typedef ScoreTime (*SequencerTempoFunc)		(ScoreTime time,
						 gpointer user_data);
typedef void (*SequencerMeterFunc)		(ScoreTime time,
						 ScoreMeter *sm,
						 gpointer user_data);

typedef struct {
	SequencerLoadEventsFunc load_func;
	SequencerTempoFunc tempo_func;
	SequencerMeterFunc meter_func;
	ScoreTime offset_time;
	gpointer func_user_data;

	SequencerEchoMode echo_mode;
	SequencerMetroMode metro_mode;
	SequencerLoopMode loop_mode;
	SequencerPunchInMode punch_in_mode;

} SequencerState;

void		sequencer_state_set_defaults	(Sequencer *sequencer,
						 SequencerState *state);

#endif /* __SEQUENCER_STATE_H__ */
