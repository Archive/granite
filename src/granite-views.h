/**************************************************************************

    granite-views.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __GRANITE_VIEWS__
#define __GRANITE_VIEWS__

/* Score Views */
#include "score-edit.h"

/* Pattern Views */
#include "pattern-edit.h"

/* Clip Views */
#include "piano-roll-edit.h"

#endif /* __GRANITE_VIEWS__ */
