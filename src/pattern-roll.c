/**************************************************************************

    pattern-roll.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "pattern-roll.h"

static void pattern_roll_class_init (PatternRollClass *klass);
static void pattern_roll_init (PatternRoll *obj);
static void pattern_roll_finalize (GtkObject *obj);
static void pattern_roll_set_title_height (TimeRoll *tr, gint height);
static void pattern_roll_vert_set_ppu (TimeRoll *tr, double amount);

static void pattern_roll_set_extents (PatternRoll *pr);

GtkType
pattern_roll_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"PatternRoll",
			sizeof (PatternRoll),
			sizeof (PatternRollClass),
			(GtkClassInitFunc) pattern_roll_class_init,
			(GtkObjectInitFunc) pattern_roll_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (TYPE_EVENT_ROLL, &type_info);
	}

	return type;
}

static EventRollClass *parent_class = NULL;

static void
pattern_roll_class_init (PatternRollClass *klass)
{
	GtkObjectClass *object_class;
	TimeRollClass *time_roll_class;

	parent_class = gtk_type_class (event_roll_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = pattern_roll_finalize;

	time_roll_class = TIME_ROLL_CLASS (klass);
	time_roll_class->set_title_height = pattern_roll_set_title_height;
	time_roll_class->vert_set_ppu = pattern_roll_vert_set_ppu;
}

static void
pattern_roll_init (PatternRoll *pr)
{
	pr->pattern = NULL;
}

static void
pattern_roll_finalize (GtkObject *obj)
{
	PatternRoll *pr;

	pr = PATTERN_ROLL (obj);
	score_pattern_unref (pr->pattern);

        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

static void
pattern_roll_vadj_value_changed (GtkAdjustment *adj, PatternRoll *pr)
{
	time_roll_vert_set_y_scroll (TIME_ROLL (pr), adj->value);
	time_roll_synchronize (TIME_ROLL (pr));
}

static void
activate_cell (GtkTPCList *tpclist, gint row, gint column, gchar **value, PatternRoll *pr)
{
	ScoreTrack *track;

	track = gtk_tpclist_row_get_data (tpclist, row);
	if (!track) {
		debugf ("No track in activate cell");
		return;
	}

	switch (column) {
	case GPR_PARM_NAME:
		g_string_sprintf (track->name, "%s", *value);
		break;

	case GPR_PARM_PORT:
		track->port = atoi (*value) - 1;
		break;

	case GPR_PARM_CHN:
		track->channel = atoi (*value) - 1;
		break;
	}
}

static void
pattern_roll_set_extents (PatternRoll *pr)
{
	TimeRoll *tr;
	SequencerSlave *slave;
	Sequencer *sequencer;

	g_return_if_fail (pr != NULL);
	g_return_if_fail (IS_PATTERN_ROLL (pr));

	tr = TIME_ROLL (pr);
	slave = SEQUENCER_SLAVE (&tr->gtkpeer);
	sequencer = SEQUENCER_MASTER (slave->master)->sequencer;

	debugf ("Set time span: %d", pr->pattern->duration);
	time_roll_time_set_span (tr, 0, pr->pattern->duration);
	time_roll_vert_set_span (tr, 0, g_slist_length (pr->pattern->tracks) * tr->vert_ppu);
	time_roll_synchronize (tr);
}

static void
clip_changed (EventRollItem *item, EventRollRect *rect, PatternRoll *pr)
{
	GnomeCanvasRE *re;
	ScoreClip *clip;
	ScoreTime old_time, delta_time, new_time;
	ScoreTrack *from_track, *to_track;
	gint min, max, row;

	re = GNOME_CANVAS_RE (item);

	clip = gtk_object_get_user_data (GTK_OBJECT (item));
	score_clip_get_time_span (clip, 0, &min, &max);
	from_track = gtk_tpclist_row_get_data (pr->tpclist, (gint) re->y1);
	row = (gint) rect->y1;
	if (row >= 0 && row < pr->tpclist->nrows)
		to_track = gtk_tpclist_row_get_data (pr->tpclist, row);
	else {
		/* Row out of range, cancel track move */
		rect->y1 = re->y1;
		rect->y2 = re->y2;
		to_track = NULL;
	}
	old_time = (ScoreTime) EVENT_ROLL_ITEM (item)->user_data;
	delta_time = (ScoreTime) (rect->x1 - re->x1);
	new_time = old_time + delta_time;

	debugf ("X delta: %d Y delta: %.1f", delta_time, rect->y1 - re->y1);

	if (delta_time) {
		debugf ("Clip start time changed to %d from %d!", old_time, new_time);
		score_track_change_clip_time (from_track, clip, old_time, new_time);
		EVENT_ROLL_ITEM (item)->user_data = GINT_TO_POINTER (new_time);
	} else
		debugf ("Clip start time not changed!");

	if (to_track && from_track != to_track) {
		debugf ("Track changed from %s to %s", from_track->name->str, to_track->name->str);
		score_track_remove_clip (from_track, clip, new_time);
		score_track_insert_clip (to_track, clip, new_time);
	}
}

static EventRollItem *
insert_clip (PatternRoll *pr, gint row,
	     ScoreClip *clip, ScoreTime start_time)
{
	GnomeCanvas *canvas;
	GnomeCanvasGroup *root;
	GnomeCanvasItem *item;
	double x1, y1, x2, y2;
	ScoreTime min, max;

	g_return_val_if_fail (pr != NULL, NULL);
	g_return_val_if_fail (IS_PATTERN_ROLL (pr), NULL);
	g_return_val_if_fail (clip != NULL, NULL);

	score_clip_ref (clip);
	score_clip_get_time_span (clip, 0, &min, &max);
	debugf ("Pattern roll add clip to row %d: range %d to %d", row, min, max);

	canvas = GNOME_CANVAS (EVENT_ROLL (pr)->canvas);
	root = GNOME_CANVAS_GROUP (gnome_canvas_root (canvas));

	x1 = min; x2 = max;
	x1 += start_time; x2 += start_time;
	y1 = row; y2 = row + 1.0;

	debugf ("Loading clip: x1: %f x2: %f y1: %f y2: %f min: %d max: %d",
		x1, x2, y1, y2, min, max);
	item = gnome_canvas_item_new (root, event_roll_item_get_type (),
				      "x1", x1, "y1", y1,
				      "x2", x2, "y2", y2,
				      "fill_color", "rgb:ff/bb/88",
				      "name", clip->name->str,
				      NULL);
	g_assert (item != NULL);
	gtk_object_set_user_data (GTK_OBJECT (item), clip);
	EVENT_ROLL_ITEM (item)->user_data = (gpointer) start_time;
	gtk_signal_connect (GTK_OBJECT (item), "changed", clip_changed, pr);

	return EVENT_ROLL_ITEM (item);
}

void
pattern_roll_refresh_clip (PatternRoll *pr, ScoreClip *clip)
{
	EventRoll *er;
	GnomeCanvas *canvas;
	GnomeCanvasGroup *root;
	GnomeCanvasItem *item;
	ScoreTime start_time;
	GList *list;
	GSList *slist;
	gint row;

	g_return_if_fail (pr != NULL);
	g_return_if_fail (IS_PATTERN_ROLL (pr));
	g_return_if_fail (clip != NULL);

	er = EVENT_ROLL (pr);
	canvas = GNOME_CANVAS (er->canvas);
	root = GNOME_CANVAS_GROUP (gnome_canvas_root (canvas));

	score_clip_ref (clip);
	for (slist = NULL, list = root->item_list; list;) {
		item = list->data;
		list = list->next;
		if (gtk_object_get_user_data (GTK_OBJECT (item)) == clip)
			slist = g_slist_prepend (slist, item);
	}
	for (; slist; slist = slist->next) {
		item = slist->data;
		start_time = (ScoreTime) EVENT_ROLL_ITEM (item)->user_data;
		row = (gint) GNOME_CANVAS_RE (item)->y1;
		event_roll_replace_item (
			er, EVENT_ROLL_ITEM (item),
			insert_clip (pr, row, clip, start_time));
		gtk_object_destroy (GTK_OBJECT (item));
		score_clip_unref (clip);
	}
	g_slist_free (slist);
	score_clip_unref (clip);
}

static void
insert_track (PatternRoll *pr, gint row, ScoreTrack *track)
{
	gchar s[64];

	debugf ("Insert track %p: cur row %d", track, row);
	gtk_tpclist_row_insert (pr->tpclist, row);
	gtk_tpclist_row_set_data (pr->tpclist, row, track);
	gtk_tpclist_cell_set_value (pr->tpclist, row, GPR_PARM_NAME, track->name->str);
	sprintf (s, "%d", track->port + 1);
	gtk_tpclist_cell_set_value (pr->tpclist, row, GPR_PARM_PORT, s);
	sprintf (s, "%d", track->channel + 1);
	gtk_tpclist_cell_set_value (pr->tpclist, row, GPR_PARM_CHN, s);
}

typedef struct {
	PatternRoll *pr;
	gint row;
} InsertClipData;

static gint
insert_clip_slot (ScoreTime slot_time, GSList *slist, InsertClipData *icd)
{
	ScoreClip *clip;

	for (; slist; slist = slist->next) {
		clip = slist->data;
		insert_clip (icd->pr, icd->row, clip, slot_time);
	}
	return 0;
}

static void
init_state (PatternRoll *pr)
{
	GSList *slist;
	ScoreTrack *track;
	InsertClipData icd;

	icd.pr = pr;
	for (slist = pr->pattern->tracks; slist; slist = slist->next) {
		track = slist->data;
		icd.row = pr->tpclist->nrows;
		insert_track (pr, pr->tpclist->nrows, track);
		g_tree_traverse (track->clips, (GTraverseFunc) insert_clip_slot,
				 G_IN_ORDER, &icd);
	}
}

GtkWidget *
pattern_roll_new (ScorePattern *pattern, SequencerGtkPeer *gtkmaster)
{
	TimeRoll *tr;
	EventRoll *er;
	PatternRoll *pr;
	GtkWidget *widget;
	Sequencer *sequencer;
	SequencerSlave *slave;
	ScoreTimebase timebase;

	widget = GTK_WIDGET (gtk_type_new (TYPE_PATTERN_ROLL));
	tr = TIME_ROLL (widget);
	er = EVENT_ROLL (widget);
	pr = PATTERN_ROLL (widget);

	event_roll_construct (er, gtkmaster);

	slave = SEQUENCER_SLAVE (&tr->gtkpeer);
	sequencer = SEQUENCER_MASTER (slave->master)->sequencer;

	score_pattern_ref (pattern);
	pr->pattern = pattern;

	pr->tpclist = GTK_TPCLIST (gtk_tpclist_new ());
	gtk_tpclist_column_add (pr->tpclist, _("Name"), NULL, NULL);
	gtk_tpclist_column_set_width (pr->tpclist, 0, 120);
	gtk_tpclist_column_add (pr->tpclist, _("Port"), NULL, NULL);
	gtk_tpclist_column_add (pr->tpclist, _("Chn"), NULL, NULL);
	gtk_widget_show (GTK_WIDGET (pr->tpclist));
	gtk_signal_connect (GTK_OBJECT (pr->tpclist), "activate_cell", activate_cell, pr);
	gtk_signal_connect (GTK_OBJECT (pr->tpclist->vadjustment), "value_changed",
			    GTK_SIGNAL_FUNC (pattern_roll_vadj_value_changed), pr);

	time_roll_set_vadjustment (tr, pr->tpclist->vadjustment);
	time_roll_set_left (tr, GTK_WIDGET (pr->tpclist), pr->tpclist->hadjustment);
	time_roll_set_graph (tr, GTK_WIDGET (er->canvas), GTK_LAYOUT (er->canvas)->hadjustment);

	time_roll_time_set_ppu (tr, .1);
	timebase = sequencer_get_timebase (sequencer);
	time_roll_time_set_snap (tr, timebase);
	time_roll_vert_set_ppu (tr, GNOME_PAD_BIG + GNOME_PAD_SMALL);
	time_roll_vert_set_snap (tr, 1.0);
	time_roll_set_title_height (tr, GNOME_PAD_BIG * 2);

	init_state (pr);
	pattern_roll_set_extents (pr);

	return widget;
}

void
pattern_roll_track_insert_clip (PatternRoll *pr, ScoreTrack *track,
					ScoreClip *clip, ScoreTime start_time)
{
	gint row;

	g_return_if_fail (pr != NULL);
	g_return_if_fail (IS_PATTERN_ROLL (pr));
	g_return_if_fail (track != NULL);
	g_return_if_fail (clip != NULL);

	if ((row = gtk_tpclist_row_get_from_data (pr->tpclist, track)) == -1) {
		debugf ("Warning: couldn't find track");
		return;
	}

	score_track_insert_clip (track, clip, start_time);
	insert_clip (pr, row, clip, start_time);
}

ScoreTrack *
pattern_roll_get_current_track (PatternRoll *pr, gint *row_ret)
{
	ScoreTrack *track;
	gint row;

	g_return_val_if_fail (pr != NULL, NULL);
	g_return_val_if_fail (IS_PATTERN_ROLL (pr), NULL);

	if (pr->tpclist->nrows == 0)
		return NULL;

	row = pr->tpclist->focus_row;
	if (row < 0)
		row = 0;

	track = gtk_tpclist_row_get_data (pr->tpclist, row);
	if (row_ret && track)
		*row_ret = row;

	return track;
}

gint
pattern_roll_insert_track (PatternRoll *pr, gint position)
{
	ScoreTrack *track;
	gint row;

	g_return_val_if_fail (pr != NULL, -1);
	g_return_val_if_fail (IS_PATTERN_ROLL (pr), -1);

	if (position == -1)
		row = pr->tpclist->focus_row;
	else
		row = position;
	if (row < 0)
		row = pr->tpclist->nrows;

	track = score_track_new ();
	score_pattern_insert_track (pr->pattern, track, row);
	event_roll_shift_bottom (EVENT_ROLL (pr), row, 1);
	insert_track (pr, row, track);
	pattern_roll_set_extents (pr);

	return row;
}

void
pattern_roll_remove_track (PatternRoll *pr, gint position)
{
	ScoreTrack *track;
	gint row;

	g_return_if_fail (pr != NULL);
	g_return_if_fail (IS_PATTERN_ROLL (pr));

	if (position == -1)
		row = pr->tpclist->focus_row;
	else
		row = position;
	if (row < 0)		/* Do not haphazardly delete tracks */
		return;

	track = gtk_tpclist_row_get_data (pr->tpclist, row);
	if (!track) {
		debugf ("No data at row %d, not removing", row);
		return;
	}
	debugf ("Remove track %p %s, %d", track, track->name->str, row);
	score_pattern_remove_track (pr->pattern, track);
	score_track_destroy (track);
	gtk_tpclist_row_remove (pr->tpclist, row);
	event_roll_row_remove (EVENT_ROLL (pr), row);
	pattern_roll_set_extents (pr);
}

static void
pattern_roll_set_title_height (TimeRoll *tr, gint height)
{
	PatternRoll *pr;

	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_PATTERN_ROLL (tr));

	if (TIME_ROLL_CLASS (parent_class)->set_title_height)
		(* TIME_ROLL_CLASS (parent_class)->set_title_height)
			(tr, height);

	pr = PATTERN_ROLL (tr);
	gtk_tpclist_column_set_label_height (pr->tpclist, height);
}

static void
pattern_roll_vert_set_ppu (TimeRoll *tr, double amount)
{
	PatternRoll *pr;

	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_PATTERN_ROLL (tr));

	if (TIME_ROLL_CLASS (parent_class)->vert_set_ppu)
		(* TIME_ROLL_CLASS (parent_class)->vert_set_ppu)
			(tr, amount);

	pr = PATTERN_ROLL (tr);
	gtk_tpclist_row_set_height (pr->tpclist, amount);
}
