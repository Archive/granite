/**************************************************************************

    clip-edit.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __CLIP_EDIT_H__
#define __CLIP_EDIT_H__

#include "granite-mdi-child.h"

#define TYPE_CLIP_EDIT                      (clip_edit_get_type ())
#define CLIP_EDIT(obj)                      (GTK_CHECK_CAST ((obj), TYPE_CLIP_EDIT, ClipEdit))
#define CLIP_EDIT_CLASS(klass)              (GTK_CHECK_CLASS_CAST ((klass), TYPE_CLIP_EDIT, ClipEditClass))
#define IS_CLIP_EDIT(obj)                   (GTK_CHECK_TYPE ((obj), TYPE_CLIP_EDIT))
#define IS_CLIP_EDIT_CLASS(klass)           (GTK_CHECK_CLASS_TYPE ((klass), TYPE_CLIP_EDIT))

typedef struct _ClipEdit                    ClipEdit;
typedef struct _ClipEditClass               ClipEditClass;

struct _ClipEdit
{
	GraniteMDIChild parent;

	ScoreClip *clip;
};

struct _ClipEditClass
{
	GraniteMDIChildClass parent_class;
};

GtkType			clip_edit_get_type	(void);
void			clip_edit_construct	(ClipEdit *child,
						 Granite *granite,
						 ScoreClip *clip);
void			clip_edit_set_name	(GnomeMDIChild *child,
						 const char *name);

#endif /* __CLIP_EDIT_H__ */
