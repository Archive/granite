/**************************************************************************

    granite-entry.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __GRANITE_ENTRY_H__
#define __GRANITE_ENTRY_H__

#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"

#define GRANITE_TYPE_ENTRY                  (granite_entry_get_type ())
#define GRANITE_ENTRY(obj)                  (GTK_CHECK_CAST ((obj), GRANITE_TYPE_ENTRY, GraniteEntry))
#define GRANITE_ENTRY_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), GRANITE_TYPE_ENTRY, GraniteEntryClass))
#define GRANITE_IS_ENTRY(obj)               (GTK_CHECK_TYPE ((obj), GRANITE_TYPE_ENTRY))
#define GRANITE_IS_ENTRY_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), GRANITE_TYPE_ENTRY))

typedef struct _GraniteEntry                GraniteEntry;
typedef struct _GraniteEntryClass           GraniteEntryClass;

struct _GraniteEntry
{
	GtkEntry parent;

	gint width;
	gint min, max;
};

struct _GraniteEntryClass
{
	GtkEntryClass parent_class;
};

GtkType			granite_entry_get_type		(void);
GtkWidget *		granite_entry_new 		(void);
void			granite_entry_set_width		(GraniteEntry *entry,
							 gint width);
void			granite_entry_set_min_max	(GraniteEntry *entry,
							 gint min,
							 gint max);

#endif /* __GRANITE_ENTRY_H__ */
