/**************************************************************************

    event-roll-item.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __EVENT_ROLL_ITEM_H__
#define __EVENT_ROLL_ITEM_H__

#include <libgnomeui/gnome-canvas-rect-ellipse.h>

#define TYPE_EVENT_ROLL_ITEM               (event_roll_item_get_type ())
#define EVENT_ROLL_ITEM(obj)               (GTK_CHECK_CAST ((obj), TYPE_EVENT_ROLL_ITEM, EventRollItem))
#define EVENT_ROLL_ITEM_CLASS(klass)       (GTK_CHECK_CLASS_CAST ((klass), TYPE_EVENT_ROLL_ITEM, EventRollItemClass))
#define IS_EVENT_ROLL_ITEM(obj)            (GTK_CHECK_TYPE ((obj), TYPE_EVENT_ROLL_ITEM))
#define IS_EVENT_ROLL_ITEM_CLASS(klass)    (GTK_CHECK_CLASS_TYPE ((klass), TYPE_EVENT_ROLL_ITEM))

typedef enum
{
	EVENT_ROLL_ITEM_NORMAL,
	EVENT_ROLL_ITEM_SELECTED
} EventRollItemState;

typedef struct _EventRollItem              EventRollItem;
typedef struct _EventRollItemClass         EventRollItemClass;
typedef struct _EventRollRect              EventRollRect;

struct _EventRollItem
{
	GnomeCanvasRect parent;

	gchar *name;

	EventRollItemState state;

	gpointer user_data;

	/* Private variables */
	guint dirty : 1;
};

struct _EventRollItemClass
{
	GnomeCanvasRectClass parent_class;

	void (* changed)	 (EventRollItem *item,
				  EventRollRect *new_position);
};

struct _EventRollRect {
	double x1, y1;
	double x2, y2;
};

GtkType              event_roll_item_get_type        (void);
void                 event_roll_item_set_state       (EventRollItem *item,
						      EventRollItemState state);

#endif /* __EVENT_ROLL_ITEM_H__ */
