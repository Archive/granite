/**************************************************************************

    score-clip.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SCORE_CLIP_H
#define __SCORE_CLIP_H

typedef struct _ScoreMIDIClipServant        ScoreMIDIClipServant;

typedef enum {
	SCORE_CLIP_MIDI,
	SCORE_CLIP_AUDIO,
	SCORE_CLIP_NUM_TYPES
} ScoreClipType;

typedef struct {
	ScoreEvents *events;
	ScoreMIDIClipServant *servant; /* Automation */
} ScoreClipMIDI;

typedef struct {
	gint not_implemented;
} ScoreClipAudio;

typedef struct {
	ScoreMarkType mark_type;

	ScoreClipType type;
	gint refs;
	GString *name;

	union {
		ScoreClipMIDI MIDI;
		ScoreClipAudio Audio;
	} u;

	gpointer user_data;

	guint id;		/* For saving and loading */
} ScoreClip;

struct _ScoreMIDIClipServant {
	POA_Granite_ScoreMIDIClip servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	ScoreClip *clip;
	ScoreTime cur_time;	/* Current working time */
	GSList *cur_slist;	/* Current time slot list */
	GSList *next_slist;	/* Next time slot list node */
	GHashTable *changed_times; /* Don't recurse visited events */
};

ScoreClip *		score_clip_midi_new		(ScoreEvents *events);
void			score_clip_ref			(ScoreClip *clip);
void			score_clip_unref		(ScoreClip *clip);
void			score_clip_get_time_span	(ScoreClip *clip,
							 ScoreTime offset_time,
							 ScoreTime *from_time,
							 ScoreTime *to_time);

#endif /* __SCORE_CLIP_H */
