/**************************************************************************

    sequencer-peer.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"

#define FAIL_IF_NOT_MASTER			do {	\
	if (peer->type != SEQUENCER_PEER_MASTER)	\
		return;					\
} while (0)

#define FAIL_IF_NOT_MASTER_RV			do {	\
	if (peer->type != SEQUENCER_PEER_MASTER)	\
		return FALSE;				\
} while (0)

#define FAIL_IF_NOT_OWNED			do {			\
	FAIL_IF_NOT_MASTER;						\
	if (!sequencer_master_is_owned (&peer->u.Master)) {		\
		debugf ("Operation denied, sequencer locked by peer");	\
		return;							\
	}								\
} while (0)

#define FAIL_IF_NOT_OWNED_RV			do {			\
	FAIL_IF_NOT_MASTER_RV;						\
	if (!sequencer_master_is_owned (&peer->u.Master)) {		\
		debugf ("Operation denied, sequencer locked by peer");	\
		return FALSE;						\
	}								\
} while (0)

#define FAIL_IF_NOT_OWNED_MASTER		do {			\
	if (!sequencer_master_is_owned (master)) {			\
		debugf ("Operation denied, sequencer locked by peer");	\
		return;							\
	}								\
} while (0)

#define get_sequencer_from_peer(peer)						\
	((peer)->type == SEQUENCER_PEER_MASTER					\
	 ? SEQUENCER_MASTER (peer)->sequencer					\
	 : SEQUENCER_MASTER (SEQUENCER_SLAVE (peer)->master)->sequencer)

static gboolean
sequencer_master_is_owned (SequencerMaster *master)
{
	g_return_val_if_fail (master != NULL, FALSE);
	g_return_val_if_fail (master->sequencer != NULL, FALSE);

	return master->sequencer->master == master;
}

void
sequencer_peer_master_construct (SequencerPeer *peer, Sequencer *sequencer)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);
	g_return_if_fail (sequencer != NULL);

	peer->type = SEQUENCER_PEER_MASTER;
	peer->destroy = NULL;
	master = &peer->u.Master;
	memset (master, 0, sizeof (*master));
	master->sequencer = sequencer;
	sequencer_state_set_defaults (sequencer, &master->saved_state);
}

void
sequencer_peer_slave_construct (SequencerPeer *peer, SequencerPeer *chain)
{
	SequencerSlave *slave;

	g_return_if_fail (peer != NULL);
	g_return_if_fail (chain != NULL);
	g_return_if_fail (chain->type == SEQUENCER_PEER_MASTER ||
			  chain->type == SEQUENCER_PEER_SLAVE);

	peer->type = SEQUENCER_PEER_SLAVE;
	peer->destroy = NULL;
	slave = &peer->u.Slave;
	memset (slave, 0, sizeof (*slave));
	if (chain->type == SEQUENCER_PEER_MASTER)
		slave->master = chain;
	else
		slave->master = SEQUENCER_SLAVE (chain)->master;
}

void
sequencer_peer_deconstruct (SequencerPeer *peer)
{
	g_return_if_fail (peer != NULL);

	if (peer->type == SEQUENCER_PEER_UNKNOWN)
		return;

	if (peer->destroy)
	    (* peer->destroy) (peer);

	if (peer->type == SEQUENCER_PEER_MASTER)
		if (sequencer_master_is_owned (&peer->u.Master)) {
			sequencer_unlock_master (peer->u.Master.sequencer);
	}
}

void
sequencer_peer_set_load_func (SequencerPeer *peer, SequencerLoadEventsFunc func,
			      ScoreTime offset_time, gpointer user_data)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);

	FAIL_IF_NOT_OWNED;
	master = &peer->u.Master;
	sequencer_set_load_func (master->sequencer, func, offset_time, user_data);
}

gboolean
sequencer_peer_goto (SequencerPeer *peer, ScoreTime time)
{
	SequencerMaster *master;

	g_return_val_if_fail (peer != NULL, FALSE);

	FAIL_IF_NOT_OWNED_RV;
	master = &peer->u.Master;
	sequencer_goto (master->sequencer, time);

	return TRUE;
}

gboolean
sequencer_peer_play (SequencerPeer *peer)
{
	SequencerMaster *master;

	g_return_val_if_fail (peer != NULL, FALSE);

	FAIL_IF_NOT_OWNED_RV;
	master = &peer->u.Master;
	sequencer_play (master->sequencer);

	return TRUE;
}

void
sequencer_peer_stop (SequencerPeer *peer)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);

	FAIL_IF_NOT_OWNED;
	master = &peer->u.Master;
	sequencer_stop (master->sequencer);
}

void
sequencer_peer_record (SequencerPeer *peer, gboolean enable)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);

	FAIL_IF_NOT_OWNED;
	master = &peer->u.Master;
	sequencer_record (master->sequencer, enable);
}

void
sequencer_peer_get_echo_mode (SequencerPeer *peer, SequencerEchoMode *em)
{
	Sequencer *sequencer;

	g_return_if_fail (peer != NULL);

	sequencer = get_sequencer_from_peer (peer);
	sequencer_get_echo_mode (sequencer, em);
}

void
sequencer_peer_set_echo_mode (SequencerPeer *peer, const SequencerEchoMode *em)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);

	FAIL_IF_NOT_OWNED;
	master = &peer->u.Master;
	sequencer_set_echo_mode (master->sequencer, em);
}

void
sequencer_peer_get_metro_mode	(SequencerPeer *peer, SequencerMetroMode *mm)
{
	Sequencer *sequencer;

	g_return_if_fail (peer != NULL);

	sequencer = get_sequencer_from_peer (peer);
	sequencer_get_metro_mode (sequencer, mm);
}

void
sequencer_peer_set_metro_mode	(SequencerPeer *peer, const SequencerMetroMode *mm)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);

	FAIL_IF_NOT_OWNED;
	master = &peer->u.Master;
	sequencer_set_metro_mode (master->sequencer, mm);
}

void
sequencer_peer_get_loop_mode (SequencerPeer *peer, SequencerLoopMode *lm)
{
	Sequencer *sequencer;

	g_return_if_fail (peer != NULL);

	sequencer = get_sequencer_from_peer (peer);
	sequencer_get_loop_mode (sequencer, lm);
}

void
sequencer_peer_set_loop_mode (SequencerPeer *peer, const SequencerLoopMode *lm)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);

	FAIL_IF_NOT_OWNED;
	master = &peer->u.Master;
	sequencer_set_loop_mode (master->sequencer, lm);
}

void
sequencer_peer_get_punch_in_mode (SequencerPeer *peer, SequencerPunchInMode *pim)
{
	Sequencer *sequencer;

	g_return_if_fail (peer != NULL);

	sequencer = get_sequencer_from_peer (peer);
	sequencer_get_punch_in_mode (sequencer, pim);
}

void
sequencer_peer_set_punch_in_mode (SequencerPeer *peer, const SequencerPunchInMode *pim)
{
	SequencerMaster *master;

	g_return_if_fail (peer != NULL);

	FAIL_IF_NOT_OWNED;
	master = &peer->u.Master;
	sequencer_set_punch_in_mode (master->sequencer, pim);
}

static void
sequencer_master_load_state_to_sequencer (SequencerMaster *master)
{
	g_return_if_fail (master != NULL);
	g_return_if_fail (master->sequencer != NULL);

	FAIL_IF_NOT_OWNED_MASTER;
	master->sequencer->active_state = master->saved_state;
#if 0
	if (master->sequencer->current_meter.D == 0) {
		master->sequencer->current_tempo = master->saved_state.initial_tempo;
		master->sequencer->current_meter = master->saved_state.initial_meter;
	}
#endif
}

static void
sequencer_master_save_state_from_sequencer (SequencerMaster *master)
{
	g_return_if_fail (master != NULL);
	g_return_if_fail (master->sequencer != NULL);

	FAIL_IF_NOT_OWNED_MASTER;
	master->saved_state = master->sequencer->active_state;
}

gboolean
sequencer_in_use (Sequencer *sequencer)
{
	g_return_val_if_fail (sequencer != NULL, FALSE);

	return sequencer->playing;
}

gboolean
sequencer_is_locked (Sequencer *sequencer)
{
	gboolean rv;

	g_return_val_if_fail (sequencer != NULL, FALSE);

	pthread_mutex_lock (&sequencer->master_mutex);
	rv = sequencer->master != NULL;
	pthread_mutex_unlock (&sequencer->master_mutex);

	return rv;
}

gboolean
sequencer_lock_master (Sequencer *sequencer, SequencerMaster *master)
{
	g_return_val_if_fail (sequencer != NULL, FALSE);
	g_return_val_if_fail (master != NULL, FALSE);

	if (sequencer_is_locked (sequencer))
		return FALSE;

	pthread_mutex_lock (&sequencer->master_mutex);
	sequencer->master = master;
	g_assert (master->sequencer == sequencer);
	sequencer_master_load_state_to_sequencer (master);
	pthread_mutex_unlock (&sequencer->master_mutex);

	return TRUE;
}

void
sequencer_unlock_master (Sequencer *sequencer)
{
	g_return_if_fail (sequencer != NULL);

	if (!sequencer_is_locked (sequencer))
		return;

	pthread_mutex_lock (&sequencer->master_mutex);
	if (sequencer->playing) {
		debugf ("Stopping play on owned master");
		sequencer_stop (sequencer);
	}
	sequencer_master_save_state_from_sequencer (sequencer->master);
	sequencer->master = NULL;
	pthread_mutex_unlock (&sequencer->master_mutex);
}

gboolean
sequencer_trylock (Sequencer *sequencer, SequencerPeer *peer)
{
	g_return_val_if_fail (peer != NULL, FALSE);
	g_return_val_if_fail (sequencer != NULL, FALSE);

	if (peer->type == SEQUENCER_PEER_MASTER) {
		if (sequencer->master == &peer->u.Master)
			return TRUE;
		if (sequencer_is_locked (sequencer)) {
			if (sequencer_in_use (sequencer))
				return FALSE;
			sequencer_unlock_master (sequencer);
		}
		sequencer_lock_master (sequencer, &peer->u.Master);
		return TRUE;
	}
	debugf ("Sequencer slave tried to obtain sequencer lock");

	return FALSE;
}

void
sequencer_state_set_defaults (Sequencer *sequencer, SequencerState *state)
{
	g_return_if_fail (state != NULL);

	state->echo_mode.enabled = TRUE;
	state->echo_mode.use_port = TRUE;
	state->echo_mode.use_channel = TRUE;
	state->echo_mode.use_filter = TRUE;
	state->echo_mode.port = 0;
	state->echo_mode.channel = 0;

	state->metro_mode.enabled = TRUE;
	state->metro_mode.counting_in = FALSE;
	state->metro_mode.record_count_in = 1;

	/* For now assume the usual "rimshot" on drum channel */
	state->metro_mode.FirstBeat.port = 0;
	state->metro_mode.FirstBeat.channel = 9;
	state->metro_mode.FirstBeat.type = SCORE_EVENT_NOTE;
	state->metro_mode.FirstBeat.u.Note.value = 37;
	state->metro_mode.FirstBeat.u.Note.velocity = 100;
	state->metro_mode.FirstBeat.u.Note.duration =
		sequencer_get_timebase (sequencer) / 4;

	state->metro_mode.Beat = state->metro_mode.FirstBeat;
	state->metro_mode.Beat.u.Note.velocity = 80;

	state->loop_mode.enabled = FALSE;
	state->punch_in_mode.enabled = FALSE;
}
