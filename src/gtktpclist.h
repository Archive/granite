/**************************************************************************

    gtktpclist.h

    Typed Parameter Column List Widget

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __GTKTPCLIST_H__
#define __GTKTPCLIST_H__

#include <gtk/gtk.h>

#define GTK_TYPE_TPCLIST                    (gtk_tpclist_get_type ())
#define GTK_TPCLIST(obj)                    (GTK_CHECK_CAST ((obj), GTK_TYPE_TPCLIST, GtkTPCList))
#define GTK_TPCLIST_CLASS(klass)            (GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_TPCLIST, GtkTPCListClass))
#define GTK_IS_TPCLIST(obj)                 (GTK_CHECK_TYPE ((obj), GTK_TYPE_TPCLIST))
#define GTK_IS_TPCLIST_CLASS(klass)         (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_TPCLIST))

typedef struct _GtkTPCList                  GtkTPCList;
typedef struct _GtkTPCListClass             GtkTPCListClass;

typedef gchar *(* GtkTPCListValueFunc)	    (GtkTPCList *tpclist,
					     gint row,
					     gint column,
					     gpointer user_data);

struct _GtkTPCList
{
	GtkContainer parent;

	/* Misc */
	GdkGC *xor_gc;
	gint focus_row, focus_column;

	/* Column */
	gint column_label_height;
	gint ncolumns;
	GdkWindow *column_window;
	GdkRectangle column_window_area;
	GtkAdjustment *hadjustment;
	GArray *columns;
	GArray *column_index;
	GdkPixmap *column_backing_store;

	/* Row */
	gint row_height;
	gint row_label_width;
	gint nrows;
	GdkWindow *row_window;
	GdkRectangle row_window_area;
	GtkAdjustment *vadjustment;
	GdkPixmap *row_backing_store;
	GArray *row_data, *row_flags;
	GSList *selected_rows;

 	/* Data */
	GdkWindow *data_window;
	GdkRectangle data_window_area;
	GdkPixmap *data_backing_store;
	gint edit_row, edit_column;
	GtkWidget *entry;
	GtkWidget *entry_top;

	/* Flags */
	guint column_window_dirty : 1;
	guint row_window_dirty : 1;
	guint data_window_dirty : 1;
};

struct _GtkTPCListClass
{
	GtkContainerClass parent_class;

	void   (* set_scroll_adjustments)	(GtkTPCList      *tpclist,
						 GtkAdjustment   *hadjustment,
						 GtkAdjustment   *vadjustment);
	void   (* activate_cell)		(GtkTPCList      *tpclist,
						 gint             row,
						 gint             column,
						 gchar          **value);
	void   (* focus_cell)			(GtkTPCList      *tpclist,
						 gint             row,
						 gint             column);
	void   (* key_press_cell)		(GtkTPCList      *tpclist,
						 gint             row,
						 gint             column,
						 GdkEventKey     *event);
};

GtkType		gtk_tpclist_get_type			(void);
GtkWidget *	gtk_tpclist_new				(void);

/* Column setting */
void		gtk_tpclist_column_add			(GtkTPCList *tpclist,
							 const gchar *name,
							 GtkTPCListValueFunc func,
							 gpointer func_data);
void		gtk_tpclist_column_swap			(GtkTPCList *tpclist,
							 gint col1,
							 gint col2);
gchar *		gtk_tpclist_preserve_column_state	(GtkTPCList *tpclist);
void		gtk_tpclist_restore_column_state	(GtkTPCList *tpclist,
							 gchar *order);
void		gtk_tpclist_column_set_label_height	(GtkTPCList *tpclist,
							 gint height);
void		gtk_tpclist_column_set_width		(GtkTPCList *tpclist,
							 gint column,
							 gint width);
void		gtk_tpclist_column_swap			(GtkTPCList *tpclist,
							 gint col1,
							 gint col2);

/* Row setting */
void		gtk_tpclist_row_set_data		(GtkTPCList *tpclist,
							 gint row,
							 gpointer data);
gpointer	gtk_tpclist_row_get_data		(GtkTPCList *tpclist,
							 gint row);
gint		gtk_tpclist_row_get_from_data		(GtkTPCList *tpclist,
							 gpointer data);
gint		gtk_tpclist_row_append			(GtkTPCList *tpclist);
void		gtk_tpclist_row_insert			(GtkTPCList *tpclist,
							 gint position);
void		gtk_tpclist_row_remove			(GtkTPCList *tpclist,
							 gint position);
void		gtk_tpclist_row_remove_all		(GtkTPCList *tpclist);
void		gtk_tpclist_row_set_label_width		(GtkTPCList *tpclist,
							 gint width);
void		gtk_tpclist_row_set_height		(GtkTPCList *tpclist,
							 gint height);
void		gtk_tpclist_row_select_all		(GtkTPCList *tpclist);
void		gtk_tpclist_row_unselect_all		(GtkTPCList *tpclist);
void		gtk_tpclist_row_set_selected		(GtkTPCList *tpclist,
							 gint row,
							 gboolean selected);
void		gtk_tpclist_row_toggle_selected		(GtkTPCList *tpclist,
							 gint row);
gboolean	gtk_tpclist_row_is_selected		(GtkTPCList *tpclist,
							 gint row);

/* Cell setting */
void		gtk_tpclist_cell_set_focus		(GtkTPCList *tpclist,
							 gint row,
							 gint column);
void		gtk_tpclist_cell_make_viewable		(GtkTPCList *tpclist,
							 gint row,
							 gint column);
const gchar *	gtk_tpclist_cell_get_value		(GtkTPCList *tpclist,
							 gint row,
							 gint column);
void		gtk_tpclist_cell_set_value		(GtkTPCList *tpclist,
							 gint row,
							 gint column,
							 const gchar *value);

#endif /* __GTKTPCLIST_H__ */
