/**************************************************************************

    granite-entry.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "granite-entry.h"
#include <gdk/gdkkeysyms.h>

#define MIN_ENTRY_WIDTH  64

static void granite_entry_class_init (GraniteEntryClass *klass);
static void granite_entry_init (GraniteEntry *obj);
static void granite_entry_size_request (GtkWidget *widget, GtkRequisition *requisition);
static gint granite_entry_key_press (GtkWidget *widget, GdkEventKey *event);

GtkType
granite_entry_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"GraniteEntry",
			sizeof (GraniteEntry),
			sizeof (GraniteEntryClass),
			(GtkClassInitFunc) granite_entry_class_init,
			(GtkObjectInitFunc) granite_entry_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GTK_TYPE_ENTRY, &type_info);
	}

	return type;
}

static GtkEntryClass *parent_class = NULL;

static void
granite_entry_class_init (GraniteEntryClass *klass)
{
	GtkWidgetClass *widget_class;

	parent_class = gtk_type_class (gtk_entry_get_type ());

	widget_class = GTK_WIDGET_CLASS (klass);
	widget_class->size_request = granite_entry_size_request;
	widget_class->key_press_event = granite_entry_key_press;
}

static void
granite_entry_init (GraniteEntry *entry)
{
	granite_entry_set_min_max (entry, G_MININT, G_MAXINT);
	entry->width = MIN_ENTRY_WIDTH;
}

GtkWidget *
granite_entry_new (void)
{
	return GTK_WIDGET (gtk_type_new (GRANITE_TYPE_ENTRY));
}

void
granite_entry_set_width (GraniteEntry *entry, gint width)
{
	g_return_if_fail (entry != NULL);
	g_return_if_fail (GRANITE_IS_ENTRY (entry));

	entry->width = width;
}

void
granite_entry_set_min_max (GraniteEntry *entry, gint min, gint max)
{
	g_return_if_fail (entry != NULL);
	g_return_if_fail (GRANITE_IS_ENTRY (entry));

	entry->min = min;
	entry->max = max;
}

static void
granite_entry_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (GRANITE_IS_ENTRY (widget));
	g_return_if_fail (requisition != NULL);

	(* GTK_WIDGET_CLASS (parent_class)->size_request) (widget, requisition);
	requisition->width = GRANITE_ENTRY (widget)->width +
		(widget->style->klass->xthickness + 2) * 2;
}

static gint
granite_entry_key_press (GtkWidget *widget, GdkEventKey *event)
{
	GraniteEntry *entry;
	const char *str;
	char tmp[64];
	gint val;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GRANITE_IS_ENTRY (widget), FALSE);

	entry = GRANITE_ENTRY (widget);
	
	switch (event->keyval) {
	case GDK_KP_Subtract:
		str = gtk_entry_get_text (GTK_ENTRY (widget));
		val = atoi (str) - 1;
		if (val >= entry->min) {
			sprintf (tmp, "%d", val);
			gtk_entry_set_text (GTK_ENTRY (widget), tmp);
		}
		return TRUE;

	case GDK_KP_Add:
		str = gtk_entry_get_text (GTK_ENTRY (widget));
		val = atoi (str) + 1;
		if (val <= entry->max) {
			sprintf (tmp, "%d", val);
			gtk_entry_set_text (GTK_ENTRY (widget), tmp);
		}
		return TRUE;

	case GDK_Left:
		if (GTK_EDITABLE (widget)->current_pos == 0)
			return FALSE;
		else
			return (* GTK_WIDGET_CLASS (parent_class)->key_press_event) (widget, event);

	case GDK_Right:
		if (GTK_EDITABLE (widget)->current_pos == GTK_ENTRY (entry)->text_length)
			return FALSE;
		else
			return (* GTK_WIDGET_CLASS (parent_class)->key_press_event) (widget, event);

	default:
		return (* GTK_WIDGET_CLASS (parent_class)->key_press_event) (widget, event);
	}
}
