/**************************************************************************

    clip-roll.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include "clip-roll.h"

static void clip_roll_class_init (ClipRollClass *klass);
static void clip_roll_init (ClipRoll *obj);
static void clip_roll_finalize (GtkObject *obj);

GtkType
clip_roll_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"ClipRoll",
			sizeof (ClipRoll),
			sizeof (ClipRollClass),
			(GtkClassInitFunc) clip_roll_class_init,
			(GtkObjectInitFunc) clip_roll_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (TYPE_EVENT_ROLL, &type_info);
	}

	return type;
}

static EventRollClass *parent_class = NULL;

static void
clip_roll_class_init (ClipRollClass *klass)
{
	GtkObjectClass *object_class;

	parent_class = gtk_type_class (event_roll_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = clip_roll_finalize;
}

static void
clip_roll_init (ClipRoll *clip_roll)
{
	clip_roll->clip = NULL;
}

static void
clip_roll_finalize (GtkObject *obj)
{
	ClipRoll *cr;

	cr = CLIP_ROLL (obj);
	score_clip_unref (cr->clip);

        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

void
clip_roll_construct (ClipRoll *clip_roll, ScoreClip *clip,
		     SequencerGtkPeer *gtkmaster)
{
	event_roll_construct (EVENT_ROLL (clip_roll), gtkmaster);
	clip_roll->clip = clip;
	score_clip_ref (clip);
}
