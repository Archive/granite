/**************************************************************************

    pattern-roll.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __PATTERN_ROLL_H__
#define __PATTERN_ROLL_H__

#include <gnome.h>
#include "common.h"
#include "score.h"
#include "event-roll.h"
#include "gtktpclist.h"

#define TYPE_PATTERN_ROLL                    (pattern_roll_get_type ())
#define PATTERN_ROLL(obj)                    (GTK_CHECK_CAST ((obj), TYPE_PATTERN_ROLL, PatternRoll))
#define PATTERN_ROLL_CLASS(klass)            (GTK_CHECK_CLASS_CAST ((klass), TYPE_PATTERN_ROLL, PatternRollClass))
#define IS_PATTERN_ROLL(obj)                 (GTK_CHECK_TYPE ((obj), TYPE_PATTERN_ROLL))
#define IS_PATTERN_ROLL_CLASS(klass)         (GTK_CHECK_CLASS_TYPE ((klass), TYPE_PATTERN_ROLL))

#define GPR_PARM_NAME	                      0
#define GPR_PARM_PORT	                      1
#define GPR_PARM_CHN	                      2

typedef struct _PatternRoll                   PatternRoll;
typedef struct _PatternRollClass              PatternRollClass;

struct _PatternRoll
{
	EventRoll parent;

	ScorePattern *pattern;

	GtkTPCList *tpclist;
};

struct _PatternRollClass
{
	EventRollClass parent_class;
};

GtkType		pattern_roll_get_type			(void);
GtkWidget *	pattern_roll_new			(ScorePattern *pattern,
							 SequencerGtkPeer *gtkmaster);
ScoreTrack *	pattern_roll_get_current_track		(PatternRoll *pr,
							 gint *row_ret);
void		pattern_roll_refresh_clip		(PatternRoll *pr,
							 ScoreClip *clip);
void		pattern_roll_track_insert_clip		(PatternRoll *pr,
							 ScoreTrack *track,
							 ScoreClip *clip,
							 ScoreTime start_time);
gint		pattern_roll_insert_track		(PatternRoll *pr,
							 gint position);
void		pattern_roll_remove_track		(PatternRoll *pr,
							 gint position);

#endif /* __PATTERN_ROLL_H__ */
