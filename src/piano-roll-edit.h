/**************************************************************************

    piano-roll-edit.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __PIANO_ROLL_EDIT_H__
#define __PIANO_ROLL_EDIT_H__

#include "clip-edit.h"

#define TYPE_PIANO_ROLL_EDIT               (piano_roll_edit_get_type ())
#define PIANO_ROLL_EDIT(obj)               (GTK_CHECK_CAST ((obj), TYPE_PIANO_ROLL_EDIT, PianoRollEdit))
#define PIANO_ROLL_EDIT_CLASS(klass)       (GTK_CHECK_CLASS_CAST ((klass), TYPE_PIANO_ROLL_EDIT, PianoRollEditClass))
#define IS_PIANO_ROLL_EDIT(obj)            (GTK_CHECK_TYPE ((obj), TYPE_PIANO_ROLL_EDIT))
#define IS_PIANO_ROLL_EDIT_CLASS(klass)    (GTK_CHECK_CLASS_TYPE ((klass), TYPE_PIANO_ROLL_EDIT))

typedef struct _PianoRollEdit        PianoRollEdit;
typedef struct _PianoRollEditClass   PianoRollEditClass;

struct _PianoRollEdit
{
	ClipEdit parent;

	GtkWidget *pr;
};

struct _PianoRollEditClass
{
	ClipEditClass parent_class;
};

GtkType			piano_roll_edit_get_type	(void);
GnomeMDIChild *		piano_roll_edit_new 		(Granite *granite,
							 ScoreClip *clip,
							 SequencerGtkPeer *gtkpeer);

#endif /* __PIANO_ROLL_EDIT_H__ */
