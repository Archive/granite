/**************************************************************************

    clip-roll.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __CLIP_ROLL_H__
#define __CLIP_ROLL_H__

#include <gnome.h>
#include "common.h"
#include "score.h"
#include "event-roll.h"

#define TYPE_CLIP_ROLL                     (clip_roll_get_type ())
#define CLIP_ROLL(obj)                     (GTK_CHECK_CAST ((obj), TYPE_CLIP_ROLL, ClipRoll))
#define CLIP_ROLL_CLASS(klass)             (GTK_CHECK_CLASS_CAST ((klass), TYPE_CLIP_ROLL, ClipRollClass))
#define IS_CLIP_ROLL(obj)                  (GTK_CHECK_TYPE ((obj), TYPE_CLIP_ROLL))
#define IS_CLIP_ROLL_CLASS(klass)          (GTK_CHECK_CLASS_TYPE ((klass), TYPE_CLIP_ROLL))

typedef struct _ClipRoll                   ClipRoll;
typedef struct _ClipRollClass              ClipRollClass;

struct _ClipRoll
{
	EventRoll parent;

	ScoreClip *clip;
};

struct _ClipRollClass
{
	EventRollClass parent_class;
};

GtkType			clip_roll_get_type	(void);
void			clip_roll_construct	(ClipRoll *clip_roll,
						 ScoreClip *clip,
						 SequencerGtkPeer *gtkmaster);

#endif /* __CLIP_ROLL_H__ */
