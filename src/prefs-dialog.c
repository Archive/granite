/**************************************************************************

    prefs-dialog.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include "prefs-dialog.h"

static void prefs_dialog_class_init (PrefsDialogClass *klass);
static void prefs_dialog_init (PrefsDialog *obj);
static void finalize (GtkObject *obj);
static void apply (GnomePropertyBox *dlg, gint page_num);

GtkType
prefs_dialog_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"PrefsDialog",
			sizeof (PrefsDialog),
			sizeof (PrefsDialogClass),
			(GtkClassInitFunc) prefs_dialog_class_init,
			(GtkObjectInitFunc) prefs_dialog_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (gnome_property_box_get_type (), &type_info);
	}

	return type;
}

static GnomePropertyBoxClass *parent_class = NULL;

static void
prefs_dialog_class_init (PrefsDialogClass *klass)
{
	GtkObjectClass *object_class;
	GnomePropertyBoxClass *property_box_class;

	parent_class = gtk_type_class (gnome_property_box_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = finalize;

	property_box_class = GNOME_PROPERTY_BOX_CLASS (klass);
	property_box_class->apply = apply;
}

static void
set_granite_prefs (PrefsDialog *dlg, const GranitePrefs *prefs)
{
	gtk_entry_set_text (GTK_ENTRY (dlg->device_entry), prefs->device);
}

static void
get_granite_prefs (PrefsDialog *dlg, GranitePrefs *prefs)
{
	g_free (prefs->device);
	prefs->device = g_strdup (gtk_entry_get_text (GTK_ENTRY (dlg->device_entry)));
}

static void
apply_granite_prefs (PrefsDialog *dlg)
{
	Granite *granite = gtk_object_get_user_data (GTK_OBJECT (dlg));
	GranitePrefs prefs;

	granite_prefs_init (&prefs);
	get_granite_prefs (dlg, &prefs);
	granite_prefs_changed (granite, &prefs);
	granite_prefs_free (&prefs);
}

static void
set_score_prefs (PrefsDialog *dlg, const ScorePrefs *prefs)
{
}

static void
get_score_prefs (PrefsDialog *dlg, ScorePrefs *prefs)
{
}

static void
apply_score_prefs (PrefsDialog *dlg)
{
	Granite *granite = gtk_object_get_user_data (GTK_OBJECT (dlg));
	ScorePrefs prefs;

	score_prefs_init (&prefs);
	get_score_prefs (dlg, &prefs);
	score_prefs_changed (granite->score, &prefs);
	score_prefs_free (&prefs);
}

static void
set_sequencer_prefs (PrefsDialog *dlg, const SequencerPrefs *prefs)
{
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dlg->send_mtc), prefs->send_mtc);
}

static void
get_sequencer_prefs (PrefsDialog *dlg, SequencerPrefs *prefs)
{
	prefs->send_mtc = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dlg->send_mtc));
}

static void
apply_sequencer_prefs (PrefsDialog *dlg)
{
	Granite *granite = gtk_object_get_user_data (GTK_OBJECT (dlg));
	SequencerPrefs prefs;

	sequencer_prefs_init (&prefs);
	get_sequencer_prefs (dlg, &prefs);
	sequencer_prefs_changed (&granite->sequencer, &prefs);
	sequencer_prefs_free (&prefs);
}

static void
prefs_dialog_init (PrefsDialog *dlg)
{
	GtkWidget *frame;
	GtkWidget *label;
	GtkWidget *vbox;
	GtkWidget *hbox;

	/* Page 0 */
	frame = gtk_frame_new (_("These settings are common to every project"));
	gtk_widget_show (frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD);

	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_widget_show (hbox);
	label = gtk_label_new (_("Music device:"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
	dlg->device_entry = gtk_entry_new ();
	gtk_signal_connect_object (GTK_OBJECT (dlg->device_entry), "changed",
				   gnome_property_box_changed, GTK_OBJECT (dlg));
	gtk_widget_show (dlg->device_entry);
	gtk_box_pack_start (GTK_BOX (hbox), dlg->device_entry, FALSE, FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, GNOME_PAD);

	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (frame), vbox);

	label = gtk_label_new (_("General"));
	gtk_widget_show (label);
	gnome_property_box_append_page (GNOME_PROPERTY_BOX (dlg), frame, label);

	/* Page 1 */
	frame = gtk_frame_new (_("These settings are saved with each project"));
	gtk_widget_show (frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD);
	label = gtk_label_new (_("Score"));
	gtk_widget_show (label);
	gnome_property_box_append_page (GNOME_PROPERTY_BOX (dlg), frame, label);

	/* Page 2 */
	frame = gtk_frame_new (_("These settings are saved with each project"));
	gtk_widget_show (frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD);

	dlg->send_mtc = gtk_check_button_new_with_label (_("Send MTC"));
	gtk_signal_connect_object (GTK_OBJECT (dlg->send_mtc), "toggled",
				   gnome_property_box_changed, GTK_OBJECT (dlg));
	gtk_widget_show (dlg->send_mtc);
	gtk_box_pack_start (GTK_BOX (vbox), dlg->send_mtc, FALSE, FALSE, GNOME_PAD);

	gtk_widget_show (vbox);
	gtk_container_add (GTK_CONTAINER (frame), vbox);

	label = gtk_label_new (_("Sequencer"));
	gtk_widget_show (label);
	gnome_property_box_append_page (GNOME_PROPERTY_BOX (dlg), frame, label);
}

GtkWidget *
prefs_dialog_new (Granite *granite)
{
	GtkWidget *dlg;

	g_return_val_if_fail (granite != NULL, NULL);

	dlg = GTK_WIDGET (gtk_type_new (TYPE_PREFS_DIALOG));
	gtk_object_set_user_data (GTK_OBJECT (dlg), granite);

	set_granite_prefs (PREFS_DIALOG (dlg), &granite->prefs);
	set_score_prefs (PREFS_DIALOG (dlg), &granite->score->prefs);
	set_sequencer_prefs (PREFS_DIALOG (dlg), &granite->sequencer.prefs);
	gnome_property_box_set_modified (GNOME_PROPERTY_BOX (dlg), FALSE);

	return dlg;
}

static void
finalize (GtkObject *obj)
{
	Granite *granite = gtk_object_get_user_data (obj);

	granite->prefs_dialog = NULL;
	debugf ("prefs dialog finalize");

        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

static void
apply (GnomePropertyBox *dlg, gint page_num)
{
	PrefsDialog *prefs_dlg;
	Granite *granite;

	prefs_dlg = PREFS_DIALOG (dlg);
	granite = gtk_object_get_user_data (GTK_OBJECT (dlg));

	switch (page_num) {
	case 0:
		debugf ("Apply General settings");
		apply_granite_prefs (prefs_dlg);
		break;

	case 1:
		debugf ("Apply Score settings");
		apply_score_prefs (prefs_dlg);
		break;

	case 2:
		debugf ("Apply Sequencer settings");
		apply_sequencer_prefs (prefs_dlg);
		break;

	default:
		debugf ("Apply %d", page_num);
		break;
	}
}
