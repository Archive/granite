/**************************************************************************

    sequencer.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"

ScoreTime
sequencer_string_to_time (Sequencer *sequencer, const gchar *input,
			  ScoreTimeFormat format, gboolean delta)
{
	ScoreMeter sm;
	gint a, b, c;

	g_return_val_if_fail (sequencer != NULL, -1);

	switch (sscanf (input, "%d:%d:%d", &a, &b, &c)) {
	case 1:
		return a;
	case 2:
		c = 0; /* fall through */
	case 3:
		if (!delta) {
			if (a >= 1) a--;
			if (b >= 1) b--;
		}
#if 0
		sequencer_get_meter_at_time (sequencer, &sm, 0); /* FIXME */
#else
		sm.N = 4;
		sm.D = 4;
#endif
		return (a * sm.N + b) * sequencer->timebase + c;
	default:
		return -1;
	}
}

void
sequencer_time_to_string (Sequencer *sequencer, ScoreTime time, gchar *str,
			  ScoreTimeFormat format, gboolean delta)
{
	gint meas, beat, ticks;

	g_return_if_fail (sequencer != NULL);

	sequencer_get_MBT_from_time (sequencer, time, &meas, &beat, &ticks, delta);
	sprintf (str, "%3d:%02d:%03d", meas, beat, ticks);
}

void
sequencer_get_MBT_from_time (Sequencer *sequencer, ScoreTime time,
			     gint *m, gint *b, gint *t,
			     gboolean delta)
{
	ScoreMeter sm;
	gint M, B, T;

	g_return_if_fail (sequencer != NULL);

#if 0
	sequencer_get_meter_at_time (sequencer, &sm, 0); /* FIXME */
#else
		sm.N = 4;
		sm.D = 4;
#endif

	T = time % sequencer->timebase;
	B = time / sequencer->timebase;
	M = B / sm.N;
	B %= sm.N;
	if (!delta) {
		++B;
		++M;
	}
	if (m) *m = M;
	if (b) *b = B;
	if (t) *t = T;
}

void
sequencer_set_score (Sequencer *sequencer, Score *score)
{
	g_return_if_fail (sequencer != NULL);
	g_return_if_fail (score != NULL);

	if (sequencer->playing) {
		g_warning ("Try to set sequencer score while playing");
		return;
	}
	if (sequencer->score)
		score_unref (sequencer->score);
	sequencer->score = score;
	score_ref (sequencer->score);
	debugf ("Sequencer set score to %s", score->name->str);
}

ScoreTime
sequencer_get_ticks_per_measure (Sequencer *sequencer)
{
	ScoreMeter sm;

	g_return_val_if_fail (sequencer != NULL, -1);

#if 0
	sequencer_get_meter_at_time (sequencer, &sm, 0); /* FIXME */
#else
	sm.N = sm.D = 4;
#endif

	return sequencer->timebase * sm.N;
}

void
sequencer_set_load_func (Sequencer *sequencer, SequencerLoadEventsFunc func,
			 ScoreTime offset_time, gpointer user_data)
{
	g_return_if_fail (sequencer != NULL);

	sequencer->active_state.load_func = func;
	sequencer->active_state.offset_time = offset_time;
	sequencer->active_state.func_user_data = user_data;
}

#if 0
ScoreTempo
sequencer_get_tempo_at_time (Sequencer *sequencer, ScoreTime time)
{
	g_return_val_if_fail (sequencer != NULL, 100);

	if (sequencer->active_state.tempo_func)
		return (* sequencer->active_state.tempo_func) (
			time, sequencer->active_state.func_user_data);
	else {
		g_warning ("Sequencer tempo function is NULL, returning 50");
		return 50;
	}
}

void
sequencer_get_meter_at_time (Sequencer *sequencer, ScoreMeter *sm, ScoreTime time)
{
	g_return_if_fail (sequencer != NULL);

	if (sequencer->active_state.meter_func)
		(* sequencer->active_state.meter_func) (
			time, sm, sequencer->active_state.func_user_data);
	else {
		g_warning ("Sequencer meter function is NULL, returning 4/4");
		sm->N = sm->D = 4;
	}
}
#endif

void
sequencer_get_echo_mode (Sequencer *sequencer, SequencerEchoMode *em)
{
	g_return_if_fail (sequencer != NULL);

	*em = sequencer->active_state.echo_mode;
}

void
sequencer_set_echo_mode (Sequencer *sequencer, const SequencerEchoMode *em)
{
	g_return_if_fail (sequencer != NULL);

	sequencer->active_state.echo_mode = *em;
}

void
sequencer_get_metro_mode (Sequencer *sequencer, SequencerMetroMode *em)
{
	g_return_if_fail (sequencer != NULL);

	*em = sequencer->active_state.metro_mode;
}

void
sequencer_set_metro_mode (Sequencer *sequencer, const SequencerMetroMode *em)
{
	g_return_if_fail (sequencer != NULL);

	sequencer->active_state.metro_mode = *em;
}

void
sequencer_get_loop_mode (Sequencer *sequencer, SequencerLoopMode *lm)
{
	g_return_if_fail (sequencer != NULL);

	*lm = sequencer->active_state.loop_mode;
}

void
sequencer_set_loop_mode (Sequencer *sequencer, const SequencerLoopMode *lm)
{
	g_return_if_fail (sequencer != NULL);

	sequencer->active_state.loop_mode = *lm;
}

void
sequencer_get_punch_in_mode (Sequencer *sequencer, SequencerPunchInMode *pm)
{
	g_return_if_fail (sequencer != NULL);

	*pm = sequencer->active_state.punch_in_mode;
}

void
sequencer_set_punch_in_mode (Sequencer *sequencer, const SequencerPunchInMode *pm)
{
	g_return_if_fail (sequencer != NULL);

	sequencer->active_state.punch_in_mode = *pm;
}

/*
 * CORBA servers
 */

static PortableServer_ServantBase__epv base_epv = {
	NULL, NULL, NULL,
};

/*
 * Sequencer CORBA server implementation
 */

static Granite_ScoreTimebase
sequencer_servant_get_timebase (PortableServer_Servant servant, CORBA_Environment *ev)
{
	SequencerServant *ss = (SequencerServant *)servant;

	return sequencer_get_timebase (ss->sequencer);
}

static void
sequencer_servant_get_current_meter (PortableServer_Servant servant, const Granite_ScoreTime time,
				     Granite_ScoreMeter *meter, CORBA_Environment *ev)
{
	SequencerServant *ss = (SequencerServant *)servant;
	ScoreMeter sm;

#if 0
	sequencer_get_meter_at_time (ss->sequencer, &sm, 0); /* FIXME */
#else
	sm.N = sm.D = 4;
#endif
	meter->N = sm.N;
	meter->D = sm.D;
}

static POA_Granite_Sequencer__epv sequencer_epv = {
	NULL,
	sequencer_servant_get_timebase,
	NULL,
	NULL,
	sequencer_servant_get_current_meter,
};
static POA_Granite_Sequencer__vepv sequencer_vepv = {
	&base_epv, &sequencer_epv
};

SequencerServant *
sequencer_servant_new (Sequencer *sequencer)
{
	SequencerServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (SequencerServant, 1);
	servant->servant.vepv = &sequencer_vepv;
	servant->sequencer = sequencer;

	POA_Granite_Sequencer__init ((POA_Granite_Sequencer *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_exception_free (&ev);

	return servant;
}

void
sequencer_servant_free (SequencerServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_Sequencer__fini ((POA_Granite_Sequencer *)servant, &ev);
	CORBA_exception_free (&ev);
	g_free (servant);
}
