/**************************************************************************

    sequencer-gtk-peer.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "sequencer-gtk-peer.h"

#define UPDATE_TIME_INTERVAL	50

static __inline__ SequencerPeer *
get_master_from_peer (SequencerPeer *peer)
{
	if (peer->type == SEQUENCER_PEER_MASTER)
		return peer;
	else
		return SEQUENCER_SLAVE (peer)->master;
}

static void
sequencer_gtk_peer_destroy (SequencerPeer *peer)
{
	g_return_if_fail (peer != NULL);

	if (peer->type == SEQUENCER_PEER_MASTER) {
		if (SEQUENCER_GTK_MASTER (peer)->update_timer)
			g_source_remove (SEQUENCER_GTK_MASTER (peer)->update_timer);
		if (SEQUENCER_GTK_MASTER (peer)->cur_time_adj)
			gtk_object_destroy (GTK_OBJECT (
			SEQUENCER_GTK_MASTER (peer)->cur_time_adj));
	}
}

void
sequencer_gtk_peer_master_construct (SequencerGtkPeer *gtkpeer, Sequencer *sequencer)
{
	g_return_if_fail (gtkpeer != NULL);
	g_return_if_fail (sequencer != NULL);

	sequencer_peer_master_construct (SEQUENCER_PEER (gtkpeer), sequencer);
	SEQUENCER_PEER (gtkpeer)->destroy = sequencer_gtk_peer_destroy;
	SEQUENCER_GTK_MASTER (gtkpeer)->cur_time_adj =
		GTK_ADJUSTMENT (gtk_adjustment_new (
			0.0, 0.0, G_MAXFLOAT, 1.0, 1.0, 1.0));
	SEQUENCER_GTK_MASTER (gtkpeer)->update_timer = 0;
}

void
sequencer_gtk_peer_slave_construct (SequencerGtkPeer *gtkpeer, SequencerGtkPeer *master)
{
	g_return_if_fail (gtkpeer != NULL);
	g_return_if_fail (master != NULL);

	sequencer_peer_slave_construct (SEQUENCER_PEER (gtkpeer), SEQUENCER_PEER (master));
	SEQUENCER_PEER (gtkpeer)->destroy = sequencer_gtk_peer_destroy;
}

static void
adj_set_value (GtkAdjustment *adjustment, gfloat value)
{
	adjustment->value = CLAMP (value, adjustment->lower, adjustment->upper);
	gtk_adjustment_value_changed (adjustment);
}

static void
sequencer_peer_update_current_time (SequencerGtkPeer *gtkpeer)
{
	g_return_if_fail (gtkpeer != NULL);

	adj_set_value (SEQUENCER_GTK_MASTER (gtkpeer)->cur_time_adj,
		       sequencer_get_current_time (SEQUENCER_MASTER (gtkpeer)->sequencer));
}

static gboolean
sequencer_gtk_peer_update_timer (SequencerGtkPeer *gtkpeer)
{
	sequencer_peer_update_current_time (gtkpeer);
	return TRUE;
}

void
sequencer_gtk_peer_play (SequencerGtkPeer *gtkpeer)
{
	if (sequencer_peer_play (SEQUENCER_PEER (gtkpeer)) &&
	    !SEQUENCER_GTK_MASTER (gtkpeer)->update_timer)
		SEQUENCER_GTK_MASTER (gtkpeer)->update_timer = g_timeout_add (
			UPDATE_TIME_INTERVAL,
			(GSourceFunc) sequencer_gtk_peer_update_timer, gtkpeer);
}

void
sequencer_gtk_peer_stop (SequencerGtkPeer *gtkpeer)
{
	sequencer_peer_stop (SEQUENCER_PEER (gtkpeer));
	if (SEQUENCER_GTK_MASTER (gtkpeer)->update_timer) {
		g_source_remove (SEQUENCER_GTK_MASTER (gtkpeer)->update_timer);
		SEQUENCER_GTK_MASTER (gtkpeer)->update_timer = 0;
	}
}

void
sequencer_gtk_peer_goto (SequencerGtkPeer *gtkpeer, ScoreTime time)
{
	if (sequencer_peer_goto (SEQUENCER_PEER (gtkpeer), time))
		sequencer_peer_update_current_time (gtkpeer);
}

GtkAdjustment *
sequencer_gtk_peer_get_time_adjustment (SequencerGtkPeer *gtkpeer)
{
	SequencerPeer *master;

	g_return_val_if_fail (gtkpeer != NULL, NULL);

	master = get_master_from_peer (SEQUENCER_PEER (gtkpeer));

	return SEQUENCER_GTK_MASTER (master)->cur_time_adj;
}
