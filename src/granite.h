/**************************************************************************

    granite.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __GRANITE_H
#define __GRANITE_H

#include "granite-prefs.h"

#define GRANITE_TYPE                        (granite_get_type ())
#define GRANITE(obj)                        (GTK_CHECK_CAST ((obj), GRANITE_TYPE, Granite))
#define GRANITE_CLASS(klass)                (GTK_CHECK_CLASS_CAST ((klass), GRANITE_TYPE, GraniteClass))
#define IS_GRANITE(obj)                     (GTK_CHECK_TYPE ((obj), GRANITE_TYPE))
#define IS_GRANITE_CLASS(klass)             (GTK_CHECK_CLASS_TYPE ((klass), GRANITE_TYPE))

#define GRANITE_CFG			    "/Granite/"

typedef struct _Granite                     Granite;
typedef struct _GraniteClass                GraniteClass;
typedef struct _GraniteAppServant           GraniteAppServant;
typedef struct _GraniteContextServant       GraniteContextServant;

struct _Granite {
	GtkObject parent;

	/* Preferences management */
	GranitePrefs prefs;
	GtkWidget *prefs_dialog;

	/* External interfacing */
	GoadServerList *servers;

	/* Primary data structures */
	Sequencer sequencer;
	Score *score;
	guint modified : 1;
	guint initializing : 1;

	/* User Interface */
	GnomeMDI *mdi;
	GtkObject *score_edit;
	guint update_timer;

	/* Automation */
	GraniteAppServant *app_servant;
	SequencerServant *seq_servant;
	GraniteContextServant *cxt_servant;
};

struct _GraniteClass {
	GtkObjectClass parent_class;

	void	(* prefs_changed)		(Granite *granite,
						 const GranitePrefs *prefs);
	void	(* score_changed)		(Granite *granite,
						 Score *score);
	void	(* score_name_changed)		(Granite *granite,
						 GString *new_name);
};

struct _GraniteAppServant {
	POA_Granite_App servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	Granite *granite;
};

struct _GraniteContextServant {
	POA_Granite_Context servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	ScorePatternInstance *pi;
	ScorePattern *pattern;
	ScoreTrack *track;
	ScoreClip *clip;
};

GtkType		granite_get_type		(void);
Granite *	granite_new			(const gchar *device,
						 const gchar *filename);
void		granite_construct		(Granite *granite,
						 const gchar *filename);
void		granite_prefs_changed		(Granite *granite,
						 const GranitePrefs *prefs);
void		granite_new_score		(Granite *granite);
void		granite_set_score		(Granite *granite,
						 Score *score);
void		granite_set_score_name		(Granite *granite,
						 const gchar *name);
gchar *		granite_filter_time_input	(Granite *granite,
						 gchar *input,
						 ScoreTime *new_time,
						 gboolean delta);

#endif /* __GRANITE_H */
