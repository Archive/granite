/**************************************************************************

    granite-prefs.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "granite-prefs.h"

void
granite_prefs_init (GranitePrefs *prefs)
{
	memset (prefs, 0, sizeof (GranitePrefs));
}

void
granite_prefs_load (GranitePrefs *prefs)
{
	debugf ("get granite prefs");
	gnome_config_push_prefix (GRANITE_CFG "General/");
	prefs->device = gnome_config_get_string ("Device=/dev/music");
	gnome_config_pop_prefix ();
}

void
granite_prefs_save (const GranitePrefs *prefs)
{
	debugf ("save granite prefs");
	gnome_config_push_prefix (GRANITE_CFG "General/");
	gnome_config_set_string ("Device", prefs->device);
	gnome_config_pop_prefix ();
	gnome_config_sync ();
}

void
granite_prefs_free (GranitePrefs *prefs)
{
	g_free (prefs->device);
}
