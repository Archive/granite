/**************************************************************************

    granite.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "ui.h"
#include "granite-xml-io.h"

enum {
	PREFS_CHANGED,
	SCORE_CHANGED,
	SCORE_NAME_CHANGED,
	LAST_SIGNAL
};

static void granite_class_init (GraniteClass *klass);
static void granite_init (Granite *obj);
static void granite_finalize (GtkObject *obj);
static void granite_prefs_changed_real (Granite *granite, const GranitePrefs *prefs);
static void granite_score_changed_real (Granite *granite, Score *score);
static void granite_score_name_changed_real (Granite *granite, GString *new_name);

static GraniteAppServant *granite_app_servant_new (Granite *granite);
static void granite_app_servant_free (GraniteAppServant *servant);

static GraniteContextServant *granite_context_servant_new (void);
static void granite_context_servant_free (GraniteContextServant *servant);

static guint signals[LAST_SIGNAL] = { 0 };

GtkType
granite_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"Granite",
			sizeof (Granite),
			sizeof (GraniteClass),
			(GtkClassInitFunc) granite_class_init,
			(GtkObjectInitFunc) granite_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (gtk_object_get_type (), &type_info);
	}

	return type;
}

static GtkObjectClass *parent_class = NULL;

static void
granite_class_init (GraniteClass *klass)
{
	GtkObjectClass *object_class;

	parent_class = gtk_type_class (granite_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	signals[PREFS_CHANGED] =
		gtk_signal_new ("prefs_changed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GraniteClass, prefs_changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	signals[SCORE_CHANGED] =
		gtk_signal_new ("score_changed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GraniteClass, score_changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	signals[SCORE_NAME_CHANGED] =
		gtk_signal_new ("score_name_changed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GraniteClass, score_name_changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

	object_class->finalize = granite_finalize;

	klass->prefs_changed = granite_prefs_changed_real;
	klass->score_changed = granite_score_changed_real;
	klass->score_name_changed = granite_score_name_changed_real;
}

static void
granite_init (Granite *granite)
{
}

static void
granite_finalize (GtkObject *obj)
{
	Granite *granite;

	granite = GRANITE (obj);
	granite_context_servant_free (granite->cxt_servant);
	sequencer_servant_free (granite->seq_servant);
	granite_app_servant_free (granite->app_servant);
	granite_prefs_free (&granite->prefs);

        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

void
granite_construct (Granite *granite, const gchar *filename)
{
	GtkWidget *mb;
	GString *msg;
	gboolean rv;
	GranitePrefs prefs;

	granite_prefs_init (&granite->prefs);

	granite_prefs_init (&prefs);
	granite->initializing = TRUE;
	granite_prefs_load (&prefs);
	if (granite->prefs.device) {
		g_free (prefs.device);
		prefs.device = granite->prefs.device;
	}
	granite_prefs_changed (granite, &prefs);
	granite_prefs_free (&prefs);
	granite->initializing = FALSE;

	rv = sequencer_construct (&granite->sequencer, granite->prefs.device);
	if (!rv) {
		msg = g_string_new (_("Sequencer initialization failed for "));
		g_string_append (msg, granite->prefs.device);
		mb = gnome_message_box_new (msg->str,
					    GNOME_MESSAGE_BOX_ERROR,
					    GNOME_STOCK_BUTTON_OK,
					    NULL);
		gnome_dialog_run_and_close (GNOME_DIALOG (mb));
		g_string_free (msg, TRUE);
#if 0
		gtk_exit (1);
#endif
	}

	granite->servers = goad_server_list_get ();
	granite->score = NULL;
	if (filename) {
		debugf ("Loading %s", filename);
		granite_xml_load_state (granite, filename);
		granite_set_score_name (granite, filename);
		granite->score->named = TRUE;
		granite->score->modified = FALSE;
	} else
		granite_new_score (granite);
	debugf ("Starting sequencer");
	sequencer_startup (&granite->sequencer);
	granite->app_servant = granite_app_servant_new (granite);
	granite->seq_servant = sequencer_servant_new (&granite->sequencer);
	granite->cxt_servant = granite_context_servant_new ();
	granite_ui_construct (granite);
}

Granite *
granite_new (const gchar *device, const gchar *filename)
{
	Granite *granite;

	granite = GRANITE (gtk_type_new (GRANITE_TYPE));
	granite->prefs.device = g_strdup (device);
	granite_construct (granite, filename);

	return granite;
}

void
granite_prefs_changed (Granite *granite, const GranitePrefs *prefs)
{
	g_return_if_fail (granite != NULL);
	g_return_if_fail (IS_GRANITE (granite));

	if (prefs == NULL)
		return;

	gtk_signal_emit (GTK_OBJECT (granite), signals[PREFS_CHANGED], prefs);
}

gchar *
granite_filter_time_input (Granite *granite, gchar *input,
			   ScoreTime *new_time, gboolean delta)
{
	ScoreTime time;

	g_return_val_if_fail (granite != NULL, NULL);
	g_return_val_if_fail (IS_GRANITE (granite), NULL);
	g_return_val_if_fail (input != NULL, NULL);

	if ((time = sequencer_string_to_time (
		&granite->sequencer, input, SCORE_TIME_DETECT, delta)) == -1) {
		g_free (input);
		return NULL;
	}
	sequencer_time_to_string (&granite->sequencer, time, input, SCORE_TIME_MBT, delta);
	if (new_time)
		*new_time = time;

	return input;
}

static void
granite_prefs_changed_real (Granite *granite, const GranitePrefs *prefs)
{
	GtkWidget *msg;
	GnomeApp *app;
	gboolean need_restart = FALSE;

	g_return_if_fail (granite != NULL);
	g_return_if_fail (IS_GRANITE (granite));

	if (!granite->prefs.device || strcmp (granite->prefs.device, prefs->device)) {
		g_free (granite->prefs.device);
		granite->prefs.device = g_strdup (prefs->device);
		debugf ("device changed to %s", granite->prefs.device);
		need_restart = TRUE;
	}

	if (!granite->initializing && need_restart) {
		app = gnome_mdi_get_active_window (granite->mdi);
		gnome_app_message (app,
				   _("Some changes made will "
				     "require that you restart Granite."));
	}
}

static void
granite_score_changed_real (Granite *granite, Score *score)
{
	g_return_if_fail (granite != NULL);
	g_return_if_fail (IS_GRANITE (granite));

	if (score == granite->score)
		return;

	if (granite->score)
		score_unref (granite->score);
	granite->score = score;
	score_ref (granite->score);
	sequencer_set_score (&granite->sequencer, score);
}

static void
granite_score_name_changed_real (Granite *granite, GString *new_name)
{
	g_return_if_fail (granite != NULL);
	g_return_if_fail (IS_GRANITE (granite));

	g_string_assign (granite->score->name, new_name->str);
}

void
granite_new_score (Granite *granite)
{
	Score *score;

	g_return_if_fail (granite != NULL);
	g_return_if_fail (IS_GRANITE (granite));

	score = score_new ();
	gtk_signal_emit (GTK_OBJECT (granite), signals[SCORE_CHANGED], score);
}

void
granite_set_score (Granite *granite, Score *score)
{
	g_return_if_fail (granite != NULL);
	g_return_if_fail (IS_GRANITE (granite));

	gtk_signal_emit (GTK_OBJECT (granite), signals[SCORE_CHANGED], score);
}

void
granite_set_score_name (Granite *granite, const gchar *name)
{
	GString *s;

	g_return_if_fail (granite != NULL);
	g_return_if_fail (IS_GRANITE (granite));

	s = g_string_new (name);
	gtk_signal_emit (GTK_OBJECT (granite), signals[SCORE_NAME_CHANGED], s);
	g_string_free (s, TRUE);
}

/*
 * CORBA servers
 */

static PortableServer_ServantBase__epv base_epv = {
	NULL, NULL, NULL,
};

/*
 * Granite/App CORBA server implementation
 */

static Granite_Score
granite_app_get_score (PortableServer_Servant pservant, CORBA_Environment *ev)
{
	GraniteAppServant *servant = pservant;

	debugf ("Get score 1");
	debugf ("Get score, name: %s", servant->granite->score->name->str);

	return CORBA_OBJECT_NIL;
}

static Granite_Context
granite_app_get_cxt (PortableServer_Servant pservant, CORBA_Environment *ev)
{
	GraniteAppServant *servant = pservant;

	debugf ("Get context servant");

	return CORBA_Object_duplicate (servant->granite->cxt_servant->obj, ev);
}

static void
granite_app_new (PortableServer_Servant pservant, CORBA_Environment *ev)
{
	GraniteAppServant *servant = pservant;

	granite_new_score (servant->granite);
}

static POA_Granite_App__epv granite_app_epv = {
	NULL,
	granite_app_get_score,
	NULL,
	granite_app_get_cxt,
	granite_app_new,
};
static POA_Granite_App__vepv granite_app_vepv = {
	&base_epv, &granite_app_epv
};

static GraniteAppServant *
granite_app_servant_new (Granite *granite)
{
	GraniteAppServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (GraniteAppServant, 1);
	servant->servant.vepv = &granite_app_vepv;
	servant->granite = granite;

	POA_Granite_App__init ((POA_Granite_App *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	goad_server_register (CORBA_OBJECT_NIL, servant->obj, "granite", "object", &ev);

	CORBA_exception_free (&ev);

	return servant;
}

static void
granite_app_servant_free (GraniteAppServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	goad_server_unregister (CORBA_OBJECT_NIL, "granite", "object", &ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_App__fini ((POA_Granite_App *)servant, &ev);
	CORBA_exception_free (&ev);
	g_free (servant);
}

/*
 * Granite/Context CORBA server implementation
 */

static Granite_ScorePatternInstance
granite_context_get_pattern_instance (PortableServer_Servant pservant, CORBA_Environment *ev)
{
	GraniteContextServant *servant = pservant;

	debugf ("Get pattern inst");

	return CORBA_OBJECT_NIL;
}

static POA_Granite_Context__epv granite_context_epv = {
	NULL,
	granite_context_get_pattern_instance,
};
static POA_Granite_Context__vepv granite_context_vepv = {
	&base_epv, &granite_context_epv
};

static GraniteContextServant *
granite_context_servant_new (void)
{
	GraniteContextServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (GraniteContextServant, 1);
	servant->servant.vepv = &granite_context_vepv;
	servant->pi = NULL;
	servant->pattern = NULL;
	servant->track = NULL;
	servant->clip = NULL;

	POA_Granite_Context__init ((POA_Granite_Context *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_exception_free (&ev);

	return servant;
}

static void
granite_context_servant_free (GraniteContextServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_Context__fini ((POA_Granite_Context *)servant, &ev);
	CORBA_exception_free (&ev);
	g_free (servant);
}
