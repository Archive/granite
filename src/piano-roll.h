/**************************************************************************

    piano-roll.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __PIANO_ROLL_H__
#define __PIANO_ROLL_H__

#include <gnome.h>
#include "common.h"
#include "score.h"
#include "clip-roll.h"

#define TYPE_PIANO_ROLL             (piano_roll_get_type ())
#define PIANO_ROLL(obj)             (GTK_CHECK_CAST ((obj), TYPE_PIANO_ROLL, PianoRoll))
#define PIANO_ROLL_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), TYPE_PIANO_ROLL, PianoRollClass))
#define IS_PIANO_ROLL(obj)          (GTK_CHECK_TYPE ((obj), TYPE_PIANO_ROLL))
#define IS_PIANO_ROLL_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), TYPE_PIANO_ROLL))

typedef struct _PianoRoll            PianoRoll;
typedef struct _PianoRollClass       PianoRollClass;

struct _PianoRoll
{
	ClipRoll parent;
};

struct _PianoRollClass
{
	ClipRollClass parent_class;
};

GtkType			piano_roll_get_type	(void);
GtkWidget *		piano_roll_new 		(ScoreClip *clip,
						 SequencerGtkPeer *gtkmaster);

#endif /* __PIANO_ROLL_H__ */
