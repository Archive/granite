/**************************************************************************

    score-edit.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <gnome.h>
#include <gdk/gdkkeysyms.h>
#include "ui.h"
#include "score-edit.h"
#include "pattern-edit.h"

enum {
	PAT_NAME_CHANGED,
	LAST_SIGNAL
};

static void score_edit_class_init (ScoreEditClass *klass);
static void score_edit_init (ScoreEdit *obj);
static void score_edit_finalize (GtkObject *obj);
static GtkWidget *score_edit_create_view (GnomeMDIChild *child, gpointer user_data);
static void score_edit_sync_score (ScoreEdit *se, Score *score);
static void score_edit_pat_name_changed_real (ScoreEdit *se, ScorePattern *pattern, GString *new_name);

static guint signals[LAST_SIGNAL] = { 0 };

GtkType
score_edit_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"ScoreEdit",
			sizeof (ScoreEdit),
			sizeof (ScoreEditClass),
			(GtkClassInitFunc) score_edit_class_init,
			(GtkObjectInitFunc) score_edit_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GRANITE_TYPE_MDI_CHILD, &type_info);
	}

	return type;
}

static GraniteMDIChildClass *parent_class = NULL;

static void
score_edit_class_init (ScoreEditClass *klass)
{
	GtkObjectClass *object_class;
	GnomeMDIChildClass *child_class;

	parent_class = gtk_type_class (granite_mdi_child_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = score_edit_finalize;
	signals[PAT_NAME_CHANGED] =
		gtk_signal_new ("pat_name_changed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ScoreEditClass, pat_name_changed),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2,
				GTK_TYPE_POINTER,
				GTK_TYPE_POINTER);
	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

	child_class = GNOME_MDI_CHILD_CLASS (klass);
	child_class->create_view = score_edit_create_view;

	klass->pat_name_changed = score_edit_pat_name_changed_real;
}

static void
score_edit_init (ScoreEdit *se)
{
	se->score = NULL;
}

static void
score_edit_finalize (GtkObject *obj)
{
	ScoreEdit *se;

	se = SCORE_EDIT (obj);
	score_unref (se->score);
	
        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

static guint
score_edit_load_score_events (ScoreEvents *se, ScoreTime offset_time,
			      ScoreTime from_time, ScoreTime to_time,
			      ScoreEdit *score_edit)
{
	guint num = 0;

	debugf ("Score load score events: offset time %d, from %d to %d",
		offset_time, from_time, to_time);

	num = score_load_event_range (se, offset_time, from_time,
				      to_time, score_edit->score);

	return num;
}

#if 0
static ScoreTempo
score_edit_tempo_func (ScoreTime time, ScoreEdit *se)
{
	ScoreTempo tempo;

	debugf ("Score edit get tempo func at %d", time);
	tempo = score_get_tempo_at_time (se->score, &time);
	debugf ("Return %d from %d", tempo, time);
	return tempo;
}

static void
score_edit_meter_func (ScoreTime time, ScoreMeter *sm, ScoreEdit *se)
{
	debugf ("Score edit get meter func at %d", time);
	score_get_meter_at_time (se->score, sm, &time);
	debugf ("Return %d/%d from %d", sm->N, sm->D, time);
}
#endif

GnomeMDIChild *
score_edit_new (Granite *granite, Score *score)
{
	GnomeMDIChild *child = GNOME_MDI_CHILD (gtk_type_new (TYPE_SCORE_EDIT));
	GraniteMDIChild *granite_child;

	granite_mdi_child_construct (GRANITE_MDI_CHILD (child), granite);

	SCORE_EDIT (child)->score = score;
	score_ref (score);

	granite_child = GRANITE_MDI_CHILD (child);
	sequencer_gtk_peer_master_construct (&granite_child->gtkpeer,
					     &granite_child->granite->sequencer);
	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.load_func =
		(SequencerLoadEventsFunc) score_edit_load_score_events;
#if 0
	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.tempo_func =
		(SequencerTempoFunc) score_edit_tempo_func;
	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.meter_func =
		(SequencerMeterFunc) score_edit_meter_func;
#endif
	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.func_user_data = child;

	return child;
}

static void
edit_pattern_cmd (GtkWidget *widget, ScoreEdit *se)
{
	Granite *granite;
	Score *score;
	ScorePatternInstance *spi;
	GraniteMDIChild *new_child;
	gint row;

	g_return_if_fail (se != NULL);
	g_return_if_fail (IS_SCORE_EDIT (se));

	row = se->tpclist->focus_row;
	if (row < 0)
		return;

	granite = GRANITE_MDI_CHILD (se)->granite;
	score = granite->score;

	spi = gtk_tpclist_row_get_data (se->tpclist, row);
	if (spi->pattern->user_data)
		return;

	debugf ("Edit pattern: %s", spi->pattern->name->str);
	new_child = GRANITE_MDI_CHILD (pattern_edit_new (granite, spi->pattern));
	spi->pattern->user_data = (gpointer) new_child;
	gnome_mdi_add_child (granite->mdi, GNOME_MDI_CHILD (new_child));
	gnome_mdi_add_view (granite->mdi, GNOME_MDI_CHILD (new_child));
}

static void
insert_pattern_instance (ScoreEdit *se, gint row, ScorePatternInstance *spi)
{
	Sequencer *sequencer;
	gchar s[64];

	sequencer = &GRANITE_MDI_CHILD (se)->granite->sequencer;

	gtk_tpclist_row_insert (se->tpclist, row);
	gtk_tpclist_row_set_data (se->tpclist, row, spi);
	gtk_tpclist_cell_set_value (se->tpclist, row, GSE_PARM_NAME, spi->pattern->name->str);
	sequencer_time_to_string (sequencer, spi->time, s, SCORE_TIME_MBT, FALSE);
	gtk_tpclist_cell_set_value (se->tpclist, row, GSE_PARM_START, s);
	sprintf (s, "%d", spi->repeat);
	gtk_tpclist_cell_set_value (se->tpclist, row, GSE_PARM_REPEAT, s);
	sequencer_time_to_string (sequencer, spi->pattern->duration, s, SCORE_TIME_MBT, TRUE);
	gtk_tpclist_cell_set_value (se->tpclist, row, GSE_PARM_LENGTH, s);
}

static void
insert_pattern_cmd (GtkWidget *widget, ScoreEdit *se)
{
	Score *score;
	ScorePattern *pattern;
	ScorePatternInstance *spi;
	gint row;

	g_return_if_fail (se != NULL);
	g_return_if_fail (IS_SCORE_EDIT (se));

	row = se->tpclist->focus_row;
	if (row < 0)
		row = se->tpclist->nrows;

	score = GRANITE_MDI_CHILD (se)->granite->score;

	pattern = score_pattern_new ();
	spi = score_insert_pattern_instance (score, pattern, 0);
	insert_pattern_instance (se, row, spi);
}

static void
delete_pattern_cmd (GtkWidget *widget, ScoreEdit *se)
{
	Score *score;
	ScorePatternInstance *spi;
	gint row;

	g_return_if_fail (se != NULL);
	g_return_if_fail (IS_SCORE_EDIT (se));

	row = se->tpclist->focus_row;
	if (row < 0)		/* Do not haphazardly delete patterns */
		return;

	score = GRANITE_MDI_CHILD (se)->granite->score;
	spi = gtk_tpclist_row_get_data (se->tpclist, row);
	gtk_tpclist_row_remove (se->tpclist, row);
	score_remove_pattern_instance (score, spi);
}

static void
score_edit_set_name (ScoreEdit *se, const gchar *name)
{
	GString *s;

	s = g_string_new (NULL);
	g_string_sprintf (s, "%s - %s", _("Score Edit"), name);
	gnome_mdi_child_set_name (GNOME_MDI_CHILD (se), s->str);
	g_string_free (s, TRUE);
}

static GnomeUIInfo pattern_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("_Insert"), N_("Insert a new pattern"),
	  insert_pattern_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_MENU_NEW, GDK_Insert, 0 },

	{ GNOME_APP_UI_ITEM, N_("_Delete"), N_("Delete the current pattern"),
	  delete_pattern_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_MENU_TRASH, GDK_d, GDK_SHIFT_MASK },

	{ GNOME_APP_UI_ITEM, N_("_Edit"), N_("Edit the current pattern"),
	  edit_pattern_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_MENU_OPEN, GDK_Return, GDK_SHIFT_MASK },

        GNOMEUIINFO_END
};

static GnomeUIInfo score_edit_menu[] = {
	GNOMEUIINFO_SUBTREE (N_("_Pattern"), pattern_menu),
        GNOMEUIINFO_END
};

static void
activate_cell (GtkTPCList *tpclist, gint row, gint column, gchar **value, ScoreEdit *se)
{
	Granite *granite;
	ScorePatternInstance *spi;
	ScoreTime new_time;
	gchar *s;
	gint new_val;

	granite = GRANITE_MDI_CHILD (se)->granite;

	spi = gtk_tpclist_row_get_data (tpclist, row);
	if (!spi) {
		debugf ("No pattern instance in activate cell");
		return;
	}

	switch (column) {
	case GSE_PARM_NAME:
		score_edit_set_pattern_name (se, spi->pattern, *value);
		debugf ("Change pattern name to %s", *value);
		break;

	case GSE_PARM_START:
		if ((s = granite_filter_time_input (granite, *value, &new_time, FALSE))) {
			*value = s;
			if (spi->time != new_time) {
				score_change_pattern_instance_time (
					granite->score, spi, new_time);
				debugf ("Change pattern start time to %d", new_time);
			}
		}
		break;

	case GSE_PARM_REPEAT:
		sscanf (*value, "%d", &new_val);
		spi->repeat = new_val;
		debugf ("Change pattern repeat to %d", new_val);
		break;

	case GSE_PARM_LENGTH:
		if ((s = granite_filter_time_input (granite, *value, &new_time, TRUE))) {
			*value = s;
			spi->pattern->duration = new_time;
			debugf ("Change pattern length to %d", new_time);
			/* Signal pattern duration change? */
		}
		break;
	}
}

static void
key_press_cell (GtkTPCList *tpclist, gint row, gint column, GdkEventKey *event, ScoreEdit *se)
{
	Sequencer *sequencer;
	ScoreTime new_time;
	const gchar *val;
	gchar s[64];
	gboolean delta;
	gint new_val;

	if (!(column == GSE_PARM_START ||
	      column == GSE_PARM_REPEAT ||
	      column == GSE_PARM_LENGTH))
		return;

	delta = column == GSE_PARM_LENGTH;

	sequencer = &GRANITE_MDI_CHILD (se)->granite->sequencer;
	val = gtk_tpclist_cell_get_value (tpclist, row, column);

	if (column == GSE_PARM_REPEAT) {
		sscanf (val, "%d", &new_val);
		switch (event->keyval) {
		case GDK_KP_Add:
			new_val++;
			sprintf (s, "%d", new_val);
			break;

		case GDK_KP_Subtract:
			if (--new_val < 1)
				new_val = 1;
			sprintf (s, "%d", new_val);
			break;

		default:
			return;
		}
	} else {
		s[0] = 0;
		switch (event->keyval) {
		case GDK_KP_Add:
			if ((new_time = sequencer_string_to_time (
				sequencer, val, SCORE_TIME_DETECT, delta)) == -1)
				return;
			sequencer_time_to_string (sequencer, new_time + 1, s, SCORE_TIME_MBT, delta);
			break;

		case GDK_KP_Subtract:
			if ((new_time = sequencer_string_to_time (
				sequencer, val, SCORE_TIME_DETECT, delta)) < 0)
				return;
			if (--new_time < 0)
				new_time = 0;
			sequencer_time_to_string (sequencer, new_time, s, SCORE_TIME_MBT, delta);
			break;

		default:
			return;
		}
	}

	gtk_tpclist_cell_set_value (tpclist, row, column, s);
}

static void
score_changed (Granite *granite, Score *score, ScoreEdit *se)
{
	score_edit_set_name (se, g_basename (score->name->str));
	score_edit_sync_score (se, score);
}

static void
score_name_changed (Granite *granite, GString *new_name, ScoreEdit *se)
{
	score_edit_set_name (se, g_basename (new_name->str));
}

static GtkWidget *
score_edit_create_view (GnomeMDIChild *child, gpointer user_data)
{
	Granite *granite;
	ScoreEdit *se;
	GtkWidget *contents;
	GtkTPCList *tpclist;
	GtkWidget *sw;
	GtkWidget *frame;
	gint i;

	granite = GRANITE_MDI_CHILD (child)->granite;
	se = SCORE_EDIT (child);

	gtk_signal_connect (GTK_OBJECT (granite), "score_changed",
			    score_changed, se);
	gtk_signal_connect (GTK_OBJECT (granite), "score_name_changed",
			    score_name_changed, se);
	gnome_mdi_child_set_menu_template (child, score_edit_menu);
	
	tpclist = GTK_TPCLIST (gtk_tpclist_new ());
	gtk_tpclist_column_add (tpclist, _("Name"), NULL, NULL);
	gtk_tpclist_column_add (tpclist, _("Start"), NULL, NULL);
	gtk_tpclist_column_add (tpclist, _("Repeat"), NULL, NULL);
	gtk_tpclist_column_add (tpclist, _("Length"), NULL, NULL);
	gtk_tpclist_column_set_width (tpclist, 0, 120);
	for (i = 1; i < 4; ++i)
		gtk_tpclist_column_set_width (tpclist, i, 80);
	gtk_tpclist_row_set_height (tpclist, GNOME_PAD_BIG + GNOME_PAD_SMALL);
	gtk_tpclist_column_set_label_height (tpclist, GNOME_PAD_BIG * 2);
	gtk_widget_show (GTK_WIDGET (tpclist));
	gtk_signal_connect (GTK_OBJECT (tpclist), "activate_cell", activate_cell, se);
	gtk_signal_connect (GTK_OBJECT (tpclist), "key_press_cell", key_press_cell, se);

	sw = gtk_scrolled_window_new (GTK_TPCLIST (tpclist)->hadjustment,
				      GTK_TPCLIST (tpclist)->vadjustment);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (sw), GTK_WIDGET (tpclist));
	gtk_widget_show (sw);

	frame = gtk_frame_new (NULL);
	gtk_widget_show (frame);
	gtk_container_add (GTK_CONTAINER (frame), sw);

	se->tpclist = tpclist;

	score_edit_set_name (se, g_basename (granite->score->name->str));
	score_edit_sync_score (se, granite->score);

	contents = frame;

	return contents;
}

static gint
load_pattern_instance_slot (ScoreTime time, GSList *slist, ScoreEdit *se)
{
	ScorePatternInstance *spi;

	for (; slist; slist = slist->next) {
		spi = slist->data;
		insert_pattern_instance (se, se->tpclist->nrows, spi);
	}
	return 0;
}

static void
score_edit_sync_score (ScoreEdit *se, Score *score)
{
	Granite *granite;

	g_return_if_fail (se != NULL);
	g_return_if_fail (IS_SCORE_EDIT (se));

	granite = GRANITE_MDI_CHILD (se)->granite;
	gtk_tpclist_row_remove_all (se->tpclist);
	g_tree_traverse (score->patterns, (GTraverseFunc) load_pattern_instance_slot,
			 G_IN_ORDER, se);
}

static void
score_edit_pat_name_changed_real (ScoreEdit *se, ScorePattern *sp,
				  GString *new_name)
{
	g_return_if_fail (se != NULL);
	g_return_if_fail (IS_SCORE_EDIT (se));

	g_string_assign (sp->name, new_name->str);
}

void
score_edit_set_pattern_name (ScoreEdit *se, ScorePattern *sp, const gchar *name)
{
	GString *s;

	g_return_if_fail (se != NULL);
	g_return_if_fail (sp != NULL);
	g_return_if_fail (IS_SCORE_EDIT (se));

	s = g_string_new (name);
	gtk_signal_emit (GTK_OBJECT (se), signals[PAT_NAME_CHANGED], sp, s);
	g_string_free (s, TRUE);
}
