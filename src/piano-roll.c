/**************************************************************************

    piano-roll.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include "piano-roll.h"

static void piano_roll_class_init (PianoRollClass *klass);
static void piano_roll_init (PianoRoll *obj);

GtkType
piano_roll_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"PianoRoll",
			sizeof (PianoRoll),
			sizeof (PianoRollClass),
			(GtkClassInitFunc) piano_roll_class_init,
			(GtkObjectInitFunc) piano_roll_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (TYPE_CLIP_ROLL, &type_info);
	}

	return type;
}

static ClipRollClass *parent_class = NULL;

static void
piano_roll_class_init (PianoRollClass *klass)
{
	TimeRollClass *time_roll_class;

	parent_class = gtk_type_class (clip_roll_get_type ());

	time_roll_class = TIME_ROLL_CLASS (klass);
}

static void
piano_roll_init (PianoRoll *piano_roll)
{
}

static void
note_changed (EventRollItem *item, EventRollRect *rect, PianoRoll *piano_roll)
{
	GnomeCanvasRE *re;
	ScoreEvents *sev;
	ScoreEvent *event;
	ScoreTime old_time, delta_time, new_time;

	g_return_if_fail (item != NULL);
	g_return_if_fail (piano_roll != NULL);
	g_return_if_fail (IS_EVENT_ROLL_ITEM (item));
	g_return_if_fail (IS_PIANO_ROLL (piano_roll));

	re = GNOME_CANVAS_RE (item);
	sev = CLIP_ROLL (piano_roll)->clip->u.MIDI.events;
	event = gtk_object_get_user_data (GTK_OBJECT (item));
	old_time = (ScoreTime) EVENT_ROLL_ITEM (item)->user_data;
	delta_time = (ScoreTime) (rect->x1 - re->x1);
	new_time = old_time + delta_time;

	/* Update event data */
	event->u.Note.value = 127 - (gint) rect->y1;
	event->u.Note.duration = (ScoreTime) (rect->x2 - rect->x1 + 0.5);

	/* If the time has changed, we must do a heap reinsertion */
	if (old_time != new_time) {
		debugf ("Note time changed!");
		score_events_change_event_time (sev, event, old_time, new_time);
		EVENT_ROLL_ITEM (item)->user_data = (gpointer) new_time;
	} else
		debugf ("Note time not changed!");
	debugf ("Note changed: old time %d, new time is %d new value: %d new duration: %d",
		old_time, new_time, event->u.Note.value, event->u.Note.duration);
}

static gint
piano_roll_load_score_event (gint slot_time, GSList *slist, PianoRoll *pr)
{
	GnomeCanvas *canvas;
	GnomeCanvasGroup *root;
	GnomeCanvasItem *item;
	double x1, y1, x2, y2;

	g_return_val_if_fail (pr != NULL, FALSE);
	g_return_val_if_fail (IS_PIANO_ROLL (pr), FALSE);

	x1 = slot_time;
	canvas = GNOME_CANVAS (EVENT_ROLL (pr)->canvas);
	root = GNOME_CANVAS_GROUP (gnome_canvas_root (canvas));

	for (; slist; slist = slist->next) {
		ScoreEvent *event = slist->data;

		if (event->type == SCORE_EVENT_NOTE) {
			x2 = x1 + event->u.Note.duration;
			y1 = 127 - (int) event->u.Note.value;
			y2 = y1 + 1;

			debugf ("Loading event: time %d x1: %f x2: %f y1: %f y2: %f v: %d d: %d",
				slot_time, x1, x2, y1, y2, event->u.Note.value, event->u.Note.duration);
			item = gnome_canvas_item_new (root, event_roll_item_get_type (),
						      "x1", x1, "y1", y1,
						      "x2", x2, "y2", y2,
						      "fill_color", "rgb:cc/dd/ff",
						      NULL);
			g_assert (item != NULL);
			gtk_object_set_user_data (GTK_OBJECT (item), event);
			EVENT_ROLL_ITEM (item)->user_data = (gpointer) slot_time;
			gtk_signal_connect (GTK_OBJECT (item), "changed",
					    note_changed, pr);
		}
	}

	return FALSE;
}

static void
piano_roll_set_extents (PianoRoll *pr)
{
	TimeRoll *tr;
	ClipRoll *cr;
	SequencerSlave *slave;
	Sequencer *sequencer;

	g_return_if_fail (pr != NULL);
	g_return_if_fail (IS_PIANO_ROLL (pr));

	tr = TIME_ROLL (pr);
	cr = CLIP_ROLL (pr);

	slave = SEQUENCER_SLAVE (&tr->gtkpeer);
	sequencer = SEQUENCER_MASTER (slave->master)->sequencer;

	time_roll_time_set_span (
		tr, 0, score_events_get_max_time (cr->clip->u.MIDI.events) + 1);
	time_roll_vert_set_span (tr, 0, 128.0);
	time_roll_synchronize (tr);
}

static void
piano_roll_load_clip (PianoRoll *pr)
{
	ScoreClip *clip;

	g_return_if_fail (pr != NULL);
	g_return_if_fail (IS_PIANO_ROLL (pr));

	clip = CLIP_ROLL (pr)->clip;
	g_tree_traverse (clip->u.MIDI.events->heap,
			 (GTraverseFunc) piano_roll_load_score_event,
			 G_IN_ORDER, pr);
	piano_roll_set_extents (pr);
	
}

GtkWidget *
piano_roll_new (ScoreClip *clip, SequencerGtkPeer *gtkmaster)
{
	GtkWidget *widget = GTK_WIDGET (gtk_type_new (TYPE_PIANO_ROLL));
	TimeRoll *tr;
	EventRoll *er;

	tr = TIME_ROLL (widget);
	er = EVENT_ROLL (widget);

	clip_roll_construct (CLIP_ROLL (widget), clip, gtkmaster);
	time_roll_set_vadjustment (tr, GTK_LAYOUT (er->canvas)->vadjustment);
	time_roll_set_graph (tr, GTK_WIDGET (er->canvas), GTK_LAYOUT (er->canvas)->hadjustment);
	time_roll_time_set_ppu (tr, 1.0);
	time_roll_vert_set_ppu (tr, GNOME_PAD_SMALL);
	time_roll_vert_set_snap (tr, 1.0);
	time_roll_set_title_height (tr, GNOME_PAD_BIG * 2);
	piano_roll_load_clip (PIANO_ROLL (widget));

	return widget;
}
