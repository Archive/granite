/**************************************************************************

    sequencer-misc.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SEQUENCER_MISC_H
#define __SEQUENCER_MISC_H

typedef struct _Sequencer	Sequencer;

typedef enum {
	SEQUENCER_FILTER_AFTERTOUCH		= (1 << 0)
} SequencerEventFilterFlags;

typedef struct {
	guint flags;
	gint transpose;
} SequencerEventFilter;

typedef struct {
	guint enabled : 1;
	guint use_port : 1;
	guint use_channel : 1;
	guint use_filter : 1;
	guint8 port;
	guint8 channel;
	SequencerEventFilter filter;
} SequencerEchoMode;

typedef struct {
	guint enabled : 1;
	guint counting_in : 1;	/* Doing count-in */
	gint record_count_in;	/* Measures of count-in */
	ScoreEvent FirstBeat;
	ScoreEvent Beat;
} SequencerMetroMode;

typedef struct {
	guint enabled : 1;
	ScoreTime from_time;
	ScoreTime to_time;
} SequencerLoopMode;

typedef struct {
	guint enabled : 1;
	ScoreTime from_time;
	ScoreTime to_time;
} SequencerPunchInMode;

typedef enum {
	SEQUENCER_THREAD_STOP,
	SEQUENCER_THREAD_START,
	SEQUENCER_THREAD_RUNNING
} SequencerThreadState;

typedef struct {
	pthread_t tid;
	gboolean running;
	SequencerThreadState state;
} SequencerThread;

#endif /* __SEQUENCER_MISC_H */
