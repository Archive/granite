/**************************************************************************

    sequencer-oss.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"

#undef _SEQ_NEEDBUF
#define _SEQ_NEEDBUF(len)				do {	\
	if ((_seqbufptr+(len)) > _seqbuflen)			\
		sequencer_buf_dump (sequencer);			\
} while (0)

#define sequencer_flush(sequencer)			do {	\
	if (ioctl ((sequencer)->fd, SNDCTL_SEQ_SYNC, 0) < 0) {	\
		g_warning ("Sequencer flush failed");		\
	}							\
} while (0)

#define sequencer_buf_dump(s)				do {	\
	pthread_mutex_lock (&(s)->midi_write_lock);		\
	if (_seqbufptr) {					\
		write (sequencer->fd, _seqbuf, _seqbufptr);	\
		_seqbufptr = 0;					\
	}							\
	pthread_mutex_unlock (&(s)->midi_write_lock);		\
} while (0)

#define sequencer_reset_timer_and_go(sequencer)		do {	\
	ioctl ((sequencer)->fd, SNDCTL_TMR_START, 0);		\
} while (0)

#define sequencer_reset_timer_stop(sequencer)		do {	\
	ioctl ((sequencer)->fd, SNDCTL_TMR_START, 0);		\
	ioctl ((sequencer)->fd, SNDCTL_TMR_STOP, 0);		\
} while (0)

#define EVENT_SIZE			8
#define EVENT_QUEUE_SIZE		256

SEQ_DEFINEBUF				(EVENT_SIZE * EVENT_QUEUE_SIZE);

#define SEQ_MSG_REFILL_MIDI_OUT		1
#define SEQ_MSG_LOOP_RESTART		2
#define SEQ_MSG_RECORD_ARM		3
#define SEQ_MSG_RECORD_START		4

#define SEQ_MSG(value)			SEQ_ECHO_BACK (value)

static void flush_notes_off (Sequencer *sequencer);
static void sequencer_midi_reset (SequencerMIDI *midi);

gboolean
sequencer_midi_construct (SequencerMIDI *midi, const char *name)
{
	int i;

	g_return_val_if_fail (midi != NULL, FALSE);
	g_return_val_if_fail (name != NULL, FALSE);

	midi->name = g_string_new (name);
	for (i = 0; i < 16; ++i)
		midi->note_table[i] = g_hash_table_new (g_direct_hash, g_direct_equal);

	return TRUE;
}

static void
sequencer_midi_reset (SequencerMIDI *midi)
{
	int i;

	for (i = 0; i < 16; ++i) {
		if (midi->note_table[i])
			g_hash_table_destroy (midi->note_table[i]);
		midi->note_table[i] = g_hash_table_new (g_direct_hash, g_direct_equal);
	}
}

void
sequencer_midi_deconstruct (SequencerMIDI *midi)
{
	int i;

	g_return_if_fail (midi != NULL);

	g_string_free (midi->name, TRUE);
	for (i = 0; i < 16; ++i)
		g_hash_table_destroy (midi->note_table[i]);
}

gboolean
sequencer_construct (Sequencer *sequencer, const char *music_device)
{
	int rv, i;

	g_return_val_if_fail (sequencer != NULL, FALSE);
	g_return_val_if_fail (music_device != NULL, FALSE);

	sequencer_prefs_init (&sequencer->prefs);
	sequencer_prefs_load (&sequencer->prefs);

	sequencer->playing = FALSE;
	sequencer->recording = FALSE;

	sequencer->timebase = 192;
	sequencer_state_set_defaults (sequencer, &sequencer->default_state);
	sequencer->active_state = sequencer->default_state;
	sequencer->master = NULL;

	sequencer->events_in = NULL;
	sequencer->events_out = score_events_new ();
	sequencer->note_off_out = score_events_new ();

	sequencer->start_time = 0;
	sequencer->real_time = 0;
	sequencer->start_time_changed = FALSE;

	/*
	 * Initialize music device (OSS sequencer level 2)
	 */
	debugf ("Music device: %s", music_device);
	rv = open (music_device, O_RDWR);
	if (rv == -1) {
		g_warning ("Error opening %s", music_device);
		return FALSE;
	}
	sequencer->fd = rv;
	sequencer_set_timebase (sequencer, sequencer->timebase);
	debugf ("Initial timebase set to %d PPQ", sequencer_get_timebase (sequencer));

	/*
	 * Initialize synth devices
	 */
	if (ioctl (sequencer->fd, SNDCTL_SEQ_NRSYNTHS, &sequencer->num_midi_ports) < 0)
		g_error ("NRSYNTHS ioctl failed");
	debugf ("%d midi ports", sequencer->num_midi_ports);
	sequencer->midi_ports = g_new0 (SequencerMIDI, sequencer->num_midi_ports);
	for (i = 0; i < sequencer->num_midi_ports; ++i) {
		struct synth_info si;
		si.device = i;
		if (ioctl (sequencer->fd, SNDCTL_SYNTH_INFO, &si) < 0)
			g_error ("SYNTH INFO ioctl failed for synth %d: %s",
				 i, g_strerror (errno));
		sequencer_midi_construct (&sequencer->midi_ports[i], si.name);
	}

	/*
	 * Synchonization devices
	 */
	pthread_mutex_init (&sequencer->master_mutex, NULL);

	pthread_cond_init (&sequencer->midi_out_refill_cond, NULL);
	pthread_mutex_init (&sequencer->midi_out_refill_lock, NULL);

	pthread_cond_init (&sequencer->midi_out_cond, NULL);
	pthread_mutex_init (&sequencer->midi_out_lock, NULL);

	pthread_cond_init (&sequencer->midi_stop_cond, NULL);
	pthread_mutex_init (&sequencer->midi_stop_lock, NULL);
	pthread_mutex_lock (&sequencer->midi_stop_lock);

	pthread_mutex_init (&sequencer->midi_write_lock, NULL);

	return TRUE;
}

void
sequencer_deconstruct (Sequencer *sequencer)
{
	int i;

	g_return_if_fail (sequencer != NULL);

	pthread_mutex_destroy (&sequencer->midi_write_lock);

	pthread_mutex_destroy (&sequencer->midi_stop_lock);
	pthread_cond_destroy (&sequencer->midi_stop_cond);

	pthread_mutex_destroy (&sequencer->midi_out_lock);
	pthread_cond_destroy (&sequencer->midi_out_cond);

	pthread_mutex_destroy (&sequencer->midi_out_refill_lock);
	pthread_cond_destroy (&sequencer->midi_out_refill_cond);

	pthread_mutex_destroy (&sequencer->master_mutex);

	for (i = 0; i < sequencer->num_midi_ports; ++i)
		sequencer_midi_deconstruct (&sequencer->midi_ports[i]);
	g_free (sequencer->midi_ports);

	sequencer_prefs_free (&sequencer->prefs);

	close (sequencer->fd);
}

ScoreTimebase
sequencer_get_timebase (Sequencer *sequencer)
{
	ScoreTimebase tb = 0;
	ioctl (sequencer->fd, SNDCTL_TMR_TIMEBASE, &tb);
	return tb;
}

void
sequencer_set_timebase (Sequencer *sequencer, ScoreTimebase timebase)
{
	ioctl (sequencer->fd, SNDCTL_TMR_TIMEBASE, &timebase);
	sequencer->timebase = 0;
	ioctl (sequencer->fd, SNDCTL_TMR_TIMEBASE, &sequencer->timebase);
}

static __inline__ void
sequencer_set_tempo (Sequencer *sequencer, ScoreTempo tempo)
{
	g_return_if_fail (sequencer != NULL);

	debugf ("Tempo set to %d", tempo);

	SEQ_SET_TEMPO (tempo);
}

static __inline__ void
sequencer_process_midi_in_event (Sequencer *sequencer, unsigned char event[])
{
	register ScoreEvent *score_event;
	register GHashTable *table;
	register guint8 *vslot;	
	register int fd = sequencer->fd;
	SequencerMIDI *sm;
	gint8 port, channel;
	
#if 0
	debugf ("Got Event: %x %x %x %x %x %x %x %x\n",
		event[0], event[1], event[2], event[3],
		event[4], event[5], event[6], event[7]);
#endif

	/* Event echo */
	if (sequencer->active_state.echo_mode.enabled &&
	    (event[0] == EV_CHN_COMMON || event[0] == EV_CHN_VOICE)) {

		if (sequencer->active_state.echo_mode.use_port)
			event[1] = sequencer->active_state.echo_mode.port;

		if (sequencer->active_state.echo_mode.use_channel)
			event[3] = sequencer->active_state.echo_mode.channel;

		/* Echo the event */
		pthread_mutex_lock (&sequencer->midi_write_lock);
		ioctl (fd, SNDCTL_SEQ_OUTOFBAND, event);
		pthread_mutex_unlock (&sequencer->midi_write_lock);
	}

	/* Event record */
	if (sequencer->playing) {
		switch (event[0]) {
		case EV_CHN_COMMON:
			if (!sequencer->recording)
				break;
			switch (event[2]) {
			case MIDI_CHN_PRESSURE:
				score_event = score_events_new_event (
					sequencer->events_in);
				score_event->port = event[1];
				score_event->channel = event[3];
				score_event->type = SCORE_EVENT_AFTERTOUCH;
				score_event->u.Aftertouch.value = event[4];
				score_events_insert_event (
					sequencer->events_in, score_event,
					sequencer->last_midi_in_time);
				debugf ("Channel aftertouch: chan %d: value: %d",
					event[3], event[4]);
				break;
				
			case MIDI_PGM_CHANGE:
				score_event = score_events_new_event (
					sequencer->events_in);
				score_event->port = event[1];
				score_event->channel = event[3];
				score_event->type = SCORE_EVENT_PROGRAM_CHANGE;
				score_event->u.ProgramChange.value = event[4];
				score_events_insert_event (
					sequencer->events_in, score_event,
					sequencer->last_midi_in_time);
				debugf ("Program change: chan %d: value: %d",
					event[3], event[4]);
				break;
				
			case MIDI_CTL_CHANGE:
				score_event = score_events_new_event (
					sequencer->events_in);
				score_event->port = event[1];
				score_event->channel = event[3];
				score_event->type = SCORE_EVENT_CONTROLLER;
				score_event->u.Controller.number = event[4];
				score_event->u.Controller.value = event[6];
				score_events_insert_event (
					sequencer->events_in, score_event,
					sequencer->last_midi_in_time);
				debugf ("Controller: chan %d: ctrl: %d value: %d",
					event[3], event[4], event[6]);
				break;

			case MIDI_PITCH_BEND:
				score_event = score_events_new_event (
					sequencer->events_in);
				score_event->port = event[1];
				score_event->channel = event[3];
				score_event->type = SCORE_EVENT_PITCH_BEND;
				score_event->u.PitchBend.value = *(short *)&event[6];
				score_events_insert_event (
					sequencer->events_in, score_event,
					sequencer->last_midi_in_time);
				debugf ("Pitch bend: chan %d: value: %d",
					event[3], *(short *)&event[6]);
				break;				
				
			default:
				debugf ("MIDI message 0x%02x p1: 0x%02x p2: 0x%02x, "
					"on port %d, chan %d, value %d",
					event[2], event[4], event[5],
					event[1], event[3], *(short *)&event[6]);
				break;
			}
			break;
			
		case EV_CHN_VOICE:
			if (!sequencer->recording ||
			    sequencer->active_state.metro_mode.counting_in)
				break;

			/* Key Aftertouch */
			if (event[2] == MIDI_KEY_PRESSURE) {
				score_event = score_events_new_event (
					sequencer->events_in);
				score_event->port = event[1];
				score_event->channel = event[3];
				score_event->type = SCORE_EVENT_KEY_AFTERTOUCH;
				score_event->u.KeyAftertouch.note = event[4];
				score_event->u.KeyAftertouch.value = event[5];
				score_events_insert_event (
					sequencer->events_in, score_event,
					sequencer->last_midi_in_time);
				debugf ("Key aftertouch: chan %d: note: %d value: %d",
					event[3], event[4], event[5]);
				break;
			}

			/* Note on/off event */
			channel = event[3];
			port = event[1];
			sm = &sequencer->midi_ports[port];
			table = sm->note_table[channel];
			vslot = &sm->velocity_table[channel][event[4]];

			if (event[2] == MIDI_NOTEON) {
				/* Insert into note on hash table */
				g_hash_table_insert (table, (gpointer) (int) event[4],
						     (gpointer) sequencer->last_midi_in_time);
				*vslot = event[5];
			} else if (event[2] == MIDI_NOTEOFF) {
				gint on_time;
				/* Lookup note on in hash table */
				if (g_hash_table_lookup_extended (table, (gpointer) (int) event[4],
								  NULL, (gpointer) &on_time)) {
					g_hash_table_remove (table, (gpointer) (int) event[4]);
					
					/* Load event into event heap */
					score_event = score_events_new_event (
						sequencer->events_in);
					score_event->port = port;
					score_event->channel = channel;
					score_event->type = SCORE_EVENT_NOTE;
					score_event->u.Note.value = event[4];
					score_event->u.Note.velocity = *vslot;
					score_event->u.Note.duration =
						sequencer->last_midi_in_time - on_time;
					score_events_insert_event (
						sequencer->events_in, score_event,
						on_time);
					debugf ("Note %d created, port %d, chan %d, "
						"velocity %d, start %d, duration %d",
						event[4], event[1], event[3],
						*vslot, on_time, sequencer->last_midi_in_time - on_time);
				}
			}
			break;

		case EV_TIMING:
			if (event[1] == TMR_WAIT_ABS) {
				if (!sequencer->active_state.metro_mode.counting_in)
					sequencer->last_midi_in_time = *(unsigned int *)&event[4] +
						sequencer->real_time;
			} else if (event[1] == TMR_ECHO) {
				switch (*(unsigned int *)&event[4]) {
				case SEQ_MSG_LOOP_RESTART:
					debugf ("MSG: Loop restart");
					sequencer->real_time =
						sequencer->active_state.loop_mode.from_time;
					flush_notes_off (sequencer);
					break;

				case SEQ_MSG_REFILL_MIDI_OUT:
					debugf ("MSG: Refill midi out %d", sequencer->last_midi_in_time);
					pthread_mutex_lock (&sequencer->midi_out_refill_lock);
					pthread_cond_broadcast (&sequencer->midi_out_refill_cond);
					pthread_mutex_unlock (&sequencer->midi_out_refill_lock);
					break;

				case SEQ_MSG_RECORD_ARM:
					sequencer->last_midi_in_time = sequencer->real_time;
					sequencer->recording = TRUE;
					debugf ("MSG: Recording armed");
					break;

				case SEQ_MSG_RECORD_START:
					sequencer->active_state.metro_mode.counting_in = FALSE;
					sequencer->recording = TRUE;
					debugf ("MSG: Record start");
					break;
				}
			}
			break;

		default:
			g_warning ("Unhandled event type: %d", event[0]);
			break;
		}
	}
}

static void *
midi_in_thread (Sequencer *sequencer)
{
	SequencerThread *st = &sequencer->midi_in;
	int fd = sequencer->fd;
	fd_set fds;
	unsigned char event[EVENT_SIZE];

	g_return_val_if_fail (sequencer->fd > 0, NULL);

	st->running = TRUE;
	st->state = SEQUENCER_THREAD_RUNNING;
	FD_ZERO (&fds);	
	while (st->state == SEQUENCER_THREAD_RUNNING) {
		struct timeval tv = { 0, 500000UL };

		FD_SET (fd, &fds);
		select (fd + 1, &fds, NULL, NULL, &tv);
		if (FD_ISSET (fd, &fds)) {
			if (read (fd, event, sizeof (event)) > 0)
				sequencer_process_midi_in_event (sequencer, event);
		}
#ifdef DEBUG
		else {
			if (sequencer->playing) {
				gint inst;
				ioctl (sequencer->fd, SNDCTL_SEQ_GETTIME, &inst);
				debugf ("Input idle: time %d (inst %d)",
					sequencer_get_current_time (sequencer), inst);
			}
		}
#endif
	}

	return NULL;
}

static __inline__ void
output_score_event (Sequencer *sequencer, ScoreEvent *se, gint cur_time)
{
	switch (se->type) {
	case SCORE_EVENT_NOTE:
		if (se->u.Note.velocity) {
			debugf ("Note %d on, length of %d, end time of %d",
				se->u.Note.value, se->u.Note.duration,
				se->u.Note.duration + cur_time);
			SEQ_START_NOTE (se->port, se->channel,
					se->u.Note.value,
					se->u.Note.velocity);
		} else {
			debugf ("Note %d off at %d, removing from note off out",
				se->u.Note.value, cur_time);
			SEQ_STOP_NOTE (se->port, se->channel, se->u.Note.value, 0);
			if (se->data) {
				debugf ("Removing from note off out");
				score_events_remove_event (sequencer->note_off_out,
							   (ScoreEvent *)se->data,
							   cur_time);
			} else {
				debugf ("Note removing, dangling note off");
			}
		}
		break;

	case SCORE_EVENT_AFTERTOUCH:
		SEQ_CHN_PRESSURE (se->port, se->channel, se->u.Aftertouch.value);
		break;

	case SCORE_EVENT_KEY_AFTERTOUCH:
		SEQ_KEY_PRESSURE (se->port, se->channel, se->u.KeyAftertouch.note,
				  se->u.KeyAftertouch.value);
		break;

	case SCORE_EVENT_PROGRAM_CHANGE:
		SEQ_PGM_CHANGE (se->port, se->channel, se->u.ProgramChange.value);
		break;

	case SCORE_EVENT_CONTROLLER:
		SEQ_CONTROL (se->port, se->channel, se->u.Controller.number,
			     se->u.Controller.value);
		break;

	case SCORE_EVENT_PITCH_BEND:
		SEQ_BENDER (se->port, se->channel, se->u.PitchBend.value);
		break;

	case SCORE_EVENT_TEMPO:
		debugf ("Setting tempo to %d at %d", se->u.Tempo.value, cur_time);
		SEQ_SET_TEMPO (se->u.Tempo.value);
		break;

	case SCORE_EVENT_METER:
		debugf ("Setting meter to %d %d at %d",
			se->u.Meter.value.N, se->u.Meter.value.D, cur_time);
		sequencer->current_meter = se->u.Meter.value;
		break;

	case SCORE_EVENT_LOCAL:
		debugf ("Output local event %d at %d", se->u.Local.value, cur_time);
		SEQ_MSG (se->u.Local.value);
		break;

	default:
		g_warning ("Unhandled output event type: %d", se->type);
		break;
	}
}

static gint
flush_note_off_slot (ScoreTime time, GSList *slist, Sequencer *sequencer)
{
	ScoreEvent *event;
	gchar ev[8];

	for (; slist; slist = slist->next) {
		event = slist->data;
		event->data = NULL;
		ev[0] = EV_CHN_VOICE;
		ev[1] = event->port;
		ev[2] = MIDI_NOTEOFF;
		ev[3] = event->channel;
		ev[4] = event->u.Note.value;
		ev[5] = 64;
		ev[6] = 0;
		ev[7] = 0;
		ioctl (sequencer->fd, SNDCTL_SEQ_OUTOFBAND, ev);
		debugf ("Note %d forced off", ev[4]);
	}
	return 0;
}

static void
flush_notes_off (Sequencer *sequencer)
{
	g_tree_traverse (sequencer->note_off_out->heap,
			 (GTraverseFunc) flush_note_off_slot,
			 G_IN_ORDER, sequencer);
	score_events_make_empty (sequencer->note_off_out);
}

static gint
prepare_events (ScoreTime slot_time, GSList *slist, Sequencer *sequencer)
{
	ScoreEvent *event;
	ScoreEvent *note_off_event;

	for (; slist; slist = slist->next) {
		event = slist->data;
		if (event->type == SCORE_EVENT_NOTE) {
			note_off_event = score_events_new_event (sequencer->note_off_out);
			memcpy (note_off_event, event, sizeof (ScoreEvent));
			note_off_event->u.Note.velocity = 0;
			score_events_insert_event (sequencer->note_off_out, note_off_event,
						   slot_time + event->u.Note.duration);
		}
	}
	return 0;
}

typedef struct {
	Sequencer *sequencer;
	ScoreTime max_time;
} LoadNoteOffData;

static gint
load_note_off (ScoreTime slot_time, GSList *slist, LoadNoteOffData *lnod)
{
	ScoreEvent *event, *note_off_event;

	if (slot_time > lnod->max_time)
		return 1;

	for (; slist; slist = slist->next) {
		event = slist->data;
		note_off_event = score_events_new_event (lnod->sequencer->events_out);
		memcpy (note_off_event, event, sizeof (ScoreEvent));
		note_off_event->data = event;
		score_events_insert_event (lnod->sequencer->events_out, note_off_event, slot_time);
	}
	return 0;
}

static __inline__ void
load_metro_events (Sequencer *sequencer, ScoreTime from_time, ScoreTime to_time)
{
	ScoreEvent *event;
	gint ms = (from_time + sequencer->timebase - 1) / sequencer->timebase;
	gint bcount, i;

	bcount = ms % sequencer->current_meter.N;
	for (i = ms * sequencer->timebase; i < to_time; i += sequencer->timebase) {
		event = score_events_new_event (sequencer->events_out);
		if (bcount == 0)
			*event = sequencer->active_state.metro_mode.FirstBeat;
		else
			*event = sequencer->active_state.metro_mode.Beat;
		if (++bcount > sequencer->current_meter.N)
			bcount = 0;
		score_events_insert_event (sequencer->events_out, event, i);
		debugf ("Metro event at %d", i);
	}
}

static __inline__ void
load_midi_out_queue (Sequencer *sequencer, SequencerState *state,
		     ScoreTime from_time, ScoreTime to_time)
{
	LoadNoteOffData lnod;
	gint num;

	/* Load events from score */
	num = (*state->load_func) (
		sequencer->events_out, state->offset_time,
		from_time, to_time - 1,
		state->func_user_data);
	debugf ("Loaded %d events", num);

	/* Prepare events */
	g_tree_traverse (sequencer->events_out->heap,
			 (GTraverseFunc) prepare_events,
			 G_IN_ORDER, sequencer);

	/* Load note off events for chunk */
	lnod.sequencer = sequencer;
	lnod.max_time = to_time - 1;
	g_tree_traverse (sequencer->note_off_out->heap,
			 (GTraverseFunc) load_note_off,
			 G_IN_ORDER, &lnod);

	if (sequencer->active_state.metro_mode.enabled)
		load_metro_events (sequencer, from_time, to_time);
}

#if 0
static __inline__ void
load_and_queue_gsevents (Sequencer *sequencer, ScoreTime time)
{
	ScoreEvent *event;

	sequencer->current_tempo = sequencer_get_tempo_at_time (sequencer, time);
	sequencer_get_meter_at_time (sequencer, &sequencer->current_meter, time);

	debugf ("Queing tempo change to %d at %d", sequencer->current_tempo, time);
#if 0
	event = score_events_new_event (sequencer->events_out);
	event->type = SCORE_EVENT_TEMPO;
	event->u.Tempo.value = sequencer->current_tempo;
	score_events_insert_event (sequencer->events_out, event, time);
#endif
	sequencer_set_tempo (sequencer, sequencer->current_tempo);

	debugf ("Queing meter change to %d/%d at %d",
		sequencer->current_meter.N, sequencer->current_meter.D, time);
#if 0
	event = score_events_new_event (sequencer->events_out);
	event->type = SCORE_EVENT_METER;
	event->u.Meter.value = sequencer->current_meter;
	score_events_insert_event (sequencer->events_out, event, time);
#endif
}
#endif

static gint
output_time_slot_events (ScoreTime slot_time, GSList *slist, Sequencer *sequencer)
{
#if 0
	debugf ("Wait time %d (%d - %d)",
		slot_time - sequencer->start_time,
		slot_time, sequencer->start_time);
#endif
	SEQ_WAIT_TIME (slot_time - sequencer->start_time);
	for (; slist; slist = slist->next)
		output_score_event (sequencer, (ScoreEvent *) slist->data, slot_time);
	return 0;
}

static __inline__ void
sequencer_handle_midi_playback (Sequencer *sequencer)
{
	ScoreEvent *le;
	ScoreTime from_time, to_time;
	ScoreTime chunk_refill;
	ScoreTime chunk_length, chunk_left;
	SequencerState *state;

	state = &sequencer->active_state;
	chunk_length = sequencer->timebase * 2;
	ioctl (sequencer->fd, SNDCTL_SEQ_RESET, 0);

#if 0
	/* These should get set in event preprocessing */
	debugf ("Getting start tempo and meter");
	load_and_queue_gsevents (sequencer, sequencer->start_time);
#endif

	/* This gives us a cleaner start */
	SEQ_START_TIMER ();
	SEQ_WAIT_TIME (sequencer->timebase / 8);

	/* Do a count-in for recording if desired */
	if (state->metro_mode.record_count_in && sequencer->recording) {
		gint i, max, divider, time;

		debugf ("Inserting count-in events");
		max = sequencer->current_meter.N * state->metro_mode.record_count_in;
		state->metro_mode.counting_in = TRUE;
		SEQ_START_TIMER ();
		for (i = 0, divider = 0, time = 0; i < max; ++i, time += sequencer->timebase) {
			SEQ_WAIT_TIME (time);
			if (divider)
				output_score_event (sequencer, &state->metro_mode.Beat, time);
			else
				output_score_event (sequencer, &state->metro_mode.FirstBeat, time);
			if (++divider >= sequencer->current_meter.N)
				divider = 0;
		}
		SEQ_WAIT_TIME (time - sequencer->timebase / 2);
		SEQ_MSG (SEQ_MSG_RECORD_ARM);
		SEQ_WAIT_TIME (time);
		SEQ_START_TIMER ();
		SEQ_MSG (SEQ_MSG_RECORD_START);
	} else {
		state->metro_mode.counting_in = FALSE;
		SEQ_START_TIMER ();
	}

	/* If our start time changes, it is faster to jump here */
time_changed:
	if (sequencer->start_time_changed) {
		debugf ("Start time changed");
		sequencer->start_time_changed = FALSE;
		sequencer->start_time = sequencer->real_time =
			sequencer->new_start_time;
		sequencer_reset_timer_stop (sequencer);
		ioctl (sequencer->fd, SNDCTL_SEQ_RESET, 0);
		flush_notes_off (sequencer);
#if 0
		debugf ("Getting start tempo and meter for start time change");
		load_and_queue_gsevents (sequencer, sequencer->start_time);
#endif
		SEQ_START_TIMER ();
	}
	from_time = sequencer->start_time;
	pthread_mutex_lock (&sequencer->midi_out_refill_lock);
	while (sequencer->playing) {
		if (sequencer->start_time_changed)
			goto time_changed;

		chunk_refill = chunk_length / 2;
		if (state->loop_mode.enabled) {
			chunk_left = chunk_length;
			while (chunk_left > 0) {
				to_time = from_time + chunk_left;
				if (to_time <= state->loop_mode.to_time) {
					/* We are able to queue the entire chunk */
					debugf ("*No loop yet* Loading events from %d to %d",
						from_time, to_time);
					load_midi_out_queue (sequencer, state, from_time, to_time);
					chunk_left -= to_time - from_time;
					g_assert (chunk_left == 0);
					le = score_events_new_event (sequencer->events_out);
					le->type = SCORE_EVENT_LOCAL;
					le->u.Local.value = SEQ_MSG_REFILL_MIDI_OUT;
					score_events_insert_event (sequencer->events_out,
								   le, from_time + chunk_refill);
					from_time = to_time;
				} else {
					/* Queue from here to end of loop */
					to_time = state->loop_mode.to_time;
					debugf ("*LOOP HIT* Loading events from %d to %d",
						from_time, to_time);
					load_midi_out_queue (sequencer, state, from_time, to_time);
					chunk_left -= to_time - from_time;
					if (chunk_refill && chunk_left < chunk_refill) {
						le = score_events_new_event (sequencer->events_out);
						le->type = SCORE_EVENT_LOCAL;
						le->u.Local.value = SEQ_MSG_REFILL_MIDI_OUT;
						score_events_insert_event (sequencer->events_out,
									   le, from_time + chunk_refill);
						chunk_refill = 0;
					}
					g_tree_traverse (sequencer->events_out->heap,
							 (GTraverseFunc) output_time_slot_events,
							 G_IN_ORDER, sequencer);
					score_events_make_empty (sequencer->events_out);

					/* Restart the loop */
					debugf ("Looping, wait until (inst) %d",
						to_time - sequencer->start_time);
					SEQ_WAIT_TIME (to_time - sequencer->start_time);
					SEQ_MSG (SEQ_MSG_LOOP_RESTART);
					from_time = state->loop_mode.from_time;
#if 0
					debugf ("Getting tempo and meter for loop");
					load_and_queue_gsevents (sequencer, from_time);
#endif
					SEQ_START_TIMER ();
					sequencer->start_time = from_time;
				}
			}
		} else {
			to_time = from_time + chunk_length;
			debugf ("Loading events from %d to %d, chunk refill at %d",
				from_time, to_time, from_time + chunk_refill);
			load_midi_out_queue (sequencer, state, from_time, to_time);
			le = score_events_new_event (sequencer->events_out);
			le->type = SCORE_EVENT_LOCAL;
			le->u.Local.value = SEQ_MSG_REFILL_MIDI_OUT;
			score_events_insert_event (sequencer->events_out, le, from_time + chunk_refill);
			from_time = to_time;
		}
		g_tree_traverse (sequencer->events_out->heap,
				 (GTraverseFunc) output_time_slot_events,
				 G_IN_ORDER, sequencer);
		sequencer_buf_dump (sequencer);
		score_events_make_empty (sequencer->events_out);
		debugf ("Block on refill");
		pthread_cond_wait (&sequencer->midi_out_refill_cond, &sequencer->midi_out_refill_lock);
		debugf ("Unblock on refill");
	}
	pthread_mutex_unlock (&sequencer->midi_out_refill_lock);
#if 0
	pthread_mutex_lock (&sequencer->midi_stop_lock);
#endif
	sequencer->last_record_start_time = sequencer->start_time;
	if (!state->metro_mode.counting_in)
		sequencer->real_time = sequencer->start_time =
			sequencer_get_current_time (sequencer);
	sequencer_reset_timer_stop (sequencer);
	ioctl (sequencer->fd, SNDCTL_SEQ_RESET, 0);
	flush_notes_off (sequencer);
#if 0
	pthread_cond_broadcast (&sequencer->midi_stop_cond);
	pthread_mutex_unlock (&sequencer->midi_stop_lock);
#endif
}

static void *
midi_out_thread (Sequencer *sequencer)
{
	SequencerThread *st = &sequencer->midi_out;

	g_return_val_if_fail (sequencer->fd > 0, NULL);

	st->running = TRUE;
	st->state = SEQUENCER_THREAD_RUNNING;
	pthread_mutex_lock (&sequencer->midi_out_lock);
	while (1) {
		pthread_cond_wait (&sequencer->midi_out_cond, &sequencer->midi_out_lock);
		if (st->state == SEQUENCER_THREAD_STOP)
			break;
		if (sequencer->playing == TRUE)
			sequencer_handle_midi_playback (sequencer);
	}
	pthread_mutex_unlock (&sequencer->midi_out_lock);
	return NULL;
}

gboolean
sequencer_startup (Sequencer *sequencer)
{
	g_return_val_if_fail (sequencer != NULL, FALSE);

	if (!sequencer->master)
		sequencer->active_state = sequencer->default_state;

	sequencer->midi_in.running = FALSE;
	sequencer->midi_in.state = SEQUENCER_THREAD_START;
	if (pthread_create (&sequencer->midi_in.tid, NULL,
			    (void*(*)(void*)) midi_in_thread, sequencer)) {
		g_warning ("Couldn't create input thread\n");
		return FALSE;
	}

	sequencer->midi_out.running = FALSE;
	sequencer->midi_out.state = SEQUENCER_THREAD_START;
	if (pthread_create (&sequencer->midi_out.tid, NULL,
			    (void*(*)(void*)) midi_out_thread, sequencer)) {
		g_warning ("Couldn't create output thread\n");
		return FALSE;
	}

	return TRUE;
}

void
sequencer_shutdown (Sequencer *sequencer)
{
	g_return_if_fail (sequencer != NULL);

	debugf ("Sequencer shutting down...");
	sequencer->midi_in.state = SEQUENCER_THREAD_STOP;
	sequencer->midi_out.state = SEQUENCER_THREAD_STOP;
	sequencer->recording = FALSE;
	sequencer->playing = FALSE;
	debugf ("Joining MIDI in thread...");
	pthread_join (sequencer->midi_in.tid, NULL);

	pthread_mutex_lock (&sequencer->midi_out_refill_lock);
	pthread_cond_broadcast (&sequencer->midi_out_refill_cond);
	pthread_mutex_unlock (&sequencer->midi_out_refill_lock);
	pthread_mutex_lock (&sequencer->midi_out_lock);
	pthread_cond_broadcast (&sequencer->midi_out_cond);
	pthread_mutex_unlock (&sequencer->midi_out_lock);

	debugf ("Joining MIDI out thread...");
	pthread_join (sequencer->midi_out.tid, NULL);

	close (sequencer->fd);
}

ScoreTime
sequencer_get_current_time (Sequencer *sequencer)
{
	gint cur_time;

	g_return_val_if_fail (sequencer != NULL, -1);

	if (sequencer->active_state.metro_mode.counting_in)
		return sequencer->start_time;

	if (ioctl (sequencer->fd, SNDCTL_SEQ_GETTIME, &cur_time) < 0)
		g_warning ("Get current time ioctl failed");

	return cur_time + sequencer->real_time;
}

ScoreEvents *
sequencer_get_events_recorded (Sequencer *sequencer)
{
	ScoreEvents *ev;

	g_return_val_if_fail (sequencer != NULL, NULL);

	if (sequencer->playing && sequencer->recording)
		return NULL;

	ev = sequencer->events_in;
	sequencer->events_in = score_events_new ();

	return ev;
}

ScoreTime
sequencer_get_events_recorded_time (Sequencer *sequencer)
{
	g_return_val_if_fail (sequencer != NULL, 0);

	return sequencer->last_record_start_time;
}

void
sequencer_goto (Sequencer *sequencer, ScoreTime time)
{
	g_return_if_fail (sequencer != NULL);

	debugf ("Goto time %d", time);

	if (sequencer->playing) {
		sequencer->new_start_time = time;
		sequencer->start_time_changed = TRUE;
		pthread_cond_broadcast (&sequencer->midi_out_refill_cond);
	} else
		sequencer->real_time = sequencer->start_time = time;
}

void
sequencer_play (Sequencer *sequencer)
{
	g_return_if_fail (sequencer != NULL);

	if (sequencer->playing)
		return;

#if 0
	if (sequencer->start_time == 0 || !sequencer->tempo_set) {
		debugf ("Setting initial meter and tempo");
		sequencer->current_meter = sequencer->active_state.initial_meter;
		sequencer->current_tempo = sequencer->active_state.initial_tempo;
		sequencer_set_tempo (sequencer, sequencer->current_tempo);
		sequencer->tempo_set = TRUE;
	}
#else
	sequencer->current_meter.N = sequencer->current_meter.D = 4;
	sequencer->current_tempo = 100;
#endif

	debugf ("Starting play...");
	sequencer->playing = TRUE;
	pthread_mutex_lock (&sequencer->midi_out_lock);
	pthread_cond_broadcast (&sequencer->midi_out_cond);
	pthread_mutex_unlock (&sequencer->midi_out_lock);
}

void
sequencer_stop (Sequencer *sequencer)
{
	g_return_if_fail (sequencer != NULL);

	if (!sequencer->playing)
		return;

	sequencer->playing = FALSE;
	pthread_cond_broadcast (&sequencer->midi_out_refill_cond);
#if 0
	debugf ("Sleep on stop wait");
	pthread_cond_wait (&sequencer->midi_stop_cond, &sequencer->midi_stop_lock);
	debugf ("WOke up stop wait");
#endif
	sequencer->active_state.metro_mode.counting_in = FALSE;
}

void
sequencer_record (Sequencer *sequencer, gboolean enable)
{
	g_return_if_fail (sequencer != NULL);

	if (enable) {
		if (sequencer->recording)
			return;
		if (sequencer->events_in)
			score_events_make_empty (sequencer->events_in);
		else
			sequencer->events_in = score_events_new ();
		sequencer->recording = TRUE;
	} else {
		if (!sequencer->recording)
			return;
		sequencer->recording = FALSE;
	}
}

void
sequencer_prefs_changed (Sequencer *sequencer, const SequencerPrefs *prefs)
{
	g_return_if_fail (sequencer != NULL);

	if (prefs == NULL)
		return;

	debugf ("Sequencer prefs changed");
	if (sequencer->prefs.send_mtc != prefs->send_mtc) {
		debugf ("Send MTC toggled");
		sequencer->prefs.send_mtc = prefs->send_mtc;
	}
}
