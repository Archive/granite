/**************************************************************************

    score-edit.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SCORE_EDIT_H__
#define __SCORE_EDIT_H__

#include "granite-mdi-child.h"

#define TYPE_SCORE_EDIT             (score_edit_get_type ())
#define SCORE_EDIT(obj)             (GTK_CHECK_CAST ((obj), TYPE_SCORE_EDIT, ScoreEdit))
#define SCORE_EDIT_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), TYPE_SCORE_EDIT, ScoreEditClass))
#define IS_SCORE_EDIT(obj)          (GTK_CHECK_TYPE ((obj), TYPE_SCORE_EDIT))
#define IS_SCORE_EDIT_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), TYPE_SCORE_EDIT))

#define GSE_PARM_NAME			    0
#define GSE_PARM_START			    1
#define GSE_PARM_REPEAT			    2
#define GSE_PARM_LENGTH			    3

typedef struct _ScoreEdit            ScoreEdit;
typedef struct _ScoreEditClass       ScoreEditClass;

struct _ScoreEdit
{
	GraniteMDIChild parent;

	Score *score;

	GtkTPCList *tpclist;
};

struct _ScoreEditClass
{
	GraniteMDIChildClass parent_class;

	void	(* pat_name_changed)		(ScoreEdit *se,
						 ScorePattern *pattern,
						 GString *new_name);
};

GtkType			score_edit_get_type		(void);
GnomeMDIChild *		score_edit_new			(Granite *granite,
							 Score *score);
void			score_edit_set_pattern_name	(ScoreEdit *se,
							 ScorePattern *sp,
							 const gchar *name);

#endif /* __SCORE_EDIT_H__ */
