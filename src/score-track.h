/**************************************************************************

    score-track.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SCORE_TRACK_H
#define __SCORE_TRACK_H

typedef struct _ScoreTrackServant           ScoreTrackServant;

typedef struct {
	GString *name;

	gint port;
	gint channel;

	GTree *clips;		/* ScoreClipInstances */

	gpointer user_data;

	ScoreTrackServant *servant; /* Automation */
} ScoreTrack;

struct _ScoreTrackServant {
	POA_Granite_ScoreTrack servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	ScoreTrack *track;
};

ScoreTrack *	score_track_new			(void);
void		score_track_destroy		(ScoreTrack *track);
void		score_track_get_time_span	(ScoreTrack *track,
						 ScoreTime offset_time,
						 ScoreTime *from_time,
						 ScoreTime *to_time);
guint		score_track_load_event_range	(ScoreEvents *se,
						 ScoreTime offset_time,
						 ScoreTime from_time,
						 ScoreTime to_time,
						 ScoreTrack *Track);
void		score_track_insert_clip		(ScoreTrack *track,
						 ScoreClip *clip,
						 ScoreTime time);
void		score_track_remove_clip		(ScoreTrack *track,
						 ScoreClip *clip,
						 ScoreTime time);
void		score_track_change_clip_time	(ScoreTrack *track,
						 ScoreClip *clip,
						 ScoreTime old_time,
						 ScoreTime new_time);

#endif /* __SCORE_TRACK_H */
