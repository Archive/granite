/**************************************************************************

    selection-hruler.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <math.h>
#include <gtk/gtksignal.h>
#include "selection-hruler.h"

enum {
	SELECTION_CHANGED,
	SELECTION_VALUE_CHANGED,
	LAST_SIGNAL
};

static void selection_hruler_class_init (SelectionHRulerClass *klass);
static void selection_hruler_init (SelectionHRuler *obj);
static void selection_hruler_realize (GtkWidget *widget);
static void selection_hruler_unrealize (GtkWidget *widget);
static gint selection_hruler_expose (GtkWidget *widget, GdkEventExpose *event);
static gint selection_hruler_handle_selection (GtkWidget *widget, GdkEvent *event);

static guint signals[LAST_SIGNAL] = { 0 };

GtkType
selection_hruler_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"SelectionHRuler",
			sizeof (SelectionHRuler),
			sizeof (SelectionHRulerClass),
			(GtkClassInitFunc) selection_hruler_class_init,
			(GtkObjectInitFunc) selection_hruler_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (gtk_hruler_get_type (), &type_info);
	}

	return type;
}

static GtkHRulerClass *parent_class = NULL;

static void
selection_hruler_class_init (SelectionHRulerClass *klass)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	parent_class = gtk_type_class (gtk_hruler_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	signals[SELECTION_CHANGED] =
		gtk_signal_new ("selection_changed",
				GTK_RUN_FIRST | GTK_RUN_NO_RECURSE,
				object_class->type,
				GTK_SIGNAL_OFFSET (SelectionHRulerClass, selection_changed),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);
	signals[SELECTION_VALUE_CHANGED] =
		gtk_signal_new ("selection_value_changed",
				GTK_RUN_FIRST | GTK_RUN_NO_RECURSE,
				object_class->type,
				GTK_SIGNAL_OFFSET (SelectionHRulerClass, selection_value_changed),
				gtk_marshal_NONE__NONE,
				GTK_TYPE_NONE, 0);
	gtk_object_class_add_signals (object_class, signals, LAST_SIGNAL);

	widget_class = GTK_WIDGET_CLASS (klass);
	widget_class->realize = selection_hruler_realize;
	widget_class->unrealize = selection_hruler_unrealize;
	widget_class->expose_event = selection_hruler_expose;
	widget_class->button_press_event =
		(gint (*) (GtkWidget *, GdkEventButton *))
		selection_hruler_handle_selection;
	widget_class->motion_notify_event =
		(gint (*) (GtkWidget *, GdkEventMotion *))
		selection_hruler_handle_selection;
	widget_class->button_release_event =
		(gint (*) (GtkWidget *, GdkEventButton *))
		selection_hruler_handle_selection;
}

static void
selection_hruler_init (SelectionHRuler *selection_hruler)
{
	selection_hruler->snap_amount = 0.0;
	selection_hruler->selection_from = 0.0;
	selection_hruler->selection_to = 0.0;
	gtk_widget_set_events (GTK_WIDGET (selection_hruler),
			       gtk_widget_get_events (GTK_WIDGET (selection_hruler)) |
			       GDK_BUTTON_PRESS_MASK |
			       GDK_BUTTON_RELEASE_MASK);
}

GtkWidget *
selection_hruler_new (void)
{
	return GTK_WIDGET (gtk_type_new (TYPE_SELECTION_HRULER));
}

static void
selection_hruler_draw_selection (SelectionHRuler *mr)
{
	gint sel_from, sel_width;

	sel_from = (SELECTION_HRULER (mr)->selection_from - GTK_RULER (mr)->lower) *
		GTK_WIDGET (mr)->allocation.width /
		(GTK_RULER (mr)->upper - GTK_RULER (mr)->lower);
	sel_width = (SELECTION_HRULER (mr)->selection_to - GTK_RULER (mr)->lower) *
		GTK_WIDGET (mr)->allocation.width /
		(GTK_RULER (mr)->upper - GTK_RULER (mr)->lower) - sel_from;

	if (sel_width > 0)
		gdk_draw_rectangle (GTK_RULER (mr)->backing_store,
				    mr->sel_gc,
				    TRUE,
				    sel_from, 2,
				    sel_width, GTK_WIDGET (mr)->allocation.height - 4);
}

static void
selection_hruler_realize (GtkWidget *widget)
{
	SelectionHRuler *mr;

	if (GTK_WIDGET_CLASS (parent_class)->realize)
		(* GTK_WIDGET_CLASS (parent_class)->realize) (widget);

	mr = SELECTION_HRULER (widget);
	mr->sel_gc = gdk_gc_new (widget->window);
	gdk_gc_set_clip_rectangle (mr->sel_gc, NULL);
	gdk_gc_set_function (mr->sel_gc, GDK_INVERT);
}

static void
selection_hruler_unrealize (GtkWidget *widget)
{
	gdk_gc_destroy (SELECTION_HRULER (widget)->sel_gc);
	if (GTK_WIDGET_CLASS (parent_class)->unrealize)
		(* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static gint
selection_hruler_expose (GtkWidget *widget, GdkEventExpose *event)
{
	GtkRuler *ruler;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_RULER (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	if (GTK_WIDGET_DRAWABLE (widget)) {
		ruler = GTK_RULER (widget);
		gtk_paint_box (widget->style, widget->window,
			       GTK_STATE_NORMAL, GTK_SHADOW_OUT,
			       NULL, widget, "ruler",
			       0, 0, -1, -1);

		gtk_ruler_draw_ticks (ruler);
		selection_hruler_draw_selection (SELECTION_HRULER (ruler));
		gdk_draw_pixmap (widget->window,
				 ruler->non_gr_exp_gc,
				 ruler->backing_store,
				 0, 0, 0, 0,
				 widget->allocation.width,
				 widget->allocation.height);
		gtk_ruler_draw_pos (ruler);
	}

	return FALSE;
}

#define window2position(widget,x)					\
	(GTK_RULER (widget)->lower +					\
	((GTK_RULER (widget)->upper - GTK_RULER (widget)->lower) * x) /	\
	widget->allocation.width)

static gint
selection_hruler_handle_selection (GtkWidget *widget, GdkEvent *event)
{
	static gboolean b1_pressed = FALSE;
	SelectionHRuler *mr;
	GdkCursor *cursor;
	gint x;

	mr = SELECTION_HRULER (widget);

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button == 1) {
			cursor = gdk_cursor_new (GDK_SB_H_DOUBLE_ARROW);
			gdk_pointer_grab (widget->window,
					  FALSE,
					  GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
					  NULL,
					  cursor,
					  event->button.time);
			gdk_cursor_destroy (cursor);
			mr->selection_from = window2position (widget, event->button.x);
			if (mr->snap_amount != 0.0)
				mr->selection_from =
					floor ((mr->selection_from + mr->snap_amount / 2) /
					       mr->snap_amount) * mr->snap_amount;
			mr->selection_to = mr->selection_from;
			gtk_signal_emit (GTK_OBJECT (widget), signals[SELECTION_VALUE_CHANGED]);
			b1_pressed = TRUE;
		}
		break;

	case GDK_MOTION_NOTIFY:
		if (event->motion.is_hint)
			gdk_window_get_pointer (widget->window, &x, NULL, NULL);
		else
			x = event->motion.x;

		if (b1_pressed) {
			GTK_RULER (widget)->position = window2position (widget, x);
			if (mr->snap_amount != 0.0)
				GTK_RULER (widget)->position =
					floor ((GTK_RULER (widget)->position + mr->snap_amount / 2) /
					       mr->snap_amount) * mr->snap_amount;
			mr->selection_to = GTK_RULER (widget)->position;
			gtk_signal_emit (GTK_OBJECT (widget), signals[SELECTION_VALUE_CHANGED]);
			if (GTK_RULER (widget)->backing_store != NULL)
				gtk_ruler_draw_pos (GTK_RULER (widget));
		}
		break;

	case GDK_BUTTON_RELEASE:
		if (b1_pressed) {
			gdk_pointer_ungrab (event->button.time);
			if (mr->selection_to < mr->selection_from) {
				gfloat tmp = mr->selection_from;
				mr->selection_from = mr->selection_to;
				mr->selection_to = tmp;
			}
			gtk_widget_queue_clear (widget);
			gtk_signal_emit (GTK_OBJECT (widget), signals[SELECTION_CHANGED]);
			b1_pressed = FALSE;
		}
		break;

	default:
		break;
	}

	return FALSE;
}
