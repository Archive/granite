/**************************************************************************

    time-roll.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include "time-roll.h"
#include "selection-hruler.h"

#define MEASURE_OFFSET		1

static void time_roll_class_init (TimeRollClass *klass);
static void time_roll_init (TimeRoll *obj);
static void time_roll_finalize (GtkObject *object);

GtkType
time_roll_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"TimeRoll",
			sizeof (TimeRoll),
			sizeof (TimeRollClass),
			(GtkClassInitFunc) time_roll_class_init,
			(GtkObjectInitFunc) time_roll_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GTK_TYPE_VBOX, &type_info);
	}

	return type;
}

static GtkVBoxClass *parent_class = NULL;

static void
time_roll_class_init (TimeRollClass *klass)
{
	GtkObjectClass *object_class;
	TimeRollClass *time_roll_class;

	parent_class = gtk_type_class (gtk_vbox_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = time_roll_finalize;

	time_roll_class = TIME_ROLL_CLASS (klass);
	time_roll_class->set_title_height = NULL;
	time_roll_class->time_set_current = NULL;
	time_roll_class->time_set_span = NULL;
	time_roll_class->time_set_x_scroll = NULL;
	time_roll_class->time_make_viewable = NULL;
	time_roll_class->time_set_ppu = NULL;
	time_roll_class->time_set_division = NULL;
	time_roll_class->time_set_snap = NULL;
	time_roll_class->vert_set_span = NULL;
	time_roll_class->vert_set_ppu = NULL;
	time_roll_class->vert_set_y_scroll = NULL;
	time_roll_class->vert_make_viewable = NULL;
	time_roll_class->vert_set_snap = NULL;
	time_roll_class->synchronize = NULL;
}

static const GtkRulerMetric default_metric = {
	"Measures", "Bars",
	64.0,
	{ 1, 2, 4, 8, 16, 32, 64, 128, 256 },
	{ 1, 1, 1, 1, 1 }
};

static void
time_roll_init (TimeRoll *tr)
{
	tr->metric = default_metric;
	tr->time_ruler = NULL;
	tr->paned = NULL;
	tr->table1 = NULL;
	tr->table2 = NULL;
	tr->left = NULL;
	tr->graph = NULL;
	tr->left_hsb = NULL;
	tr->hsb = NULL;
	tr->vsb = NULL;
}

static void
time_roll_adjustment_changed (GtkAdjustment *adj, TimeRoll *tr)
{
	time_roll_time_set_current (tr, (ScoreTime) adj->value);
}

void
time_roll_construct (TimeRoll *tr, SequencerGtkPeer *gtkmaster)
{
	GtkAdjustment *adj;

	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));
	g_return_if_fail (gtkmaster != NULL);

	sequencer_gtk_peer_slave_construct (&tr->gtkpeer, gtkmaster);

	adj = sequencer_gtk_peer_get_time_adjustment (gtkmaster);
	if (adj)
		gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
				    GTK_SIGNAL_FUNC (time_roll_adjustment_changed),
				    (gpointer) tr);
	else
		g_warning ("No Gtk time adjustment");
}

static void
time_roll_finalize (GtkObject *object)
{
	TimeRoll *tr;
	GtkAdjustment *adj;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_TIME_ROLL (object));

	tr = TIME_ROLL (object);
	adj = SEQUENCER_GTK_MASTER (SEQUENCER_SLAVE (&tr->gtkpeer)->master)->cur_time_adj;
	if (adj)
		gtk_signal_disconnect_by_data (GTK_OBJECT (adj), (gpointer) tr);
	sequencer_peer_deconstruct (SEQUENCER_PEER (&tr->gtkpeer));

	(* GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

void
time_roll_set_vadjustment (TimeRoll *tr, GtkAdjustment *vadj)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->vsb = gtk_vscrollbar_new (vadj);
}

void
time_roll_set_left (TimeRoll *tr, GtkWidget *contents, GtkAdjustment *hadj)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->left = contents;
	if (hadj)
		tr->left_hsb = gtk_hscrollbar_new (hadj);
}

static void
size_allocate_sync (GtkWidget *widget, GtkAllocation *allocation, TimeRoll *tr)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	time_roll_synchronize (tr);
}

static void
paned_realize (GtkWidget *widget, TimeRoll *tr)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	gtk_paned_set_position (GTK_PANED (tr->paned), tr->paned->allocation.width / 2);
	gtk_signal_disconnect_by_func (GTK_OBJECT (widget), paned_realize, tr);
}

void
time_roll_set_graph (TimeRoll *tr, GtkWidget *contents, GtkAdjustment *hadj)
{
	GtkWidget *bin;

	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->table2 = gtk_table_new (4, 2, FALSE);
        gtk_table_set_row_spacings (GTK_TABLE (tr->table2), 0);
	gtk_table_set_col_spacings (GTK_TABLE (tr->table2), 0);
	gtk_widget_show (tr->table2);

	tr->time_ruler = selection_hruler_new ();
	GTK_RULER (tr->time_ruler)->position = 0.0;
	GTK_RULER (tr->time_ruler)->metric = &tr->metric;
        gtk_table_attach (GTK_TABLE (tr->table2), tr->time_ruler,
                          0, 1, 0, 1,
                          GTK_EXPAND | GTK_FILL | GTK_SHRINK,
                          GTK_FILL,
                          0, 0);
	gtk_widget_show (tr->time_ruler);
	gtk_signal_connect (GTK_OBJECT (tr->time_ruler), "size_allocate",
			    size_allocate_sync, tr);

	if (!tr->vsb)
		tr->vsb = gtk_vscrollbar_new (NULL);
	gtk_table_attach (GTK_TABLE (tr->table2), tr->vsb,
			  1, 2, 1, 2,
                          GTK_FILL,
                          GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  0, 0);
	gtk_widget_show (tr->vsb);

	tr->hsb = gtk_hscrollbar_new (hadj);
	gtk_table_attach (GTK_TABLE (tr->table2), tr->hsb,
			  0, 1, 2, 3,
                          GTK_EXPAND | GTK_FILL | GTK_SHRINK,
                          GTK_FILL,
			  0, 0);
	gtk_widget_show (tr->hsb);

	tr->graph = contents;
	gtk_table_attach (GTK_TABLE (tr->table2), tr->graph,
			  0, 1, 1, 2,
                          GTK_EXPAND | GTK_FILL | GTK_SHRINK,
                          GTK_EXPAND | GTK_FILL | GTK_SHRINK,
			  0, 0);
	gtk_signal_connect (GTK_OBJECT (tr->graph), "size_allocate",
			    size_allocate_sync, tr);

	if (tr->left) {
		tr->table1 = gtk_table_new (2, 1, FALSE);
		gtk_table_set_row_spacings (GTK_TABLE (tr->table1), 0);
		gtk_table_set_col_spacings (GTK_TABLE (tr->table1), 0);
		gtk_widget_show (tr->table1);

		gtk_table_attach (GTK_TABLE (tr->table1), tr->left,
				  0, 1, 0, 1,
				  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
				  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
				  0, 0);
		gtk_signal_connect (GTK_OBJECT (tr->left), "size_allocate",
				    size_allocate_sync, tr);

		if (!tr->left_hsb)
			tr->left_hsb = gtk_hscrollbar_new (NULL);
		gtk_table_attach (GTK_TABLE (tr->table1), tr->left_hsb,
				  0, 1, 1, 2,
				  GTK_EXPAND | GTK_FILL | GTK_SHRINK,
				  GTK_FILL,
				  0, 0);
		gtk_widget_show (tr->left_hsb);

		tr->paned = gtk_hpaned_new ();
		gtk_paned_set_gutter_size (GTK_PANED (tr->paned), GNOME_PAD);
		gtk_paned_set_handle_size (GTK_PANED (tr->paned), GNOME_PAD);
		gtk_paned_add1 (GTK_PANED (tr->paned), tr->table1);
		gtk_paned_add2 (GTK_PANED (tr->paned), tr->table2);
		gtk_signal_connect (GTK_OBJECT (tr->paned), "realize",
				    paned_realize, tr);
		gtk_widget_show (tr->paned);

		bin = tr->paned;
	} else
		bin = tr->table2;
	gtk_box_pack_start (GTK_BOX (tr), bin, TRUE, TRUE, 0);
}

ScoreTime
time_roll_get_selection_from (TimeRoll *tr)
{
	g_return_val_if_fail (tr != NULL, -1);
	g_return_val_if_fail (IS_TIME_ROLL (tr), -1);

	return SELECTION_HRULER (tr->time_ruler)->selection_from -
		tr->metric.pixels_per_unit * MEASURE_OFFSET;
}

ScoreTime
time_roll_get_selection_to (TimeRoll *tr)
{
	g_return_val_if_fail (tr != NULL, -1);
	g_return_val_if_fail (IS_TIME_ROLL (tr), -1);

	return SELECTION_HRULER (tr->time_ruler)->selection_to -
		tr->metric.pixels_per_unit * MEASURE_OFFSET;
}

void
time_roll_set_title_height (TimeRoll *tr, gint height)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	gtk_widget_set_usize (tr->time_ruler, 1, height);
	
	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->set_title_height)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->set_title_height)
			(tr, height);
}

void
time_roll_time_set_current (TimeRoll *tr, ScoreTime time)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	if (time < tr->time_lower || time > tr->time_upper)
		return;

	time_roll_time_make_viewable (tr, time);
	GTK_RULER (tr->time_ruler)->position = time +
		tr->metric.pixels_per_unit * MEASURE_OFFSET;
	gtk_ruler_draw_pos (GTK_RULER (tr->time_ruler));

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_current)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_current)
			(tr, time);
}

void
time_roll_time_set_span (TimeRoll *tr, ScoreTime from_time, ScoreTime to_time)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->time_lower = from_time;
	tr->time_upper = to_time;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_span)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_span)
			(tr, from_time, to_time);
}

void
time_roll_time_set_x_scroll (TimeRoll *tr, double offset)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->time_view_start = offset;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_x_scroll)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_x_scroll)
			(tr, offset);
}

void
time_roll_time_make_viewable (TimeRoll *tr, ScoreTime time)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	if (time < tr->time_lower || time > tr->time_upper)
		return;

	if (time < tr->time_view_start || time > tr->time_view_end)
		gtk_adjustment_set_value (GTK_RANGE (tr->hsb)->adjustment,
					  time * tr->time_ppu);

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_make_viewable)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_make_viewable)
			(tr, time);
}

void
time_roll_time_set_ppu (TimeRoll *tr, double amount)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->time_ppu = amount;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_ppu)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_ppu)
			(tr, amount);
}

void
time_roll_time_set_division (TimeRoll *tr, double amount)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->time_division = amount;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_division)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_division)
			(tr, amount);
}

void
time_roll_time_set_snap (TimeRoll *tr, double amount)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->time_snap = amount;
	SELECTION_HRULER (tr->time_ruler)->snap_amount = amount;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_snap)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->time_set_snap)
			(tr, amount);
}

void
time_roll_vert_set_span (TimeRoll *tr, double from_vert, double to_vert)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->vert_lower = from_vert;
	tr->vert_upper = to_vert;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_span)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_span)
			(tr, from_vert, to_vert);
}

void
time_roll_vert_set_ppu (TimeRoll *tr, double amount)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->vert_ppu = amount;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_ppu)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_ppu)
			(tr, amount);
}

void
time_roll_vert_set_y_scroll (TimeRoll *tr, double offset)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->vert_view_start = offset;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_y_scroll)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_y_scroll)
			(tr, offset);
}

void
time_roll_vert_make_viewable (TimeRoll *tr, double position)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_make_viewable)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_make_viewable)
			(tr, position);
}

void
time_roll_vert_set_snap (TimeRoll *tr, double amount)
{
	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	tr->vert_snap = amount;

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_snap)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->vert_set_snap)
			(tr, amount);
}

void
time_roll_synchronize (TimeRoll *tr)
{
	SequencerSlave *slave;
	Sequencer *sequencer;
	ScoreMeter sm;
	ScoreTime timebase;
	gfloat width, height;
	gfloat ruler_lower, ruler_upper;

	g_return_if_fail (tr != NULL);
	g_return_if_fail (IS_TIME_ROLL (tr));

	slave = SEQUENCER_SLAVE (&tr->gtkpeer);
	sequencer = SEQUENCER_MASTER (slave->master)->sequencer;
	width = tr->time_ruler->allocation.width;
	height = tr->time_ruler->allocation.height;
#if 0
	sequencer_get_meter_at_time (sequencer, &sm,
				     (ScoreTime) (GTK_RULER (tr->time_ruler)->position -
						  tr->metric.pixels_per_unit * MEASURE_OFFSET));
#else
	sm.N = sm.D = 4;
#endif
	timebase = sequencer_get_timebase (sequencer);

	tr->duration = tr->time_upper - tr->time_lower;
	ruler_lower = GTK_RANGE (tr->hsb)->adjustment->value / tr->time_ppu;
	ruler_upper = ruler_lower + GTK_RANGE (tr->hsb)->adjustment->page_size / tr->time_ppu;
	tr->time_view_end = ruler_upper;
	tr->time_division = sm.N * timebase;
	tr->metric.pixels_per_unit = tr->time_division;
	if (GTK_RULER (tr->time_ruler)->position < tr->metric.pixels_per_unit * MEASURE_OFFSET)
		GTK_RULER (tr->time_ruler)->position = tr->metric.pixels_per_unit * MEASURE_OFFSET;
	gtk_ruler_set_range (GTK_RULER (tr->time_ruler),
			     ruler_lower + tr->metric.pixels_per_unit * MEASURE_OFFSET,
			     ruler_upper + tr->metric.pixels_per_unit * MEASURE_OFFSET,
			     GTK_RULER (tr->time_ruler)->position,
			     ruler_upper + tr->metric.pixels_per_unit * MEASURE_OFFSET);
	gtk_ruler_draw_pos (GTK_RULER (tr->time_ruler));

	if (TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->synchronize)
		(* TIME_ROLL_CLASS (GTK_OBJECT (tr)->klass)->synchronize)
			(tr);
}
