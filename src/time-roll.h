/**************************************************************************

    time-roll.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __TIME_ROLL_H__
#define __TIME_ROLL_H__

#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "sequencer-gtk-peer.h"

#define TYPE_TIME_ROLL              (time_roll_get_type ())
#define TIME_ROLL(obj)              (GTK_CHECK_CAST ((obj), TYPE_TIME_ROLL, TimeRoll))
#define TIME_ROLL_CLASS(klass)      (GTK_CHECK_CLASS_CAST ((klass), TYPE_TIME_ROLL, TimeRollClass))
#define IS_TIME_ROLL(obj)           (GTK_CHECK_TYPE ((obj), TYPE_TIME_ROLL))
#define IS_TIME_ROLL_CLASS(klass)   (GTK_CHECK_CLASS_TYPE ((klass), TYPE_TIME_ROLL))

typedef struct _TimeRoll             TimeRoll;
typedef struct _TimeRollClass        TimeRollClass;

struct _TimeRoll
{
	GtkVBox parent;

	SequencerGtkPeer gtkpeer;

	/* Primary parameters */
	gfloat time_snap;	/* Snap to closest tick multiple */
	gfloat time_ppu;	/* Pixels per tick */
	ScoreTime time_lower;	/* Max time tick */
	ScoreTime time_upper;	/* Min time tick */
	gfloat time_view_start; /* Start time tick in view */

	gfloat vert_snap;
	gfloat vert_ppu;
	gfloat vert_lower;
	gfloat vert_upper;
	gfloat vert_view_start;

	/* Secondary (calculated) */
	gfloat time_division;	/* Time division (ticks/measure) */
	ScoreTime duration;	/* Number of time ticks in view */
	gfloat time_view_end; /* End time tick in view */
	gfloat vert_view_end;

	GtkRulerMetric metric;
	GtkWidget *time_ruler;
	GtkWidget *paned;
	GtkWidget *table1;
	GtkWidget *table2;
	GtkWidget *left;
	GtkWidget *graph;
	GtkWidget *left_hsb;
	GtkWidget *hsb;
	GtkWidget *vsb;
};

struct _TimeRollClass
{
	GtkVBoxClass parent_class;

	void (* set_title_height)	(TimeRoll *tr,
					 gint height);

	void (* time_set_current)	(TimeRoll *tr,
					 ScoreTime cur_time);
	void (* time_set_span)		(TimeRoll *tr,
					 ScoreTime from_time,
					 ScoreTime to_time);
	void (* time_set_x_scroll)	(TimeRoll *tr,
					 double offset);
	void (* time_make_viewable)	(TimeRoll *tr,
					 ScoreTime time);
	void (* time_set_ppu)		(TimeRoll *tr,
					 double amount);
	void (* time_set_division)	(TimeRoll *tr,
					 double amount);
	void (* time_set_snap)		(TimeRoll *tr,
					 double amount);

	void (* vert_set_span)		(TimeRoll *tr,
					 double from_vert,
					 double to_vert);
	void (* vert_set_ppu)		(TimeRoll *tr,
					 double amount);
	void (* vert_set_y_scroll)	(TimeRoll *tr,
					 double offset);
	void (* vert_make_viewable)	(TimeRoll *tr,
					 double position);
	void (* vert_set_snap)		(TimeRoll *tr,
					 double amount);

	void (* synchronize)		(TimeRoll *tr);
};

GtkType		time_roll_get_type		(void);
void		time_roll_construct		(TimeRoll *tr,
						 SequencerGtkPeer *gtkmaster);
void		time_roll_set_vadjustment	(TimeRoll *tr,
						 GtkAdjustment *vadj);
void		time_roll_set_left		(TimeRoll *tr,
						 GtkWidget *contents,
						 GtkAdjustment *hadj);
void		time_roll_set_graph		(TimeRoll *tr,
						 GtkWidget *contents,
						 GtkAdjustment *hadj);
ScoreTime	time_roll_get_selection_from	(TimeRoll *tr);
ScoreTime	time_roll_get_selection_to	(TimeRoll *tr);
void		time_roll_set_title_height	(TimeRoll *tr,
						 gint height);

void		time_roll_time_set_current	(TimeRoll *tr,
						 ScoreTime time);
void		time_roll_time_set_span		(TimeRoll *tr,
						 ScoreTime from_time,
						 ScoreTime to_time);
void		time_roll_time_set_x_scroll	(TimeRoll *tr,
						 double offset);
void		time_roll_time_make_viewable	(TimeRoll *tr,
						 ScoreTime time);
void		time_roll_time_set_ppu		(TimeRoll *tr,
						 double amount);
void		time_roll_time_set_division	(TimeRoll *tr,
						 double amount);
void		time_roll_time_set_snap		(TimeRoll *tr,
						 double amount);

void		time_roll_vert_set_span		(TimeRoll *tr,
						 double from_vert,
						 double to_vert);
void		time_roll_vert_set_ppu		(TimeRoll *tr,
						 double offset);
void		time_roll_vert_set_y_scroll	(TimeRoll *tr,
						 double offset);
void		time_roll_vert_make_viewable	(TimeRoll *tr,
						 double position);
void		time_roll_vert_set_snap		(TimeRoll *tr,
						 double amount);

void		time_roll_synchronize		(TimeRoll *tr);

#endif /* __TIME_ROLL_H__ */
