/**************************************************************************

    score-xml-io.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "ui.h"
#include "granite-xml-io.h"
#include "score-xml-io.h"

static xmlNodePtr
score_write_tempo (XMLContext *xc, const gchar *name, ScoreTempo tempo)
{
	xmlNodePtr node;
	gchar s[32];

	node = xmlNewDocNode (xc->doc, xc->ns, "Tempo", NULL);
	xmlSetProp (node, "Name", name);
	sprintf (s, "%d", tempo);
	xmlSetProp (node, "Value", s);

	return node;
}

static xmlNodePtr
score_write_meter (XMLContext *xc, const gchar *name, ScoreMeter *meter)
{
	xmlNodePtr node;
	gchar s[32];

	node = xmlNewDocNode (xc->doc, xc->ns, "Meter", NULL);
	xmlSetProp (node, "Name", name);
	sprintf (s, "%d", meter->N);
	xmlSetProp (node, "N", s);
	sprintf (s, "%d", meter->D);
	xmlSetProp (node, "D", s);

	return node;
}

static xmlNodePtr
score_write_score_event (XMLContext *xc, ScoreEvent *event)
{
	xmlNodePtr event_node;
	gchar s[64];

	event_node = xmlNewDocNode (xc->doc, xc->ns, "Event", NULL);

	switch (event->type) {
	case SCORE_EVENT_NOTE:
		xmlSetProp (event_node, "Type", "Note");
		sprintf (s, "%d", event->u.Note.value); xmlSetProp (event_node, "Value", s);
		sprintf (s, "%d", event->u.Note.velocity); xmlSetProp (event_node, "Vel", s);
		sprintf (s, "%d", event->u.Note.duration); xmlSetProp (event_node, "Dur", s);
		break;

	case SCORE_EVENT_AFTERTOUCH:
		xmlSetProp (event_node, "Type", "Aft");
		sprintf (s, "%d", event->u.Aftertouch.value); xmlSetProp (event_node, "Value", s);
		break;

	case SCORE_EVENT_KEY_AFTERTOUCH:
		xmlSetProp (event_node, "Type", "KAft");
		sprintf (s, "%d", event->u.KeyAftertouch.note); xmlSetProp (event_node, "Note", s);
		sprintf (s, "%d", event->u.KeyAftertouch.value); xmlSetProp (event_node, "Value", s);
		break;

	case SCORE_EVENT_PROGRAM_CHANGE:
		xmlSetProp (event_node, "Type", "PrgCh");
		sprintf (s, "%d", event->u.ProgramChange.value); xmlSetProp (event_node, "Value", s);
		break;

	case SCORE_EVENT_CONTROLLER:
		xmlSetProp (event_node, "Type", "Ctrl");
		sprintf (s, "%d", event->u.Controller.number); xmlSetProp (event_node, "Number", s);
		sprintf (s, "%d", event->u.Controller.value); xmlSetProp (event_node, "Value", s);
		break;

	case SCORE_EVENT_PITCH_BEND:
		xmlSetProp (event_node, "Type", "PitchBend");
		sprintf (s, "%d", event->u.PitchBend.value); xmlSetProp (event_node, "Value", s);
		break;

	default:
		g_warning ("Unhandled event type for XML write: %d", event->type);
		break;
	}

	return event_node;
}

static gint
score_write_score_event_slot_cb (ScoreTime slot_time, GSList *slist, XMLContextData *xcd)
{
	ScoreEvent *event;
	xmlNodePtr event_node;
	gchar s[64];

	for (; slist; slist = slist->next) {
		event = slist->data;
		event_node = score_write_score_event (xcd->xc, event);
		sprintf (s, "%d", slot_time); xmlSetProp (event_node, "Time", s);
		xmlAddChild (xcd->data, event_node);
	}

	return 0;
}

static xmlNodePtr
score_write_clip (XMLContext *xc, ScoreClip *clip)
{
	xmlNodePtr clip_node;
	gchar s[64];
	XMLContextData xcd;

	clip_node = xmlNewDocNode (xc->doc, xc->ns, "Clip", NULL);
	xmlSetProp (clip_node, "Name", clip->name->str);
	sprintf (s, "%u", clip->id); xmlSetProp (clip_node, "ID", s);
	if (clip->type == SCORE_CLIP_MIDI) {
		xmlSetProp (clip_node, "Type", "MIDI");
		xcd.xc = xc;
		xcd.data = clip_node;
		g_tree_traverse (clip->u.MIDI.events->heap,
				 (GTraverseFunc) score_write_score_event_slot_cb,
				 G_IN_ORDER, &xcd);
	}

	return clip_node;
}

static gint
score_write_clip_cb (ScoreTime slot_time, GSList *slist, XMLContextData *xcd)
{
	ScoreClip *clip;
	xmlNodePtr clip_node;
	gchar s[64];

	for (; slist; slist = slist->next) {
		clip = slist->data;

		if (!clip->id) {
			clip->id = xcd->xc->clip_id++;
			clip_node = score_write_clip (xcd->xc, clip);
		} else {
			clip_node = xmlNewDocNode (xcd->xc->doc, xcd->xc->ns, "Clip", NULL);
			sprintf (s, "%u", clip->id); xmlSetProp (clip_node, "LinkID", s);
		}
		sprintf (s, "%d", slot_time); xmlSetProp (clip_node, "Time", s);
		xmlAddChild (xcd->data, clip_node);
	}

	return 0;
}

static xmlNodePtr
score_write_track (XMLContext *xc, ScoreTrack *track)
{
	xmlNodePtr track_node;
	gchar s[64];
	XMLContextData xcd;

	track_node = xmlNewDocNode (xc->doc, xc->ns, "Track", NULL);
	xmlSetProp (track_node, "Name", track->name->str);
	sprintf (s, "%u", track->port); xmlSetProp (track_node, "Port", s);
	sprintf (s, "%u", track->channel); xmlSetProp (track_node, "Channel", s);

	xcd.xc = xc;
	xcd.data = track_node;
	g_tree_traverse (track->clips,
			 (GTraverseFunc) score_write_clip_cb,
			 G_IN_ORDER, &xcd);

	return track_node;
}

static xmlNodePtr
score_write_pattern (XMLContext *xc, ScorePattern *pattern)
{
	xmlNodePtr pattern_node, track_node;
	gchar s[64];
	GSList *slist;
	ScoreTrack *track;

	pattern_node = xmlNewDocNode (xc->doc, xc->ns, "Pattern", NULL);
	sprintf (s, "%u", pattern->id); xmlSetProp (pattern_node, "ID", s);
	xmlSetProp (pattern_node, "Name", pattern->name->str);
	sprintf (s, "%u", pattern->duration); xmlSetProp (pattern_node, "Duration", s);

	for (slist = pattern->tracks; slist; slist = slist->next) {
		track = slist->data;
		track_node = score_write_track (xc, track);
		xmlAddChild (pattern_node, track_node);
	}

	return pattern_node;
}

static gint
score_write_pattern_instance_cb (ScoreTime slot_time, GSList *slist, XMLContextData *xcd)
{
	xmlNodePtr pattern_node;
	ScorePatternInstance *pi;
	ScorePattern *pattern;
	gchar s[64];

	for (; slist; slist = slist->next) {
		pi = slist->data;
		pattern = pi->pattern;

		if (!pattern->id) {
			pattern->id = xcd->xc->pattern_id++;
			pattern_node = score_write_pattern (xcd->xc, pattern);
			xmlAddChild (xcd->data, pattern_node);
		}

		g_assert (slot_time == pi->time);

		pattern_node = xmlNewDocNode (xcd->xc->doc, xcd->xc->ns, "PatternInstance", NULL);
		sprintf (s, "%u", pattern->id); xmlSetProp (pattern_node, "LinkID", s);
		sprintf (s, "%d", pi->time); xmlSetProp (pattern_node, "Time", s);
		sprintf (s, "%u", pi->repeat); xmlSetProp (pattern_node, "Repeat", s);
		xmlAddChild (xcd->data, pattern_node);
	}

	return 0;
}

static xmlNodePtr
score_write_prefs (XMLContext *xc, const ScorePrefs *prefs)
{
	xmlNodePtr node;

	node = xmlNewDocNode (xc->doc, xc->ns, "Preferences", NULL);
	debugf ("Write score prefs");

	return node;
}

xmlNodePtr
score_write_xml (XMLContext *xc, Score *score)
{
	xmlNodePtr node;
	xmlNodePtr tmp;
	xmlNodePtr prefs;
	xmlNodePtr patterns;
	XMLContextData xcd;

	node = xmlNewDocNode (xc->doc, xc->ns, "Score", NULL);

	prefs = score_write_prefs (xc, &score->prefs);
	xmlAddChild (node, prefs);

	score_reset_ids (score);
	xc->clip_id = 1;
	xc->pattern_id = 1;
	patterns = xmlNewChild (node, xc->ns, "PatternInstances", NULL);
	xcd.xc = xc;
	xcd.data = patterns;
	g_tree_traverse (score->patterns,
			 (GTraverseFunc) score_write_pattern_instance_cb,
			 G_IN_ORDER, &xcd);

	return node;
}

static void
read_event (XMLContext *xc, xmlNodePtr event_node, ScoreEvents *se)
{
	ScoreEvent *event;
	ScoreTime event_time;
	gchar *type;

	event = score_events_new_event (se);
	type = xmlGetProp (event_node, "Type");
	if (strcmp (type, "Note") == 0) {
		event->type = SCORE_EVENT_NOTE;
		event->u.Note.value = xmlGetInt (event_node, "Value");
		event->u.Note.velocity = xmlGetInt (event_node, "Vel");
		event->u.Note.duration = xmlGetInt (event_node, "Dur");
	} else if (strcmp (type, "Aft") == 0) {
		event->type = SCORE_EVENT_AFTERTOUCH;
		event->u.Aftertouch.value = xmlGetInt (event_node, "Value");
	} else if (strcmp (type, "KAft") == 0) {
		event->type = SCORE_EVENT_KEY_AFTERTOUCH;
		event->u.KeyAftertouch.note = xmlGetInt (event_node, "Note");
		event->u.KeyAftertouch.value = xmlGetInt (event_node, "Value");
	} else if (strcmp (type, "PrgCh") == 0) {
		event->type = SCORE_EVENT_PROGRAM_CHANGE;
		event->u.ProgramChange.value = xmlGetInt (event_node, "Value");
	} else if (strcmp (type, "Ctrl") == 0) {
		event->type = SCORE_EVENT_CONTROLLER;
		event->u.Controller.number = xmlGetInt (event_node, "Number");
		event->u.Controller.value = xmlGetInt (event_node, "Value");
	} else if (strcmp (type, "PitchBend") == 0) {
		event->type = SCORE_EVENT_PITCH_BEND;
		event->u.PitchBend.value = xmlGetInt (event_node, "Value");
	} else {
		g_warning ("Unandled event type of %s", type);
		score_events_free_event (se, event);
		return;
	}
	event_time = xmlGetInt (event_node, "Time");
	score_events_insert_event (se, event, event_time);
}

static ScoreClip *
read_clip (XMLContext *xc, xmlNodePtr clip_node)
{
	ScoreClip *clip;
	xmlNodePtr node;

	if (strcmp (xmlGetProp (clip_node, "Type"), "MIDI") == 0) {
		clip = score_clip_midi_new (score_events_new ());
		g_string_assign (clip->name, xmlGetProp (clip_node, "Name"));
		debugf ("Reading MIDI clip: %s", clip->name->str);
		for (node = clip_node->childs; node; node = node->next) {
			if (strcmp (node->name, "Event") == 0)
				read_event (xc, node, clip->u.MIDI.events);
		}
	} else {
		g_warning ("Unhandled clip type: %s", xmlGetProp (clip_node, "Type"));
		clip = NULL;
	}
	
	return clip;
}

static ScoreTrack *
read_track (XMLContext *xc, xmlNodePtr track_node)
{
	ScoreTrack *track;
	ScoreClip *clip;
	xmlNodePtr node;
	gchar *strid;
	gint id;
	ScoreTime clip_time;

	track = score_track_new ();
	g_string_assign (track->name, xmlGetProp (track_node, "Name"));
	track->port = xmlGetInt (track_node, "Port");
	track->channel = xmlGetInt (track_node, "Channel");
	debugf ("Reading track: %s, port %d chn %d",
		track->name->str, track->port, track->channel);
	for (node = track_node->childs; node; node = node->next) {
		if (strcmp (node->name, "Clip") == 0) {
			strid = xmlGetProp (node, "ID");
			clip_time = xmlGetInt (node, "Time");
			if (strid) {
				id = atoi (strid);
				debugf ("Reading clip definition %d", id);
				clip = read_clip (xc, node);
				if (!clip)
					continue;
				g_hash_table_insert (xc->clip_ht, GUINT_TO_POINTER (id), clip);
			} else {
				id = xmlGetInt (node, "LinkID");
				debugf ("Reading clip link %d", id);
				clip = g_hash_table_lookup (xc->clip_ht, GUINT_TO_POINTER (id));
				if (!clip) {
					g_warning ("Clip link ID %d referenced before definition", id);
					continue;
				}
			}
			score_track_insert_clip (track, clip, clip_time);
		}
	}

	return track;
}

static ScorePattern *
read_pattern (XMLContext *xc, xmlNodePtr p_node)
{
	ScorePattern *pattern;
	ScoreTrack *track;
	xmlNodePtr node;
	gint i = 0;

	pattern = score_pattern_new ();
	g_string_assign (pattern->name, xmlGetProp (p_node, "Name"));
	pattern->duration = xmlGetInt (p_node, "Duration");
	debugf ("Reading Pattern %s", pattern->name->str);
	for (node = p_node->childs; node; node = node->next) {
		if (strcmp (node->name, "Track") == 0) {
			track = read_track (xc, node);
			score_pattern_insert_track (pattern, track, i++);
		}
	}
	debugf ("%d tracks loaded from %s", i, pattern->name->str);
	
	return pattern;
}

static void
read_pattern_instances (XMLContext *xc, Score *score, xmlNodePtr pi_node)
{
	ScorePattern *pattern;
	ScorePatternInstance *spi;
	xmlNodePtr node;
	guint id;
	ScoreTime pi_time;

	for (node = pi_node->childs; node; node = node->next) {
		if (strcmp (node->name, "Pattern") == 0) {
			id = xmlGetInt (node, "ID");
			debugf ("Pattern, ID %d", id);
			pattern = read_pattern (xc, node);
			g_hash_table_insert (xc->pattern_ht, GUINT_TO_POINTER (id), pattern);
		} else if (strcmp (node->name, "PatternInstance") == 0) {
			id = xmlGetInt (node, "LinkID");
			pi_time = xmlGetInt (node, "Time");
			pattern = g_hash_table_lookup (xc->pattern_ht, GUINT_TO_POINTER (id));
			if (!pattern) {
				g_warning ("PatternInstance ID %d referenced before definition", id);
				continue;
			}
			spi = score_insert_pattern_instance (score, pattern, pi_time);
			spi->time = pi_time;
			spi->repeat = xmlGetInt (node, "Repeat");
			debugf ("PatternInstance, LinkID %d, Time %d Repeat %d",
				id, pi_time, spi->repeat);
		}
	}
}

static void
score_read_prefs (XMLContext *xc, ScorePrefs *prefs)
{
	xmlNodePtr tree;
	xmlNodePtr node;

	tree = xc->doc->root;
	node = xmlSearchChild (tree, "Preferences");
	debugf ("Read score prefs");
	if (!node) {
		debugf ("Prefs node not found");
		return;
	}
}

Score *
score_read_xml (XMLContext *xc)
{
	Score *score;
	ScorePrefs score_prefs;
	xmlNodePtr tree;
	xmlNodePtr score_node;
	xmlNodePtr pi_node;

	tree = xc->doc->root;
	score_node = xmlSearchChild (tree, "Score");
	if (!score_node) {
		g_warning ("Couldn't find Score node");
		return NULL;
	}

	score = score_new ();
	score_prefs_init (&score_prefs);
	score_read_prefs (xc, &score->prefs);
	pi_node = xmlSearchChild (score_node, "PatternInstances");
	g_assert (pi_node != NULL);

	xc->clip_ht = g_hash_table_new (g_direct_hash, g_direct_equal);
	xc->pattern_ht = g_hash_table_new (g_direct_hash, g_direct_equal);
	read_pattern_instances (xc, score, pi_node);
	g_hash_table_destroy (xc->clip_ht);
	g_hash_table_destroy (xc->pattern_ht);

	score_prefs_changed (score, &score_prefs);
	score_prefs_free (&score_prefs);

	return score;
}
