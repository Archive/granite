/**************************************************************************

    granite-mdi-child.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include "granite-mdi-child.h"

static void granite_mdi_child_class_init (GraniteMDIChildClass *klass);
static void granite_mdi_child_init (GraniteMDIChild *obj);
static void granite_mdi_child_finalize (GtkObject *obj);

GtkType
granite_mdi_child_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"GraniteMDIChild",
			sizeof (GraniteMDIChild),
			sizeof (GraniteMDIChildClass),
			(GtkClassInitFunc) granite_mdi_child_class_init,
			(GtkObjectInitFunc) granite_mdi_child_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (gnome_mdi_child_get_type (), &type_info);
	}

	return type;
}

static GnomeMDIChildClass *parent_class = NULL;

static void
granite_mdi_child_class_init (GraniteMDIChildClass *klass)
{
	GtkObjectClass *object_class;

	parent_class = gtk_type_class (gnome_mdi_child_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = granite_mdi_child_finalize;
}

static void
granite_mdi_child_init (GraniteMDIChild *mdi_child)
{
	mdi_child->granite = NULL;
	SEQUENCER_PEER (&mdi_child->gtkpeer)->type = SEQUENCER_PEER_UNKNOWN;
}

static void
granite_mdi_child_finalize (GtkObject *obj)
{
	GraniteMDIChild *gm;

	gm = GRANITE_MDI_CHILD (obj);
	sequencer_peer_deconstruct (SEQUENCER_PEER (&gm->gtkpeer));

        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

void
granite_mdi_child_construct (GraniteMDIChild *child, Granite *granite)
{
	debugf ("GraniteMDI child constructing");
	child->granite = granite;
}
