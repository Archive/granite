/**************************************************************************

    event-roll.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __EVENT_ROLL_H__
#define __EVENT_ROLL_H__

#include "time-roll.h"
#include "event-roll-item.h"

#define TYPE_EVENT_ROLL             (event_roll_get_type ())
#define EVENT_ROLL(obj)             (GTK_CHECK_CAST ((obj), TYPE_EVENT_ROLL, EventRoll))
#define EVENT_ROLL_CLASS(klass)     (GTK_CHECK_CLASS_CAST ((klass), TYPE_EVENT_ROLL, EventRollClass))
#define IS_EVENT_ROLL(obj)          (GTK_CHECK_TYPE ((obj), TYPE_EVENT_ROLL))
#define IS_EVENT_ROLL_CLASS(klass)  (GTK_CHECK_CLASS_TYPE ((klass), TYPE_EVENT_ROLL))

typedef struct _EventRoll            EventRoll;
typedef struct _EventRollClass       EventRollClass;

struct _EventRoll
{
	TimeRoll parent;

	double band_sel_start_x, band_sel_start_y;
	GnomeCanvasItem *band_sel_item;

	GnomeCanvas *canvas;
	GSList *selected_items;
};

struct _EventRollClass
{
	TimeRollClass parent_class;
};

GtkType			event_roll_get_type		(void);
void			event_roll_construct		(EventRoll *event_roll,
							 SequencerGtkPeer *gtkmaster);
void			event_roll_shift_bottom		(EventRoll *er,
							 gint row,
							 gint amt);
void			event_roll_row_remove		(EventRoll *event_roll,
							 gint row);

void			event_roll_selection_clear	(EventRoll *event_roll);
void			event_roll_selection_add	(EventRoll *event_roll,
							 EventRollItem *item);
void			event_roll_selection_remove	(EventRoll *event_roll,
							 EventRollItem *item);
gboolean		event_roll_selection_empty	(EventRoll *event_roll);
void			event_roll_selection_foreach	(EventRoll *event_roll,
							 GFunc func,
							 gpointer user_data);
void			event_roll_replace_item		(EventRoll *event_roll,
							 EventRollItem *old_item,
							 EventRollItem *new_item);

#endif /* __EVENT_ROLL_H__ */
