/**************************************************************************

    pattern-edit.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __PATTERN_EDIT_H__
#define __PATTERN_EDIT_H__

#include "granite-mdi-child.h"

#define TYPE_PATTERN_EDIT                   (pattern_edit_get_type ())
#define PATTERN_EDIT(obj)                   (GTK_CHECK_CAST ((obj), TYPE_PATTERN_EDIT, PatternEdit))
#define PATTERN_EDIT_CLASS(klass)           (GTK_CHECK_CLASS_CAST ((klass), TYPE_PATTERN_EDIT, PatternEditClass))
#define IS_PATTERN_EDIT(obj)                (GTK_CHECK_TYPE ((obj), TYPE_PATTERN_EDIT))
#define IS_PATTERN_EDIT_CLASS(klass)        (GTK_CHECK_CLASS_TYPE ((klass), TYPE_PATTERN_EDIT))

typedef struct _PatternEdit                 PatternEdit;
typedef struct _PatternEditClass            PatternEditClass;

struct _PatternEdit
{
	GraniteMDIChild parent;

	ScorePattern *pattern;

	GtkWidget *dock;
	GnomeDockLayout *layout;
	GtkWidget *pr;

	GtkWidget *from_loop;
	GtkWidget *to_loop;
};

struct _PatternEditClass
{
	GraniteMDIChildClass parent_class;

	GSList *midi_clip_filters;
	GnomeUIInfo *modify_menu; /* Dynamically generated */
};

GtkType			pattern_edit_get_type	(void);
GnomeMDIChild *		pattern_edit_new 	(Granite *granite,
						 ScorePattern *pattern);

#endif /* __PATTERN_EDIT_H__ */
