/**************************************************************************

    score.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SCORE_H
#define __SCORE_H

typedef gint	ScoreTimebase;
typedef gint	ScoreTime;
typedef gint	ScoreTempo;

typedef enum {
	SCORE_TIME_DETECT,
	SCORE_TIME_MBT
} ScoreTimeFormat;

typedef struct {
	gushort N;
	gushort D;
} ScoreMeter;

typedef enum {
	SCORE_MARK_START,
	SCORE_MARK_END
} ScoreMarkType;

typedef struct {
	ScoreMarkType type;

	gpointer data;
} ScoreMark;

typedef struct _Score		Score;

#include "score-event.h"
#include "score-clip.h"
#include "score-track.h"
#include "score-pattern.h"
#include "score-prefs.h"

typedef struct _ScoreServant                ScoreServant;

struct _Score {
	int refs;

	GString *name;

	ScorePrefs prefs;

	guint named : 1;
	guint modified : 1;

#if 0
	ScoreEvents *gsevents;	/* Global singular events (tempo, meter) */
#endif
	GTree *patterns;	/* Heap of ScorePatternInstances */

	ScoreServant *servant;	/* Automation */
};

struct _ScoreServant {
	POA_Granite_Score servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	Score *score;
};

Score *		score_new			(void);
void		score_ref			(Score *score);
void		score_unref			(Score *score);

void		score_prefs_changed		(Score *score,
						 const ScorePrefs *prefs);
void		score_reset_ids			(Score *score);
void		score_get_time_span		(Score *score,
						 ScoreTime offset_time,
						 ScoreTime *from_time,
						 ScoreTime *to_time);
guint		score_load_event_range		(ScoreEvents *se,
						 ScoreTime offset_time,
						 ScoreTime from_time,
						 ScoreTime to_time,
						 Score *score);

ScoreTempo	score_get_tempo_at_time		(Score *score,
						 ScoreTime *time);
void		score_get_meter_at_time		(Score *score,
						 ScoreMeter *sm,
						 ScoreTime *time);

ScorePatternInstance *score_insert_pattern_instance	(Score *score,
							 ScorePattern *pattern,
							 ScoreTime time);
void		score_remove_pattern_instance		(Score *score,
							 ScorePatternInstance *pi);
void		score_change_pattern_instance_time	(Score *score,
							 ScorePatternInstance *pi,
							 ScoreTime new_time);
ScoreMark *	score_mark_new			(ScoreMarkType type);
void		score_mark_destroy		(ScoreMark *mark);

#endif /* __SCORE_H */
