/**************************************************************************

    gtreewalk.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <glib.h>
#include "gtreewalk.h"

typedef struct _GRealTree  GRealTree;
typedef struct _GTreeNode  GTreeNode;

/* !!! Keep the structures below synchronized with glib.  Yes, it is
 *     evil, but sometimes evil begets us the wonders of blindingly
 *     fast partial tree traversals ;-).
 */

struct _GRealTree
{
	GTreeNode *root;
	GCompareFunc key_compare;
};

struct _GTreeNode
{
	gint balance;
	GTreeNode *left;
	GTreeNode *right;
	gpointer key;
	gpointer value;
};

typedef struct _GTreeWalkData  GTreeWalkData;
struct _GTreeWalkData
{
	gboolean is_searching;
	GSearchFunc search_func;
	GTraverseFunc traverse_func;
	gpointer data;
};

static gint g_tree_walk_forward_real (GTreeNode *node, GTreeWalkData *walk_data);
static gint g_tree_walk_backward_real (GTreeNode *node, GTreeWalkData *walk_data);

void
g_tree_walk_forward (GTree *tree, GSearchFunc search_func,
		     GTraverseFunc traverse_func, gpointer data)
{
	GTreeWalkData walk_data;
	GRealTree *rtree;

	g_return_if_fail (tree != NULL);

	rtree = (GRealTree*) tree;
	g_return_if_fail (rtree->root != NULL);

	walk_data.is_searching = TRUE;
	walk_data.search_func = search_func;
	walk_data.traverse_func = traverse_func;
	walk_data.data = data;
	g_tree_walk_forward_real (rtree->root, &walk_data);
}

static gint
g_tree_walk_forward_real (GTreeNode *node, GTreeWalkData *walk_data)
{
	register gint dir;

	if (walk_data->is_searching) {

		dir = (* walk_data->search_func) (node->key, walk_data->data);

		if (dir == 0) {
			walk_data->is_searching = FALSE;
			goto start_mid;
		} else if (dir > 0) {
			if (node->right == NULL)
				return FALSE;
			goto start_post;
		} else {
			if (node->left && g_tree_walk_forward_real (node->left, walk_data))
				return TRUE;
			goto start_mid;
		}
	}

	if (node->left)
		if (g_tree_walk_forward_real (node->left, walk_data))
			return TRUE;
 start_mid:
	if ((*walk_data->traverse_func) (node->key, node->value, walk_data->data))
		return TRUE;

 start_post:
	if (node->right)
		if (g_tree_walk_forward_real (node->right, walk_data))
			return TRUE;

	return FALSE;
}

void
g_tree_walk_backward (GTree *tree, GSearchFunc search_func,
		     GTraverseFunc traverse_func, gpointer data)
{
	GTreeWalkData walk_data;
	GRealTree *rtree;

	g_return_if_fail (tree != NULL);

	rtree = (GRealTree*) tree;
	g_return_if_fail (rtree->root != NULL);

	walk_data.is_searching = TRUE;
	walk_data.search_func = search_func;
	walk_data.traverse_func = traverse_func;
	walk_data.data = data;
	g_tree_walk_backward_real (rtree->root, &walk_data);
}

static gint
g_tree_walk_backward_real (GTreeNode *node, GTreeWalkData *walk_data)
{
	register gint dir;

	if (walk_data->is_searching) {

		dir = (* walk_data->search_func) (node->key, walk_data->data);

		if (dir == 0) {
			walk_data->is_searching = FALSE;
			goto start_mid;
		} else if (dir > 0) {
			if (node->left == NULL)
				return FALSE;
			goto start_post;
		} else {
			if (node->right && g_tree_walk_backward_real (node->right, walk_data))
				return TRUE;
			goto start_mid;
		}
	}

	if (node->right)
		if (g_tree_walk_backward_real (node->right, walk_data))
			return TRUE;
 start_mid:
	if ((*walk_data->traverse_func) (node->key, node->value, walk_data->data))
		return TRUE;

 start_post:
	if (node->left)
		if (g_tree_walk_backward_real (node->left, walk_data))
			return TRUE;

	return FALSE;
}
