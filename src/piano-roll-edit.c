/**************************************************************************

    piano-roll-edit.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <gnome.h>
#include <gdk/gdkkeysyms.h>
#include "piano-roll-edit.h"
#include "piano-roll.h"

static void piano_roll_edit_class_init (PianoRollEditClass *klass);
static void piano_roll_edit_init (PianoRollEdit *obj);
static GtkWidget *piano_roll_edit_create_view (GnomeMDIChild *child, gpointer user_data);

GtkType
piano_roll_edit_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"PianoRollEdit",
			sizeof (PianoRollEdit),
			sizeof (PianoRollEditClass),
			(GtkClassInitFunc) piano_roll_edit_class_init,
			(GtkObjectInitFunc) piano_roll_edit_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (TYPE_CLIP_EDIT, &type_info);
	}

	return type;
}

static ClipEditClass *parent_class = NULL;

static void
piano_roll_edit_class_init (PianoRollEditClass *klass)
{
	GnomeMDIChildClass *child_class;

	parent_class = gtk_type_class (clip_edit_get_type ());

	child_class = GNOME_MDI_CHILD_CLASS (klass);
	child_class->create_view = piano_roll_edit_create_view;
}

static void
piano_roll_edit_init (PianoRollEdit *piano_roll_edit)
{
}

GnomeMDIChild *
piano_roll_edit_new (Granite *granite, ScoreClip *clip, SequencerGtkPeer *gtkpeer)
{
	GnomeMDIChild *child = GNOME_MDI_CHILD (gtk_type_new (TYPE_PIANO_ROLL_EDIT));

	clip_edit_construct (CLIP_EDIT (child), granite, clip);
	sequencer_gtk_peer_slave_construct (&GRANITE_MDI_CHILD (child)->gtkpeer, gtkpeer);

	return child;
}

static void
zoom_in_time_cmd (GtkWidget *widget, PianoRollEdit *pe)
{
	TimeRoll *tr;
	gfloat new_time_ppu;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PIANO_ROLL_EDIT (pe));

	tr = TIME_ROLL (pe->pr);

	new_time_ppu = tr->time_ppu + .005;

	time_roll_time_set_ppu (tr, new_time_ppu);
	time_roll_synchronize (tr);
}

static void
zoom_out_time_cmd (GtkWidget *widget, PianoRollEdit *pe)
{
	TimeRoll *tr;
	gfloat new_time_ppu;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PIANO_ROLL_EDIT (pe));

	tr = TIME_ROLL (pe->pr);

	new_time_ppu = tr->time_ppu - .005;
	if (new_time_ppu < .005)
		new_time_ppu = .005;

	time_roll_time_set_ppu (tr, new_time_ppu);
	time_roll_synchronize (tr);
}

static GnomeUIInfo note_menu[] = {
	GNOMEUIINFO_MENU_NEW_ITEM (N_("_New note"), NULL, NULL, NULL),
	{ GNOME_APP_UI_ITEM, N_("Delete note"), NULL, NULL, NULL, NULL,
	  GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_TRASH, GDK_d, 0 },

        GNOMEUIINFO_END
};

static GnomeUIInfo view_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("Zoom in time"), NULL, zoom_in_time_cmd, NULL, NULL,
	  GNOME_APP_PIXMAP_NONE, NULL, GDK_z, 0 },
	{ GNOME_APP_UI_ITEM, N_("Zoom out time"), NULL, zoom_out_time_cmd, NULL, NULL,
	  GNOME_APP_PIXMAP_NONE, NULL, GDK_x, 0 },

        GNOMEUIINFO_END
};

static GnomeUIInfo piano_roll_menu[] = {
	GNOMEUIINFO_SUBTREE (N_("_Note"), note_menu),
	GNOMEUIINFO_SUBTREE (N_("_View"), view_menu),
        GNOMEUIINFO_END
};

static GtkWidget *
piano_roll_edit_create_view (GnomeMDIChild *child, gpointer user_data)
{
	GtkWidget *contents;
	PianoRollEdit *pe;

	clip_edit_set_name (child, _("Piano Roll"));
	gnome_mdi_child_set_menu_template (child, piano_roll_menu);

	pe = PIANO_ROLL_EDIT (child);
	pe->pr = contents = piano_roll_new (CLIP_EDIT (child)->clip,
					    &GRANITE_MDI_CHILD (child)->gtkpeer);

	return contents;
}
