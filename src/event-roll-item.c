/**************************************************************************

    event-roll-item.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <math.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtksignal.h>
#include "event-roll-item.h"
#include "event-roll.h"
#include "selection-hruler.h"

#define PROXIMITY_PERCENTAGE		.1

enum {
	ARG_0,
        ARG_NAME
};

enum {
	CHANGED,
	LAST_SIGNAL
};

static void event_roll_item_class_init (EventRollItemClass *klass);
static void event_roll_item_init       (EventRollItem *obj);
static void event_roll_item_finalize    (GtkObject          *object);
static void event_roll_item_set_arg    (GtkObject          *object,
					GtkArg             *arg,
					guint               arg_id);
static void event_roll_item_get_arg    (GtkObject          *object,
					GtkArg             *arg,
					guint               arg_id);
static void event_roll_item_draw       (GnomeCanvasItem *item, GdkDrawable *drawable,
					int x, int y, int width, int height);
static void event_roll_item_real_changed (EventRollItem *item,
					  EventRollRect *rect);

static guint item_signals[LAST_SIGNAL] = { 0 };

GtkType
event_roll_item_get_type (void)
{
	static GtkType type = 0;

	if (!type)
	{
		static const GtkTypeInfo type_info =
		{
			"EventRollItem",
			sizeof (EventRollItem),
			sizeof (EventRollItemClass),
			(GtkClassInitFunc) event_roll_item_class_init,
			(GtkObjectInitFunc) event_roll_item_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GNOME_TYPE_CANVAS_RECT, &type_info);
	}

	return type;
}

static GnomeCanvasRectClass *parent_class = NULL;

static void
event_roll_item_class_init (EventRollItemClass *klass)
{
	GtkObjectClass *object_class;
	GnomeCanvasItemClass *item_class;

	gtk_object_add_arg_type ("EventRollItem::name",
				 GTK_TYPE_POINTER, GTK_ARG_READWRITE,
				 ARG_NAME);
	
	object_class = GTK_OBJECT_CLASS (klass);
	item_signals[CHANGED] =
		gtk_signal_new ("changed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (EventRollItemClass, changed),
				gtk_marshal_NONE__POINTER,
				GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	gtk_object_class_add_signals (object_class, item_signals, LAST_SIGNAL);

	object_class->finalize = event_roll_item_finalize;
        object_class->set_arg = event_roll_item_set_arg;
        object_class->get_arg = event_roll_item_get_arg;

	parent_class = gtk_type_class (gnome_canvas_rect_get_type ());

	item_class = GNOME_CANVAS_ITEM_CLASS (klass);
	item_class->draw = event_roll_item_draw;

	klass->changed = event_roll_item_real_changed;
}

typedef struct {
	double x, y;
	guint x_is_length : 1;
	guint y_is_length : 1;
	guint x_right : 1;
	guint y_bottom : 1;
	guint absolute : 1;
} ItemConfigureData;

static void
configure_item (GnomeCanvasItem *item, ItemConfigureData *data)
{
	EventRollRect rect;
	GnomeCanvasRE *re;
	gboolean changed = FALSE;

	re = GNOME_CANVAS_RE (item);

	rect.x1 = re->x1;
	rect.y1 = re->y1;
	rect.x2 = re->x2;
	rect.y2 = re->y2;

	if (data->x_is_length) {
		if (data->x_right)
			rect.x2 = data->absolute ? data->x : data->x + rect.x2;
		else
			rect.x1 = data->absolute ? data->x : data->x + rect.x1;
		changed = TRUE;
	} else if (data->x != 0.0) {
		rect.x1 += data->x;
		rect.x2 += data->x;
		changed = TRUE;
	}

	if (data->y_is_length) {
		if (data->y_bottom)
			rect.y2 = data->absolute ? data->y : data->y + rect.y2;
		else
			rect.y1 = data->absolute ? data->y : data->y + rect.y1;
		changed = TRUE;
	} else if (data->y != 0.0) {
		rect.y1 += data->y;
		rect.y2 += data->y;
		changed = TRUE;
	}

	if (changed)
		gtk_signal_emit (GTK_OBJECT (item), item_signals[CHANGED], &rect);

	EVENT_ROLL_ITEM (item)->dirty = TRUE;
}

static gint
event_roll_item_event (GnomeCanvasItem *item, GdkEvent *event, gpointer user_data)
{
	EventRoll *er;
	TimeRoll *tr;
	static enum  {
		ITEM_ADJUST_XY,
		ITEM_ADJUST_START,
		ITEM_ADJUST_END
	} adjust_mode;
	static double start_x, start_y;
	static double prev_x, prev_y;
	static double x_snap_offset;
	GdkCursor *cursor;
	double cur_x, cur_y;
	ItemConfigureData data;

	er = EVENT_ROLL (gtk_object_get_user_data (GTK_OBJECT (item->canvas)));
	tr = TIME_ROLL (er);

	switch (event->type) {
	case GDK_BUTTON_PRESS: {
		if (event->button.button == 1) {
			double x1, x2;
			double width, proximity;
			double x_snap_amount;

			start_x = event->button.x;
			start_y = event->button.y;
			gnome_canvas_item_w2i (item->parent, &start_x, &start_y);
			prev_x = start_x;
			prev_y = start_y;

			x1 = GNOME_CANVAS_RE (item)->x1;
			x2 = GNOME_CANVAS_RE (item)->x2;
			width = x2 - x1;
			proximity = width * PROXIMITY_PERCENTAGE;

			debugf ("Event roll item: start: x %.1f y %.1f", start_x, start_y);
			x_snap_amount = SELECTION_HRULER (
				TIME_ROLL (er)->time_ruler)->snap_amount;
#if 0
			if (x_snap_amount != 0.0) {
				x_snap_offset =
					(start_x - x_snap_amount * floor (start_x / x_snap_amount)) -
					(x1 - x_snap_amount * floor (x1 / x_snap_amount));
				debugf ("X snap offset is %.1f", x_snap_offset);
			} else
#endif
				x_snap_offset = 0.0;

			if (!(event->button.state & GDK_CONTROL_MASK))
				event_roll_selection_clear (er);
			event_roll_selection_add (er, EVENT_ROLL_ITEM (item));

			if (prev_x - x1 < proximity) {
				adjust_mode = ITEM_ADJUST_START;
				cursor = gdk_cursor_new (GDK_LEFT_SIDE);
			} else if (prev_x > x2 - proximity) {
				adjust_mode = ITEM_ADJUST_END;
				cursor = gdk_cursor_new (GDK_RIGHT_SIDE);
			} else {
				adjust_mode = ITEM_ADJUST_XY;
				cursor = gdk_cursor_new (GDK_FLEUR);
			}

			gnome_canvas_item_grab (item,
						GDK_POINTER_MOTION_MASK |
						GDK_BUTTON_RELEASE_MASK,
						cursor,
						event->button.time);
			gdk_cursor_destroy (cursor);
		} else if (event->button.button == 2) {
			if (event->button.state & GDK_SHIFT_MASK)
				gnome_canvas_item_raise_to_top (item);
			else
				gnome_canvas_item_lower_to_bottom (item);
		}
		break;
	}

	case GDK_MOTION_NOTIFY:
		if (event->motion.state & GDK_BUTTON1_MASK) {
			double delta_y, delta_x;
			double move_y, move_x;
			double y_snap_amount;
			double x_snap_amount;

			cur_x = event->motion.x;
			cur_y = event->motion.y;
			gnome_canvas_item_w2i (item->parent, &cur_x, &cur_y);
			delta_x = cur_x - prev_x;
			delta_y = cur_y - prev_y;

			debugf ("Event roll item: cur: x %.1f y %.1f", cur_x, cur_y);

			x_snap_amount =  SELECTION_HRULER (
				TIME_ROLL (er)->time_ruler)->snap_amount;
			y_snap_amount = tr->vert_snap;

			if (x_snap_amount == 0.0) {
				move_x = delta_x;
				prev_x = cur_x;
			} else if (fabs (delta_x) < x_snap_amount) {
				move_x = 0.0;
			} else {
				if (delta_x >= 0.0)
					move_x = x_snap_amount * floor (delta_x / x_snap_amount) +
						x_snap_offset;
				else
					move_x = x_snap_amount * ceil (delta_x / x_snap_amount) +
						x_snap_offset;
				prev_x = cur_x - delta_x + move_x;
			}

			if (y_snap_amount == 0.0) {
				move_y = delta_y;
				prev_y = cur_y;
			} else if (fabs (delta_y) < y_snap_amount)
				move_y = 0.0;
			else {
				if (delta_y >= 0.0)
					move_y = y_snap_amount * floor (delta_y / y_snap_amount);
				else
					move_y = y_snap_amount * ceil (delta_y / y_snap_amount);
				prev_y = cur_y - delta_y + move_y;
			}

			switch (adjust_mode) {
			case ITEM_ADJUST_XY:
				if (move_x == 0.0 && move_y == 0.0)
					break;
				data.x = move_x;
				data.y = move_y;
				data.x_is_length = FALSE;
				data.y_is_length = FALSE;
				event_roll_selection_foreach (
					er, (GFunc) configure_item, &data);
				break;

			case ITEM_ADJUST_START:
			case ITEM_ADJUST_END:
				data.y = 0;
				data.y_is_length = FALSE;
				if (event->button.state & GDK_MOD1_MASK) {
					data.x = cur_x - move_x + delta_x;
					data.x_is_length = TRUE;
					data.absolute = TRUE;
				} else {
					if (move_x == 0.0)
						break;
					data.x = move_x;
					data.x_is_length = !(event->button.state & GDK_SHIFT_MASK);
					data.absolute = FALSE;
				}
				data.x_right = adjust_mode == ITEM_ADJUST_END;
				event_roll_selection_foreach (
					er, (GFunc) configure_item, &data);
				break;
			}
		}
		break;

	case GDK_BUTTON_RELEASE:
		if (event->button.button == 1)
			gnome_canvas_item_ungrab (item, event->button.time);
		break;

	default:
		break;
	}

	return FALSE;
}

static void
event_roll_item_init (EventRollItem *event_item)
{
	event_item->name = NULL;
	event_item->state = EVENT_ROLL_ITEM_NORMAL;
	event_item->dirty = FALSE;
	gtk_signal_connect (GTK_OBJECT (event_item), "event",
			    GTK_SIGNAL_FUNC (event_roll_item_event), NULL);
}

static void
event_roll_item_finalize (GtkObject *object)
{
	EventRollItem *event_item;

        g_return_if_fail (object != NULL);
        g_return_if_fail (IS_EVENT_ROLL_ITEM (object));

	event_item = EVENT_ROLL_ITEM (object);
	g_free (event_item->name);

	if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (*GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
event_roll_item_set_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	EventRollItem *event_item = EVENT_ROLL_ITEM (object);

	switch (arg_id) {
	case ARG_NAME:
		if (event_item->name)
			g_free (event_item->name);
		event_item->name = g_strdup (GTK_VALUE_POINTER (*arg));
		break;

	default:
		break;
	}
}

static void
event_roll_item_get_arg (GtkObject *object, GtkArg *arg, guint arg_id)
{
	EventRollItem *event_item = EVENT_ROLL_ITEM (object);

	switch (arg_id) {
	case ARG_NAME:
		GTK_VALUE_POINTER (*arg) = g_strdup (event_item->name);
		break;

	default:
		break;
	}
}

void
event_roll_item_set_state (EventRollItem *item,
			   EventRollItemState state)
{
	GnomeCanvasItem *ci;

	g_return_if_fail (item != NULL);
	g_return_if_fail (IS_EVENT_ROLL_ITEM (item));
	item->state = state;

	ci = GNOME_CANVAS_ITEM (item);

	gnome_canvas_item_request_update (GNOME_CANVAS_ITEM (item));
	gnome_canvas_request_redraw (ci->canvas, ci->x1, ci->y1, ci->x2, ci->y2);
}

static void
event_roll_item_draw (GnomeCanvasItem *item, GdkDrawable *drawable,
		      int x, int y, int width, int height)
{
	EventRollItem *event_item = EVENT_ROLL_ITEM (item);
	GtkStyle *style;
	GnomeCanvasRE *re;
	GdkRectangle rect;
	double i2w[6], w2c[6], i2c[6];
	int x1, y1, x2, y2;
	ArtPoint i1, i2;
	ArtPoint c1, c2;

	re = GNOME_CANVAS_RE (item);

	gnome_canvas_item_i2w_affine (item, i2w);
	gnome_canvas_w2c_affine (item->canvas, w2c);
	art_affine_multiply (i2c, i2w, w2c);

	i1.x = re->x1;
	i1.y = re->y1;
	i2.x = re->x2;
	i2.y = re->y2;
	art_affine_point (&c1, &i1, i2c);
	art_affine_point (&c2, &i2, i2c);
	x1 = c1.x;
	y1 = c1.y;
	x2 = c2.x;
	y2 = c2.y;
	rect.x = x1 - x;
	rect.y = y1 - y;
	rect.width = x2 - x1;
	rect.height = y2 - y1;

	if (re->fill_set) {
                if (re->fill_stipple)
                        gnome_canvas_set_stipple_origin (item->canvas, re->fill_gc);
		
                gdk_draw_rectangle (drawable, re->fill_gc, TRUE,
				    rect.x, rect.y,
				    rect.width, rect.height);
        }

	style = GTK_WIDGET (GNOME_CANVAS_ITEM (event_item)->canvas)->style;

	if (event_item->name)
		gtk_paint_string (style, drawable, GTK_STATE_NORMAL,
				  &rect, NULL, NULL,
				  x1 - x + style->font->ascent,
				  y2 - y - style->font->descent - 1,
				  event_item->name);

	gtk_draw_shadow (style, drawable,
			 GTK_STATE_NORMAL,
			 event_item->state == EVENT_ROLL_ITEM_SELECTED
			 ? GTK_SHADOW_IN
			 : GTK_SHADOW_OUT,
			 rect.x, rect.y,
			 rect.width, rect.height);
}

static void
event_roll_item_real_changed (EventRollItem *item, EventRollRect *rect)
{
	g_return_if_fail (item != NULL);
	g_return_if_fail (rect != NULL);
	g_return_if_fail (IS_EVENT_ROLL_ITEM (item));

	gnome_canvas_item_set (GNOME_CANVAS_ITEM (item),
			       "x1", rect->x1, "y1", rect->y1,
			       "x2", rect->x2, "y2", rect->y2,
			       NULL);
}
