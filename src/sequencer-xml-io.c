/**************************************************************************

    sequencer-xml-io.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"
#include "ui.h"
#include "granite-xml-io.h"
#include "sequencer-xml-io.h"

static xmlNodePtr
sequencer_write_prefs (XMLContext *xc, const SequencerPrefs *prefs)
{
	xmlNodePtr node;

	node = xmlNewDocNode (xc->doc, xc->ns, "Preferences", NULL);

	debugf ("Write sequencer prefs");
	xmlSetProp (node, "SendMTC", prefs->send_mtc ? "1" : "0");

	return node;
}

xmlNodePtr
sequencer_write_xml (XMLContext *xc, const SequencerPrefs *prefs)
{
	xmlNodePtr node;
	xmlNodePtr prefs_node;

	node = xmlNewDocNode (xc->doc, xc->ns, "Sequencer", NULL);
	prefs_node = sequencer_write_prefs (xc, prefs);
	xmlAddChild (node, prefs_node);

	return node;
}

static void
sequencer_read_prefs (XMLContext *xc, SequencerPrefs *prefs)
{
	xmlNodePtr tree;
	xmlNodePtr node;

	tree = xc->doc->root;
	node = xmlSearchChild (tree, "Preferences");
	debugf ("Read sequencer prefs");
	if (!node) {
		debugf ("Prefs node not found");
		return;
	}
	prefs->send_mtc = !!xmlGetInt (node, "SendMTC");
}

void
sequencer_read_xml (XMLContext *xc, SequencerPrefs *prefs)
{
	xmlNodePtr tree;
	xmlNodePtr sequencer_node;
	xmlNodePtr pi_node;

	g_return_if_fail (xc != NULL);
	g_return_if_fail (prefs != NULL);

	tree = xc->doc->root;
	sequencer_node = xmlSearchChild (tree, "Sequencer");
	if (!sequencer_node) {
		g_warning ("Couldn't find Sequencer node");
		return;
	}

	sequencer_read_prefs (xc, prefs);
}
