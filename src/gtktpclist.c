/**************************************************************************

    gtktpclist.c

    Typed Parameter Column List Widget

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "gtktpclist.h"
#include <gdk/gdkkeysyms.h>

#define DEFAULT_COLUMN_WIDTH	40
#define MIN_COLUMN_WIDTH	5
#define PIXEL_PROXIMITY		5

#define ROW_SELECTED		0x01

#define GTK_TPCLIST_POS(tpclist, column)			\
	g_array_index (tpclist->columns, GtkTPCListColumn,	\
		       column)
#define GTK_TPCLIST_COL(tpclist, column)			\
	g_array_index (tpclist->columns, GtkTPCListColumn,	\
		       GTK_TPCLIST_POS2COL (tpclist, column))
#define GTK_TPCLIST_POS2COL(tpclist, column)			\
	g_array_index (tpclist->column_index, gint, column)
#define GTK_TPCLIST_ROW_FLAGS(tpclist, row)		\
	g_array_index (tpclist->row_flags, guint8, row)

typedef struct _GtkTPCListColumn            GtkTPCListColumn;
typedef struct _GtkTPCListCell              GtkTPCListCell;

enum {
	ARG_0
};

enum {
	ACTIVATE_CELL,
	FOCUS_CELL,
	KEY_PRESS_CELL,
	LAST_SIGNAL
};

struct _GtkTPCListCell
{
	gchar *value;
};

struct _GtkTPCListColumn
{
	GString *name;
	gint x, width;

	GArray *rows;
	GtkTPCListValueFunc value_func;
	gpointer value_func_data;
};

static void gtk_tpclist_class_init (GtkTPCListClass *klass);
static void gtk_tpclist_init (GtkTPCList *obj);

static void gtk_tpclist_finalize (GtkObject *object);

static void gtk_tpclist_set_scroll_adjustments (GtkTPCList *tpcclist, GtkAdjustment *hadjustment,
						GtkAdjustment *vadjustment);
static void gtk_tpclist_realize (GtkWidget *widget);
static void gtk_tpclist_unrealize (GtkWidget *widget);
static void gtk_tpclist_map (GtkWidget *widget);
static void gtk_tpclist_unmap (GtkWidget *widget);
static gint gtk_tpclist_expose (GtkWidget *widget, GdkEventExpose *event);
static gint gtk_tpclist_key_press (GtkWidget *widget, GdkEventKey *event);
static gint gtk_tpclist_button_press (GtkWidget *widget, GdkEventButton *event);
static gint gtk_tpclist_button_release (GtkWidget *widget, GdkEventButton *event);
static gint gtk_tpclist_motion_notify (GtkWidget *widget, GdkEventMotion *event);
static void gtk_tpclist_size_request (GtkWidget *widget, GtkRequisition *requisition);
static void gtk_tpclist_size_allocate (GtkWidget *widget, GtkAllocation *allocation);
static void gtk_tpclist_draw_focus (GtkWidget *widget);
static gint gtk_tpclist_focus_in (GtkWidget *widget, GdkEventFocus *event);
static gint gtk_tpclist_focus_out (GtkWidget *widget, GdkEventFocus *event);

static gint gtk_tpclist_focus (GtkContainer *container, GtkDirectionType direction);

static void gtk_tpclist_hadj_value_changed (GtkAdjustment *adj, GtkTPCList *tpclist);
static void gtk_tpclist_vadj_value_changed (GtkAdjustment *adj, GtkTPCList *tpclist);
static void gtk_tpclist_make_backing_store (GtkTPCList *tpclist);

static void gtk_tpclist_start_edit (GtkTPCList *tpclist, gint row, gint column);
static void gtk_tpclist_stop_edit (GtkTPCList *tpclist, gboolean accept);

static void gtk_tpclist_real_activate_cell (GtkTPCList *tpclist, gint row, gint column, gchar **value);
static void gtk_tpclist_real_focus_cell (GtkTPCList *tpclist, gint row, gint column);

static gint gtk_tpclist_col2pos (GtkTPCList *tpclist, gint column);
static void select_row (gint row, GtkTPCList *tpclist);
static void unselect_row (gint row, GtkTPCList *tpclist);

static guint tpclist_signals[LAST_SIGNAL];

GtkType
gtk_tpclist_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"GtkTPCList",
			sizeof (GtkTPCList),
			sizeof (GtkTPCListClass),
			(GtkClassInitFunc) gtk_tpclist_class_init,
			(GtkObjectInitFunc) gtk_tpclist_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GTK_TYPE_CONTAINER, &type_info);
	}

	return type;
}

static GtkContainerClass *parent_class = NULL;

static void
gtk_tpclist_class_init (GtkTPCListClass *klass)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;
	GtkContainerClass *container_class;
	GtkTPCListClass *tpclist_class;

	parent_class = gtk_type_class (gtk_container_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = gtk_tpclist_finalize;

	tpclist_signals[ACTIVATE_CELL] =
		gtk_signal_new ("activate_cell",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GtkTPCListClass, activate_cell),
				gtk_marshal_NONE__INT_INT_POINTER,
				GTK_TYPE_NONE, 3,
				GTK_TYPE_INT,
				GTK_TYPE_INT,
				GTK_TYPE_POINTER);

	tpclist_signals[FOCUS_CELL] =
		gtk_signal_new ("focus_cell",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GtkTPCListClass, focus_cell),
				gtk_marshal_NONE__INT_INT,
				GTK_TYPE_NONE, 2,
				GTK_TYPE_INT,
				GTK_TYPE_INT);

	tpclist_signals[KEY_PRESS_CELL] =
		gtk_signal_new ("key_press_cell",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GtkTPCListClass, key_press_cell),
				gtk_marshal_NONE__INT_INT_POINTER,
				GTK_TYPE_NONE, 3,
				GTK_TYPE_INT,
				GTK_TYPE_INT,
				GTK_TYPE_POINTER);

	gtk_object_class_add_signals (object_class, tpclist_signals, LAST_SIGNAL);

	widget_class = GTK_WIDGET_CLASS (klass);
	widget_class->set_scroll_adjustments_signal =
		gtk_signal_new ("set_scroll_adjustments",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GtkTPCListClass, set_scroll_adjustments),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2,
				GTK_TYPE_ADJUSTMENT,
				GTK_TYPE_ADJUSTMENT);
	widget_class->realize = gtk_tpclist_realize;
	widget_class->unrealize = gtk_tpclist_unrealize;
	widget_class->map = gtk_tpclist_map;
	widget_class->unmap = gtk_tpclist_unmap;
	widget_class->expose_event = gtk_tpclist_expose;
	widget_class->key_press_event = gtk_tpclist_key_press;
	widget_class->button_press_event = gtk_tpclist_button_press;
	widget_class->button_release_event = gtk_tpclist_button_release;
	widget_class->motion_notify_event = gtk_tpclist_motion_notify;
	widget_class->size_request = gtk_tpclist_size_request;
	widget_class->size_allocate = gtk_tpclist_size_allocate;
	widget_class->focus_in_event = gtk_tpclist_focus_in;
	widget_class->focus_out_event = gtk_tpclist_focus_out;
	widget_class->draw_focus = gtk_tpclist_draw_focus;

	container_class = GTK_CONTAINER_CLASS (klass);
	container_class->focus = gtk_tpclist_focus;

	tpclist_class = GTK_TPCLIST_CLASS (klass);
	tpclist_class->set_scroll_adjustments = gtk_tpclist_set_scroll_adjustments;
	tpclist_class->activate_cell = gtk_tpclist_real_activate_cell;
	tpclist_class->focus_cell = gtk_tpclist_real_focus_cell;
	tpclist_class->key_press_cell = NULL;
}

static void
gtk_tpclist_init (GtkTPCList *tpclist)
{
	GTK_WIDGET_SET_FLAGS (tpclist, GTK_CAN_FOCUS);

	/* Misc */
	tpclist->xor_gc = NULL;
	tpclist->focus_row = -1;
	tpclist->focus_column = -1;

	/* Column */
	tpclist->ncolumns = 0;
	tpclist->column_window = NULL;
	tpclist->column_label_height = 0;
	memset (&tpclist->column_window_area, 0, sizeof (tpclist->column_window_area));
	tpclist->hadjustment = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
	gtk_object_ref (GTK_OBJECT (tpclist->hadjustment));
	gtk_signal_connect (GTK_OBJECT (tpclist->hadjustment), "value_changed",
			    GTK_SIGNAL_FUNC (gtk_tpclist_hadj_value_changed), tpclist);
	tpclist->column_backing_store = NULL;
	tpclist->columns = g_array_new (FALSE, FALSE, sizeof (GtkTPCListColumn));
	tpclist->column_index = g_array_new (FALSE, FALSE, sizeof (gint));

	/* Row */
	tpclist->nrows = 0;
	tpclist->row_window = NULL;
	tpclist->row_label_width = 0;
	memset (&tpclist->row_window_area, 0, sizeof (tpclist->row_window_area));
	tpclist->vadjustment = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
	gtk_object_ref (GTK_OBJECT (tpclist->vadjustment));
	gtk_signal_connect (GTK_OBJECT (tpclist->vadjustment), "value_changed",
			    GTK_SIGNAL_FUNC (gtk_tpclist_vadj_value_changed), tpclist);
	tpclist->row_backing_store = NULL;
	tpclist->row_data = g_array_new (FALSE, FALSE, sizeof (gpointer));
	tpclist->row_flags = g_array_new (FALSE, FALSE, sizeof (guint8));
	tpclist->selected_rows = NULL;

	/* Data */
	tpclist->data_window = NULL;
	memset (&tpclist->data_window_area, 0, sizeof (tpclist->data_window_area));
	tpclist->data_backing_store = NULL;
	tpclist->entry = NULL;
	tpclist->entry_top = NULL;

	/* Flags */
	tpclist->column_window_dirty =
		tpclist->row_window_dirty =
		tpclist->data_window_dirty = FALSE;
}

GtkWidget *
gtk_tpclist_new (void)
{
	return GTK_WIDGET (gtk_type_new (GTK_TYPE_TPCLIST));
}

static void
gtk_tpclist_finalize (GtkObject *object)
{
	GtkTPCList *tpclist;
	gint i, j;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (object));

	tpclist = GTK_TPCLIST (object);

	g_slist_free (tpclist->selected_rows);
	tpclist->selected_rows = NULL;
	gtk_object_destroy (GTK_OBJECT (tpclist->hadjustment));
	gtk_object_destroy (GTK_OBJECT (tpclist->vadjustment));
	for (j = 0; j < tpclist->ncolumns; ++j) {
		for (i = 0; i < tpclist->nrows; ++i) {
			gchar *value = g_array_index (GTK_TPCLIST_POS (tpclist, j).rows,
						      GtkTPCListCell, i).value;
			if (value) g_free (value);
		}
		g_array_free (GTK_TPCLIST_POS (tpclist, j).rows, TRUE);
	}
	g_array_free (tpclist->columns, TRUE);
	g_array_free (tpclist->column_index, TRUE);
	g_array_free (tpclist->row_data, TRUE);
	g_array_free (tpclist->row_flags, TRUE);

	if (GTK_OBJECT_CLASS (parent_class)->finalize)
		(*GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
gtk_tpclist_realize (GtkWidget *widget)
{
	GtkTPCList *tpclist;
	GdkGCValues values;
	GdkWindowAttr attributes;
	guint attributes_mask;
	gint border_width;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (widget));

	tpclist = GTK_TPCLIST (widget);

	border_width = GTK_CONTAINER (widget)->border_width;

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = widget->allocation.x + border_width;
	attributes.y = widget->allocation.y + border_width;
	attributes.width = widget->allocation.width - border_width * 2;
	attributes.height = widget->allocation.height - border_width * 2;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.colormap = gtk_widget_get_colormap (widget);
	attributes.event_mask = gtk_widget_get_events (widget) |
		GDK_EXPOSURE_MASK |
		GDK_BUTTON_PRESS_MASK |
		GDK_BUTTON_RELEASE_MASK |
		GDK_KEY_PRESS_MASK;
	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

	/* Widget */
	widget->window = gdk_window_new (
		gtk_widget_get_parent_window (widget), &attributes, attributes_mask);
	gdk_window_set_user_data (widget->window, widget);
	widget->style = gtk_style_attach (widget->style, widget->window);
	gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);

	/* Column */
	attributes.x = tpclist->column_window_area.x;
	attributes.y = tpclist->column_window_area.y;
	attributes.width = tpclist->column_window_area.width;
	attributes.height = tpclist->column_window_area.height;
	tpclist->column_window = gdk_window_new (widget->window, &attributes, attributes_mask);
	gdk_window_set_user_data (tpclist->column_window, tpclist);
	gtk_style_set_background (widget->style, tpclist->column_window, GTK_STATE_NORMAL);
	gdk_window_show (tpclist->column_window);

	/* Row */
	if (!tpclist->row_label_width)
		tpclist->row_label_width = gdk_string_width (widget->style->font, "wwww");
	attributes.x = tpclist->row_window_area.x;
	attributes.y = tpclist->row_window_area.y;
	attributes.width = tpclist->row_window_area.width;
	attributes.height = tpclist->row_window_area.height;
	tpclist->row_window = gdk_window_new (widget->window, &attributes, attributes_mask);
	gdk_window_set_user_data (tpclist->row_window, tpclist);
	gtk_style_set_background (widget->style, tpclist->row_window, GTK_STATE_NORMAL);
	gdk_window_show (tpclist->row_window);

	/* Data */
	attributes.x = tpclist->data_window_area.x;
	attributes.y = tpclist->data_window_area.y;
	attributes.width = tpclist->data_window_area.width;
	attributes.height = tpclist->data_window_area.height;
	tpclist->data_window = gdk_window_new (widget->window, &attributes, attributes_mask);
	gdk_window_set_user_data (tpclist->data_window, tpclist);
	gtk_style_set_background (widget->style, tpclist->data_window, GTK_STATE_NORMAL);
	gdk_window_show (tpclist->data_window);

	gtk_tpclist_make_backing_store (tpclist);

	/* Misc */
	values.foreground = widget->style->white;
	values.function = GDK_XOR;
	values.subwindow_mode = GDK_INCLUDE_INFERIORS;
	tpclist->xor_gc = gdk_gc_new_with_values (widget->window, &values,
						  GDK_GC_FOREGROUND |
						  GDK_GC_FUNCTION |
						  GDK_GC_SUBWINDOW);
	gdk_gc_set_line_attributes (tpclist->xor_gc, 1, GDK_LINE_SOLID, 0, 0);

	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
}

static void
gtk_tpclist_unrealize (GtkWidget *widget)
{
	GtkTPCList *tpclist;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (widget));

	tpclist = GTK_TPCLIST (widget);

	gdk_gc_destroy (tpclist->xor_gc);
	gdk_window_destroy (tpclist->data_window);
	gdk_window_destroy (tpclist->row_window);
	gdk_window_destroy (tpclist->column_window);

	if (tpclist->data_backing_store) {
		gdk_pixmap_unref (tpclist->data_backing_store);
		tpclist->data_backing_store = NULL;
	} if (tpclist->column_backing_store) {
		gdk_pixmap_unref (tpclist->column_backing_store);
		tpclist->column_backing_store = NULL;
	} if (tpclist->row_backing_store) {
		gdk_pixmap_unref (tpclist->row_backing_store);
		tpclist->row_backing_store = NULL;
	}

	if (GTK_WIDGET_CLASS (parent_class)->unrealize)
		(* GTK_WIDGET_CLASS (parent_class)->unrealize) (widget);
}

static void
gtk_tpclist_map (GtkWidget *widget)
{
	GtkTPCList *tpclist;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (widget));

	tpclist = GTK_TPCLIST (widget);

	if (!GTK_WIDGET_MAPPED (widget)) {
		GTK_WIDGET_SET_FLAGS (widget, GTK_MAPPED);
		gdk_window_show (widget->window);
	}
}

static void
gtk_tpclist_unmap (GtkWidget *widget)
{
	GtkTPCList *tpclist;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (widget));

	tpclist = GTK_TPCLIST (widget);

	if (GTK_WIDGET_MAPPED (widget)) {
		GTK_WIDGET_UNSET_FLAGS (widget, GTK_MAPPED);
		gdk_window_hide (widget->window);
	}
}

static void
draw_rows (GtkTPCList *tpclist, GdkRectangle *area)
{
	GtkWidget *widget;
	GdkRectangle rect;
	gfloat row_start, row_end;
	gint row;
	gint y1;
	gint width;
	char s[20];

	widget = GTK_WIDGET (tpclist);

	gtk_paint_box (widget->style, tpclist->row_backing_store,
		       GTK_STATE_NORMAL, GTK_SHADOW_NONE,
		       area, widget, "gtktpclist",
		       0, 0, -1, -1);

	row_start = tpclist->vadjustment->value / tpclist->row_height;
	row_end = MIN (row_start + (tpclist->row_window_area.height + .5) / tpclist->row_height,
		       tpclist->nrows);

	for (row = row_start; row < row_end; ++row) {
		y1 = (row - row_start) * tpclist->row_height;
		sprintf (s, "%d", (gint) row + 1);
		width = gdk_string_width (widget->style->font, s);
		rect.x = 0;
		rect.y = y1;
		rect.width = tpclist->row_window_area.width;
		rect.height = tpclist->row_height;
		if (GTK_TPCLIST_ROW_FLAGS (tpclist, row) & ROW_SELECTED) {
			gtk_paint_box (widget->style, tpclist->row_backing_store,
				       GTK_STATE_SELECTED, GTK_SHADOW_IN,
				       area, widget, "gtktpclist",
				       rect.x, rect.y,
				       rect.width, rect.height);
			gtk_paint_string (widget->style, tpclist->row_backing_store,
					  GTK_STATE_SELECTED,
					  &rect, widget, "gtktpclist",
					  (rect.width - width) / 2,
					  rect.y + rect.height / 2 + widget->style->font->descent,
					  s);
		} else {
			gtk_paint_box (widget->style, tpclist->row_backing_store,
				       GTK_STATE_NORMAL, GTK_SHADOW_OUT,
				       area, widget, "gtktpclist",
				       rect.x, rect.y,
				       rect.width, rect.height);
			gtk_paint_string (widget->style, tpclist->row_backing_store,
					  GTK_STATE_NORMAL,
					  &rect, widget, "gtktpclist",
					  (rect.width - width) / 2,
					  rect.y + rect.height / 2 + widget->style->font->descent,
					  s);
		}
	}
}

static void
draw_cols (GtkTPCList *tpclist, GdkRectangle *area)
{
	GtkWidget *widget;
	GdkRectangle rect;
	gint x1, x2;
	gint i;
	gint width;

	widget = GTK_WIDGET (tpclist);

	x1 = tpclist->hadjustment->value;
	x2 = x1 + tpclist->column_window_area.width;

	gtk_paint_box (widget->style, tpclist->column_backing_store,
		       GTK_STATE_NORMAL, GTK_SHADOW_NONE,
		       area, widget, "gtktpclist",
		       0, 0, -1, -1);

	for (i = 0; i < tpclist->ncolumns; ++i) {
		if (GTK_TPCLIST_COL (tpclist, i).x + GTK_TPCLIST_COL (tpclist, i).width > x1 &&
		    GTK_TPCLIST_COL (tpclist, i).x < x2) {
			rect.x = GTK_TPCLIST_COL (tpclist, i).x - x1;
			rect.y = 0;
			rect.width = GTK_TPCLIST_COL (tpclist, i).width;
			rect.height = tpclist->column_window_area.height;
			gtk_paint_box (widget->style, tpclist->column_backing_store,
				       GTK_STATE_NORMAL, GTK_SHADOW_OUT,
				       area, widget, "gtktpclist",
				       rect.x, rect.y,
				       rect.width, rect.height);
			width = gdk_string_width (widget->style->font,
						  GTK_TPCLIST_COL (tpclist, i).name->str);
			gtk_paint_string (widget->style, tpclist->column_backing_store,
					  GTK_STATE_NORMAL,
					  &rect, widget, "gtktpclist",
					  rect.x + (rect.width - width) / 2,
					  rect.height / 2 + widget->style->font->descent,
					  GTK_TPCLIST_COL (tpclist, i).name->str);
		}
	}
}

static void
draw_data (GtkTPCList *tpclist, GdkRectangle *area)
{
	GtkWidget *widget;
	GdkRectangle rect;
	gint x1, x2, y1, i;
	gfloat row_start, row_end;
	gint row;
	gint width;
	GdkRectangle focus;
	GtkTPCListColumn *col;
	GtkTPCListCell *cell;
	GtkStateType state;

	widget = GTK_WIDGET (tpclist);

	x1 = tpclist->hadjustment->value;
	x2 = x1 + tpclist->column_window_area.width;

	gtk_paint_flat_box (widget->style, tpclist->data_backing_store,
			    GTK_STATE_NORMAL, GTK_SHADOW_IN,
			    area, widget, "entry_bg",
			    0, 0, -1, -1);

	memset (&focus, 0, sizeof (focus));
	row_start = tpclist->vadjustment->value / tpclist->row_height;
	row_end = MIN (row_start + (tpclist->data_window_area.height + .5) / tpclist->row_height,
		       tpclist->nrows);
	for (row = row_start; row <= row_end; ++row) {
		y1 = (row - row_start) * tpclist->row_height;

		gdk_draw_line (tpclist->data_backing_store,
			       widget->style->dark_gc[GTK_STATE_NORMAL],
			       0,  y1,
			       tpclist->data_window_area.width, y1);

		if (row == tpclist->nrows)
			continue;

		state = GTK_STATE_NORMAL;

		if ((gint) floor (row) == tpclist->focus_row) {
			focus.y = y1;
			focus.height = tpclist->row_height;
			state = GTK_STATE_PRELIGHT;
			gtk_paint_flat_box (widget->style, tpclist->data_backing_store,
					    state, GTK_SHADOW_IN,
					    area, widget, "gtktpclist",
					    0, y1 + 1,
					    tpclist->data_window_area.width,
					    tpclist->row_height - 2);
		}

		for (i = 0; i < tpclist->ncolumns; ++i) {
			col = &GTK_TPCLIST_COL (tpclist, i);
			cell = &g_array_index (col->rows, GtkTPCListCell, row);
			if (!cell->value)
				continue;
			if (col->x + col->width > x1 && col->x < x2) {
				rect.x = col->x - x1;
				rect.y = y1;
				rect.width = col->width;
				rect.height = tpclist->row_height;
				width = gdk_string_width (widget->style->font, cell->value);
				gtk_paint_string (widget->style, tpclist->data_backing_store,
						  state, &rect, widget, "gtktpclist",
						  rect.x + (rect.width - width) / 2,
						  rect.y + rect.height / 2 +
						  widget->style->font->descent,
						  cell->value);
			}
		}
	}

	for (i = 0; i < tpclist->ncolumns; ++i) {
		col = &GTK_TPCLIST_COL (tpclist, i);

		if (col->x + col->width > x1 && col->x < x2) {
			gint x = col->x + col->width - x1;
			gdk_draw_line (tpclist->data_backing_store,
				       widget->style->dark_gc[GTK_STATE_NORMAL],
				       x,  0,
				       x, tpclist->data_window_area.height);
		}
		if (GTK_TPCLIST_POS2COL (tpclist, i) == tpclist->focus_column) {
			focus.x = col->x - x1;
			focus.width = col->width;
		}
	}

	if (GTK_WIDGET_HAS_FOCUS (widget) &&
	    tpclist->focus_row >= 0 &&
	    tpclist->focus_column >= 0 &&
	    focus.width > 0 &&
	    focus.height > 0)
		gtk_paint_focus (widget->style, tpclist->data_backing_store,
				 NULL, widget, "gtktpclist",
				 focus.x, focus.y,
				 focus.width - 1, focus.height - 1);
}

static void
replace_backing_store (GdkWindow *window, GdkPixmap *pixmap)
{
	gdk_window_set_back_pixmap (window, pixmap, FALSE);
	gdk_window_clear (window);
}

static gboolean
make_backing_store (GdkWindow *window, GdkPixmap **pixmap)
{
	gint width, height;
	gint width2, height2;

	gdk_window_get_size (window, &width, &height);
	if (*pixmap) {
		gdk_window_get_size (*pixmap, &width2, &height2);
		if (width == width2 && height == height2)
			return FALSE;
		gdk_pixmap_unref (*pixmap);
	}
	*pixmap = gdk_pixmap_new (window, width, height, -1);
	return TRUE;
}

static void
gtk_tpclist_make_backing_store (GtkTPCList *tpclist)
{
	tpclist->column_window_dirty =
		tpclist->row_window_dirty =
		tpclist->data_window_dirty = TRUE;
	make_backing_store (tpclist->column_window, &tpclist->column_backing_store);
	make_backing_store (tpclist->row_window, &tpclist->row_backing_store);
	make_backing_store (tpclist->data_window, &tpclist->data_backing_store);
}

static gint
gtk_tpclist_expose (GtkWidget *widget, GdkEventExpose *event)
{
	GtkTPCList *tpclist;
	static GdkRectangle rect = { 0, 0, -1, -1 };

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	if (!GTK_WIDGET_DRAWABLE (widget))
		return FALSE;

	tpclist = GTK_TPCLIST (widget);

	if (tpclist->column_window_dirty) {
		draw_cols (tpclist, &rect);
		replace_backing_store (tpclist->column_window,
				       tpclist->column_backing_store);
		tpclist->column_window_dirty = FALSE;
	}

	if (tpclist->row_window_dirty) {
		draw_rows (tpclist, &rect);
		replace_backing_store (tpclist->row_window,
				       tpclist->row_backing_store);
		tpclist->row_window_dirty = FALSE;
	}

	if (tpclist->data_window_dirty) {
		draw_data (tpclist, &rect);
		replace_backing_store (tpclist->data_window,
				       tpclist->data_backing_store);
		tpclist->data_window_dirty = FALSE;
	}

	return FALSE;
}

typedef enum {
	GRAB_MIDDLE,
	GRAB_SIDE
} GrabType;

static gint
column_from_point (GtkTPCList *tpclist, gint x, GrabType *type)
{
	GtkTPCListColumn *col;
	gint i;
	gint x1, x2;

	for (i = 0; i < tpclist->ncolumns; ++i) {
		col = &GTK_TPCLIST_COL (tpclist, i);

		x1 = col->x - tpclist->hadjustment->value;
		x2 = x1 + col->width;

		if (type &&
		    x >= x2 - PIXEL_PROXIMITY && x <= x2 + PIXEL_PROXIMITY) {
			*type = GRAB_SIDE;
			return i;
		}

		if (x >= x1 && x <= x2) {
			if (type) *type = GRAB_MIDDLE;
			return i;
		}
	}
	return -1;
}

static gint
position_from_point (GtkTPCList *tpclist, gint x, GrabType *type)
{
	GtkTPCListColumn *col;
	gint i;
	gint x1, x2;

	for (i = 0; i < tpclist->ncolumns; ++i) {
		col = &g_array_index (tpclist->columns, GtkTPCListColumn, i);

		x1 = col->x - tpclist->hadjustment->value;
		x2 = x1 + col->width;

		if (type &&
		    x >= x2 - PIXEL_PROXIMITY && x <= x2 + PIXEL_PROXIMITY) {
			*type = GRAB_SIDE;
			return i;
		}

		if (x >= x1 && x <= x2) {
			if (type) *type = GRAB_MIDDLE;
			return i;
		}
	}
	return -1;
}

static gint
row_from_point (GtkTPCList *tpclist, gint y)
{
	gfloat row_start;
	gint row;

	row_start = tpclist->vadjustment->value / tpclist->row_height;
	row = row_start + (gfloat) y / (gfloat) tpclist->row_height;

	if (row < 0 || row >= tpclist->nrows)
		row = -1;

	return row;
}

static void
point_from_row_column (GtkTPCList *tpclist, gint row, gint col,
		       gint *x, gint *y, gint *width, gint *height)
{
	if (x)
		*x = GTK_TPCLIST_POS (tpclist, col).x - tpclist->hadjustment->value;

	if (width)
		*width = GTK_TPCLIST_POS (tpclist, col).width;

	if (y)
		*y = tpclist->row_height *
			(row - tpclist->vadjustment->value / tpclist->row_height);
	
	if (height)
		*height = tpclist->row_height;
}

static gint
gtk_tpclist_col2pos (GtkTPCList *tpclist, gint column)
{
	gint i;

	if (column == -1)
		return -1;
	for (i = 0; i < tpclist->ncolumns; ++i)
		if (GTK_TPCLIST_POS2COL(tpclist, i) == column)
			break;
	if (i == tpclist->ncolumns) {
		g_warning ("col2pos: Column not found");
		return -1;
	}

	return i;
}

static gint
next_column (GtkTPCList *tpclist, gint column)
{
	gint pos, c;

	if (column == -1)
		return GTK_TPCLIST_POS2COL(tpclist, 0);
	pos = gtk_tpclist_col2pos (tpclist, column);
	c = GTK_TPCLIST_POS2COL(tpclist, MIN (pos + 1, tpclist->ncolumns - 1));

	return c;
}

static gint
prev_column (GtkTPCList *tpclist, gint column)
{
	gint pos, c;

	if (column == -1)
		return GTK_TPCLIST_POS2COL(tpclist, tpclist->ncolumns - 1);
	pos = gtk_tpclist_col2pos (tpclist, column);
	c = GTK_TPCLIST_POS2COL(tpclist, MAX (pos - 1, 0));

	return c;
}

static double start_x, cur_x;
static gint drag_column;
static gboolean b1_pressed;
static GrabType grabtype;

static void
column_window_button_press (GtkTPCList *tpclist, GdkEventButton *event)
{
	GtkWidget *widget;
	GdkCursor *cursor;
	gint x;

	widget = GTK_WIDGET (tpclist);

	drag_column = column_from_point (tpclist, event->x, &grabtype);
	if (drag_column < 0)
		return;

	if (event->button == 1 && !b1_pressed) {
		b1_pressed = TRUE;
		gtk_widget_get_pointer (widget, &x, NULL);
		start_x = cur_x = x;
		if (grabtype == GRAB_MIDDLE)
			cursor = gdk_cursor_new (GDK_GUMBY);
		else {
			cursor = gdk_cursor_new (GDK_SB_H_DOUBLE_ARROW);
			gdk_draw_line (widget->window, tpclist->xor_gc,
				       cur_x, 0, cur_x, widget->allocation.height);
		}
		gdk_pointer_grab (tpclist->column_window, FALSE,
				  GDK_POINTER_MOTION_HINT_MASK |
				  GDK_BUTTON1_MOTION_MASK |
				  GDK_BUTTON_RELEASE_MASK,
				  NULL, cursor, event->time);
		gtk_grab_add (widget);
		gdk_cursor_destroy (cursor);
	}
}

static void
column_window_motion_notify (GtkTPCList *tpclist, GdkEventMotion *event)
{
	GtkWidget *widget;
	gint new_x, x;

	widget = GTK_WIDGET (tpclist);

	if (b1_pressed) {
		if (event->is_hint)
			gtk_widget_get_pointer (widget, &new_x, NULL);
		else {
			gdk_window_get_position (tpclist->column_window, &x, NULL);
			new_x = event->x + x;
		}

		if (grabtype == GRAB_SIDE && cur_x != new_x) {
			gdk_draw_line (widget->window, tpclist->xor_gc,
				       cur_x, 0, cur_x, widget->allocation.height);
			cur_x = new_x;
			gdk_draw_line (widget->window, tpclist->xor_gc,
				       cur_x, 0, cur_x, widget->allocation.height);
		}
	}
}

static void
column_window_button_release (GtkTPCList *tpclist, GdkEventButton *event)
{
	GtkWidget *widget;
	gint delta_x;
	gint new_width;
	gint new_column;

	widget = GTK_WIDGET (tpclist);

	if (b1_pressed) {
		gdk_pointer_ungrab (event->time);
		gtk_grab_remove (widget);
		b1_pressed = FALSE;
		if (grabtype == GRAB_SIDE) {
			gdk_draw_line (widget->window, tpclist->xor_gc,
				       cur_x, 0, cur_x, widget->allocation.height);
			delta_x = cur_x - start_x;
			new_width = GTK_TPCLIST_COL (tpclist, drag_column).width + delta_x;
			if (new_width < MIN_COLUMN_WIDTH)
				return;
			gtk_tpclist_column_set_width (tpclist, drag_column, new_width);
		} else {
			new_column = column_from_point (tpclist, event->x, NULL);
			gtk_tpclist_column_swap (tpclist, drag_column, new_column);
		}
	}
}

static void
data_window_entry_activate (GtkWidget *entry, GtkTPCList *tpclist)
{
	gtk_tpclist_stop_edit (tpclist, TRUE);
}

static gint
data_window_entry_key_press (GtkWidget *entry, GdkEventKey *event, GtkTPCList *tpclist)
{
	if (event->keyval == GDK_Escape) {
		gtk_tpclist_stop_edit (tpclist, FALSE);
		return TRUE;
	}
	return FALSE;
}

static void
gtk_tpclist_start_edit (GtkTPCList *tpclist, gint row, gint column)
{
	const gchar *text;
	gint x, y;
	gint relx, rely, width, height;

	if (row < 0 || column < 0 || tpclist->entry)
		return;

	tpclist->entry = gtk_entry_new ();
	tpclist->entry_top = gtk_window_new (GTK_WINDOW_POPUP);

	tpclist->edit_row = tpclist->focus_row;
	tpclist->edit_column = tpclist->focus_column;

	text = gtk_tpclist_cell_get_value (tpclist, row, column);
	if (text)
		gtk_entry_set_text (GTK_ENTRY (tpclist->entry), text);

	gdk_window_get_deskrelative_origin (tpclist->data_window, &x, &y);
	point_from_row_column (tpclist, tpclist->focus_row, tpclist->focus_column,
			       &relx, &rely, &width, &height);
	x += relx;
	y += rely;

	gtk_signal_connect (GTK_OBJECT (tpclist->entry), "activate",
			    data_window_entry_activate, tpclist);
	gtk_signal_connect (GTK_OBJECT (tpclist->entry), "key_press_event",
			    GTK_SIGNAL_FUNC (data_window_entry_key_press), tpclist);
	gtk_widget_set_usize (tpclist->entry, width, height);
	gtk_container_add (GTK_CONTAINER (tpclist->entry_top), GTK_WIDGET (tpclist->entry));
	gtk_widget_show (tpclist->entry);
	gtk_widget_popup (tpclist->entry_top, x, y);

	gtk_widget_grab_focus (tpclist->entry);
	gdk_keyboard_grab (tpclist->entry->window, FALSE, GDK_CURRENT_TIME);
}

static void
gtk_tpclist_stop_edit (GtkTPCList *tpclist, gboolean accept)
{
	gchar *new_value;

	if (!tpclist->entry)
		return;
	
	if (accept) {
		new_value = g_strdup (gtk_entry_get_text (GTK_ENTRY (tpclist->entry)));
		gtk_signal_emit (GTK_OBJECT (tpclist), tpclist_signals[ACTIVATE_CELL],
				 tpclist->focus_row, tpclist->focus_column, &new_value);
	}

	gdk_keyboard_ungrab (GDK_CURRENT_TIME);
	gtk_widget_destroy (tpclist->entry_top);

	tpclist->entry_top = NULL;
	tpclist->entry = NULL;
}

static gint
gtk_tpclist_key_press (GtkWidget *widget, GdkEventKey *event)
{
	GtkTPCList *tpclist;
	GtkContainer *container;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	tpclist = GTK_TPCLIST (widget);
	container = GTK_CONTAINER (widget);
	
	if (tpclist->entry) {
		if (event->keyval == GDK_Escape)
			gtk_tpclist_stop_edit (tpclist, FALSE);
		else
			return gtk_widget_event (tpclist->entry, (GdkEvent *) event);
	}

	if (tpclist->focus_row >= 0 && tpclist->focus_column >= 0) {
		if (!tpclist->entry &&
		    (event->keyval == GDK_Return || event->keyval == GDK_KP_Enter) &&
		    event->state == 0) {
			gtk_tpclist_start_edit (tpclist, tpclist->focus_row,
						tpclist->focus_column);
			return FALSE;
		}
		gtk_signal_emit (GTK_OBJECT (tpclist), tpclist_signals[KEY_PRESS_CELL],
				 tpclist->focus_row, tpclist->focus_column, event);
	}

	if (GTK_WIDGET_HAS_FOCUS (widget)) {
		switch (event->keyval) {
		case GDK_Right:
			gtk_container_focus (container, GTK_DIR_RIGHT);
			break;

		case GDK_Left:
			gtk_container_focus (container, GTK_DIR_LEFT);
			break;

		case GDK_Up:
			gtk_container_focus (container, GTK_DIR_UP);
			break;

		case GDK_Down:
			gtk_container_focus (container, GTK_DIR_DOWN);
			break;

		case GDK_Home:
			if (event->state & GDK_CONTROL_MASK)
				gtk_signal_emit (GTK_OBJECT (tpclist),
						 tpclist_signals[FOCUS_CELL],
						 0,
						 tpclist->focus_column);
			else
				gtk_signal_emit (GTK_OBJECT (tpclist),
						 tpclist_signals[FOCUS_CELL],
						 tpclist->focus_row,
						 GTK_TPCLIST_POS2COL (tpclist, 0));
			break;

		case GDK_End:
			if (event->state & GDK_CONTROL_MASK)
				gtk_signal_emit (GTK_OBJECT (tpclist),
						 tpclist_signals[FOCUS_CELL],
						 tpclist->nrows - 1,
						 tpclist->focus_column);
			else
				gtk_signal_emit (GTK_OBJECT (tpclist),
						 tpclist_signals[FOCUS_CELL],
						 tpclist->focus_row,
						 GTK_TPCLIST_POS2COL (tpclist, tpclist->ncolumns - 1));
			break;

		default:
			return FALSE;
		}
		return TRUE;
	}
	
	return FALSE;
}

static void
data_button_press (GtkTPCList *tpclist, GdkEventButton *event)
{
	gint row, column;

	row = row_from_point (tpclist, event->y);
	column = position_from_point (tpclist, event->x, NULL);

	if (row < 0 || column < 0)
		return;

	if (tpclist->focus_row != row || tpclist->focus_column != column)
		gtk_signal_emit (GTK_OBJECT (tpclist), tpclist_signals[FOCUS_CELL],
				 row, column);
	else
		gtk_tpclist_start_edit (tpclist, tpclist->focus_row, tpclist->focus_column);
}

static void
row_button_press (GtkTPCList *tpclist, GdkEventButton *event)
{
	gint row;
	gboolean prev_state;

	row = row_from_point (tpclist, event->y);
	if (row < 0)
		return;

	prev_state = gtk_tpclist_row_is_selected (tpclist, row);
	if (!(event->state & GDK_CONTROL_MASK))
		gtk_tpclist_row_unselect_all (tpclist);
	gtk_tpclist_row_set_selected (tpclist, row, !prev_state);
}

static gint
gtk_tpclist_button_press (GtkWidget *widget, GdkEventButton *event)
{
	GtkTPCList *tpclist;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	tpclist = GTK_TPCLIST (widget);
	gtk_widget_grab_focus (widget);

	if (tpclist->entry)
		gtk_tpclist_stop_edit (tpclist, FALSE);

	if (event->window == tpclist->column_window)
		column_window_button_press (tpclist, event);
	else if (event->window == tpclist->data_window)
		data_button_press (tpclist, event);
	else if (event->window == tpclist->row_window)
		row_button_press (tpclist, event);

	return FALSE;
}

static gint
gtk_tpclist_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
	GtkTPCList *tpclist;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	tpclist = GTK_TPCLIST (widget);

	if (event->window == tpclist->column_window)
		column_window_motion_notify (tpclist, event);

	return FALSE;
}

static gint
gtk_tpclist_button_release (GtkWidget *widget, GdkEventButton *event)
{
	GtkTPCList *tpclist;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	tpclist = GTK_TPCLIST (widget);

	if (event->window == tpclist->column_window)
		column_window_button_release (tpclist, event);

	return FALSE;
}

static void
gtk_tpclist_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	GtkTPCList *tpclist;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (widget));
	g_return_if_fail (requisition != NULL);

	tpclist = GTK_TPCLIST (widget);
	requisition->width = 10;
	requisition->height = 10;
}

static void
gtk_tpclist_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
	GtkTPCList *tpclist;
	GdkRectangle wa;
	gint border_width;
	gint height_min;
	gint row_height;
	gint column_label_height;
	gint i, sum;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (widget));
	g_return_if_fail (allocation != NULL);

	tpclist = GTK_TPCLIST (widget);

	widget->allocation = *allocation;
	border_width = GTK_CONTAINER (widget)->border_width;
	wa.x = allocation->x + border_width;
	wa.y = allocation->y + border_width;
	wa.width = allocation->width - border_width * 2;
	wa.height = allocation->height - border_width * 2;

	/* Setup row and column windows */
	height_min = widget->style->font->ascent + widget->style->font->descent + 2;
	if (tpclist->row_height < height_min)
		tpclist->row_height = height_min;
	if (tpclist->column_label_height < height_min)
		tpclist->column_label_height = height_min;

	row_height = tpclist->row_height;
	column_label_height = tpclist->column_label_height;

	tpclist->row_window_area.x = 0;
	tpclist->row_window_area.y = column_label_height;
	tpclist->row_window_area.width = tpclist->row_label_width;
	tpclist->row_window_area.height = wa.height - column_label_height;
	if (tpclist->row_window_area.height > tpclist->nrows * tpclist->row_height)
		tpclist->row_window_area.height = tpclist->nrows * tpclist->row_height;

	tpclist->column_window_area.x = tpclist->row_window_area.width;
	tpclist->column_window_area.y = 0;
	tpclist->column_window_area.width = wa.width - tpclist->row_window_area.width;
	tpclist->column_window_area.height = column_label_height;

	for (sum = 0, i = 0; i < tpclist->ncolumns; ++i) {
		GTK_TPCLIST_COL (tpclist, i).x = sum;
		sum += GTK_TPCLIST_COL (tpclist, i).width;
	}

	tpclist->data_window_area.x = tpclist->column_window_area.x;
	tpclist->data_window_area.y = column_label_height;
	tpclist->data_window_area.width = MIN (sum, tpclist->column_window_area.width);
	tpclist->data_window_area.height = tpclist->row_window_area.height;

	if (GTK_WIDGET_REALIZED (widget)) {
		gdk_window_move_resize (widget->window, wa.x, wa.y, wa.width, wa.height);

		gdk_window_move_resize (tpclist->row_window,
					tpclist->row_window_area.x,
					tpclist->row_window_area.y,
					tpclist->row_window_area.width,
					tpclist->row_window_area.height);

		gdk_window_move_resize (tpclist->column_window,
					tpclist->column_window_area.x,
					tpclist->column_window_area.y,
					tpclist->column_window_area.width,
					tpclist->column_window_area.height);

		gdk_window_move_resize (tpclist->data_window,
					tpclist->data_window_area.x,
					tpclist->data_window_area.y,
					tpclist->data_window_area.width,
					tpclist->data_window_area.height);

		gtk_tpclist_make_backing_store (tpclist);

		tpclist->vadjustment->lower = 0;
		tpclist->vadjustment->upper = tpclist->nrows * tpclist->row_height;
		tpclist->vadjustment->page_size = MIN ((tpclist->row_window_area.height + .5),
						       tpclist->nrows * tpclist->row_height);
		tpclist->vadjustment->page_increment = tpclist->vadjustment->page_size / 2;
		tpclist->vadjustment->step_increment = tpclist->row_height;
		gtk_adjustment_changed (tpclist->vadjustment);
		if (tpclist->vadjustment->value + tpclist->row_window_area.height >
		    tpclist->nrows * tpclist->row_height)
			gtk_adjustment_set_value (tpclist->vadjustment,
						  tpclist->nrows * tpclist->row_height -
						  tpclist->row_window_area.height);

		tpclist->hadjustment->lower = 0;
		tpclist->hadjustment->upper = sum;
		tpclist->hadjustment->page_size = tpclist->column_window_area.width;
		tpclist->hadjustment->page_increment = tpclist->hadjustment->page_size / 2;
		tpclist->hadjustment->step_increment = sum / tpclist->ncolumns;
		gtk_adjustment_changed (tpclist->hadjustment);
		if (tpclist->hadjustment->value > sum - tpclist->column_window_area.width)
			gtk_adjustment_set_value (tpclist->hadjustment,
						  sum - tpclist->column_window_area.width);
	}
}

static void
gtk_tpclist_draw_focus (GtkWidget *widget)
{
	GtkTPCList *tpclist;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (widget));

	if (!GTK_WIDGET_DRAWABLE (widget))
		return;

	tpclist = GTK_TPCLIST (widget);
	tpclist->row_window_dirty = TRUE;
	tpclist->data_window_dirty = TRUE;
	gtk_widget_queue_clear (widget);
}

static gint
gtk_tpclist_focus_in (GtkWidget *widget, GdkEventFocus *event)
{
	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	if (GTK_TPCLIST (widget)->entry)
		return FALSE;
	GTK_WIDGET_SET_FLAGS (widget, GTK_HAS_FOCUS);
	gtk_widget_draw_focus (widget);

	return FALSE;
}

static gint
gtk_tpclist_focus_out (GtkWidget *widget, GdkEventFocus *event)
{
	GtkTPCList *tpclist;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (widget), FALSE);
	g_return_val_if_fail (event != NULL, FALSE);

	tpclist = GTK_TPCLIST (widget);
	GTK_WIDGET_UNSET_FLAGS (widget, GTK_HAS_FOCUS);
	if (tpclist->entry)
		gtk_tpclist_stop_edit (tpclist, FALSE);
	gtk_widget_draw_focus (widget);

	return FALSE;
}

static gint
gtk_tpclist_focus (GtkContainer *container, GtkDirectionType direction)
{
	GtkTPCList *tpclist;
	GtkWidget *widget;

	g_return_val_if_fail (container != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (container), FALSE);

	if (!GTK_WIDGET_SENSITIVE (container))
		return FALSE;

	tpclist = GTK_TPCLIST (container);
	widget = GTK_WIDGET (container);

	switch (direction) {
	case GTK_DIR_TAB_FORWARD:
	case GTK_DIR_TAB_BACKWARD:
		return FALSE;

	case GTK_DIR_RIGHT:
		gtk_signal_emit (GTK_OBJECT (tpclist),
				 tpclist_signals[FOCUS_CELL],
				 tpclist->focus_row,
				 next_column (tpclist, tpclist->focus_column));
		break;

	case GTK_DIR_LEFT:
		gtk_signal_emit (GTK_OBJECT (tpclist),
				 tpclist_signals[FOCUS_CELL],
				 tpclist->focus_row,
				 prev_column (tpclist, tpclist->focus_column));
		break;

	case GTK_DIR_UP:
		gtk_signal_emit (GTK_OBJECT (tpclist),
				 tpclist_signals[FOCUS_CELL],
				 MAX (tpclist->focus_row - 1, 0),
				 tpclist->focus_column);
		break;

	case GTK_DIR_DOWN:
		gtk_signal_emit (GTK_OBJECT (tpclist),
				 tpclist_signals[FOCUS_CELL],
				 MIN (tpclist->focus_row + 1, tpclist->nrows - 1),
				 tpclist->focus_column);
		break;
	}

	return TRUE;
}

static void
gtk_tpclist_hadj_value_changed (GtkAdjustment *adj, GtkTPCList *tpclist)
{
	GtkWidget *widget;

	g_return_if_fail (adj != NULL);
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_ADJUSTMENT (adj));
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	widget = GTK_WIDGET (tpclist);
	if (GTK_WIDGET_VISIBLE (widget)) {
		tpclist->column_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_clear (widget);
	}
}

static void
gtk_tpclist_vadj_value_changed (GtkAdjustment *adj, GtkTPCList *tpclist)
{
	GtkWidget *widget;

	g_return_if_fail (adj != NULL);
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_ADJUSTMENT (adj));
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	widget = GTK_WIDGET (tpclist);
	if (GTK_WIDGET_VISIBLE (widget)) {
		tpclist->row_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_clear (widget);
	}
}

void
gtk_tpclist_column_add (GtkTPCList *tpclist, const gchar *name,
			GtkTPCListValueFunc value_func, gpointer user_data)
{
	GtkTPCListColumn c;
	gint pos;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	c.name = g_string_new (name);
	c.x = 0;
	c.width = DEFAULT_COLUMN_WIDTH;
	c.rows = g_array_new (FALSE, FALSE, sizeof (GtkTPCListCell));
	c.value_func = value_func;
	c.value_func_data = user_data;
	g_array_append_val (tpclist->columns, c);

	pos = tpclist->ncolumns++;
	g_array_append_val (tpclist->column_index, pos);

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->column_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_column_swap (GtkTPCList *tpclist, gint col1, gint col2)
{
	gint tmp;
	
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	if (col1 < 0 || col1 >= tpclist->ncolumns ||
	    col2 < 0 || col2 >= tpclist->ncolumns ||
	    col1 == col2)
		return;

	tmp = GTK_TPCLIST_POS2COL (tpclist, col1);
	GTK_TPCLIST_POS2COL (tpclist, col1) = GTK_TPCLIST_POS2COL (tpclist, col2);
	GTK_TPCLIST_POS2COL (tpclist, col2) = tmp;

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->column_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

gchar *
gtk_tpclist_preserve_column_state (GtkTPCList *tpclist)
{
	GString *s;
	gint i;
	char *rv, tmp[64];

	g_return_val_if_fail (tpclist != NULL, NULL);
	g_return_val_if_fail (GTK_IS_TPCLIST (tpclist), NULL);

	s = g_string_new ("");
	for (i = 0; i < tpclist->ncolumns; ++i) {
		sprintf (tmp, "%d:%d",
			 GTK_TPCLIST_POS2COL (tpclist, i),
			 GTK_TPCLIST_COL (tpclist, i).width);
		g_string_append (s, tmp);
		if (i < tpclist->ncolumns - 1)
			g_string_append (s, " ");
	}
	rv = s->str;
	g_string_free (s, FALSE);
	return rv;
}

void
gtk_tpclist_restore_column_state (GtkTPCList *tpclist, gchar *order)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));
}

void
gtk_tpclist_row_set_label_width (GtkTPCList *tpclist, gint width)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	tpclist->row_label_width = width;

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->column_window_dirty =
			tpclist->row_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_column_set_label_height (GtkTPCList *tpclist, gint height)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	tpclist->column_label_height = height;

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->column_window_dirty =
			tpclist->row_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_column_set_width (GtkTPCList *tpclist, gint column, gint width)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	if (column < 0 || column >= tpclist->ncolumns)
		return;

	GTK_TPCLIST_COL (tpclist, column).width = width;

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->column_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
		gtk_widget_queue_clear (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_row_set_data (GtkTPCList *tpclist, gint row, gpointer data)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));
	g_return_if_fail (row >= 0 && row < tpclist->nrows);

	g_array_index (tpclist->row_data, gpointer, row) = data;
}

gpointer
gtk_tpclist_row_get_data (GtkTPCList *tpclist, gint row)
{
	g_return_val_if_fail (tpclist != NULL, NULL);
	g_return_val_if_fail (GTK_IS_TPCLIST (tpclist), NULL);
	g_return_val_if_fail (row >= 0 && row < tpclist->nrows, NULL);

	return g_array_index (tpclist->row_data, gpointer, row);
}

gint
gtk_tpclist_row_get_from_data (GtkTPCList *tpclist, gpointer data)
{
	gint row;

	g_return_val_if_fail (tpclist != NULL, -1);
	g_return_val_if_fail (GTK_IS_TPCLIST (tpclist), -1);

	for (row = 0; row < tpclist->nrows; ++row)
		if (gtk_tpclist_row_get_data (tpclist, row) == data)
			return row;
	return -1;
}

gint
gtk_tpclist_row_append (GtkTPCList *tpclist)
{
	gint pos;

	g_return_val_if_fail (tpclist != NULL, 0);
	g_return_val_if_fail (GTK_IS_TPCLIST (tpclist), 0);

	pos = tpclist->nrows;
	gtk_tpclist_row_insert (tpclist, pos);

	return pos;
}

void
gtk_tpclist_row_insert (GtkTPCList *tpclist, gint position)
{
	GtkTPCListColumn *column;
	GtkTPCListCell cell;
	GSList *slist;
	gpointer null = NULL;
	guint8 z8 = 0;
	gint i;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	++tpclist->nrows;
	for (i = 0; i < tpclist->ncolumns; ++i) {
		column = &GTK_TPCLIST_COL (tpclist, i);
		if (column->value_func)
			cell.value = (* column->value_func) (
				tpclist, position, i, column->value_func_data);
		else
			cell.value = NULL;
		g_array_insert_val (column->rows, position, cell);
	}
	g_array_insert_val (tpclist->row_data, position, null);
	g_array_insert_val (tpclist->row_flags, position, z8);
	for (slist = tpclist->selected_rows; slist; slist = slist->next)
		if ((gint) slist->data >= position)
			++((gint) slist->data);

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->row_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_row_remove (GtkTPCList *tpclist, gint position)
{
	GtkTPCListColumn *column;
	GtkTPCListCell *cell;
	GSList *slist;
	gint i;
	
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	if (tpclist->nrows <= 0 || position >= tpclist->nrows)
		return;

	if (tpclist->focus_row == --tpclist->nrows)
		gtk_signal_emit (GTK_OBJECT (tpclist),
				 tpclist_signals[FOCUS_CELL],
				 tpclist->focus_row - 1,
				 tpclist->focus_column);

	if (gtk_tpclist_row_is_selected (tpclist, position))
		unselect_row (position, tpclist);

	g_array_remove_index (tpclist->row_flags, position);
	g_array_remove_index (tpclist->row_data, position);
	for (i = 0; i < tpclist->ncolumns; ++i) {
		column = &GTK_TPCLIST_COL (tpclist, i);
		cell = &g_array_index (column->rows, GtkTPCListCell, position);
		g_free (cell->value);
		g_array_remove_index (column->rows, position);
	}
	for (slist = tpclist->selected_rows; slist; slist = slist->next)
		if ((gint) slist->data > position)
			--((gint) slist->data);

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->row_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_row_remove_all (GtkTPCList *tpclist)
{
	gint i, j;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	g_slist_free (tpclist->selected_rows);
	tpclist->selected_rows = NULL;
	for (j = 0; j < tpclist->ncolumns; ++j) {
		for (i = 0; i < tpclist->nrows; ++i) {
			gchar *value = g_array_index (GTK_TPCLIST_POS (tpclist, j).rows,
						      GtkTPCListCell, i).value;
			if (value) g_free (value);
		}
		g_array_free (GTK_TPCLIST_POS (tpclist, j).rows, TRUE);
		GTK_TPCLIST_POS (tpclist, j).rows =
			g_array_new (FALSE, FALSE, sizeof (GtkTPCListCell));
	}
	g_array_free (tpclist->row_data, TRUE);
	g_array_free (tpclist->row_flags, TRUE);
	tpclist->row_data = g_array_new (FALSE, FALSE, sizeof (gpointer));
	tpclist->row_flags = g_array_new (FALSE, FALSE, sizeof (guint8));
	tpclist->focus_row = -1;
	tpclist->nrows = 0;

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->row_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_row_set_height (GtkTPCList *tpclist, gint height)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	tpclist->row_height = height;

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->row_window_dirty =
			tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
	}
}

static void
select_row (gint row, GtkTPCList *tpclist)
{
	GTK_TPCLIST_ROW_FLAGS (tpclist, row) |= ROW_SELECTED;
	tpclist->selected_rows = g_slist_prepend (tpclist->selected_rows, GINT_TO_POINTER (row));
}

static void
unselect_row (gint row, GtkTPCList *tpclist)
{
	GTK_TPCLIST_ROW_FLAGS (tpclist, row) &= ~ROW_SELECTED;
	tpclist->selected_rows = g_slist_remove (tpclist->selected_rows, GINT_TO_POINTER (row));
}

void
gtk_tpclist_row_select_all (GtkTPCList *tpclist)
{
	gint row;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	for (row = 0; row < tpclist->nrows; ++row) {
		if (!gtk_tpclist_row_is_selected (tpclist, row))
			select_row (row, tpclist);
	}

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->row_window_dirty = TRUE;
		gtk_widget_queue_clear (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_row_unselect_all (GtkTPCList *tpclist)
{
	GSList *slist;
	gint row;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	for (slist = tpclist->selected_rows; slist;) {
		row = GPOINTER_TO_INT (slist->data);
		slist = slist->next;
		unselect_row (row, tpclist);
	}

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->row_window_dirty = TRUE;
		gtk_widget_queue_clear (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_row_set_selected (GtkTPCList *tpclist, gint row, gboolean selected)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	if ((selected && gtk_tpclist_row_is_selected (tpclist, row)) ||
	    (!selected && !gtk_tpclist_row_is_selected (tpclist, row)))
		return;

	if (selected)
		select_row (row, tpclist);
	else
		unselect_row (row, tpclist);

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->row_window_dirty = TRUE;
		gtk_widget_queue_clear (GTK_WIDGET (tpclist));
	}
}

void
gtk_tpclist_row_toggle_selected (GtkTPCList *tpclist, gint row)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	gtk_tpclist_row_set_selected (
		tpclist, row,
		!gtk_tpclist_row_is_selected (tpclist, row));
}

gboolean
gtk_tpclist_row_is_selected (GtkTPCList *tpclist, gint row)
{
	g_return_val_if_fail (tpclist != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_TPCLIST (tpclist), FALSE);

	return GTK_TPCLIST_ROW_FLAGS (tpclist, row) & ROW_SELECTED ? TRUE : FALSE;
}

void
gtk_tpclist_cell_set_value (GtkTPCList *tpclist, gint row,
			    gint column, const gchar *value)
{
	gchar *new_value;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));
	g_return_if_fail (row >= 0 && row < tpclist->nrows);
	g_return_if_fail (column >= 0 && column < tpclist->ncolumns);

	new_value = value ? g_strdup (value) : NULL;
	gtk_signal_emit (GTK_OBJECT (tpclist), tpclist_signals[ACTIVATE_CELL],
			 row, column, &new_value);
}

const gchar *
gtk_tpclist_cell_get_value (GtkTPCList *tpclist, gint row, gint column)
{
	GtkTPCListColumn *col;
	GtkTPCListCell *cell;

	g_return_val_if_fail (tpclist != NULL, NULL);
	g_return_val_if_fail (GTK_IS_TPCLIST (tpclist), NULL);
	g_return_val_if_fail (row >= 0 && row < tpclist->nrows, NULL);
	g_return_val_if_fail (column >= 0 && column < tpclist->ncolumns, NULL);

	col = &GTK_TPCLIST_POS (tpclist, column);
	cell = &g_array_index (col->rows, GtkTPCListCell, row);

	return cell->value ? cell->value : "";
}

static void
gtk_tpclist_set_scroll_adjustments (GtkTPCList *tpclist, GtkAdjustment *hadjustment,
				    GtkAdjustment *vadjustment)
{
	gboolean resize = FALSE;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	if (hadjustment && tpclist->hadjustment != hadjustment) {
		gtk_signal_disconnect_by_data (GTK_OBJECT (tpclist->hadjustment), tpclist);
		gtk_object_unref (GTK_OBJECT (tpclist->hadjustment));
		tpclist->hadjustment = hadjustment;
		if (hadjustment) {
			gtk_object_ref (GTK_OBJECT (hadjustment));
			gtk_object_sink (GTK_OBJECT (hadjustment));
			gtk_signal_connect (GTK_OBJECT (hadjustment), "value_changed",
					    GTK_SIGNAL_FUNC (gtk_tpclist_hadj_value_changed),
					    tpclist);
		}
		resize = TRUE;
	}

	if (vadjustment && tpclist->vadjustment != vadjustment) {
		gtk_signal_disconnect_by_data (GTK_OBJECT (tpclist->vadjustment), tpclist);
		gtk_object_unref (GTK_OBJECT (tpclist->vadjustment));
		tpclist->vadjustment = vadjustment;
		if (vadjustment) {
			gtk_object_ref (GTK_OBJECT (vadjustment));
			gtk_object_sink (GTK_OBJECT (vadjustment));
			gtk_signal_connect (GTK_OBJECT (vadjustment), "value_changed",
					    GTK_SIGNAL_FUNC (gtk_tpclist_vadj_value_changed),
					    tpclist);
		}
		resize = TRUE;
	}

	if (resize)
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
}

static void
gtk_tpclist_real_activate_cell (GtkTPCList *tpclist, gint row, gint column, gchar **value)
{
	GtkTPCListColumn *col;
	GtkTPCListCell *cell;

	g_return_if_fail (value != NULL);

	col = &GTK_TPCLIST_POS (tpclist, column);
	cell = &g_array_index (col->rows, GtkTPCListCell, row);

	if (cell->value)
		g_free (cell->value);

	cell->value = *value ? g_strdup (*value) : NULL;

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist))) {
		tpclist->data_window_dirty = TRUE;
		gtk_widget_queue_clear (GTK_WIDGET (tpclist));
	}

	g_free (*value);
}

void
gtk_tpclist_cell_set_focus (GtkTPCList *tpclist, gint row, gint column)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	gtk_signal_emit (GTK_OBJECT (tpclist), tpclist_signals[FOCUS_CELL], row, column);
}

void
gtk_tpclist_cell_make_viewable (GtkTPCList *tpclist, gint row, gint column)
{
	gboolean changed = FALSE;
	gint pos;
	GtkTPCListColumn *col;

	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	if ((row + 1) * tpclist->row_height >
	    tpclist->vadjustment->value + tpclist->data_window_area.height) {
		gtk_adjustment_set_value (tpclist->vadjustment,
					  (row + 1) * tpclist->row_height -
					  tpclist->data_window_area.height);
		changed = TRUE;
	}
	if (row * tpclist->row_height <= tpclist->vadjustment->value) {
		gtk_adjustment_set_value (tpclist->vadjustment, row * tpclist->row_height);
		changed = TRUE;
	}

	pos = gtk_tpclist_col2pos (tpclist, column);
	col = &GTK_TPCLIST_COL (tpclist, pos);
	if (col->x + col->width >= tpclist->hadjustment->value +
	    tpclist->data_window_area.width) {
		gtk_adjustment_set_value (tpclist->hadjustment,
					  col->x + col->width -
					  tpclist->data_window_area.width);
		changed = TRUE;
	}
	if (col->x < tpclist->hadjustment->value) {
		gtk_adjustment_set_value (tpclist->hadjustment, col->x);
		changed = TRUE;
	}

	if (GTK_WIDGET_VISIBLE (GTK_WIDGET (tpclist)) && changed)
		gtk_widget_queue_resize (GTK_WIDGET (tpclist));
}

static void
gtk_tpclist_real_focus_cell (GtkTPCList *tpclist, gint row, gint column)
{
	g_return_if_fail (tpclist != NULL);
	g_return_if_fail (GTK_IS_TPCLIST (tpclist));

	if (tpclist->focus_row == row &&
	    tpclist->focus_column == column)
		return;

	tpclist->focus_row = row;
	tpclist->focus_column = column;
	gtk_tpclist_cell_make_viewable (tpclist, row, column);
	gtk_widget_draw_focus (GTK_WIDGET (tpclist));
}
