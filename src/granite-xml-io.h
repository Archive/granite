/**************************************************************************

    granite-xml-io.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __GRANITE_XML_IO_H
#define __GRANITE_XML_IO_H

#include <gnome-xml/tree.h>
#include <gnome-xml/parser.h>

typedef struct {
	xmlDocPtr doc;
	xmlNsPtr ns;

	/* Score Link Resolution */
	guint clip_id;
	GHashTable *clip_ht;
	guint pattern_id;
	GHashTable *pattern_ht;
} XMLContext;

typedef struct {
	XMLContext *xc;
	gpointer data;
} XMLContextData;

extern gboolean		granite_xml_save_state		(Granite *granite,
							 const gchar *filename);
extern gboolean		granite_xml_load_state		(Granite *granite,
							 const gchar *filename);

/* Utility functions */
extern gint		xmlGetInt			(xmlNodePtr node,
							 const gchar *name);
extern xmlNodePtr	xmlSearchChild			(xmlNodePtr node,
							 const gchar *name);

#endif /* __GRANITE_XML_IO_H */
