/**************************************************************************

    main.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <sched.h>
#include <sys/mman.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "sequencer.h"
#include "granite.h"

enum {
        MUSIC_DEVICE_KEY = -1,
	REALTIME_KEY = -2
};

gboolean realtime = FALSE;

static gchar *music_device;

static void parse_an_arg (poptContext ctx, enum poptCallbackReason reason,
			  const struct poptOption *opt, const char *arg,
			  void *data)
{
        switch (opt->val) {
        case MUSIC_DEVICE_KEY:
                music_device = g_strdup (arg);
		break;

	case REALTIME_KEY:
		realtime = TRUE;
		break;
        }
}

static struct poptOption options[] = {
	{NULL, '\0', POPT_ARG_CALLBACK, &parse_an_arg, 0,NULL, NULL},
	{"music-device", '\0', POPT_ARG_STRING, NULL,
	 MUSIC_DEVICE_KEY, N_("OSS music device"), N_("DEVICE")},
	{"realtime", '\0', POPT_ARG_NONE, NULL,
	 REALTIME_KEY, N_("Set realtime scheduling"), NULL},
	{NULL, '\0', 0, NULL, 0}
};

int main (int argc, char *argv[])
{
	CORBA_ORB orb;
	CORBA_Environment ev;
	Granite *granite;

	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
        textdomain (PACKAGE);

	g_thread_init (NULL);
	if (!g_thread_supported ())
		g_error ("Granite requires thread support");

	CORBA_exception_init(&ev);
	music_device = NULL;
	orb = gnome_CORBA_init_with_popt_table ("Granite", VERSION, &argc, argv, options, 0,
						NULL, GNORBA_INIT_SERVER_FUNC, &ev);
	CORBA_exception_free(&ev);

	if (realtime) {		
		struct sched_param sp;
		memset (&sp, 0, sizeof (sp));
		sp.sched_priority = sched_get_priority_max (SCHED_RR);
		if (sched_setscheduler (0, SCHED_RR, &sp) < 0)
			perror ("sched_setscheduler");
		if (mlockall (MCL_CURRENT) < 0)
			perror ("mlockall");
	}

	granite = granite_new (music_device, argc == 2 ? argv[1] : NULL);
	gtk_main ();
	granite_prefs_save (&granite->prefs);
	gnome_config_drop_all ();

	return 0;
}
