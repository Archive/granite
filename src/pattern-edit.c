/**************************************************************************

    pattern-edit.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <gnome.h>
#include <gdk/gdkkeysyms.h>
#include "pattern-edit.h"
#include "pattern-roll.h"
#include "piano-roll-edit.h"
#include "time-label.h"
#include "Granite.h"

#include "pixmaps/record.xpm"
#include "pixmaps/zoom_in.xpm"
#include "pixmaps/zoom_out.xpm"
#include "pixmaps/load_selection.xpm"

static void pattern_edit_class_init (PatternEditClass *klass);
static void pattern_edit_init (PatternEdit *obj);
static void pattern_edit_finalize (GtkObject *obj);
static GtkWidget *pattern_edit_create_view (GnomeMDIChild *child, gpointer user_data);
static void granite_set_track_echo_mode (PatternEdit *pe, ScoreTrack *track);

GtkType
pattern_edit_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"PatternEdit",
			sizeof (PatternEdit),
			sizeof (PatternEditClass),
			(GtkClassInitFunc) pattern_edit_class_init,
			(GtkObjectInitFunc) pattern_edit_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GRANITE_TYPE_MDI_CHILD, &type_info);
	}

	return type;
}

static GraniteMDIChildClass *parent_class = NULL;

static void
pattern_edit_class_init (PatternEditClass *klass)
{
	GtkObjectClass *object_class;
	GnomeMDIChildClass *child_class;

	parent_class = gtk_type_class (granite_mdi_child_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = pattern_edit_finalize;

	child_class = GNOME_MDI_CHILD_CLASS (klass);
	child_class->create_view = pattern_edit_create_view;

	klass->modify_menu = NULL;
}

static void
pattern_edit_init (PatternEdit *pattern_edit)
{
	pattern_edit->pattern = NULL;
}

static guint
pattern_edit_load_score_events (ScoreEvents *se, ScoreTime offset_time,
					ScoreTime from_time, ScoreTime to_time,
					PatternEdit *pattern_edit)
{
	guint num = 0;

	debugf ("Pattern load score events: offset time %d, from %d to %d",
		offset_time, from_time, to_time);

	num = score_pattern_load_event_range (se, offset_time, from_time,
					      to_time, pattern_edit->pattern);

	return num;
}

#if 0
static ScoreTempo
pattern_edit_tempo_func (ScoreTime time, PatternEdit *pe)
{
	Score *score;
	ScoreTempo tempo;
	ScoreTime tmp;

	debugf ("Pattern edit get tempo func at %d", time);
	tmp = time;
	tempo = score_pattern_get_tempo_at_time (pe->pattern, &tmp);
	if (tmp < 0) {
		score = GRANITE_MDI_CHILD (pe)->granite->score;
		tmp = time;
		tempo = score_get_tempo_at_time (score, &tmp);
		if (tmp < 0) {
			g_warning ("Pattern edit tempo couldn't find tempo");
			return 99;
		}
	}

	return tempo;
}

static void
pattern_edit_meter_func (ScoreTime time, ScoreMeter *sm, PatternEdit *pe)
{
	Score *score;
	ScoreTime tmp;

	debugf ("Pattern edit get meter func at %d", time);
	tmp = time;
	score_pattern_get_meter_at_time (pe->pattern, sm, &tmp);
	if (tmp < 0) {
		score = GRANITE_MDI_CHILD (pe)->granite->score;
		tmp = time;
		score_get_meter_at_time (score, sm, &tmp);
		if (tmp < 0) {
			g_warning ("Pattern edit tempo couldn't find meter");
			sm->N = sm->D = 4;
		}
	}
}
#endif

static void
pattern_edit_set_name (PatternEdit *pe, const gchar *name)
{
	GString *s;

	s = g_string_new (NULL);
	g_string_sprintf (s, "%s - %s", _("Pattern Edit"), name);
	gnome_mdi_child_set_name (GNOME_MDI_CHILD (pe), s->str);
	g_string_free (s, TRUE);
}

GnomeMDIChild *
pattern_edit_new (Granite *granite, ScorePattern *sp)
{
	GnomeMDIChild *child = GNOME_MDI_CHILD (gtk_type_new (TYPE_PATTERN_EDIT));
	GraniteMDIChild *granite_child;

	granite_child = GRANITE_MDI_CHILD (child);

	granite_mdi_child_construct (granite_child, granite);

	PATTERN_EDIT (child)->pattern = sp;
	score_pattern_ref (sp);

	sequencer_gtk_peer_master_construct (&granite_child->gtkpeer,
					     &granite_child->granite->sequencer);

	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.load_func =
		(SequencerLoadEventsFunc) pattern_edit_load_score_events;
#if 0
	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.tempo_func =
		(SequencerTempoFunc) pattern_edit_tempo_func;
	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.meter_func =
		(SequencerMeterFunc) pattern_edit_meter_func;
#endif
	SEQUENCER_PEER (&granite_child->gtkpeer)->
		u.Master.saved_state.func_user_data = child;

	return child;
}

static void
pattern_edit_finalize (GtkObject *obj)
{
	PatternEdit *pe;
	GtkObject *se;

	se = GRANITE_MDI_CHILD (obj)->granite->score_edit;
	pe = PATTERN_EDIT (obj);

	gtk_signal_disconnect_by_data (GTK_OBJECT (se), pe);
	pe->pattern->user_data = NULL;
	score_pattern_unref (pe->pattern);

        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

#if 0
static void
pattern_edit_update_duration (PatternEdit *pe)
{
	ScoreTime ticks_per_measure;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));
	g_return_if_fail (pe->pattern != NULL);

	ticks_per_measure = sequencer_get_ticks_per_measure (
		SEQUENCER_MASTER (&GRANITE_MDI_CHILD (pe)->gtkpeer)->sequencer);
}

static void
pattern_edit_set_duration (PatternEdit *pe, ScoreTime duration)
{
	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));
	g_return_if_fail (pe->pattern != NULL);

	pe->pattern->duration = duration;
	pattern_edit_update_duration (pe);
}
#endif

static void
play_cmd (GtkWidget *widget, PatternEdit *pe)
{
	SequencerGtkPeer *gtkpeer;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	gtkpeer = &GRANITE_MDI_CHILD (pe)->gtkpeer;
	sequencer_gtk_peer_play (gtkpeer);
}

static void
record_cmd (GtkWidget *widget, PatternEdit *pe)
{
	SequencerGtkPeer *gtkpeer;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	gtkpeer = &GRANITE_MDI_CHILD (pe)->gtkpeer;
	sequencer_peer_record (SEQUENCER_PEER (gtkpeer), TRUE);
	sequencer_gtk_peer_play (gtkpeer);
}

static void
stop_cmd (GtkWidget *widget, PatternEdit *pe)
{
	Granite *granite;
	PatternRoll *pr;
	SequencerGtkPeer *gtkpeer;
	SequencerMaster *master;
	ScoreTrack *track;
	ScoreEvents *sev;
	ScoreClip *new_clip;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	granite = GRANITE_MDI_CHILD (pe)->granite;
	gtkpeer = &GRANITE_MDI_CHILD (pe)->gtkpeer;
	master = &SEQUENCER_PEER (gtkpeer)->u.Master;
	pr = PATTERN_ROLL (pe->pr);

	sequencer_gtk_peer_stop (gtkpeer);
	sequencer_gtk_peer_goto (gtkpeer, master->sequencer->start_time);

	if (master->sequencer->recording) {
		sequencer_peer_record (SEQUENCER_PEER (gtkpeer), FALSE);
		sev = sequencer_get_events_recorded (master->sequencer);
		if (!sev) {
			debugf ("No events from record!");
			return;
		}
		track = pattern_roll_get_current_track (pr, NULL);
		if (sev->size == 0 || !track) {
			score_events_destroy (sev);
			g_blow_chunks ();
			return;
		}
		new_clip = score_clip_midi_new (sev);
		pattern_roll_track_insert_clip (pr, track, new_clip, 0);
	}
	g_blow_chunks ();
}

static void
rewind_cmd (GtkWidget *widget, PatternEdit *pe)
{
	Granite *granite;
	SequencerGtkPeer *gtkpeer;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	granite = GRANITE_MDI_CHILD (pe)->granite;
	gtkpeer = &GRANITE_MDI_CHILD (pe)->gtkpeer;

	sequencer_gtk_peer_goto (gtkpeer, 0);
}

static void
toggle_loop_mode_cmd (GtkWidget *widget, PatternEdit *pe)
{
	Sequencer *sequencer;
	GnomeApp *app;
	SequencerPeer *peer;
	SequencerLoopMode lm;
	gboolean enabled;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	sequencer = &GRANITE_MDI_CHILD (pe)->granite->sequencer;
	app = gnome_mdi_get_active_window (GRANITE_MDI_CHILD (pe)->granite->mdi);
	peer = SEQUENCER_PEER (&GRANITE_MDI_CHILD (pe)->gtkpeer);
	enabled = GTK_TOGGLE_BUTTON (widget)->active;

	sequencer_peer_get_loop_mode (peer, &lm);
	if (enabled) {
		if (lm.to_time - lm.from_time < sequencer_get_timebase (sequencer)) {
			gnome_app_warning (app, _("Loop is too short, load a larger selection first"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), FALSE);
			return;
		}
		lm.enabled = TRUE;
	} else
		lm.enabled = FALSE;
	sequencer_peer_set_loop_mode (peer, &lm);
}

static void
loop_load_from_selection_cmd (GtkWidget *widget, PatternEdit *pe)
{
	Sequencer *sequencer;
	GnomeApp *app;
	SequencerPeer *peer;
	SequencerLoopMode lm;
	TimeRoll *tr;
	ScoreTime from_time, to_time;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	sequencer = &GRANITE_MDI_CHILD (pe)->granite->sequencer;
	app = gnome_mdi_get_active_window (GRANITE_MDI_CHILD (pe)->granite->mdi);
	tr = TIME_ROLL (pe->pr);
	peer = SEQUENCER_PEER (&GRANITE_MDI_CHILD (pe)->gtkpeer);

	from_time = time_roll_get_selection_from (tr);
	to_time = time_roll_get_selection_to (tr);

	if (to_time - from_time < sequencer_get_timebase (sequencer)) {
		gnome_app_warning (app, _("Loop is too short, select a larger region"));
		return;
	}

	time_label_set_value (TIME_LABEL (pe->from_loop), from_time);
	time_label_set_value (TIME_LABEL (pe->to_loop), to_time - 1);
	sequencer_peer_get_loop_mode (peer, &lm);
	lm.from_time = from_time;
	lm.to_time = to_time;
	sequencer_peer_set_loop_mode (peer, &lm);
}

static void
zoom_in_time_cmd (GtkWidget *widget, PatternEdit *pe)
{
	TimeRoll *tr;
	gfloat new_time_ppu;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	tr = TIME_ROLL (pe->pr);

	new_time_ppu = tr->time_ppu + .005;

	debugf ("New time PPU: %.4f", new_time_ppu);

	time_roll_time_set_ppu (tr, new_time_ppu);
	time_roll_synchronize (tr);
}

static void
zoom_out_time_cmd (GtkWidget *widget, PatternEdit *pe)
{
	TimeRoll *tr;
	gfloat new_time_ppu;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	tr = TIME_ROLL (pe->pr);

	new_time_ppu = tr->time_ppu - .005;
	if (new_time_ppu < .005)
		new_time_ppu = .005;

	debugf ("New time PPU: %.4f", new_time_ppu);

	time_roll_time_set_ppu (tr, new_time_ppu);
	time_roll_synchronize (tr);
}

static void
echo_mode_cmd (GtkWidget *widget, PatternEdit *pe)
{
	SequencerPeer *peer;
	SequencerEchoMode em;
	char *ret;
	gint enabled, use_port, use_channel;
	double port, channel;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	peer = SEQUENCER_PEER (&GRANITE_MDI_CHILD (pe)->gtkpeer);
	sequencer_peer_get_echo_mode (peer, &em);
	enabled = em.enabled;
	use_port = em.use_port;
	use_channel = em.use_channel;
	port = em.port + 1;
	channel = em.channel + 1;

	ret = gtk_dialog_cauldron ("Echo Mode",
				   GTK_CAULDRON_DIALOG | GTK_CAULDRON_SPACE4,
				   "(%[(%C || (%C // %SBj) || (%C // %SBj))]p)p / (%Bqrg || %Bqrg)p",
				   "Echo Mode",
				   "Enabled", &enabled,
				   "Use port", &use_port,
				   1.0, 0, &port, 1.0,
				   (double) peer->u.Master.sequencer->num_midi_ports,
				   1.0, 1.0, 1.0,
				   "Use channel", &use_channel,
				   1.0, 0, &channel, 1.0, 16.0, 1.0, 1.0, 1.0,
				   GNOME_STOCK_BUTTON_OK,
				   GNOME_STOCK_BUTTON_CANCEL);
	
	if (!strcmp (ret, GNOME_STOCK_BUTTON_CANCEL))
		return;

	em.enabled = enabled;
	em.use_port = use_port;
	em.use_channel = use_channel;
	em.port = port - 1;
	em.channel = channel - 1;
	sequencer_peer_set_echo_mode (peer, &em);
}

static void
metro_mode_cmd (GtkWidget *widget, PatternEdit *pe)
{
	SequencerPeer *peer;
	SequencerMetroMode mm;
	char *ret;
	gint enabled;
	
	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	peer = SEQUENCER_PEER (&GRANITE_MDI_CHILD (pe)->gtkpeer);
	sequencer_peer_get_metro_mode (peer, &mm);
	enabled = mm.enabled;

	ret = gtk_dialog_cauldron ("Metronome Mode",
				   GTK_CAULDRON_DIALOG | GTK_CAULDRON_SPACE4,
				   "%[ %C ]p / (%Bqrg || %Bqrg)p",
				   "Metronome Mode",
				   "Enabled", &enabled,
				   GNOME_STOCK_BUTTON_OK,
				   GNOME_STOCK_BUTTON_CANCEL);
	
	if (!strcmp (ret, GNOME_STOCK_BUTTON_CANCEL))
		return;

	mm.enabled = enabled;
	sequencer_peer_set_metro_mode (peer, &mm);
}

static void
insert_track_cmd (GtkWidget *widget, PatternEdit *pe)
{
	ScoreTrack *track;
	gint row;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	row = pattern_roll_insert_track (PATTERN_ROLL (pe->pr), -1);

	if (row < 0)
		return;

	track = gtk_tpclist_row_get_data (PATTERN_ROLL (pe->pr)->tpclist, row);
	granite_set_track_echo_mode (pe, track);
}

static void
delete_track_cmd (GtkWidget *widget, PatternEdit *pe)
{
	pattern_roll_remove_track (PATTERN_ROLL (pe->pr), -1);
}

static void
piano_roll_clip (GnomeCanvasItem *item, PatternEdit *pe)
{
	Granite *granite;
	ScoreClip *clip;
	GraniteMDIChild *new_child;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	granite = GRANITE_MDI_CHILD (pe)->granite;
	clip = gtk_object_get_user_data (GTK_OBJECT (item));

	if (clip && !clip->user_data) {
		debugf ("Start pattern edit for %s", clip->name->str);
		new_child = GRANITE_MDI_CHILD (piano_roll_edit_new (
			granite, clip, &GRANITE_MDI_CHILD (pe)->gtkpeer));
		clip->user_data = (gpointer) new_child;
		gnome_mdi_add_child (granite->mdi, GNOME_MDI_CHILD (new_child));
		gnome_mdi_add_view (granite->mdi, GNOME_MDI_CHILD (new_child));
	}
}

static void
piano_roll_cmd (GtkWidget *widget, PatternEdit *pe)
{
	event_roll_selection_foreach (EVENT_ROLL (pe->pr), (GFunc) piano_roll_clip, pe);
}

static void
granite_set_track_echo_mode (PatternEdit *pe, ScoreTrack *track)
{
	SequencerEchoMode em;
	SequencerPeer *peer;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));
	g_return_if_fail (track != NULL);

	peer = SEQUENCER_PEER (&GRANITE_MDI_CHILD (pe)->gtkpeer);
	sequencer_peer_get_echo_mode (peer, &em);
	em.port = track->port;
	em.channel = track->channel;
	sequencer_peer_set_echo_mode (peer, &em);
}

static GnomeUIInfo edit_menu[] = {
	GNOMEUIINFO_MENU_CUT_ITEM (NULL, NULL),
	GNOMEUIINFO_MENU_COPY_ITEM (NULL, NULL),
	GNOMEUIINFO_MENU_PASTE_ITEM (NULL, NULL),
        GNOMEUIINFO_END
};

static GnomeUIInfo track_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("_Insert"), N_("Insert a new track"),
	  insert_track_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_MENU_NEW, GDK_Insert, 0 },

	{ GNOME_APP_UI_ITEM, N_("_Delete"), N_("Delete the current track"),
	  delete_track_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_MENU_TRASH, GDK_d, GDK_SHIFT_MASK },

        GNOMEUIINFO_END
};

static GnomeUIInfo view_menu[] = {
	{ GNOME_APP_UI_ITEM, N_("_Piano roll"), N_("Open a piano roll view of selected clip"),
	  piano_roll_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_MENU_PROP, GDK_P, 0 },

        GNOMEUIINFO_END
};

static GnomeUIInfo realtime_menu[] = {
        { GNOME_APP_UI_ITEM, N_("_Play"), N_("Start playback from current position"),
	  play_cmd, NULL, NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, GDK_p, 0 },

        { GNOME_APP_UI_ITEM, N_("_Record"), N_("Start recording from current position"),
	  record_cmd, NULL, NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, GDK_r, 0 },

        { GNOME_APP_UI_ITEM, N_("_Stop"), N_("Stop playback or record"),
	  stop_cmd, NULL, NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, GDK_s, 0 },

        { GNOME_APP_UI_ITEM, N_("Re_wind"), N_("Rewind to beginning"),
	  rewind_cmd, NULL, NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, GDK_w, 0 },

	GNOMEUIINFO_SEPARATOR,

        { GNOME_APP_UI_ITEM, N_("_Echo mode"), N_("Setup MIDI echo"),
	  echo_mode_cmd, NULL, NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, GDK_e, GDK_CONTROL_MASK },

        { GNOME_APP_UI_ITEM, N_("_Metronome mode"), N_("Setup MIDI metronome"),
	  metro_mode_cmd, NULL, NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, GDK_m, GDK_CONTROL_MASK },

        GNOMEUIINFO_END
};

static GnomeUIInfo pattern_edit_menu[] = {
	GNOMEUIINFO_SUBTREE (N_("_Edit"), edit_menu),
	GNOMEUIINFO_SUBTREE (N_("_Modify"), NULL),
	GNOMEUIINFO_SUBTREE (N_("_Track"), track_menu),
	GNOMEUIINFO_SUBTREE (N_("_View"), view_menu),
	GNOMEUIINFO_SUBTREE (N_("_Realtime"), realtime_menu),

        GNOMEUIINFO_END
};

static GnomeUIInfo transport_toolbar[] = {
	GNOMEUIINFO_ITEM_STOCK (N_("Rewind"), N_("Rewind"),
				rewind_cmd, GNOME_STOCK_PIXMAP_FIRST),

	GNOMEUIINFO_ITEM_STOCK (N_("Stop"), N_("Stop"),
				stop_cmd, GNOME_STOCK_PIXMAP_STOP),

	GNOMEUIINFO_ITEM_STOCK (N_("Play"), N_("Play"),
				play_cmd, GNOME_STOCK_PIXMAP_FORWARD),

	GNOMEUIINFO_ITEM       (N_("Record"), N_("Record"),
				record_cmd, record_xpm),

	GNOMEUIINFO_END
};

static GnomeUIInfo loop_toolbar[] = {
        { GNOME_APP_UI_TOGGLEITEM, N_("Loop"), N_("Enable looping"),
	  toggle_loop_mode_cmd, NULL, NULL, GNOME_APP_PIXMAP_STOCK,
	  GNOME_STOCK_PIXMAP_REFRESH, 0, (GdkModifierType) 0, NULL },

	GNOMEUIINFO_ITEM       (N_("Load"), N_("Load loop range from selection"),
				loop_load_from_selection_cmd, load_selection_xpm),
	GNOMEUIINFO_END
};

static GnomeUIInfo zoom_toolbar [] = {
	GNOMEUIINFO_ITEM (N_("Zoom In Time"), N_("Zoom in time"),
			  zoom_in_time_cmd, zoom_in_xpm),
	GNOMEUIINFO_ITEM (N_("Zoom Out Time"), N_("Zoom out time"),
			  zoom_out_time_cmd, zoom_out_xpm),
	GNOMEUIINFO_END
};

static GtkWidget *
pattern_edit_add_toolbar (PatternEdit *pe, GnomeUIInfo *uiinfo, const char *name)
{
	GtkWidget *dock_item;
	GtkWidget *tb;

	tb = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_BOTH);
	gnome_app_fill_toolbar_with_data (GTK_TOOLBAR (tb), uiinfo, NULL, pe);

	dock_item = gnome_dock_item_new (name, GNOME_DOCK_ITEM_BEH_NEVER_FLOATING);
	gtk_container_set_border_width (GTK_CONTAINER (dock_item), 2);
        gtk_container_add (GTK_CONTAINER (dock_item), tb);
	gnome_dock_add_item (GNOME_DOCK (pe->dock), GNOME_DOCK_ITEM (dock_item),
			     GNOME_DOCK_TOP, 0, 0, 0, FALSE);

	gtk_widget_show (tb);
	gtk_widget_show (dock_item);

	return tb;
}

static void
pattern_edit_create_toolbars (PatternEdit *pe)
{
	GtkWidget *tb;
	GtkWidget *tl;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	tb = pattern_edit_add_toolbar (pe, transport_toolbar, _("Transport"));
	tl = time_label_new (&GRANITE_MDI_CHILD (pe)->gtkpeer);
	gtk_widget_show (tl);
	gtk_toolbar_insert_widget (GTK_TOOLBAR (tb), tl, NULL, NULL, 0);

	tb = pattern_edit_add_toolbar (pe, loop_toolbar, _("Loop"));
	tl = time_label_new (&GRANITE_MDI_CHILD (pe)->gtkpeer);
	time_label_set_update (TIME_LABEL (tl), FALSE);
	gtk_widget_show (tl);
	gtk_toolbar_insert_widget (GTK_TOOLBAR (tb), tl, NULL, NULL, 1);
	pe->from_loop = tl;
	tl = time_label_new (&GRANITE_MDI_CHILD (pe)->gtkpeer);
	time_label_set_update (TIME_LABEL (tl), FALSE);
	gtk_widget_show (tl);
	gtk_toolbar_insert_widget (GTK_TOOLBAR (tb), tl, NULL, NULL, 2);
	pe->to_loop = tl;

	pattern_edit_add_toolbar (pe, zoom_toolbar, _("Zoom"));
}

static void
focus_cell (GtkWidget *widget, gint row, gint column, PatternEdit *pe)
{
	ScoreTrack *track;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	if (row == -1)
		debugf ("No tracks");

	if (row < 0)
		return;

	track = gtk_tpclist_row_get_data (GTK_TPCLIST (widget), row);
	granite_set_track_echo_mode (pe, track);
}

static void
key_press_cell (GtkTPCList *tpclist, gint row, gint column,
		GdkEventKey *event, PatternEdit *pe)
{
	Sequencer *sequencer;
	const gchar *val;
	gchar s[64];
	gint new_val;

	sequencer = &GRANITE_MDI_CHILD (pe)->granite->sequencer;
	val = gtk_tpclist_cell_get_value (tpclist, row, column);

	sscanf (val, "%d", &new_val);
	switch (event->keyval) {
	case GDK_KP_Add:
		switch (column) {
		case GPR_PARM_PORT:
			new_val = CLAMP (new_val + 1, 1, sequencer->num_midi_ports);
			sprintf (s, "%d", new_val);
			break;

		case GPR_PARM_CHN:
			new_val = CLAMP (new_val + 1, 1, 16);
			sprintf (s, "%d", new_val);
			break;

		default:
			return;
		}
		break;

	case GDK_KP_Subtract:
		switch (column) {
		case GPR_PARM_PORT:
			new_val = CLAMP (new_val - 1, 1, sequencer->num_midi_ports);
			sprintf (s, "%d", new_val);
			break;

		case GPR_PARM_CHN:
			new_val = CLAMP (new_val - 1, 1, 16);
			sprintf (s, "%d", new_val);
			break;

		default:
			return;
		}
		break;

	default:
		return;
	}

	gtk_tpclist_cell_set_value (tpclist, row, column, s);
}

static void
activate_cell (GtkTPCList *tpclist, gint row, gint column, gchar **value, PatternEdit *pe)
{
	ScoreTrack *track;

	track = gtk_tpclist_row_get_data (tpclist, row);
	granite_set_track_echo_mode (pe, track);
}

static void
selection_changed (GtkWidget *widget, PatternEdit *pe)
{
	ScoreTime from_time;
	SequencerGtkPeer *gtkpeer;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	gtkpeer = &GRANITE_MDI_CHILD (pe)->gtkpeer;
	from_time = time_roll_get_selection_from (TIME_ROLL (pe->pr));
	sequencer_gtk_peer_goto (gtkpeer, from_time);
}

static void
do_midi_clip_filter (GtkWidget *widget, PatternEdit *pe)
{
	Granite *granite;
	PatternEditClass *pe_class;
	CORBA_Object obj;
	CORBA_Environment ev;
	ScoreClip *clip;
	GoadServer *server;
	GtkWidget *item;
	gchar *name;
	GSList *slist, *selected_clips;
	GHashTable *visited_clips;
	gboolean found;

	g_return_if_fail (pe != NULL);
	g_return_if_fail (IS_PATTERN_EDIT (pe));

	granite = GRANITE_MDI_CHILD (pe)->granite;
	pe_class = PATTERN_EDIT_CLASS (GTK_OBJECT (pe)->klass);
	selected_clips = EVENT_ROLL (pe->pr)->selected_items;
	if (!selected_clips) {
		debugf ("No clips selected");
		return;
	}

	item = GTK_BIN (widget)->child;
	g_return_if_fail (GTK_IS_LABEL (item));
	gtk_label_get (GTK_LABEL (item), &name);

	debugf ("Do filter for: %s", name);
	for (server = NULL, slist = pe_class->midi_clip_filters, found = FALSE;
	     slist; slist = slist->next) {
		server = slist->data;
		if (strcmp (server->description, name) == 0) {
			found = TRUE;
			break;
		}
	}

	if (!found) {
		g_warning ("%s not found, skipping", name);
		return;
	}

	g_assert (server != NULL);
#if 1
	obj = goad_server_activate (server, 0, NULL);
#else
	obj = goad_server_activate (server, GOAD_ACTIVATE_REMOTE, NULL);
#endif
	if (!obj) {
		g_warning ("Failed to activate %s", name);
		return;
	}

	if (!Granite_ScoreMIDIClipFilter_prepare (obj, granite->seq_servant->obj, &ev)) {
		CORBA_Object_release (obj, &ev);
		return;
	}
	visited_clips = g_hash_table_new (g_direct_hash, g_direct_equal);
	for (; selected_clips; selected_clips = selected_clips->next) {
		clip = gtk_object_get_user_data (GTK_OBJECT (selected_clips->data));

		if (g_hash_table_lookup (visited_clips, clip))
			continue;

		Granite_ScoreMIDIClipFilter_process (obj, clip->u.MIDI.servant->obj, &ev);
		pattern_roll_refresh_clip (PATTERN_ROLL (pe->pr), clip);
		g_hash_table_insert (visited_clips, clip, clip);
	}
	g_hash_table_destroy (visited_clips);
	CORBA_Object_release (obj, &ev);
}

static GnomeUIInfo *
setup_menus (PatternEdit *pe)
{
	Granite *granite;
	GoadServer *sl, *serv;
	PatternEditClass *pe_class;
	GnomeUIInfo *uiinfo;
	gint i, j, count;
	GSList *iter;

	granite = GRANITE_MDI_CHILD (pe)->granite;
	pe_class = PATTERN_EDIT_CLASS (GTK_OBJECT (pe)->klass);

	if (pe_class->modify_menu) {
		debugf ("Filter menu already created");
		return pattern_edit_menu;
	}

	sl = granite->servers->list;
	pe_class->midi_clip_filters = NULL;

	count = 0;
	for (i = 0; sl[i].repo_id; ++i) {
		for (j = 0; sl[i].repo_id[j]; ++j) {
			if (strcmp (sl[i].repo_id[j], "IDL:Granite/ScoreMIDIClipFilter:1.0") == 0) {
				debugf ("Appended %s", sl[i].description);
				pe_class->midi_clip_filters = g_slist_append (
					pe_class->midi_clip_filters, &sl[i]);
				++count;
				break;
			}
		}
	}

	uiinfo = g_new0 (GnomeUIInfo, count + 1);
	uiinfo[count].type = GNOME_APP_UI_ENDOFINFO;

	/* Fill in menu entries */
	debugf ("%d filters found for MIDI clips", count);
	for (i = 0, iter = pe_class->midi_clip_filters; iter; ++i, iter = iter->next) {
		serv = iter->data;
		debugf ("Creating menu for %s", serv->description);
		uiinfo[i].type = GNOME_APP_UI_ITEM;
		uiinfo[i].label = serv->description;
		uiinfo[i].moreinfo = do_midi_clip_filter;
	}

	pe_class->modify_menu = uiinfo;
	pattern_edit_menu[1].moreinfo = pe_class->modify_menu;

	return pattern_edit_menu;
}

static void
pat_name_changed (GtkObject *se, ScorePattern *sp,
		  GString *new_name, PatternEdit *pe)
{
	if (sp != pe->pattern)
		return;
	pattern_edit_set_name (pe, new_name->str);
}

static GtkWidget *
pattern_edit_create_view (GnomeMDIChild *child, gpointer user_data)
{
	SequencerGtkPeer *gtkpeer;
	GraniteMDIChild *gmc;
	PatternEdit *pe;
	PatternRoll *pr;
	GtkWidget *vbox;
	GtkWidget *contents;
	GtkWidget *client;
	GtkWidget *frame;
	GtkObject *se;

	pe = PATTERN_EDIT (child);
	gmc = GRANITE_MDI_CHILD (child);
	se = gmc->granite->score_edit;
	gtkpeer = &GRANITE_MDI_CHILD (pe)->gtkpeer;

	pattern_edit_set_name (pe, pe->pattern->name->str);
	gnome_mdi_child_set_menu_template (child, setup_menus (pe));
	gtk_signal_connect (GTK_OBJECT (se), "pat_name_changed",
			    pat_name_changed, pe);

	pe->dock = gnome_dock_new ();
	pe->layout = gnome_dock_layout_new ();

	pattern_edit_create_toolbars (pe);

	PATTERN_EDIT (pe)->pr =
		pattern_roll_new (PATTERN_EDIT (pe)->pattern,
					  &GRANITE_MDI_CHILD (pe)->gtkpeer);
	gtk_widget_show (PATTERN_EDIT (pe)->pr);

	pr = PATTERN_ROLL (PATTERN_EDIT (pe)->pr);
	gtk_signal_connect (GTK_OBJECT (pr->tpclist), "focus_cell",
			    GTK_SIGNAL_FUNC (focus_cell), pe);
	gtk_signal_connect (GTK_OBJECT (pr->tpclist), "key_press_cell",
			    GTK_SIGNAL_FUNC (key_press_cell), pe);
	gtk_signal_connect (GTK_OBJECT (pr->tpclist), "activate_cell",
			    activate_cell, pe);
	gtk_signal_connect (GTK_OBJECT (TIME_ROLL (pr)->time_ruler),
			    "selection_changed", selection_changed, pe);

	frame = gtk_frame_new (NULL);
	gtk_frame_set_shadow_type (GTK_FRAME (frame), GTK_SHADOW_ETCHED_IN);
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD_SMALL);
	gtk_container_add (GTK_CONTAINER (frame), PATTERN_EDIT (pe)->pr);
	gtk_widget_show (frame);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), frame, TRUE, TRUE, 0);
	gtk_widget_show (vbox);

	client = vbox;
	contents = pe->dock;

	gnome_dock_set_client_area (GNOME_DOCK (pe->dock), client);

	return contents;
}
