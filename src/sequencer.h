/**************************************************************************

    sequencer.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SEQUENCER_H
#define __SEQUENCER_H

#include "sequencer-misc.h"
#include "sequencer-midi.h"
#include "sequencer-state.h"
#include "sequencer-peer.h"
#include "sequencer-prefs.h"
#include "sequencer-oss.h"

gboolean	sequencer_construct		(Sequencer *sequencer,
						 const char *music_device);
void		sequencer_deconstruct		(Sequencer *sequencer);


void		sequencer_prefs_changed		(Sequencer *sequencer,
						 const SequencerPrefs *prefs);
gboolean	sequencer_startup		(Sequencer *sequencer);
void		sequencer_shutdown		(Sequencer *sequencer);

ScoreTimebase	sequencer_get_timebase		(Sequencer *sequencer);
void		sequencer_set_timebase		(Sequencer *sequencer,
						 ScoreTimebase timebase);

void		sequencer_set_score		(Sequencer *sequencer,
						 Score *score);

ScoreTime	sequencer_get_current_time	(Sequencer *sequencer);

ScoreTime	sequencer_string_to_time	(Sequencer *sequencer,
						 const gchar *input,
						 ScoreTimeFormat format,
						 gboolean delta);
void		sequencer_time_to_string	(Sequencer *sequencer,
						 ScoreTime time,
						 gchar *str,
						 ScoreTimeFormat format,
						 gboolean delta);
void		sequencer_get_MBT_from_time	(Sequencer *sequencer,
						 ScoreTime time,
						 gint *measures,
						 gint *beats,
						 gint *ticks,
						 gboolean delta);

ScoreTime	sequencer_get_ticks_per_measure	(Sequencer *sequencer);

ScoreEvents *	sequencer_get_events_recorded	(Sequencer *sequencer);

ScoreTime	sequencer_get_events_recorded_time (Sequencer *sequencer);

SequencerState *sequencer_get_default_state	(Sequencer *sequencer);

void		sequencer_goto			(Sequencer *sequencer,
						 ScoreTime time);
void		sequencer_play			(Sequencer *sequencer);
void		sequencer_stop			(Sequencer *sequencer);
void		sequencer_record		(Sequencer *sequencer,
						 gboolean enable);

void		sequencer_record_event		(Sequencer *sequencer,
						 ScoreEvent *event);

void		sequencer_set_load_func		(Sequencer *sequencer,
						 SequencerLoadEventsFunc func,
						 ScoreTime offset_time,
						 gpointer user_data);

ScoreTempo	sequencer_get_tempo_at_time	(Sequencer *sequencer,
						 ScoreTime time);
void		sequencer_get_meter_at_time	(Sequencer *sequencer,
						 ScoreMeter *sm,
						 ScoreTime time);

void		sequencer_get_echo_mode		(Sequencer *sequencer,
						 SequencerEchoMode *em);
void		sequencer_set_echo_mode		(Sequencer *sequencer,
						 const SequencerEchoMode *em);

void		sequencer_get_metro_mode	(Sequencer *sequencer,
						 SequencerMetroMode *mm);
void		sequencer_set_metro_mode	(Sequencer *sequencer,
						 const SequencerMetroMode *mm);

void		sequencer_get_loop_mode		(Sequencer *sequencer,
						 SequencerLoopMode *lm);
void		sequencer_set_loop_mode		(Sequencer *sequencer,
						 const SequencerLoopMode *lm);

void		sequencer_get_punch_in_mode	(Sequencer *sequencer,
						 SequencerPunchInMode *pim);
void		sequencer_set_punch_in_mode	(Sequencer *sequencer,
						 const SequencerPunchInMode *pim);

gboolean	sequencer_in_use		(Sequencer *sequencer);
gboolean	sequencer_is_locked		(Sequencer *sequencer);
gboolean	sequencer_lock_master		(Sequencer *sequencer,
						 SequencerMaster *master);
void		sequencer_unlock_master		(Sequencer *sequencer);
gboolean	sequencer_trylock		(Sequencer *sequencer,
						 SequencerPeer *peer);

typedef struct _SequencerServant	SequencerServant;

struct _SequencerServant {
	POA_Granite_Sequencer servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	Sequencer *sequencer;
};

SequencerServant * sequencer_servant_new	(Sequencer *sequencer);
void		sequencer_servant_free		(SequencerServant *servant);

#endif /* __SEQUENCER_H */
