/**************************************************************************

    event-roll.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <gnome.h>
#include "common.h"
#include "event-roll.h"

static void event_roll_class_init	(EventRollClass *klass);
static void event_roll_init		(EventRoll *obj);
static void event_roll_synchronize	(TimeRoll *tr);

GtkType
event_roll_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"EventRoll",
			sizeof (EventRoll),
			sizeof (EventRollClass),
			(GtkClassInitFunc) event_roll_class_init,
			(GtkObjectInitFunc) event_roll_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (TYPE_TIME_ROLL, &type_info);
	}

	return type;
}

static TimeRollClass *parent_class = NULL;

static void
event_roll_class_init (EventRollClass *klass)
{
	TimeRollClass *time_roll_class;

	parent_class = gtk_type_class (time_roll_get_type ());

	time_roll_class = TIME_ROLL_CLASS (klass);
	time_roll_class->synchronize = event_roll_synchronize;
}

static void
er_mark_region (EventRoll *er, GdkEvent *event, double x, double y)
{
	TimeRoll *tr;
	double x1, x2, y1, y2;

	tr = TIME_ROLL (er);

	x1 = MIN (er->band_sel_start_x, x);
	y1 = MIN (er->band_sel_start_y, y);
	x2 = MAX (er->band_sel_start_x, x);
	y2 = MAX (er->band_sel_start_y, y);

	if (x1 < 0)
		x1 = 0;
	if (y1 < 0)
		y1 = 0;
	if (x2 >= GTK_WIDGET (er->canvas)->allocation.width)
		x2 = GTK_WIDGET (er->canvas)->allocation.width - 1;
	if (y2 >= tr->vert_upper * tr->vert_ppu)
		y2 = tr->vert_upper * tr->vert_ppu - 1;

	gnome_canvas_item_set (er->band_sel_item,
			       "x1", x1, "y1", y1,
			       "x2", x2, "y2", y2,
			       NULL);
}

#define stipple_width 2
#define stipple_height 2
static const guchar stipple_bits[] = { 0x02, 0x01, };

static gint
canvas_button_press (GtkWidget *widget, GdkEventButton *event)
{
	EventRoll *er;
	GdkBitmap *stipple;

	er = gtk_object_get_user_data (GTK_OBJECT (widget));

	if (er->band_sel_item)
	        return FALSE;

	debugf ("Canvas button press: %.1f %.1f", event->x, event->y);
	gnome_canvas_window_to_world (er->canvas, event->x, event->y, 
				      &er->band_sel_start_x, &er->band_sel_start_y);

	stipple = gdk_bitmap_create_from_data (NULL, stipple_bits, stipple_width, stipple_height);
	er->band_sel_item = gnome_canvas_item_new (
		gnome_canvas_root (er->canvas),
		gnome_canvas_rect_get_type (),
		"x1", (double) event->x,
		"y1", (double) event->y,
		"x2", (double) event->x,
		"y2", (double) event->y,
		"outline_color", "black",
		"width_pixels", 1,
		"outline_stipple", stipple,
		NULL);
	gdk_bitmap_unref (stipple);

	gnome_canvas_item_grab (er->band_sel_item,
				GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
				NULL, event->time);

	return TRUE;
}

static gint
canvas_motion_notify (GtkWidget *widget, GdkEventMotion *event)
{
	EventRoll *er;
	double x, y;

	er = gtk_object_get_user_data (GTK_OBJECT (widget));

	gnome_canvas_window_to_world (er->canvas, event->x, event->y, &x, &y);
	er_mark_region (er, (GdkEvent *) event, x, y);
	debugf ("Canvas motion: %.1f %.1f", event->x, event->y);

	return TRUE;
}

static gint
canvas_button_release (GtkWidget *widget, GdkEventButton *event)
{
	EventRoll *er;
	double x, y;

	er = gtk_object_get_user_data (GTK_OBJECT (widget));

	debugf ("Canvas button release: %.1f %.1f", event->x, event->y);

	if (er->band_sel_item) {
		gnome_canvas_window_to_world (er->canvas, event->x, event->y, &x, &y);
		er_mark_region (er, (GdkEvent *) event, x, y);
		gnome_canvas_item_ungrab (er->band_sel_item, event->time);
		gtk_object_destroy (GTK_OBJECT (er->band_sel_item));
		er->band_sel_item = NULL;
	}

	return TRUE;
}

static void
canvas_hadj_value_changed (GtkAdjustment *adj, EventRoll *er)
{
	TimeRoll *tr;

	tr = TIME_ROLL (er);
	time_roll_time_set_x_scroll (tr, adj->value);
	time_roll_synchronize (tr);
}

static void
canvas_vadj_value_changed (GtkAdjustment *adj, EventRoll *er)
{
	TimeRoll *tr;

	tr = TIME_ROLL (er);
	time_roll_vert_set_y_scroll (tr, adj->value);
	time_roll_synchronize (tr);
}

static void
event_roll_init (EventRoll *er)
{
	TimeRoll *tr;

	tr = TIME_ROLL (er);
	
	er->canvas = GNOME_CANVAS (gnome_canvas_new ());
	gtk_object_set_user_data (GTK_OBJECT (er->canvas), er);
#if 0
	gtk_signal_connect (GTK_OBJECT (er->canvas), "button_press_event",
			    GTK_SIGNAL_FUNC (canvas_button_press), er);
	gtk_signal_connect (GTK_OBJECT (er->canvas), "motion_event",
			    GTK_SIGNAL_FUNC (canvas_motion_notify), er);
	gtk_signal_connect (GTK_OBJECT (er->canvas), "button_release_event",
			    GTK_SIGNAL_FUNC (canvas_button_release), er);
#endif
	gtk_widget_show (GTK_WIDGET (er->canvas));
	gtk_signal_connect (GTK_OBJECT (GTK_LAYOUT (er->canvas)->hadjustment),
			    "value_changed", canvas_hadj_value_changed, er);
	gtk_signal_connect (GTK_OBJECT (GTK_LAYOUT (er->canvas)->vadjustment),
			    "value_changed", canvas_vadj_value_changed, er);
}

void
event_roll_construct (EventRoll *event_roll, SequencerGtkPeer *gtkmaster)
{
	g_return_if_fail (event_roll != NULL);
	g_return_if_fail (IS_EVENT_ROLL (event_roll));
	g_return_if_fail (gtkmaster != NULL);

	time_roll_construct (TIME_ROLL (event_roll), gtkmaster);
}

void
event_roll_selection_clear (EventRoll *er)
{
	g_return_if_fail (er != NULL);
	g_return_if_fail (IS_EVENT_ROLL (er));

	if (er->selected_items) {
		g_slist_foreach (er->selected_items,
				 (GFunc) event_roll_item_set_state,
				 (gpointer) EVENT_ROLL_ITEM_NORMAL);
		g_slist_free (er->selected_items);
		er->selected_items = NULL;
	}
}

void
event_roll_selection_add (EventRoll *er, EventRollItem *item)
{
	g_return_if_fail (er != NULL);
	g_return_if_fail (IS_EVENT_ROLL (er));

	if (er->selected_items &&
	    g_slist_find (er->selected_items, item))
		return;

	er->selected_items = g_slist_prepend (er->selected_items, item);
	event_roll_item_set_state (EVENT_ROLL_ITEM (item),
				   EVENT_ROLL_ITEM_SELECTED);
}

void
event_roll_selection_remove (EventRoll *er, EventRollItem *item)
{
	g_return_if_fail (er != NULL);
	g_return_if_fail (IS_EVENT_ROLL (er));

	er->selected_items = g_slist_remove (er->selected_items, item);
}

gboolean
event_roll_selection_empty (EventRoll *er)
{
	g_return_val_if_fail (er != NULL, TRUE);
	g_return_val_if_fail (IS_EVENT_ROLL (er), TRUE);

	return er->selected_items ? FALSE : TRUE;
}

void
event_roll_selection_foreach (EventRoll *er, GFunc func, gpointer user_data)
{
	g_return_if_fail (er != NULL);
	g_return_if_fail (IS_EVENT_ROLL (er));

	g_slist_foreach (er->selected_items, func, user_data);
}

void
event_roll_replace_item (EventRoll *er, EventRollItem *old_item,
			 EventRollItem *new_item)
{
	GSList *slist;

	g_return_if_fail (er != NULL);
	g_return_if_fail (old_item != NULL);
	g_return_if_fail (new_item != NULL);
	g_return_if_fail (IS_EVENT_ROLL (er));

	for (slist = er->selected_items; slist; slist = slist->next)
		if (slist->data == old_item) {
			slist->data = new_item;
			event_roll_item_set_state (
				new_item, EVENT_ROLL_ITEM_SELECTED);
		}
}

static void
event_roll_synchronize (TimeRoll *tr)
{
	EventRoll *er;
	GnomeCanvas *canvas;
	GnomeCanvasGroup *root;
	double x1, y1, x2, y2;
	double affine[6];
	gfloat width, height;
	gint cx;

	if (TIME_ROLL_CLASS (parent_class)->synchronize)
		(* TIME_ROLL_CLASS (parent_class)->synchronize)
			(tr);

	er = EVENT_ROLL (tr);

	width = GTK_WIDGET (er->canvas)->allocation.width;
	height = GTK_WIDGET (er->canvas)->allocation.height;

	tr->vert_view_end = tr->vert_view_start + height / tr->vert_ppu;

	x1 = tr->time_lower * tr->time_ppu;
	x2 = tr->time_upper * tr->time_ppu;
	if (x2 - x1 < width)
		x2 = x1 + width;

	y1 = tr->vert_lower * tr->vert_ppu;
	y2 = tr->vert_upper * tr->vert_ppu;
	if (y2 - y1 < height)
		y2 = y1 + height;

	canvas = GNOME_CANVAS (er->canvas);
	root = GNOME_CANVAS_GROUP (gnome_canvas_root (canvas));

	art_affine_scale (affine, tr->time_ppu, tr->vert_ppu);
	gnome_canvas_item_affine_absolute (GNOME_CANVAS_ITEM (root), affine);
	gnome_canvas_set_scroll_region (er->canvas, x1, y1, x2, y2);
	gnome_canvas_get_scroll_offsets (er->canvas, &cx, NULL);
	gnome_canvas_scroll_to (er->canvas, cx, tr->vert_view_start);
}

void
event_roll_shift_bottom (EventRoll *er, gint row, gint amt)
{
	GnomeCanvas *canvas;
	GnomeCanvasGroup *root;
	GnomeCanvasItem *item;
	GnomeCanvasRE *re;
	GList *list;
	gint irow;

	g_return_if_fail (er != NULL);
	g_return_if_fail (IS_EVENT_ROLL (er));

	canvas = GNOME_CANVAS (er->canvas);
	root = GNOME_CANVAS_GROUP (gnome_canvas_root (canvas));

	for (list = root->item_list; list;) {
		item = list->data;
		list = list->next;
		re = GNOME_CANVAS_RE (item);
		irow = (gint) re->y1;

		if (irow >= row)
			gnome_canvas_item_set (
				item,
				"y1", GNOME_CANVAS_RE (item)->y1 + amt,
				"y2", GNOME_CANVAS_RE (item)->y2 + amt,
				NULL);
	}
}

void
event_roll_row_remove (EventRoll *er, gint row)
{
	TimeRoll *tr;
	GnomeCanvas *canvas;
	GnomeCanvasGroup *root;
	GnomeCanvasItem *item;
	GnomeCanvasRE *re;
	GList *list;
	gint irow;

	g_return_if_fail (er != NULL);
	g_return_if_fail (IS_EVENT_ROLL (er));

	tr = TIME_ROLL (er);
	canvas = GNOME_CANVAS (er->canvas);
	root = GNOME_CANVAS_GROUP (gnome_canvas_root (canvas));

	for (list = root->item_list; list;) {
		item = list->data;
		list = list->next;
		re = GNOME_CANVAS_RE (item);
		irow = (gint) re->y1;

		if (irow == row) {
			event_roll_selection_remove (
				er, EVENT_ROLL_ITEM (item));
			gtk_object_destroy (GTK_OBJECT (item));
		}
	}
	event_roll_shift_bottom (er, row + 1, -1);
}
