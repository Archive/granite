/**************************************************************************

    sequencer-gtk-peer.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SEQUENCER_GTK_PEER_H__
#define __SEQUENCER_GTK_PEER_H__

#define SEQUENCER_GTK_PEER(obj)		((SequencerGtkPeer *)(obj))
#define SEQUENCER_GTK_MASTER(obj)	(&(((SequencerGtkPeer *)(obj))->u.Master))
#define SEQUENCER_GTK_SLAVE(obj)	(&(((SequencerGtkPeer *)(obj))->u.Slave))

typedef struct {
	SequencerPeer peer;

	union {
		struct {
			GtkAdjustment *cur_time_adj;
			guint update_timer;
		} Master;
	} u;
} SequencerGtkPeer;

void		sequencer_gtk_peer_master_construct	(SequencerGtkPeer *gtkpeer,
							 Sequencer *sequencer);
void		sequencer_gtk_peer_slave_construct	(SequencerGtkPeer *gtkpeer,
							 SequencerGtkPeer *master);
void		sequencer_gtk_peer_play		(SequencerGtkPeer *gtkpeer);
void		sequencer_gtk_peer_stop		(SequencerGtkPeer *gtkpeer);
void		sequencer_gtk_peer_goto		(SequencerGtkPeer *gtkpeer,
						 ScoreTime time);
GtkAdjustment *	sequencer_gtk_peer_get_time_adjustment	(SequencerGtkPeer *gtkpeer);

#endif /* __SEQUENCER_GTK_PEER_H__ */
