/**************************************************************************

    score-pattern.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SCORE_PATTERN_H
#define __SCORE_PATTERN_H

typedef struct _ScorePatternServant         ScorePatternServant;
typedef struct _ScorePatternInstanceServant ScorePatternInstanceServant;

typedef struct {
	GString *name;

	gint refs;

	ScoreTime duration;

#if 0
	ScoreEvents *gsevents;	/* Global singular events (tempo, meter) */
#endif

	GSList *tracks;		/* Tracks in the pattern */

	gpointer user_data;

	ScorePatternServant *servant; /* Automation */

	guint id;		/* For saving and loading */
} ScorePattern;

struct _ScorePatternServant {
	POA_Granite_ScorePattern servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	ScorePattern *pattern;
};

typedef struct {
	ScoreMarkType mark_type;

	Score *score;		/* Parent score */
	ScorePattern *pattern;
	ScoreTime time;
	gint repeat;

	gpointer user_data;

	ScorePatternInstanceServant *servant; /* Automation */
} ScorePatternInstance;

struct _ScorePatternInstanceServant {
	POA_Granite_ScorePatternInstance servant;
	PortableServer_POA poa;
	CORBA_Object obj;

	ScorePatternInstance *pi;
};

ScorePattern *		score_pattern_new			(void);
void			score_pattern_destroy			(ScorePattern *pattern);
void			score_pattern_ref			(ScorePattern *pattern);
void			score_pattern_unref			(ScorePattern *pattern);
ScoreTempo		score_pattern_get_tempo_at_time		(ScorePattern *pattern,
								 ScoreTime *time);
void			score_pattern_get_meter_at_time		(ScorePattern *pattern,
								 ScoreMeter *sm,
								 ScoreTime *time);
ScorePatternInstance *	score_pattern_instance_new		(ScorePattern *pattern);
void			score_pattern_instance_free		(ScorePatternInstance *pi);
ScoreTempo		score_pattern_instance_get_tempo_at_time (ScorePatternInstance *pi,
								  ScoreTime *time);
void			score_pattern_instance_get_meter_at_time (ScorePatternInstance *pi,
								  ScoreMeter *sm,
								  ScoreTime *time);
void			score_pattern_get_time_span		(ScorePattern *pattern,
								 ScoreTime offset_time,
								 ScoreTime *from_time,
								 ScoreTime *to_time);
guint			score_pattern_load_event_range		(ScoreEvents *se,
								 ScoreTime offset_time,
								 ScoreTime from_time,
								 ScoreTime to_time,
								 ScorePattern *pattern);
void			score_pattern_insert_track		(ScorePattern *pattern,
								 ScoreTrack *track,
								 gint position);
void			score_pattern_remove_track		(ScorePattern *pattern,
								 ScoreTrack *track);
void			score_pattern_move_track		(ScorePattern *pattern,
								 ScoreTrack *track,
								 gint new_position);

#endif /* __SCORE_PATTERN_H */
