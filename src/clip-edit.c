/**************************************************************************

    clip-edit.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include "clip-edit.h"

static void clip_edit_class_init (ClipEditClass *klass);
static void clip_edit_init (ClipEdit *obj);
static void clip_edit_finalize (GtkObject *obj);

GtkType
clip_edit_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"ClipEdit",
			sizeof (ClipEdit),
			sizeof (ClipEditClass),
			(GtkClassInitFunc) clip_edit_class_init,
			(GtkObjectInitFunc) clip_edit_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GRANITE_TYPE_MDI_CHILD, &type_info);
	}

	return type;
}

static GraniteMDIChildClass *parent_class = NULL;

static void
clip_edit_class_init (ClipEditClass *klass)
{
	GtkObjectClass *object_class;

	parent_class = gtk_type_class (granite_mdi_child_get_type ());

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = clip_edit_finalize;
}

static void
clip_edit_init (ClipEdit *clip_edit)
{
	clip_edit->clip = NULL;
}

static void
clip_edit_finalize (GtkObject *obj)
{
	ClipEdit *ce;

	ce = CLIP_EDIT (obj);
	score_clip_unref (ce->clip);

        if (GTK_OBJECT_CLASS (parent_class)->finalize)
                (* GTK_OBJECT_CLASS (parent_class)->finalize) (obj);
}

void
clip_edit_construct (ClipEdit *child, Granite *granite, ScoreClip *clip)
{
	debugf ("GraniteMDI child constructing");

	granite_mdi_child_construct (GRANITE_MDI_CHILD (child), granite);

	CLIP_EDIT (child)->clip = clip;
	score_clip_ref (clip);
}

void
clip_edit_set_name (GnomeMDIChild *child, const char *name)
{
	GString *s;

	s = g_string_new (NULL);
	g_string_sprintf (s, "%s - %s", name,
			  CLIP_EDIT (child)->clip->name->str);
	gnome_mdi_child_set_name (child, s->str);
	g_string_free (s, TRUE);
}
