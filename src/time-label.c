/**************************************************************************

    time-label.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <stdio.h>
#include <string.h>
#include <gtk/gtk.h>
#include "common.h"
#include "time-label.h"

static void time_label_class_init (TimeLabelClass *klass);
static void time_label_init (TimeLabel *obj);
static void time_label_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void time_label_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id);
static void time_label_finalize (GtkObject *object);
static void time_label_realize (GtkWidget *widget);
static void time_label_size_request (GtkWidget *widget, GtkRequisition *requisition);
static void time_label_size_allocate (GtkWidget *widget, GtkAllocation *allocation);
static void time_label_draw (GtkWidget *widget, GdkRectangle *area);
static gint time_label_expose (GtkWidget *widget, GdkEventExpose *event);
static void time_label_paint (TimeLabel *tl, GdkRectangle *area);
static void time_label_adjustment_changed (GtkAdjustment *adjustment,
					   TimeLabel *tl);

static void set_value (TimeLabel *tl, ScoreTime time);

enum { ARG_0=0, ARG_GTKMASTER };

static GtkWidgetClass *parent_class = NULL;

GtkType
time_label_get_type (void)
{
	static GtkType type = 0;

	if (!type) {
		static const GtkTypeInfo type_info =
		{
			"TimeLabel",
			sizeof (TimeLabel),
			sizeof (TimeLabelClass),
			(GtkClassInitFunc) time_label_class_init,
			(GtkObjectInitFunc) time_label_init,
			/* reserved_1 */ NULL,
			/* reserved_2 */ NULL,
			(GtkClassInitFunc) NULL,
		};

		type = gtk_type_unique (GTK_TYPE_MISC, &type_info);
		parent_class = gtk_type_class (gtk_type_parent(type));
	}

	return type;
}

static void
time_label_class_init (TimeLabelClass *klass)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;

	object_class = GTK_OBJECT_CLASS (klass);
	object_class->finalize = time_label_finalize;
	object_class->set_arg = &time_label_set_arg;
	object_class->get_arg = &time_label_get_arg;

	widget_class = GTK_WIDGET_CLASS (klass);
	widget_class->realize = time_label_realize;
	widget_class->size_request = time_label_size_request;
	widget_class->size_allocate = time_label_size_allocate;
	widget_class->draw = time_label_draw;
	widget_class->expose_event = time_label_expose;

	gtk_object_add_arg_type("TimeLabel::gtkmaster", GTK_TYPE_POINTER,
				GTK_ARG_WRITABLE|GTK_ARG_CONSTRUCT_ONLY,
				ARG_GTKMASTER);
}

static void
time_label_init (TimeLabel *tl)
{
	tl->format = SCORE_TIME_MBT;
	tl->time_as_delta = FALSE;
	tl->update_enabled = TRUE;
}

static void
time_label_get_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
  switch(arg_id) {
  }
}

static void
time_label_set_arg (GtkObject * obj, GtkArg *arg, guint arg_id)
{
  GtkAdjustment *adj;
  SequencerGtkPeer *gtkmaster;
  TimeLabel *tl;

  switch(arg_id) {
  case ARG_GTKMASTER:
    tl = ((TimeLabel *)obj);
    gtkmaster = GTK_VALUE_POINTER(*arg);
    g_return_if_fail (gtkmaster);

    sequencer_gtk_peer_slave_construct (&tl->gtkpeer, gtkmaster);
    set_value (obj, 0);

    adj = SEQUENCER_GTK_MASTER (gtkmaster)->cur_time_adj;
    if (adj)
      gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			  GTK_SIGNAL_FUNC (time_label_adjustment_changed),
			  (gpointer) obj);
    break;
  }
}

static void
time_label_finalize (GtkObject *object)
{
	TimeLabel *tl;
	GtkAdjustment *adj;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_TIME_LABEL (object));

	tl = TIME_LABEL (object);
	adj = SEQUENCER_GTK_MASTER (SEQUENCER_SLAVE (&tl->gtkpeer)->master)->cur_time_adj;
	if (adj)
		gtk_signal_disconnect_by_data (GTK_OBJECT (adj), (gpointer) tl);
	sequencer_peer_deconstruct (SEQUENCER_PEER (&tl->gtkpeer));

	(* GTK_OBJECT_CLASS (parent_class)->finalize) (object);
}

static void
update_value (TimeLabel *tl)
{
	Sequencer *sequencer;
	GdkRectangle rect;

	sequencer = SEQUENCER_MASTER (SEQUENCER_SLAVE (&tl->gtkpeer)->master)->sequencer;
	sequencer_time_to_string (sequencer, tl->time, tl->str, tl->format, tl->time_as_delta);

	if (GTK_WIDGET_DRAWABLE (GTK_WIDGET (tl))) {
		rect.x = GTK_WIDGET (tl)->allocation.x;
		rect.y = GTK_WIDGET (tl)->allocation.y;
		rect.width = GTK_WIDGET (tl)->allocation.width;
		rect.height = GTK_WIDGET (tl)->allocation.height;
		time_label_paint (tl, &rect);
	}
}

static void
set_value (TimeLabel *tl, ScoreTime time)
{
	tl->time = time;
	update_value (tl);
}

static void
time_label_adjustment_changed (GtkAdjustment *adjustment, TimeLabel *tl)
{
	if (!tl->update_enabled)
		return;

	set_value (tl, (ScoreTime) adjustment->value);
}

GtkWidget *
time_label_new (SequencerGtkPeer *gtkmaster)
{
	return GTK_WIDGET(gtk_object_new(TYPE_TIME_LABEL,
					 "gtkmaster", gtkmaster, NULL));
}

static void
time_label_realize (GtkWidget *widget)
{
	TimeLabel *tl;
	GdkWindowAttr attributes;
	gint attributes_mask;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (IS_TIME_LABEL (widget));

	tl = TIME_LABEL (widget);
	GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);

	attributes.window_type = GDK_WINDOW_CHILD;
	attributes.x = widget->allocation.x;
	attributes.y = widget->allocation.y;
	attributes.width = widget->allocation.width;
	attributes.height = widget->allocation.height;
	attributes.wclass = GDK_INPUT_OUTPUT;
	attributes.visual = gtk_widget_get_visual (widget);
	attributes.colormap = gtk_widget_get_colormap (widget);
	attributes.event_mask = gtk_widget_get_events (widget) | GDK_EXPOSURE_MASK;
	attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

	widget->window = gdk_window_new (gtk_widget_get_parent_window (widget),
					 &attributes, attributes_mask);
	gdk_window_set_user_data (widget->window, widget);

	widget->style = gtk_style_attach (widget->style, widget->window);
	gtk_style_set_background (widget->style, widget->window, GTK_STATE_NORMAL);
}

static void
time_label_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (IS_TIME_LABEL (widget));
	g_return_if_fail (requisition != NULL);

	requisition->width = gdk_string_width (widget->style->font, "000:00:000");
	requisition->height = widget->style->font->ascent +
		widget->style->font->descent + 2;
}

static void
time_label_size_allocate (GtkWidget *widget, GtkAllocation *allocation)
{
	g_return_if_fail (widget != NULL);
	g_return_if_fail (IS_TIME_LABEL (widget));

	if (GTK_WIDGET_REALIZED (widget))
		gdk_window_move_resize (widget->window,
					allocation->x, allocation->y,
					allocation->width, allocation->height);
}

static void
time_label_paint (TimeLabel *tl, GdkRectangle *area)
{
	GtkWidget *widget = GTK_WIDGET (tl);
	GtkMisc *misc;

	misc = GTK_MISC (widget);

	gdk_window_clear (widget->window);
	gtk_draw_string (widget->style, widget->window, widget->state,
			 widget->allocation.x + misc->xpad,
			 widget->allocation.y + misc->ypad +
			 widget->style->font->ascent +
			 widget->style->font->descent,
			 tl->str);
}

static void
time_label_draw (GtkWidget *widget, GdkRectangle *area)
{
	TimeLabel *tl;

	g_return_if_fail (widget != NULL);
	g_return_if_fail (IS_TIME_LABEL (widget));

	tl = TIME_LABEL (widget);

	if (GTK_WIDGET_DRAWABLE (widget))
		time_label_paint (tl, area);
}

static gint
time_label_expose (GtkWidget *widget, GdkEventExpose *event)
{
	TimeLabel *tl;

	g_return_val_if_fail (widget != NULL, FALSE);
	g_return_val_if_fail (IS_TIME_LABEL (widget), FALSE);

	tl = TIME_LABEL (widget);

	if (GTK_WIDGET_DRAWABLE (widget))
		time_label_paint (tl, &event->area);

	return FALSE;
}

void
time_label_set_update (TimeLabel *tl, gboolean enable)
{
	g_return_if_fail (tl != NULL);
	g_return_if_fail (IS_TIME_LABEL (tl));

	tl->update_enabled = enable;
}

void
time_label_set_value (TimeLabel *tl, ScoreTime time)
{
	g_return_if_fail (tl != NULL);
	g_return_if_fail (IS_TIME_LABEL (tl));

	set_value (tl, time);
}

void
time_label_set_format (TimeLabel *tl, ScoreTimeFormat format)
{
	g_return_if_fail (tl != NULL);
	g_return_if_fail (IS_TIME_LABEL (tl));

	tl->format = format;

	update_value (tl);
}

void
time_label_set_delta (TimeLabel *tl, gboolean delta)
{
	g_return_if_fail (tl != NULL);
	g_return_if_fail (IS_TIME_LABEL (tl));

	tl->time_as_delta = delta;

	update_value (tl);
}
