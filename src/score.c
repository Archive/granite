/**************************************************************************

    score.c

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#include <config.h>
#include <gnome.h>
#include "common.h"
#include "score.h"
#include "gtreewalk.h"

/*
 * ScoreEvents
 */

#define HEAP_DEFAULT_ALLOC		1000

static ScoreMIDIClipServant *score_midi_clip_servant_new (ScoreClip *clip);
static void score_midi_clip_servant_free (ScoreMIDIClipServant *servant);

static ScoreTrackServant *score_track_servant_new (ScoreTrack *track);
static void score_track_servant_free (ScoreTrackServant *servant);

static ScorePatternServant *score_pattern_servant_new (ScorePattern *pattern);
static void score_pattern_servant_free (ScorePatternServant *servant);

static ScorePatternInstanceServant *score_pattern_instance_servant_new (ScorePatternInstance *pi);
static void score_pattern_instance_servant_free (ScorePatternInstanceServant *servant);

static ScoreServant *score_servant_new (Score *score);
static void score_servant_free (ScoreServant *servant);

static
gint score_time_compare (gconstpointer a, gconstpointer b)
{
//	debugf ("Compare %d with %d", (ScoreTime) a, (ScoreTime) b);
	return (ScoreTime) a - (ScoreTime) b;
}

ScoreEvents *
score_events_new (void)
{
	ScoreEvents *se = g_new (ScoreEvents, 1);

	se->lock = g_mutex_new ();
	se->mem_chunk = g_mem_chunk_create (
		ScoreEvent, HEAP_DEFAULT_ALLOC,
		G_ALLOC_AND_FREE);
	se->heap = g_tree_new (score_time_compare);
	se->size = 0;
	
	return se;
}

void
score_events_free_event_slot (ScoreEvents *se, GSList *slist)
{
	gint count;
	for (count = 0; slist; slist = slist->next, ++count)
		score_events_free_event (se, slist->data);
	g_slist_free (slist);
	se->size -= count;
}

static gint
score_events_free_event_slot_func (gint slot_time, GSList *slist, ScoreEvents *se)
{
	score_events_free_event_slot (se, slist);
	return 0;
}

void
score_events_destroy (ScoreEvents *se)
{
	g_return_if_fail (se != NULL);

	score_events_make_empty (se);
	g_mutex_free (se->lock);
	g_free (se);
}

static gint
score_events_min_time_search_func (gpointer key, ScoreTime *max_time)
{
	*max_time = (ScoreTime) key;
	return -1;
}

ScoreTime
score_events_get_min_time (ScoreEvents *se)
{
	ScoreTime min_time = 0;

	g_tree_search (se->heap, (GSearchFunc) score_events_min_time_search_func, &min_time);
	return min_time;
}

typedef struct {
	GTree *heap;
	ScoreTime max_time;
} TimeSearchData;

static gint
score_events_max_time_search_func (ScoreTime slot_time, TimeSearchData *data)
{
	GSList *slist = g_tree_lookup (data->heap, (gpointer) slot_time);
	ScoreTime max_dur_time = 0;

	g_return_val_if_fail (slist != NULL, 1);

	for (; slist; slist = slist->next) {
		ScoreEvent *ev = slist->data;
		if (ev->type == SCORE_EVENT_NOTE)
			max_dur_time = MAX (max_dur_time, slot_time + ev->u.Note.duration);
	}
	data->max_time = MAX (slot_time, max_dur_time);

	return 1;
}

ScoreTime
score_events_get_max_time (ScoreEvents *se)
{
	TimeSearchData data;

	data.heap = se->heap;
	data.max_time = 0;
	g_tree_search (se->heap, (GSearchFunc) score_events_max_time_search_func, &data);
	return data.max_time;
}

void
score_events_make_empty (ScoreEvents *se)
{
	g_return_if_fail (se != NULL);

	if (se->size == 0)
		return;
	g_tree_traverse (se->heap,
			 (GTraverseFunc) score_events_free_event_slot_func,
			 G_IN_ORDER, se);
	g_tree_destroy (se->heap);
	se->heap = g_tree_new (score_time_compare);
	se->size = 0;
}

void
score_events_insert_event (ScoreEvents *se, ScoreEvent *event, ScoreTime time)
{
	GSList *slist;

	++se->size;
	slist = g_tree_lookup (se->heap, (gpointer) time);
	if (slist == NULL) {
		slist = g_slist_append (slist, event);
		g_tree_insert (se->heap, (gpointer) time, slist);
	} else
		g_slist_append (slist, event);
}

void
score_events_remove_event (ScoreEvents *se, ScoreEvent *event, ScoreTime old_time)
{
	GSList *slist;

	slist = g_tree_lookup (se->heap, (gpointer) old_time);
	if (!slist)
		return;
	g_tree_remove (se->heap, (gpointer) old_time);
	slist = g_slist_remove (slist, event);
	if (slist)
		g_tree_insert (se->heap, (gpointer) old_time, slist);
	--se->size;
}

void
score_events_change_event_time (ScoreEvents *se, ScoreEvent *event,
				ScoreTime old_time, ScoreTime new_time)
{
	score_events_remove_event (se, event, old_time);
	score_events_insert_event (se, event, new_time);
}

typedef struct {
	ScoreEvents *se;
	ScoreTime offset_time;
	ScoreTime from_time;
	ScoreTime to_time;
	guint8 port;
	guint8 channel;
} ScoreEventLoadData;

static gint
score_event_load_slot_to_heap (ScoreTime slot_time, GSList *slist, ScoreEventLoadData *data)
{
#ifdef DEBUG
	if (slot_time < data->from_time) {
		debugf ("Warning, load event slot before start time %d", slot_time);
		return FALSE;
	} else
#endif
	if (slot_time > data->to_time)
		return TRUE;

	debugf ("Loading from event slot into heap at time %d + %d -> %d",
		slot_time, data->offset_time, slot_time + data->offset_time);

	for (; slist; slist = slist->next) {
		ScoreEvent *dup_event = score_events_new_event (data->se);
		memcpy (dup_event, slist->data, sizeof (ScoreEvent));
		dup_event->port = data->port;
		dup_event->channel = data->channel;
		score_events_insert_event (data->se, dup_event, slot_time + data->offset_time);
	}

	return FALSE;
}

static gint
score_event_search_start_slot (gpointer key, gpointer data)
{
//	debugf ("Score event search start slot key: %d", (ScoreTime) key);
	return ((ScoreEventLoadData *) data)->from_time - (ScoreTime) key;
}

typedef struct {
	ScoreTime cur_time;
	ScoreTime next_time;
} NextEventData;

static gint
find_next_slot (gpointer key, gpointer data)
{
	return ((NextEventData *) data)->cur_time - (ScoreTime) key;
}

static gint
mark_this_next_time (ScoreTime slot_time, GSList *slist, NextEventData *data)
{
	data->next_time = slot_time;
	return 1;
}

typedef struct {
	ScoreTime cur_time;
	ScoreTime prev_time;
} PrevEventData;

static gint
find_prev_slot (gpointer key, gpointer data)
{
	return (ScoreTime) key - ((NextEventData *) data)->cur_time;
}

static gint
mark_this_prev_time (ScoreTime slot_time, GSList *slist, PrevEventData *data)
{
	data->prev_time = slot_time;
	return 1;
}

ScoreEvent *
score_event_find_most_recent_unique_event (ScoreEvents *se, ScoreEventType type,
					   ScoreTime *evtime)
{
	PrevEventData data;
	GSList *slist;
	ScoreEvent *event;

	debugf ("Search for most recent event, start at %d", *evtime);
	data.prev_time = *evtime + 1; /* Include the current time as well */
	while (1) {
		data.cur_time = data.prev_time - 1;
		data.prev_time = -1;
		g_tree_walk_backward (se->heap, (GSearchFunc) find_prev_slot,
				      (GTraverseFunc) mark_this_prev_time, &data);
		if (data.prev_time == -1) { /* No more events */
			*evtime = -1;
			return NULL;
		}
		debugf ("Found most recent slot at %d", data.prev_time);
		slist = g_tree_lookup (se->heap, GINT_TO_POINTER (data.prev_time));
		if (!slist) {
			g_warning ("Backward tree lookup returned NULL!");
			*evtime = -1;
			return NULL;
		}
		for (; slist; slist = slist->next) {
			event = slist->data;
			if (event->type == type) {
				debugf ("Found most recent event of type %d at %d",
					type, data.prev_time);
				*evtime = data.prev_time;
				return event;
			}
		}
	}
}

/*
 * ScoreClips
 */

static gint clip_counter;

static ScoreClip *
score_clip_new (void)
{
	ScoreClip *clip;

	clip = g_new0 (ScoreClip, 1);
	clip->name = g_string_new (NULL);
	g_string_sprintf (clip->name, _("Clip %d"), ++clip_counter);
	clip->refs = 0;
	clip->user_data = NULL;

	return clip;
}

ScoreClip *
score_clip_midi_new (ScoreEvents *events)
{
	ScoreClip *clip;

	clip = score_clip_new ();
	clip->type = SCORE_CLIP_MIDI;
	clip->u.MIDI.events = events;
	clip->u.MIDI.servant = score_midi_clip_servant_new (clip);

	return clip;
}

void
score_clip_get_time_span (ScoreClip *clip, ScoreTime offset_time,
			  ScoreTime *from_time, ScoreTime *to_time)
{
	g_return_if_fail (clip != NULL);

	if (clip->type == SCORE_CLIP_MIDI) {
		if (from_time)
			*from_time = score_events_get_min_time (clip->u.MIDI.events) + offset_time;
		if (to_time)
			*to_time = score_events_get_max_time (clip->u.MIDI.events) + offset_time;
	}
}

void
score_clip_ref (ScoreClip *clip)
{
	++clip->refs;
}

void
score_clip_unref (ScoreClip *clip)
{
	g_return_if_fail (clip != NULL);

	if (--clip->refs > 0)
		return;
#ifdef DEBUG
	if (clip->refs < 0) {
		g_warning ("Clip refs is %d", clip->refs);
		return;
	}
#endif
	if (clip->name)
		g_string_free (clip->name, TRUE);
	if (clip->type == SCORE_CLIP_MIDI) {
		ScoreClipMIDI *midi = &clip->u.MIDI;

		if (midi->servant)
			score_midi_clip_servant_free (midi->servant);
		if (midi->events)
			score_events_destroy (midi->events);
	}
	g_free (clip);
}

/*
 * ScoreTrack
 */

static gint track_counter;

ScoreTrack *
score_track_new (void)
{
	ScoreTrack *track;

	track = g_new0 (ScoreTrack, 1);
	track->name = g_string_new (NULL);
	track->clips = g_tree_new (score_time_compare);
	g_string_sprintf (track->name, _("Track %d"), ++track_counter);
	track->servant = score_track_servant_new (track);

	return track;
}

static gint
score_track_unref_clip (ScoreTime slot_time, GSList *slist, ScoreTrack *track)
{
	ScoreClip *clip;

	for (; slist; slist = slist->next) {
		clip = slist->data;
		score_clip_unref (clip);
	}
	return 0;
}

void
score_track_destroy (ScoreTrack *track)
{
	g_return_if_fail (track != NULL);

	score_track_servant_free (track->servant);
	g_tree_traverse (track->clips,
			 (GTraverseFunc) score_track_unref_clip,
			 G_IN_ORDER, track);
	g_tree_destroy (track->clips);
	g_free (track);
}

void
score_track_get_time_span (ScoreTrack *track, ScoreTime offset_time,
			   ScoreTime *from_time, ScoreTime *to_time)
{
	*from_time = *to_time = offset_time;
}

static gint
score_track_load_clip_to_heap (ScoreTime slot_time, GSList *slist, ScoreEventLoadData *data)
{
	if (slot_time > data->to_time)
		return TRUE;

	for (; slist; slist = slist->next) {
		ScoreClip *clip = slist->data;

		data->offset_time += slot_time;
		data->from_time -= slot_time;
		data->to_time -= slot_time;
		debugf ("Load CLIP into heap, offset: %d", data->offset_time);
		if (clip && clip->u.MIDI.events->size > 0)
			g_tree_walk_forward (clip->u.MIDI.events->heap,
					     (GSearchFunc) score_event_search_start_slot,
					     (GTraverseFunc) score_event_load_slot_to_heap,
					     data);
		data->offset_time -= slot_time;
		data->from_time += slot_time;
		data->to_time += slot_time;
	}

	return FALSE;
}

guint
score_track_load_event_range (ScoreEvents *se, ScoreTime offset_time,
			      ScoreTime from_time, ScoreTime to_time,
			      ScoreTrack *track)
{
	ScoreEventLoadData data;
	gint old_count;

	old_count = se->size;
	data.se = se;
	data.offset_time = offset_time;
	data.from_time = from_time - offset_time;
	data.to_time = to_time - offset_time;
	data.port = track->port;
	data.channel = track->channel;

	if (track->clips)
		g_tree_traverse (track->clips, (GTraverseFunc) score_track_load_clip_to_heap,
				 G_IN_ORDER, &data);

	return data.se->size - old_count;
}

void
score_track_insert_clip (ScoreTrack *track, ScoreClip *clip, ScoreTime time)
{
	GSList *slist;

	score_clip_ref (clip);
	slist = g_tree_lookup (track->clips, (gpointer) time);
	if (slist == NULL) {
		slist = g_slist_append (slist, clip);
		g_tree_insert (track->clips, (gpointer) time, slist);
	} else
		g_slist_append (slist, clip);
}

void
score_track_remove_clip (ScoreTrack *track, ScoreClip *clip, ScoreTime time)
{
	GSList *slist;

	slist = g_tree_lookup (track->clips, (gpointer) time);
	if (!slist)
		return;
	g_tree_remove (track->clips, (gpointer) time);
	slist = g_slist_remove (slist, clip);
	if (slist)
		g_tree_insert (track->clips, (gpointer) time, slist);
	score_clip_unref (clip);
}

void
score_track_change_clip_time (ScoreTrack *track, ScoreClip *clip,
			      ScoreTime old_time, ScoreTime new_time)
{
	score_clip_ref (clip);
	score_track_remove_clip (track, clip, old_time);
	score_track_insert_clip (track, clip, new_time);
	score_clip_unref (clip);
}

/*
 * ScorePatterns
 */

static gint pattern_counter;

ScorePattern *
score_pattern_new (void)
{
	ScorePattern *pattern;

	pattern = g_new0 (ScorePattern, 1);
	pattern->name = g_string_new (NULL);
	g_string_sprintf (pattern->name, _("Pattern %d"), ++pattern_counter);
#if 0
	pattern->gsevents = score_events_new ();
	{
		ScoreEvent *event;
		event = score_events_new_event (pattern->gsevents);
		event->type = SCORE_EVENT_TEMPO;
		event->u.Tempo.value = 130;
		score_events_insert_event (pattern->gsevents, event, 2000);
	}
#endif
	pattern->servant = score_pattern_servant_new (pattern);

	return pattern;
}

void
score_pattern_destroy (ScorePattern *pattern)
{
	GSList *slist;

	g_return_if_fail (pattern != NULL);

	debugf ("Destroying pattern: %s", pattern->name->str);

	score_pattern_servant_free (pattern->servant);
	g_string_free (pattern->name, TRUE);
	for (slist = pattern->tracks; slist; slist = slist->next)
		score_track_destroy (slist->data);
	g_slist_free (pattern->tracks);
#if 0
	score_events_destroy (pattern->gsevents);
#endif
	g_free (pattern);
}

void
score_pattern_ref (ScorePattern *pattern)
{
	++pattern->refs;
}

void
score_pattern_unref (ScorePattern *pattern)
{
	if (--pattern->refs > 0)
		return;
#ifdef DEBUG
	if (pattern->refs < 0) {
		g_warning ("Pattern refs is %d", pattern->refs);
		return;
	}
#endif
	score_pattern_destroy (pattern);
}

#if 0
ScoreTempo
score_pattern_get_tempo_at_time (ScorePattern *pattern, ScoreTime *time)
{
	ScoreEvent *event;

	g_return_val_if_fail (pattern != NULL, 99);

	debugf ("Pattern get tempo at time %d", *time);

	event = score_event_find_most_recent_unique_event (
		pattern->gsevents, SCORE_EVENT_TEMPO, time);
	if (event) {
		debugf ("Found most recent score tempo of %d at time %d",
			event->u.Tempo.value, *time);
		return event->u.Tempo.value;
	} else {
		g_warning ("Tempo not found!  Returning 99");
		return 99;
	}
}

void
score_pattern_get_meter_at_time (ScorePattern *pattern, ScoreMeter *sm, ScoreTime *time)
{
	ScoreEvent *event;

	g_return_if_fail (pattern != NULL);

	debugf ("Score pattern get meter at time %d", *time);
	event = score_event_find_most_recent_unique_event (
		pattern->gsevents, SCORE_EVENT_METER, time);
	if (event) {
		*sm = event->u.Meter.value;
		debugf ("Found most recent score pattern meter of %d/%d at time %d",
			sm->N, sm->D, *time);
	} else {
		g_warning ("Meter not found!  Returning 4/4");
		sm->N = sm->D = 4;
	}
}
#endif

ScorePatternInstance *
score_pattern_instance_new (ScorePattern *pattern)
{
	ScorePatternInstance *pi;

	pi = g_new0 (ScorePatternInstance, 1);
	pi->repeat = 1;
	pi->pattern = pattern;
	pi->servant = score_pattern_instance_servant_new (pi);
	score_pattern_ref (pattern);

	return pi;
}

void
score_pattern_instance_free (ScorePatternInstance *pi)
{
	g_return_if_fail (pi != NULL);
	g_return_if_fail (pi->pattern != NULL);

	score_pattern_instance_servant_free (pi->servant);
	score_pattern_unref (pi->pattern);
	g_free (pi);
}

#if 0
ScoreTempo
score_pattern_instance_get_tempo_at_time (ScorePatternInstance *pi, ScoreTime *time)
{
	ScoreEvent *e1, *e2;
	ScoreTime t1, t2;
	ScoreTempo tempo = 0;

	g_return_val_if_fail (pi != NULL, 99);

	debugf ("Pattern instance get tempo at time %d", *time);
	t1 = *time - pi->time;
	e1 = score_event_find_most_recent_unique_event (
		pi->pattern->gsevents, SCORE_EVENT_TEMPO, &t1);

	t2 = *time;
	e2 = score_event_find_most_recent_unique_event (
		pi->score->gsevents, SCORE_EVENT_TEMPO, &t2);

	if (e2)
		tempo = e2->u.Tempo.value;
	if (e1 && t1 >= t2)
		tempo = e1->u.Tempo.value;

	if (tempo == 0) {
		debugf ("Cannot find tempo at %d, return 99", *time);
		return 99;
	}

	return tempo;
}

void
score_pattern_instance_get_meter_at_time (ScorePatternInstance *pi, ScoreMeter *sm, ScoreTime *time)
{
	ScoreEvent *e1, *e2;
	ScoreTime t1, t2;

	sm->D = 0;

	g_return_if_fail (pi != NULL);

	debugf ("Pattern instance get meter at time %d", *time);
	t1 = *time - pi->time;
	e1 = score_event_find_most_recent_unique_event (
		pi->pattern->gsevents, SCORE_EVENT_METER, &t1);

	t2 = *time;
	e2 = score_event_find_most_recent_unique_event (
		pi->score->gsevents, SCORE_EVENT_METER, &t2);

	if (e2)
		*sm = e2->u.Meter.value;
	if (e1 && t1 >= t2)
		*sm = e1->u.Meter.value;

	if (sm->D == 0) {
		debugf ("Cannot find meter at %d, return 4/4", *time);
		sm->N = sm->D = 4;
	}
}
#endif

void
score_pattern_get_time_span (ScorePattern *pattern, ScoreTime offset_time,
			     ScoreTime *from_time, ScoreTime *to_time)
{
	*from_time = *to_time = offset_time;
}

guint
score_pattern_load_event_range (ScoreEvents *se, ScoreTime offset_time,
				ScoreTime from_time, ScoreTime to_time,
				ScorePattern *pattern)
{
	GSList *slist;
	guint old_count;

	old_count = se->size;
	for (slist = pattern->tracks; slist; slist = slist->next)
		score_track_load_event_range (se, offset_time,
					      from_time - offset_time,
					      to_time - offset_time,
					      (ScoreTrack *) slist->data);

	return se->size - old_count;
}

void
score_pattern_insert_track (ScorePattern *pattern, ScoreTrack *track, gint position)
{
	pattern->tracks = g_slist_insert (pattern->tracks, track, position);
}

void
score_pattern_remove_track (ScorePattern *pattern, ScoreTrack *track)
{
	pattern->tracks = g_slist_remove (pattern->tracks, track);
}

void
score_pattern_move_track (ScorePattern *pattern, ScoreTrack *track, gint new_position)
{
	gint old_position;

	old_position = g_slist_index (pattern->tracks, track);

	if (new_position == old_position)
		return;

	pattern->tracks = g_slist_remove (pattern->tracks, track);
	if (new_position < old_position)
		pattern->tracks = g_slist_insert (pattern->tracks, track, new_position);
	else
		pattern->tracks = g_slist_insert (pattern->tracks, track, new_position - 1);
}

/*
 * Scores
 */

static gint score_counter;

Score *
score_new (void)
{
	Score *score;
	ScoreEvent *event;

	score = g_new0 (Score, 1);
	score->name = g_string_new (NULL);
	g_string_sprintf (score->name, _("Unnamed %d"), ++score_counter);
#if 0
	score->gsevents = score_events_new ();
#endif
	score->patterns = g_tree_new (score_time_compare);
	score->servant = score_servant_new (score);

	score_prefs_init (&score->prefs);
	score_prefs_load (&score->prefs);

#if 0
	/* Default tempo */
	event = score_events_new_event (score->gsevents);
	event->type = SCORE_EVENT_TEMPO;
	event->u.Tempo.value = 100;
	score_events_insert_event (score->gsevents, event, 0);

	/* Default meter */
	event = score_events_new_event (score->gsevents);
	event->type = SCORE_EVENT_METER;
	event->u.Meter.value.N = 4;
	event->u.Meter.value.D = 4;
	score_events_insert_event (score->gsevents, event, 0);
#endif

	return score;
}

void
score_ref (Score *score)
{
	++score->refs;
}

static gint
score_free_pattern (ScoreTime time, GSList *slist, gpointer data)
{
	ScorePatternInstance *spi;

	for (; slist; slist = slist->next) {
		spi = slist->data;
		score_pattern_instance_free (spi);
	}
	return 0;
}

void
score_unref (Score *score)
{
	g_return_if_fail (score != NULL);

	if (--score->refs > 0)
		return;
#ifdef DEBUG
	if (score->refs < 0) {
		g_warning ("Score refs is %d", score->refs);
		return;
	}
#endif
	score_servant_free (score->servant);
	if (score->name)
		g_string_free (score->name, TRUE);
	g_tree_traverse (score->patterns, (GTraverseFunc) score_free_pattern,
			 G_IN_ORDER, NULL);
	g_tree_destroy (score->patterns);
#if 0
	score_events_destroy (score->gsevents);
#endif
	score_prefs_free (&score->prefs);
	g_free (score);
	g_blow_chunks ();
}

void
score_prefs_changed (Score *score, const ScorePrefs *prefs)
{
	g_return_if_fail (score != NULL);

	if (prefs == NULL)
		return;

	debugf ("setting score preferences");
}

static gint
reset_clip_id (ScoreTime time, GSList *slist, gpointer data)
{
	ScoreClip *clip;

	for (; slist; slist = slist->next) {
		clip = slist->data;
		clip->id = 0;
	}
	return 0;
}

static void
score_pattern_reset_ids (ScorePattern *pattern)
{
	GSList *slist;
	ScoreTrack *track;

	for (slist = pattern->tracks; slist; slist = slist->next) {
		track = slist->data;
		g_tree_traverse (track->clips, (GTraverseFunc) reset_clip_id, G_IN_ORDER, NULL);
	}
	pattern->id = 0;
}

static gint
reset_pattern_id (ScoreTime time, GSList *slist, gpointer data)
{
	for (; slist; slist = slist->next)
		score_pattern_reset_ids (((ScorePatternInstance *)slist->data)->pattern);
	return 0;
}

void
score_reset_ids (Score *score)
{
	g_return_if_fail (score != NULL);

	g_tree_traverse (score->patterns, (GTraverseFunc) reset_pattern_id, G_IN_ORDER, NULL);
}

void
score_get_time_span (Score *score, ScoreTime offset_time,
		     ScoreTime *from_time, ScoreTime *to_time)
{
	g_return_if_fail (score != NULL);

	*from_time = *to_time = offset_time;
}

guint
score_load_event_range (ScoreEvents *se, ScoreTime offset_time,
			ScoreTime from_time, ScoreTime to_time,
			Score *score)
{
	return 0;
}

#if 0
ScoreEvent *
score_get_event_at_time (Score *score, ScoreEventType type, ScoreTime *time)
{
}

ScoreTempo
score_get_tempo_at_time (Score *score, ScoreTime *time)
{
	ScoreEvent *event;

	g_return_val_if_fail (score != NULL, 99);

	debugf ("Score get tempo at time %d", *time);
	event = score_event_find_most_recent_unique_event (
		score->gsevents, SCORE_EVENT_TEMPO, time);
	if (event) {
		debugf ("Found most recent score tempo of %d at time %d",
			event->u.Tempo.value, *time);
		return event->u.Tempo.value;
	} else {
		g_warning ("Tempo not found!  Returning 99");
		return 99;
	}
}

void
score_get_meter_at_time (Score *score, ScoreMeter *sm, ScoreTime *time)
{
	ScoreEvent *event;

	g_return_if_fail (score != NULL);

	debugf ("Score get meter at time %d", *time);
	event = score_event_find_most_recent_unique_event (
		score->gsevents, SCORE_EVENT_METER, time);
	if (event) {
		*sm = event->u.Meter.value;
		debugf ("Found most recent score meter of %d/%d at time %d",
			sm->N, sm->D, *time);
	} else {
		g_warning ("Meter not found!  Returning 4/4");
		sm->N = sm->D = 4;
	}
}
#endif

ScorePatternInstance *
score_insert_pattern_instance (Score *score, ScorePattern *pattern, ScoreTime time)
{
	ScorePatternInstance *pi;
	GSList *slist;

	pi = score_pattern_instance_new (pattern);
	pi->score = score;
	slist = g_tree_lookup (score->patterns, (gpointer) time);
	if (slist == NULL) {
		slist = g_slist_append (slist, pi);
		g_tree_insert (score->patterns, (gpointer) time, slist);
	} else
		g_slist_append (slist, pi);
	pi->time = time;
	return pi;
}

void
score_remove_pattern_instance (Score *score, ScorePatternInstance *pi)
{
	GSList *slist;
	ScoreTime time;

	time = pi->time;
	slist = g_tree_lookup (score->patterns, (gpointer) time);
	if (!slist)
		return;
	g_tree_remove (score->patterns, (gpointer) time);
	slist = g_slist_remove (slist, pi);
	if (slist)
		g_tree_insert (score->patterns, (gpointer) time, slist);
	score_pattern_instance_free (pi);
}

void
score_change_pattern_instance_time (Score *score, ScorePatternInstance *pi, ScoreTime new_time)
{
	GSList *slist;

	slist = g_tree_lookup (score->patterns, (gpointer) pi->time);
	if (!slist)
		return;
	g_tree_remove (score->patterns, (gpointer) pi->time);
	slist = g_slist_remove (slist, pi);
	if (slist)
		g_tree_insert (score->patterns, (gpointer) pi->time, slist);

	slist = g_tree_lookup (score->patterns, (gpointer) new_time);
	if (slist == NULL) {
		slist = g_slist_append (slist, pi);
		g_tree_insert (score->patterns, (gpointer) new_time, slist);
	} else
		g_slist_append (slist, pi);

	pi->time = new_time;
}

/*
 * ScoreMarks
 */

ScoreMark *score_mark_new (ScoreMarkType type)
{
	ScoreMark *mark;

	mark = g_new0 (ScoreMark, 1);
	mark->type = type;

	return mark;
}

void score_mark_destroy (ScoreMark *mark)
{
	g_free (mark);
}


/*
 * CORBA servers
 */

static PortableServer_ServantBase__epv base_epv = {
	NULL, NULL, NULL,
};

/*
 * ScoreMIDIClip CORBA server implementation
 */

static CORBA_char *
score_midi_clip_get_name (PortableServer_Servant servant, CORBA_Environment *ev)
{
	ScoreMIDIClipServant *scs = (ScoreMIDIClipServant *) servant;

	return scs->clip->name->str;
}

static void
score_midi_clip_set_event_start (PortableServer_Servant servant, CORBA_Environment *ev)
{
	ScoreMIDIClipServant *scs = (ScoreMIDIClipServant *) servant;

	scs->cur_time = -1;
	scs->cur_slist = NULL;
	scs->next_slist = NULL;
	if (scs->changed_times)
		g_hash_table_destroy (scs->changed_times);
	scs->changed_times = g_hash_table_new (g_direct_hash, g_direct_equal);
}

static void
gcs_to_se (const Granite_ScoreEvent *gcs, ScoreTime time, ScoreEvent *se)
{
	se->type = gcs->_d;
	switch (gcs->_d) {
	case SCORE_EVENT_NOTE:
		se->u.Note.value = CLAMP (gcs->_u.Note.value, 0, 127);
		se->u.Note.velocity = gcs->_u.Note.velocity;
		se->u.Note.duration = gcs->_u.Note.duration;
		break;

	case SCORE_EVENT_AFTERTOUCH:
		se->u.Aftertouch.value = gcs->_u.Aftertouch.value;
		break;

	case SCORE_EVENT_KEY_AFTERTOUCH:
		se->u.KeyAftertouch.note = gcs->_u.KeyAftertouch.note;
		se->u.KeyAftertouch.value = gcs->_u.KeyAftertouch.value;
		break;

	case SCORE_EVENT_PROGRAM_CHANGE:
		se->u.ProgramChange.value = gcs->_u.ProgramChange.value;
		break;

	case SCORE_EVENT_CONTROLLER:
		se->u.Controller.number = gcs->_u.Controller.number;
		se->u.Controller.value = gcs->_u.Controller.value;
		break;

	case SCORE_EVENT_PITCH_BEND:
		se->u.PitchBend.value = gcs->_u.PitchBend.value;
		break;

	case SCORE_EVENT_TEMPO:
		se->u.Tempo.value = gcs->_u.Tempo.value;
		break;

	case SCORE_EVENT_METER:
		se->u.Meter.value.N = gcs->_u.Meter.value.N;
		se->u.Meter.value.D = gcs->_u.Meter.value.D;
		break;

	default:
		g_warning ("Unhandled type %d in gcs_to_se", se->type);
		break;
	}
}

static void
se_to_gcs (ScoreTime time, const ScoreEvent *se, Granite_ScoreEvent *gcs)
{
	gcs->_d = se->type;
	switch (se->type) {
	case SCORE_EVENT_NOTE:
		gcs->_u.Note.time = time;
		gcs->_u.Note.value = se->u.Note.value;
		gcs->_u.Note.velocity = se->u.Note.velocity;
		gcs->_u.Note.duration = se->u.Note.duration;
		break;

	case SCORE_EVENT_AFTERTOUCH:
		gcs->_u.Aftertouch.time = time;
		gcs->_u.Aftertouch.value = se->u.Aftertouch.value;
		break;

	case SCORE_EVENT_KEY_AFTERTOUCH:
		gcs->_u.KeyAftertouch.time = time;
		gcs->_u.KeyAftertouch.note = se->u.KeyAftertouch.note;
		gcs->_u.KeyAftertouch.value = se->u.KeyAftertouch.value;
		break;

	case SCORE_EVENT_PROGRAM_CHANGE:
		gcs->_u.ProgramChange.time = time;
		gcs->_u.ProgramChange.value = se->u.ProgramChange.value;
		break;

	case SCORE_EVENT_CONTROLLER:
		gcs->_u.Controller.time = time;
		gcs->_u.Controller.number = se->u.Controller.number;
		gcs->_u.Controller.value = se->u.Controller.value;
		break;

	case SCORE_EVENT_PITCH_BEND:
		gcs->_u.PitchBend.time = time;
		gcs->_u.PitchBend.value = se->u.PitchBend.value;
		break;

	case SCORE_EVENT_TEMPO:
		gcs->_u.Tempo.value = se->u.Tempo.value;
		break;

	case SCORE_EVENT_METER:
		gcs->_u.Meter.value.N = se->u.Meter.value.N;
		gcs->_u.Meter.value.D = se->u.Meter.value.D;
		break;

	default:
		g_warning ("Unhandled type %d in se_to_gcs", se->type);
		break;
	}
}

static CORBA_boolean
score_midi_clip_get_next_event (PortableServer_Servant servant, Granite_ScoreEvent *gcs,
				CORBA_Environment *ev)
{
	NextEventData data;
	ScoreMIDIClipServant *scs = (ScoreMIDIClipServant *) servant;
	ScoreClip *clip = scs->clip;
	ScoreEvent *se;

	/* Skip over visited nodes */
	while (scs->next_slist && g_hash_table_lookup (
		scs->changed_times, scs->next_slist->data))
		scs->next_slist = scs->next_slist->next;

	if (scs->next_slist) {
		scs->cur_slist = scs->next_slist;
		scs->next_slist = scs->next_slist->next;
		se = scs->cur_slist->data;
		se_to_gcs (scs->cur_time, se, gcs);
		return CORBA_TRUE;
	}

	/* Look in next available time slot */
	while (1) {
		data.cur_time = scs->cur_time + 1;
		data.next_time = -1;
		g_tree_walk_forward (clip->u.MIDI.events->heap, (GSearchFunc) find_next_slot,
				     (GTraverseFunc) mark_this_next_time, &data);
		if (data.next_time == -1) /* No more events */
			return CORBA_FALSE;
		scs->cur_time = data.next_time;

		scs->cur_slist = g_tree_lookup (clip->u.MIDI.events->heap,
						GINT_TO_POINTER (scs->cur_time));
		if (!scs->cur_slist) {
			g_warning ("Tree lookup returned NULL!");
			return CORBA_FALSE;
		}

		if (g_hash_table_lookup (scs->changed_times, scs->cur_slist->data)) {
			while (scs->cur_slist && g_hash_table_lookup (
				scs->changed_times, scs->cur_slist->data))
				scs->cur_slist = scs->cur_slist->next;
			if (scs->cur_slist)
				break;
			else
				continue;
		} else
			break;
	}

	scs->next_slist = scs->cur_slist->next;	/* In case the event is moved */
	se = scs->cur_slist->data;
	se_to_gcs (scs->cur_time, se, gcs);

	return CORBA_TRUE;
}

static void
score_midi_clip_set_event (PortableServer_Servant servant, const Granite_ScoreEvent *gcs,
			   CORBA_Environment *ev)
{
	ScoreMIDIClipServant *scs = (ScoreMIDIClipServant *) servant;
	ScoreEvent *se;
	ScoreTime new_time;

	if (!scs->cur_slist) {
		g_warning ("Set event called without previous event!");
		return;
	}

	se = scs->cur_slist->data;
	gcs_to_se (gcs, scs->cur_time, se);
	new_time = gcs->_u.Note.time;
	if (new_time != scs->cur_time) {
		score_events_change_event_time (scs->clip->u.MIDI.events, se,
						scs->cur_time, new_time);
		if (new_time > scs->cur_time)
			g_hash_table_insert (scs->changed_times, se, se);
	}
}

static void
score_midi_clip_kill_event (PortableServer_Servant servant, CORBA_Environment *ev)
{
	ScoreMIDIClipServant *scs = (ScoreMIDIClipServant *) servant;
	ScoreEvent *se;

	if (!scs->cur_slist) {
		g_warning ("Kill event called without previous event!");
		return;
	}

	se = scs->cur_slist->data;
	score_events_remove_event (scs->clip->u.MIDI.events, se, scs->cur_time);
	score_events_free_event (scs->clip->u.MIDI.events, se);
}

static void
score_midi_clip_insert_event (PortableServer_Servant servant, const Granite_ScoreEvent *gcs,
			      CORBA_Environment *ev)
{
	ScoreMIDIClipServant *scs = (ScoreMIDIClipServant *) servant;
	ScoreEvent *se;
	ScoreTime new_time;

	se = score_events_new_event (scs->clip->u.MIDI.events);
	gcs_to_se (gcs, scs->cur_time, se);
	new_time = gcs->_u.Note.time;
	score_events_insert_event (scs->clip->u.MIDI.events, se, new_time);
	if (new_time >= scs->cur_time)
		g_hash_table_insert (scs->changed_times, se, se);
}

static POA_Granite_ScoreMIDIClip__epv score_midi_clip_epv = {
	NULL,
	score_midi_clip_get_name,
	score_midi_clip_set_event_start,
	score_midi_clip_get_next_event,
	score_midi_clip_set_event,
	score_midi_clip_kill_event,
	score_midi_clip_insert_event,
};
static POA_Granite_ScoreMIDIClip__vepv score_midi_clip_vepv = {
	&base_epv, &score_midi_clip_epv
};

static ScoreMIDIClipServant *
score_midi_clip_servant_new (ScoreClip *clip)
{
	ScoreMIDIClipServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (ScoreMIDIClipServant, 1);
	servant->servant.vepv = &score_midi_clip_vepv;
	servant->clip = clip;
	servant->changed_times = NULL;
	score_midi_clip_set_event_start (servant, &ev);

	POA_Granite_ScoreMIDIClip__init ((POA_Granite_ScoreMIDIClip *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_exception_free (&ev);

	return servant;
}

static void
score_midi_clip_servant_free (ScoreMIDIClipServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_ScoreMIDIClip__fini ((POA_Granite_ScoreMIDIClip *)servant, &ev);
	CORBA_exception_free (&ev);
	g_hash_table_destroy (servant->changed_times);
	g_free (servant);
}

/*
 * ScoreTrack CORBA server implementation
 */

static POA_Granite_ScoreTrack__epv score_track_epv = {
	NULL,
};
static POA_Granite_ScoreTrack__vepv score_track_vepv = {
	&base_epv, &score_track_epv
};

static ScoreTrackServant *
score_track_servant_new (ScoreTrack *track)
{
	ScoreTrackServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (ScoreTrackServant, 1);
	servant->servant.vepv = &score_track_vepv;
	servant->track = track;

	POA_Granite_ScoreTrack__init ((POA_Granite_ScoreTrack *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_exception_free (&ev);

	return servant;
}

static void
score_track_servant_free (ScoreTrackServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_ScoreTrack__fini ((POA_Granite_ScoreTrack *)servant, &ev);
	CORBA_exception_free (&ev);
	g_free (servant);
}

/*
 * ScorePattern CORBA server implementation
 */

static POA_Granite_ScorePattern__epv score_pattern_epv = {
	NULL,
};
static POA_Granite_ScorePattern__vepv score_pattern_vepv = {
	&base_epv, &score_pattern_epv
};

static ScorePatternServant *
score_pattern_servant_new (ScorePattern *pattern)
{
	ScorePatternServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (ScorePatternServant, 1);
	servant->servant.vepv = &score_pattern_vepv;
	servant->pattern = pattern;

	POA_Granite_ScorePattern__init ((POA_Granite_ScorePattern *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_exception_free (&ev);

	return servant;
}

static void
score_pattern_servant_free (ScorePatternServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_ScorePattern__fini ((POA_Granite_ScorePattern *)servant, &ev);
	CORBA_exception_free (&ev);
	g_free (servant);
}

/*
 * ScorePatternInstance CORBA server implementation
 */

static POA_Granite_ScorePatternInstance__epv score_pattern_instance_epv = {
	NULL,
};
static POA_Granite_ScorePatternInstance__vepv score_pattern_instance_vepv = {
	&base_epv, &score_pattern_instance_epv
};

static ScorePatternInstanceServant *
score_pattern_instance_servant_new (ScorePatternInstance *pi)
{
	ScorePatternInstanceServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (ScorePatternInstanceServant, 1);
	servant->servant.vepv = &score_pattern_instance_vepv;
	servant->pi = pi;

	POA_Granite_ScorePatternInstance__init ((POA_Granite_ScorePatternInstance *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_exception_free (&ev);

	return servant;
}

static void
score_pattern_instance_servant_free (ScorePatternInstanceServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_ScorePatternInstance__fini ((POA_Granite_ScorePatternInstance *)servant, &ev);
	CORBA_exception_free (&ev);
	g_free (servant);
}

/*
 * Score CORBA server implementation
 */

static POA_Granite_Score__epv score_epv = {
	NULL,
};
static POA_Granite_Score__vepv score_vepv = {
	&base_epv, &score_epv
};

static ScoreServant *
score_servant_new (Score *score)
{
	ScoreServant *servant;
	PortableServer_POA poa;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	servant = g_new0 (ScoreServant, 1);
	servant->servant.vepv = &score_vepv;
	servant->score = score;

	POA_Granite_Score__init ((POA_Granite_Score *)servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->poa = poa = (PortableServer_POA)
		CORBA_ORB_resolve_initial_references (gnome_CORBA_ORB (), "RootPOA", &ev);

	PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_free (PortableServer_POA_activate_object (poa, servant, &ev));
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	servant->obj = PortableServer_POA_servant_to_reference (poa, servant, &ev);
	g_return_val_if_fail (ev._major == CORBA_NO_EXCEPTION, NULL);

	CORBA_exception_free (&ev);

	return servant;
}

static void
score_servant_free (ScoreServant *servant)
{
	CORBA_Environment ev;
	PortableServer_ObjectId *oid;

	CORBA_exception_init (&ev);
	oid = PortableServer_POA_servant_to_id (servant->poa, servant, &ev);
	CORBA_Object_release (servant->obj, &ev);
	PortableServer_POA_deactivate_object (servant->poa, oid, &ev);
	CORBA_free (oid);
	POA_Granite_Score__fini ((POA_Granite_Score *)servant, &ev);
	CORBA_exception_free (&ev);
	g_free (servant);
}
