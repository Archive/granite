/**************************************************************************

    sequencer-peer.h

    Copyright (C) 1999 Andrew T. Veliath

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

    $Id$

***************************************************************************/
#ifndef __SEQUENCER_PEER_H__
#define __SEQUENCER_PEER_H__

#define SEQUENCER_PEER(obj)		((SequencerPeer *)(obj))
#define SEQUENCER_MASTER(obj)		(&(((SequencerPeer *)(obj))->u.Master))
#define SEQUENCER_SLAVE(obj)		(&(((SequencerPeer *)(obj))->u.Slave))

typedef enum {
	SEQUENCER_PEER_UNKNOWN,
	SEQUENCER_PEER_MASTER,
	SEQUENCER_PEER_SLAVE
} SequencerPeerType;

typedef struct _SequencerPeer		SequencerPeer;

typedef struct {
	Sequencer *sequencer;
	SequencerState saved_state;
} SequencerMaster;

typedef struct {
	SequencerPeer *master;
} SequencerSlave;

struct _SequencerPeer {
	SequencerPeerType type;

	union {
		SequencerMaster Master;
		SequencerSlave Slave;
	} u;

	void	(*destroy)	(SequencerPeer *peer);
};

void		sequencer_peer_master_construct	(SequencerPeer *peer,
						 Sequencer *sequencer);

void		sequencer_peer_slave_construct	(SequencerPeer *peer,
						 SequencerPeer *master);

void		sequencer_peer_deconstruct	(SequencerPeer *peer);

void		sequencer_peer_set_load_func	(SequencerPeer *peer,
						 SequencerLoadEventsFunc func,
						 ScoreTime offset_time,
						 gpointer user_data);

gboolean	sequencer_peer_goto		(SequencerPeer *peer,
						 ScoreTime time);
gboolean	sequencer_peer_play		(SequencerPeer *peer);
void		sequencer_peer_stop		(SequencerPeer *peer);
void		sequencer_peer_record		(SequencerPeer *peer,
						 gboolean enable);

void		sequencer_peer_get_echo_mode	(SequencerPeer *peer,
						 SequencerEchoMode *em);
void		sequencer_peer_set_echo_mode	(SequencerPeer *peer,
						 const SequencerEchoMode *mm);

void		sequencer_peer_get_metro_mode	(SequencerPeer *peer,
						 SequencerMetroMode *em);
void		sequencer_peer_set_metro_mode	(SequencerPeer *peer,
						 const SequencerMetroMode *mm);

void		sequencer_peer_get_loop_mode	(SequencerPeer *peer,
						 SequencerLoopMode *lm);
void		sequencer_peer_set_loop_mode	(SequencerPeer *peer,
						 const SequencerLoopMode *lm);

void		sequencer_peer_get_punch_in_mode (SequencerPeer *peer,
						  SequencerPunchInMode *pim);
void		sequencer_peer_set_punch_in_mode (SequencerPeer *peer,
						  const SequencerPunchInMode *pim);

#endif /* __SEQUENCER_PEER_H__ */
